# 测试框架对接模块设计



## 1、模块功能描述

给自动化测试框架提供获取组件属性和触发组件事件的JS接口。为方便指定要测试的组件，新增组件通用属性组件标识`key`。

优先支持基础组件Text、Image和Button，以及容器组件Flex、Row、Colomn和Stack。

## 2、模块的对外接口

模块对外的接口说明

#### 通用属性

| 名称 | 参数类型 | 默认值 | 描述                                   |
| ---- | -------- | ------ | -------------------------------------- |
| key  | string   | ''     | 标识一个组件，**唯一性由使用者保证**。 |

#### 接口说明

##### getInspectorByKey(key: string): string

获取指定key的组件的属性，不包括子组件。

| 接口名称          | 参数 | 默认值 | 参数描述              |
| ----------------- | ---- | ------ | --------------------- |
| getInspectorByKey | key  | -      | 要获取属性的组件的key |

##### sendEventByKey(key: string, action: number, params: string): boolean;

直接触发指定key组件的指定事件

| 接口名称       | 参数   | 默认值 | 参数描述                                                     |
| -------------- | ------ | ------ | ------------------------------------------------------------ |
| sentEventByKey | key    | -      | 要触发事件的组件的key                                        |
| sentEventByKey | action | -      | 要触发的事件类型，目前支持取值：点击事件Click 10；长按事件LongClick 11 |
| sendEventByKey | params | -      | 事件参数，目前未使用                                         |

##### getInspectorTree(void): string

获取组件树结构及组件属性

## 3、模块结构



## 4、关键流程



## 5、文件结构

涉及到文件的修改

```bash
ace_engine/frameworks
├── bridge
   ├── declarative_frontend
   ├── js_frontend
├── core
   ├── components_v2/inspector
   ├── pipeline

```

## 6、代码示例

```typescript
import router from '@system.router';

async function routePage() {
  let options = {
    uri: 'pages/second'
  }
  try {
    await router.push(options)
  } catch (err) {
    console.error(`fail callback, code: ${err.code}, msg: ${err.msg}`)
  }
  setTimeout(() => {
    console.error(getInspectorByKey("button"));
    console.error(getInspectorByKey("secondbutton"));
    console.error(`result:${sendEventByKey("secondbutton", 10, "")}`);
  }, 2000)
}

@Entry
@Component
struct Index {
  aboutToAppear(){
    console.error("aboutToAppear");
    console.error(getInspectorByKey("button"));
    setTimeout(() => {
      sendEventByKey("button", 11, "")
    }, 2000)
  }
  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Button() {
        Text('longclik')
          .fontSize(25)
          .fontWeight(FontWeight.Bold)
      }
      .type(ButtonType.Capsule)
      .margin({
        top: 20
      }).onClick(()=>{
        console.error(getInspectorByKey("button"));
        setTimeout(() => {
          sendEventByKey("button", 11, "")
        }, 2000)
      })
      Text('Hello World')
        .fontSize(50)
        .fontWeight(FontWeight.Bold)
      Button() {
        Text('next page')
          .fontSize(25)
          .fontWeight(FontWeight.Bold)
      }
      .type(ButtonType.Capsule)
      .margin({
        top: 20
      })
      .backgroundColor('#0D9FFB')
      .key('button')
      .gesture(
      LongPressGesture().onActionEnd(() => {
        routePage()
      })).key('button')
    }
    .width('100%')
    .height('100%')
  }
}
```

