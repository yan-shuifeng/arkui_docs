# Windows搭建ArkUI测试用例执行环境

#### 安装Python环境。

测试用例执行需要依赖Python环境，版本要大于3.8.

Python下载链接：https://www.python.org/downloads/windows/，选择合适版本，下载`Windows installer (64-bit)`，然后安装。

#### 测试用例编译

由于Windows环境下无法实现用例编译，因此执行用例前需要在Linux环境下进行用例编译，用例编译命令：

```
./build.sh --product-name rk3568 --build-target make_test	//全量编译
./build.sh --product-name rk3568 --build-target RenderGridLayoutAnimationTest	//编译指定测试套
```

>说明: 
>
>- product-name：指定编译产品名称，例如rk3568。
>- build-target：指定所需编译用例，make_test表示指定全部用例，实际开发中可指定特定用例。目前只支持全量编译和指定测试套编译。

编译完成后，测试用例将自动保存在out/rk3568/tests目录下。

#### 搭建执行环境

1. 在Windows环境任意目录下创建测试框架目录Test，并在此目录下创建testcase目录。

2. 从Linux环境拷贝测试框架developertest和xdevice目录到创建的Test目录下，拷贝编译好的测试用例（整个out/rk3568/tests目录）到testcase目录下

   >**说明：** 将测试框架及测试用例从Linux环境移植到Windows环境，以便后续执行。

3. 修改Windows环境下的developertest\config\user_config.xml。修改后内容如下：

   ```
   <user_config>
     <build>
       <!-- whether compile test demo case, default false -->
       <example>false</example>
       <!-- whether compile test version, default false -->
       <version>false</version>
       <!-- 由于测试用例已编译完成，此标签属性需改为false -->
       <testcase>false</testcase>
     </build>
     <environment>
       <!-- reserved field, configure devices that support HDC connection -->
       <device type="usb-hdc">
         <ip>127.0.0.1</ip>
         <port>8710</port>
         <!-- 该sn为rk3568连接Windows后实际的sn，可通过hdc list targets命令查询 -->
         <sn>7001005458323933328a511c07f23800</sn>
       </device>
       <!-- configure devices that support serial connection -->
       <device type="com" label="ipcamera">
         <serial>
           <com></com>
           <type>cmd</type>
           <baud_rate>115200</baud_rate>
           <data_bits>8</data_bits>
           <stop_bits>1</stop_bits>
           <timeout>1</timeout>
         </serial>
       </device>
     </environment>
     <!-- 由于已将测试用例拷贝到Windows环境下，测试用例输出路径发生改变，需要修改为拷贝后所存放的路径 -->
     <test_cases>
       <dir>C:\OpenHarmony\Test\testcase\tests</dir>
     </test_cases>
     <!-- reserved field, configure output path for coverage -->
     <coverage>
       <outpath></outpath>
     </coverage>
     <!-- configure NFS mount path for test cases -->
     <NFS>
       <host_dir></host_dir>
       <mnt_cmd></mnt_cmd>
       <board_dir></board_dir>
     </NFS>
   </user_config>
   ```

   >**说明：** `<testcase>`标签表示是否需要编译用例；`<dir>`标签表示测试用例查找路径。

4. 修改Windows环境下的developertest\config\framework_config.xml，主要是新增rk3568的option。修改后内容如下：

```
<framework_config>
  <productform>
    <option name="ipcamera_hispark_aries" />
    <option name="ipcamera_hispark_taurus" />
    <option name="wifiiot_hispark_pegasus" />
    <!-- 新增 -->
	<option name="rk3568" />
  </productform>
  <test_category>
    <option name="UT"
            desc="unittest"
            timeout="300" />
    <option name="MST"
            desc="moduletest"
            timeout="300" />
    <option name="ST"
            desc="systemtest"
            timeout="300" />
    <option name="PERF"
            desc="performance"
            timeout="900" />
    <option name="SEC"
            desc="security"
            timeout="900" />
    <option name="FUZZ"
            desc="fuzztest"
            timeout="900" />
    <option name="RELI"
            desc="reliability"
            timeout="900" />
    <option name="DST"
            desc="distributedtest"
            timeout="900" />
    <option name="BENCHMARK"
            desc="benchmark"
            timeout="300" />
    <option name="ALL"
            desc="ALL" />
  </test_category>
  <all_category>
    <option name="unittest" />
    <option name="moduletest" />
    <option name="systemtest" />
    <option name="performance" />
    <option name="security" />
    <option name="reliability" />
    <option name="benchmark" />
  </all_category>
</framework_config>
```

#### 执行用例

1. 设置Windows hdc_std工具环境变量，将hdc_std路径添加到系统变量Path。

   需要注意的是，将hdc_std工具单独存放在一个目录内，不要与adb放在同一个目录下，同时切记不能将hdc_std重命名为hdc使用。

2. 使用hdc_std命令设置rk3568与本地Windows连接的端口映射。在Windows上执行以下命令：

   ```
   hdc_std kill
   hdc_std -m -s 127.0.0.1:8710
   ```

3. 启动测试框架。进入developertest目录，双击执行`start.bat`脚本。

4. 选择产品形态

   进入测试框架，系统会自动提示您选择产品形态，请根据实际的开发板进行选择。例如：rk3568。

5. 执行测试用例

   当选择完产品形态，可参考如下指令执行测试用例。

   ```
   run -t UT -tp ace_engine_standard -tm graphicalbasicability	//执行指定模块的测试用例
   run -t UT -ts RenderGridLayoutAnimationTest	//执行指定测试套用例
   ```

   执行命令参数说明：

   ```
   -t [TESTTYPE]: 指定测试用例类型，有UT，MST，ST，PERF，FUZZ，BENCHMARK等。（必选参数）
   -tp [TESTPART]: 指定部件，可独立使用。
   -tm [TESTMODULE]: 指定模块，不可独立使用，需结合-tp指定上级部件使用。
   -ts [TESTSUITE]: 指定测试套，可独立使用。
   -tc [TESTCASE]: 指定测试用例，不可独立使用，需结合-ts指定上级测试套使用。
   -h : 帮助命令。
   ```

## 测试报告日志

当执行完测试指令，控制台会自动生成测试结果，若需要详细测试报告您可在相应的数据文档中进行查找。

### 测试结果

测试结果输出路径如下：

```
developertest\reports\xxxx_xx_xx_xx_xx_xx
```

>**说明：** 测试报告文件目录将自动生成。

该目录中包含以下几类结果：

| 类型                | 描述               |
| ------------------- | ------------------ |
| result              | 测试用例格式化结果 |
| summary_report.html | 测试报告汇总       |
| details_report.html | 测试报告详情       |

### 最新测试报告

```
reports/latest
```

