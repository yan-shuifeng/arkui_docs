# onAreaChange模块设计

## 1、模块功能描述

提供onAreaChange组件事件，开发者可以通过该事件获取组件的大小和位置信息。

## 2、模块的对外接口

### 通用事件

| 名称                                                         | 支持冒泡 | 功能描述                                                     |
| ------------------------------------------------------------ | -------- | ------------------------------------------------------------ |
| onAreaChange(event: (oldValue: Area, newValue: Area) => void) | 否       | 当前组件大小或者位置发生变化时触发该回调，参数包括未变化前的大小和位置以及变化后的大小和位置。 |

#### Area对象说明

| 属性名称  | 属性类型 | 描述                                   |
| --------- | -------- | -------------------------------------- |
| width     | Length   | 目标元素的宽度。                       |
| height    | Length   | 目标元素的高度。                       |
| pos       | Position | 目标元素左上角相对父元素左上角的位置。 |
| globalPos | Position | 目标元素左上角相对页面左上角的位置。   |

##### Position对象说明

| 属性名称 | 属性类型 | 描述      |
| -------- | -------- | --------- |
| x        | Length   | x轴坐标。 |
| y        | Length   | y轴坐标。 |

## 3、模块结构说明

```mermaid
classDiagram
Extension <|-- OnAreaChangeExtension
RenderNode *-- EventExtensions
EventExtensions *-- OnAreaChangeExtension
Component *-- EventExtensions
RenderNode <.. Component
RenderNode -- PipelineContext
class OnAreaChangeExtension
class Component{
	GetEventExtensions() RefPtr~EventExtensions~
	RefPtr~EventExtensions~ events_
}
class RenderNode{
	Update(RefPtr~Component~) void
	OnPaintFinish() void
	RefPtr~EventExtensions~ events_
}
class EventExtensions{
	GetOnAreaChangeExtension() ~OnAreaChangeExtension~
}
class OnAreaChangeExtension{
	UpdateRect(RefPtr~Rect~) void
}
class PipelineContext{
	FlushRenderFinish() void
}
```

### 4、关键流程说明

```mermaid
sequenceDiagram
	Element->>RenderNode: Update
	RenderNode->>Component: GetEventExtensions
	RenderNode->>EventExtensions: GetOnAreaChangeExtension
	RenderNode->>RenderNode: UpdateFlagIfHasEvent
	PipelineContext->>PipelineContext: FlushLayout&Render
	PipelineContext->>RenderContext: Repaint
	RenderContext->>RenderNode: RenderWithContext
	RenderNode->>PipelineContext: AddNeedRenderFinishNode
	PipelineContext->>PipelineContext: FlushRenderFinish
	PipelineContext->>RenderNode: OnPaintFinish
	RenderNode->>EventExtensions: GetOnAreaChangeExtension
	RenderNode->>OnAreaChangeExtension: UpdateRect
	OnAreaChangeExtension->>OnAreaChangeExtension: JudgeIfNeedFireEvent
```

- 通过前端JsViewAbstract将onAreaChange事件回调赋值给Component中的OnAreaChangeExtension对象；
- 在组件的build过程中，OnAreaChangeExtension对象会赋值给Render对象；
- Render对象在绘制流程中判断是否存在OnAreaChangeExtension，如果存在，将自身保持在Pipeline渲染管线中；
- 在Pepiline渲染管线的paintFinish阶段时，触发RenderNode的事件回调；
- 在RenderNode的事件回调中调用OnAreaChangeExtension的UpdateRect方法进行位置和大小变更判断，如果存在变更，则触发OnAreaChange事件。

## 6、伪代码示例

```typescript
@Entry
@Component
struct Index {
  @State value: string = ''

  build() {
    Column() {
      Text(this.value)
      .onAreaChange((oldValue, newValue)=>{
        console.info(`Text on Area Change: old is ${JSON.stringify(oldValue)}, new is ${JSON.stringify(newValue)}`)
      })
      TextInput()
        .onChange((newValue) => {
          this.value = newValue;
        })
    }
    .width('100%')
    .height('100%')
  }
}
```

