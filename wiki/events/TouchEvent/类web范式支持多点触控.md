# 触摸热区模块设计

## 1、模块功能描述

针对类web范式的通用touch事件，补充touch事件中的touches的多点信息。

## 2、模块的对外接口

### 通用事件
<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="14.96149614961496%" id="mcps1.1.5.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="20.242024202420243%" id="mcps1.1.5.1.2"><p>参数</p>
</th>
<th class="cellrowborder" valign="top" width="45.34453445344535%" id="mcps1.1.5.1.3"><p>描述</p>
</th>
<th class="cellrowborder" valign="top" width="19.451945194519453%" id="mcps1.1.5.1.4"><p>是否支持冒泡</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="14.96149614961496%" headers="mcps1.1.5.1.1 "><p>touchstart</p>
</td>
<td class="cellrowborder" valign="top" width="20.242024202420243%" headers="mcps1.1.5.1.2 "><p><a href="#tdb541af4e4db41d7a92e9b6e3c93f606">TouchEvent</a></p>
</td>
<td class="cellrowborder" valign="top" width="45.34453445344535%" headers="mcps1.1.5.1.3 "><p>手指刚触摸屏幕时触发该事件。</p>
</td>
<td class="cellrowborder" valign="top" width="19.451945194519453%" headers="mcps1.1.5.1.4 "><p>是<sup>5+</sup></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.96149614961496%" headers="mcps1.1.5.1.1 "><p>touchmove</p>
</td>
<td class="cellrowborder" valign="top" width="20.242024202420243%" headers="mcps1.1.5.1.2 "><p><a href="#tdb541af4e4db41d7a92e9b6e3c93f606">TouchEvent</a></p>
</td>
<td class="cellrowborder" valign="top" width="45.34453445344535%" headers="mcps1.1.5.1.3 "><p>手指触摸屏幕后移动时触发该事件。</p>
</td>
<td class="cellrowborder" valign="top" width="19.451945194519453%" headers="mcps1.1.5.1.4 "><p>是<sup>5+</sup></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.96149614961496%" headers="mcps1.1.5.1.1 "><p>touchcancel</p>
</td>
<td class="cellrowborder" valign="top" width="20.242024202420243%" headers="mcps1.1.5.1.2 "><p><a href="#tdb541af4e4db41d7a92e9b6e3c93f606">TouchEvent</a></p>
</td>
<td class="cellrowborder" valign="top" width="45.34453445344535%" headers="mcps1.1.5.1.3 "><p>手指触摸屏幕中动作被打断时触发该事件。</p>
</td>
<td class="cellrowborder" valign="top" width="19.451945194519453%" headers="mcps1.1.5.1.4 "><p>是<sup>5+</sup></p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="14.96149614961496%" headers="mcps1.1.5.1.1 "><p>touchend</p>
</td>
<td class="cellrowborder" valign="top" width="20.242024202420243%" headers="mcps1.1.5.1.2 "><p><a href="#tdb541af4e4db41d7a92e9b6e3c93f606">TouchEvent</a></p>
</td>
<td class="cellrowborder" valign="top" width="45.34453445344535%" headers="mcps1.1.5.1.3 "><p>手指触摸结束离开屏幕时触发该事件。</p>
</td>
<td class="cellrowborder" valign="top" width="19.451945194519453%" headers="mcps1.1.5.1.4 "><p>是<sup>5+</sup></p>
</td>
</tr>
</tbody>
</table>

#### TouchEvent对象属性列表

<a name="tdb541af4e4db41d7a92e9b6e3c93f606"></a>
<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="19.439999999999998%" id="mcps1.2.4.1.1"><p>属性</p>
</th>
<th class="cellrowborder" valign="top" width="20.14%" id="mcps1.2.4.1.2"><p>类型</p>
</th>
<th class="cellrowborder" valign="top" width="60.419999999999995%" id="mcps1.2.4.1.3"><p>说明</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="19.439999999999998%" headers="mcps1.2.4.1.1 "><p>touches</p>
</td>
<td class="cellrowborder" valign="top" width="20.14%" headers="mcps1.2.4.1.2 "><p>Array&lt;TouchInfo&gt;</p>
</td>
<td class="cellrowborder" valign="top" width="60.419999999999995%" headers="mcps1.2.4.1.3 "><p>触摸事件时的属性集合，包含屏幕触摸点的信息数组。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="19.439999999999998%" headers="mcps1.2.4.1.1 "><p>changedTouches</p>
</td>
<td class="cellrowborder" valign="top" width="20.14%" headers="mcps1.2.4.1.2 "><p>Array&lt;TouchInfo&gt;</p>
</td>
<td class="cellrowborder" valign="top" width="60.419999999999995%" headers="mcps1.2.4.1.3 "><p>触摸事件时的属性集合，包括产生变化的屏幕触摸点的信息数组。数据格式和touches一样。该属性表示有变化的触摸点，如从无变有，位置变化，从有变无。</p>
</td>
</tr>
</tbody>
</table>

#### TouchInfo对象说明

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="21.060000000000002%" id="mcps1.2.4.1.1"><p>属性</p>
</th>
<th class="cellrowborder" valign="top" width="15.939999999999998%" id="mcps1.2.4.1.2"><p>类型</p>
</th>
<th class="cellrowborder" valign="top" width="63%" id="mcps1.2.4.1.3"><p>说明</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>globalX</p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>距离屏幕左上角（不包括状态栏）横向距离。屏幕的左上角为原点。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>globalY</p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>距离屏幕左上角（不包括状态栏）纵向距离。屏幕的左上角为原点。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>localX</p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>距离被触摸组件左上角横向距离。组件的左上角为原点。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>localY</p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>距离被触摸组件左上角纵向距离。组件的左上角为原点。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>size</p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>触摸接触面积。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>force<sup>6+</sup></p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>接触力信息。</p>
</td>
</tr>
<tr><td class="cellrowborder" valign="top" width="21.060000000000002%" headers="mcps1.2.4.1.1 "><p>identifier<sup>8+</sup></p>
</td>
<td class="cellrowborder" valign="top" width="15.939999999999998%" headers="mcps1.2.4.1.2 "><p>number</p>
</td>
<td class="cellrowborder" valign="top" width="63%" headers="mcps1.2.4.1.3 "><p>接触点标识信息。</p>
</td>
</tr>
</tbody>
</table>

## 3、模块结构

```mermaid
classDiagram
   FlutterAceView ..> PipelineContext
   FlutterAceView ..> TouchEvent
   PipelineContext ..> RawRecognizer
   PipelineContext ..> TouchEvent
   RawRecognizer ..> JsEventHandler
   RawRecognizer ..> TouchEvent
   TouchEvent ..> TouchPoint
      class FlutterAceView{
          +DispatchTouchEvent()
          +ProcessTouchEvent()
      }
      class PipelineContext{
          +OnTouchEvent()
      }
      class RawRecognizer{
          +HandleEvent() void
          +CreateTouchEventInfo() TouchEventInfo
      }
      class JsEventHandler{
          +HandleAsyncEvent(EventMarker, BaseEventInfo) void
      }
      class TouchEvent{
          +int32_t id
          +float x
          +float y
          +std::vector<TouchPoint> pointers
      }
      class TouchPoint{
          +int32_t id
          +float x
          +float y
      }

```

## 4、关键流程

1. 在FlutterAceView收到Touch事件时通过ProcessTouchEvent函数对平台事件进行格式转换和处理，将多点信息保存在TouchEvent的points对象；
2. 组件通过裸事件识别器RawRecognizer识别手势事件时，将多点信息points从TouchEvent对象中拿出来封装为前端JS数据；
3. 前端JS侧通过JsEventHandler的HandleAsyncEvent方法将多指信息返回给前端应用。

## 5、文件结构

### 涉及到文件的修改

```bash
foundation/ace/ace_engine/adapter/ohos/entrance
├── flutter_ace_view.h
├── flutter_ace_view.cpp

foundation/ace/ace_engine/frameworks/bridge/js_frontend
├── js_frontend.cpp

foundation/ace/ace_engine/frameworks/core/gestures
├── raw_recognizer.cpp

foundation/ace/ace_engine/frameworks/core/event
├── touch_event.h

foundation/ace/ace_engine/frameworks/core/pipeline
├── pipeline_context.cpp

```

## 6、伪代码示例

```html
<div class="container" onswipe="touchMove">
    <div class="test" @touchstart="onTouchStart">
        <text>
            {{ $t('strings.hello') }} {{ title }}
        </text>
    </div>
    <div @touchmove="onTouchMove">
        <text class="title1">
            {{ $t('strings.hello') }} {{ title1 }}
        </text>
    </div>
</div>
```

```javascript
export default {
    data: {
        title: "",
        title1: ""
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    onTouchStart: function (info) {
        this.title = JSON.stringify(info.touches[0]) + JSON.stringify(info.touches[1]);
    },
    onTouchMove: function (info) {
        this.title1 = JSON.stringify(info.touches[0]) + JSON.stringify(info.touches[1]);
    }
}
```

```css
.container {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
}

.btn {
    width: 50%;
    height: 100px;
    font-size: 40px;
}

.test {
    height: 50%;
}
```
