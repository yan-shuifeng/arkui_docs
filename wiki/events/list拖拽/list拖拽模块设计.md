# List组件增加内容拖拽能力模块设计



## 1、模块功能描述

List组件新增事件onItemDragStart()、onItemDragEnter()、onItemDragMove()、onItemDragLeave()、onItemDrop()，用于实现内容拖拽能力。



## 2、模块的对外接口

#### 属性方法

| 名称     | 参数类型 | 默认值 | 描述                               |
| -------- | -------- | ------ | ---------------------------------- |
| editMode | boolean  | FALSE  | 进入编辑模式，支持List组件内部拖拽 |

#### 事件方法

| 名称            | 参数类型                                                     | 描述                                                         |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| onItemDragStart | (event: ItemDragInfo, itemIndex: number) => any              | 绑定后，第一次拖拽item 时，触发回调。<br/>itemindex : 当前被拖拽的 item 原本的索引。<br/>返回值：当跟手效果所拖拽的 item 的对象。 |
| onItemDragEnter | (event: ItemDragInfo) => void                                | 绑定后，拖拽进入组件范围内时，触发回调。（当监听了onDrop 事件时，此事件才有效） |
| onItemDragMove  | (event: ItemDragInfo, itemIndex: number, insertIndex: number) => void | 绑定后，拖拽在可放置组件范围内移动时，触发回调。（当监听了onDrop 事件时，此事件才有效）<br/>insertIndex: 当前拖入宫格进行插入后 item 的索引。<br/>itemindex: 当前被拖拽的 item 原本的索引。 |
| onItemDragLeave | (event: ItemDragInfo, itemIndex: number) => void             | 绑定后，拖拽离开组件范围内时，触发回调。（当监听了onDrop 事件时，此事件才有效）itemindex: 当前被拖拽的 item 原本的索引。 |
| onItemDrop      | (event: ItemDragInfo, itemIndex: number, insertIndex: number, isSuccess: boolean) => void | 绑定此事件的组件可作为拖拽释放目标，当在本组件范围内停止拖拽行为时，触发此回调。<br/>insertIndex : 当前拖入宫格进行插入后 item 的索引。<br/>itemindex : 当前被拖拽的 item 原本的索引。<br/>isSuccess : drop 时插入是否成功。 |





## 3、模块结构

```mermaid
classDiagram
    class JSList {
        +SetEditMode()
        +ItemDragStartCallback()
        +ItemDragEnterCallback()
    	+ItemDragMoveCallback()
    	+ItemDragLeaveCallback()
    	+ItemDropCallback()
    }

    class PositionedComponent{
        -UpdatePositionFunc updatePositionFunc_
        +GetUpdatePositionFuncId()
        +SetUpdatePositionFuncId()
    }

    class RenderPositioned{
        -UpdatePositionFunc updatePositionFunc_
    }

    class ListComponent{
    	-OnItemDragStartFunc onItemDragStartId_
        -OnItemDragEnterFunc onItemDragEnterId_
        -OnItemDragMoveFunc onItemDragMoveId_
        -OnItemDragLeaveFunc onItemDragLeaveId_
        -OnItemDropFunc onItemDropId_
        +SetOnItemDragStartId()
        +GetOnItemDragStartId()
    	+SetOnItemDragEnterId()
    	+GetOnItemDragEnterId()
    	+SetOnItemDragMoveId()
    	+GetOnItemDragMoveId()
    	+SetOnItemDragLeaveId()
    	+GetOnItemDragLeaveId()
    	+SetOnItemDropId()
    	+GetOnItemDropId()
    }

	class RenderList{
	    #size_t insertItemIndex_ = INITIAL_CHILD_INDEX
        #Offset betweenItemAndBuilder_
        #RefPtr<RenderListItem> selectedDragItem_
        -RefPtr<GestureRecognizer> dragDropGesture_
        -RefPtr<RenderList> preTargetRenderList_
		-UpdateBuilderFunc updateBuilder_
		-OnItemDragStartFunc onItemDragStart_
        -OnItemDragEnterFunc onItemDragEnter_
        -OnItemDragMoveFunc onItemDragMove_
        -OnItemDragLeaveFunc onItemDragLeave_
        -OnItemDropFunc onItemDrop_
        -void CreateDragDropRecognizer()
        -FindCurrentListItem()
        -FindTargetRenderList()
	}

    class ItemDragInfo{
    	-double x_
    	-double y_
    }

    JSList ..> ItemDragInfo
    JSList ..> ListComponent
    RenderList ..> ListComponent
    RenderPositioned ..> PositionedComponent
    RenderList --> RenderPositioned

```

## 4、关键流程

```mermaid
 sequenceDiagram
	participant AceAbility
	participant AceContainer
	Participant PipelineContext
	Participant QJSDeclarativeEngineInstance
	Participant qjs_view_register
	Participant JSView
	Participant JSList
	Participant Element
	Participant JsDragFunction
	Participant ListComponent

	AceAbility ->> AceAbility:OnStart()
	AceAbility ->> AceContainer:CreateContainer(FrontendType::DECLARATIVE_JS)
	note left of QJSDeclarativeEngineInstance: JS线程
	AceContainer -->> QJSDeclarativeEngineInstance:InitJSEnv()
	QJSDeclarativeEngineInstance ->> QJSDeclarativeEngineInstance:InitJSContext()
	QJSDeclarativeEngineInstance ->> qjs_view_register:JsRegisterViews()
	qjs_view_register ->> JSList:JSBind()
	JSList ->> JSList:StaticMethod("editMode", &JSList::SetEditMode)
    JSList ->> JSList:StaticMethod("onItemDragStart", &JSList::ItemDragStartCallback)
    JSList ->> JSList:StaticMethod("onItemDragEnter", &JSList::ItemDragEnterCallback)
    JSList ->> JSList:StaticMethod("onItemDragMove", &JSList::ItemDragMoveCallback)
    JSList ->> JSList:StaticMethod("onItemDragLeave", &JSList::ItemDragLeaveCallback)
    JSList ->> JSList:StaticMethod("onItemDrop", &JSList::ItemDropCallback)
	AceAbility ->> AceContainer:SetView()
	note left of PipelineContext: UI线程
	AceContainer -->> PipelineContext:SetupRootElement()
	PipelineContext ->> Element:Mount()
	Element ->> qjs_view_register:JsLoadDocument()
	qjs_view_register ->> JSView:CreateComponent()
	JSView ->> JSList:Create()
	JSList ->> ListComponent:MakeRefPtr()
	JSView ->> JSList:SetEditMode()
	JSList ->> ListComponent:SetEditMode(bool)
	JSView ->> JSList:ItemDragStartCallback()
	JSList ->> JsDragFunction:MakeRefPtr()
	JSList ->> ListComponent:SetOnItemDragStartId(onItemDragStartId)
	JSView ->> JSList:ItemDragEnterCallback()
	JSList ->> JsDragFunction:MakeRefPtr()
	JSList ->> ListComponent:SetOnItemDragEnterId(onItemDragEnterId)
	JSView ->> JSList:ItemDragMoveCallback()
	JSList ->> JsDragFunction:MakeRefPtr()
	JSList ->> ListComponent:SetOnItemDragMoveId(onItemDragMoveId)
	JSView ->> JSList:ItemDragLeaveCallback()
	JSList ->> JsDragFunction:MakeRefPtr()
	JSList ->> ListComponent:SetOnItemDragLeaveId(onItemDragLeaveId)
	JSView ->> JSList:ItemDropCallback()
	JSList ->> JsDragFunction:MakeRefPtr()
	JSList ->> ListComponent:SetOnItemDropId(onItemDropId)
	Element ->> ListComponent:CreateElement()
	Element ->> ListComponent:CreateRenderNode()
```

## 5、文件结构

*涉及到文件的修改*

```bash
ace_engine/frameworks/bridge/declarative_frontend
├── engine/functions
   ├── js_drag_function.h
   ├── js_drag_function.cpp

├── jsview
   ├── js_list.h
   ├── js_list.cpp

ace_engine/frameworks/core/components
├── positioned
   ├── positioned_component.cpp
   ├── render_positioned.h
   ├── render_positioned.cpp

ace_engine/frameworks/core/components_v2
├── list
   ├── list_component.h
   ├── render_list.h
   ├── render_list.cpp

ace_engine/frameworks/core
├── gestures
   ├── gesture_info.h

ace_engine/frameworks/core/pipeline
├── base
   ├── render_node.cpp

```

## 6、代码示例

```ets
@Entry
@Component
struct ListItemExample {
  @State Number1: string[] = ['0', '1', '2']
  @State Number2: string[] = ['3', '4', '5']
  @State text: string = ""
  @State bool1: boolean = false
  @State bool2: boolean = false

  @Builder pixelMapBuilder() {
    Text('-1')
      .width('100%').height(100).fontSize(16)
      .textAlign(TextAlign.Center).borderRadius(10).backgroundColor(0xFFFFFF)
  }

  build() {
    Column() {
      List() {
        ForEach(this.Number1, (item) => {
          ListItem() {
            Text('' + item)
              .width('100%').height(100).fontSize(16)
              .textAlign(TextAlign.Center).borderRadius(10).backgroundColor(0xF666FF)
          }
        }, item => item)
      }
      .editMode(true)
      .border({width: 1, style: BorderStyle.Solid, color: Color.Red})
      .onItemDragStart((event: ItemDragInfo, itemIndex: number) => {
        this.bool1 = true
        this.text = this.Number1[itemIndex]


        console.log("WZZ onItemDragStart, itemIndex:" + itemIndex);
        return this.pixelMapBuilder;
      })
      .onItemDragEnter((event: ItemDragInfo) => {
        console.log("WZZ onItemDragEnter");
      })
      .onItemDragMove((event: ItemDragInfo, itemIndex: number, insertIndex: number) => {
        console.log("WZZ onItemDragMove, itemIndex:" + itemIndex + ", insertIndex:" + insertIndex);
      })
      .onItemDragLeave((event: ItemDragInfo, itemIndex: number) => {
        console.log("WZZ onItemDragLeave, itemIndex:" + itemIndex);
      })
      .onItemDrop((event: ItemDragInfo, itemIndex: number, insertIndex: number, isSuccess: boolean) => {
        console.log("WZZ onItemDrop, itemIndex:" + itemIndex + ", insertIndex:" + insertIndex + ", isSuccess:" + isSuccess);
        if (isSuccess) {
          if(this.bool2){
            this.Number2.splice(itemIndex,1)
            this.Number1.splice(insertIndex,0,this.text)
            this.bool1 = false
            this.bool2 = false
          }else if(this.bool1){
            this.Number1.splice(itemIndex,1)
            this.Number1.splice(insertIndex,0,this.text)
            this.bool1 = false
            this.bool2 = false
          }
        }
      })

      List() {
        ForEach(this.Number2, (item) => {
          ListItem() {
            Text('' + item)
              .width('100%').height(100).fontSize(16)
              .textAlign(TextAlign.Center).borderRadius(10).backgroundColor(0xFFF888)
          }
        }, item => item)
      }
      .editMode(true)
      .border({width: 1, style: BorderStyle.Solid, color: Color.Blue})
      .onItemDragStart((event: ItemDragInfo, itemIndex: number) => {
        this.bool2 = true
        this.text = this.Number2[itemIndex]

        console.log("WZZ111 onItemDragStart, itemIndex:" + itemIndex);
        return this.pixelMapBuilder;
      })
      .onItemDragEnter((event: ItemDragInfo) => {
        console.log("WZZ111 onItemDragEnter");
      })
      .onItemDragMove((event: ItemDragInfo, itemIndex: number, insertIndex: number) => {
        console.log("WZZ111 onItemDragMove, itemIndex:" + itemIndex + ", insertIndex:" + insertIndex);
      })
      .onItemDragLeave((event: ItemDragInfo, itemIndex: number) => {
        console.log("WZZ111 onItemDragLeave, itemIndex:" + itemIndex);
      })
      .onItemDrop((event: ItemDragInfo, itemIndex: number, insertIndex: number, isSuccess: boolean) => {
        console.log("WZZ111 onItemDrop, itemIndex:" + itemIndex + ", insertIndex:" + insertIndex + ", isSuccess:" + isSuccess);
        if (isSuccess) {
          if(this.bool1){
            this.Number1.splice(itemIndex,1)
            this.Number2.splice(insertIndex,0,this.text)
            this.bool1 = false
            this.bool2 = false
          }else if(this.bool2){
            this.Number2.splice(itemIndex,1)
            this.Number2.splice(insertIndex,0,this.text)
            this.bool1 = false
            this.bool2 = false
          }
        }
      })
    }
  }
}
```

