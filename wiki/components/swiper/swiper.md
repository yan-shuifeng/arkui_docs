# Swiper模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中Swiper组件的基本功能， Swiper组件是可以滑动视图的容器组件，子组件可以在容器内滑动。支持：

1. 设置当前显示的子组件索引

2. 渲染完成后，是否自动进行播放

3. 设置自动播放时的时间间隔，单位毫秒；设置是否启用 indicator

4. 设置是否开启循环模式（子组件数量大于 2 时才生效）

5. 设置滑动动画时长、滑动方向是否为纵向,纵向时indicator 也为纵向

6. 通过onChange事件回调监听页面索引变化

### 1.1 模块架构图

![swiper](figures/swiper.png)

### 1.2 模块效果图
![figures](figures/image-1.png)

![image-2](figures/image-2.png)

![image-3](figures/image-3.png)

## 2、模块的对外接口

#### 子组件

可以包含子组件

#### 接口说明

**表**：

| 接口名称     | 参数类型         | 默认值 | 描述                                                         |
| ------------ | ---------------- | ------ | ------------------------------------------------------------ |
| index        | number           | 0      | 设置当前在容器中显示的子组件的索引值。                       |
| autoPlay     | boolean          | false  | 子组件是否自动播放，自动播放状态下，导航点不可操作。         |
| interval     | number           | 3000   | 使用自动播放时播放的时间间隔，单位为毫秒。                   |
| indicator    | boolean          | true   | 是否启用导航点指示器。                                       |
| loop         | boolean          | true   | 是否开启循环。                                               |
| duration     | number           | 400    | 子组件切换的动画时长，单位为毫秒。                           |
| vertical     | boolean          | false  | 是否为纵向滑动。                                             |
| itemSpace    | Length           | 0      | 设置子组件与子组件之间间隙。                                 |
| displayCount | number \| 'auto' | 1      | 每屏显示内容项数目，auto模式下由每项主轴方向 尺寸由开发者指定，交叉轴方向尺寸拉伸至swiper等尺寸。 |
| effectMode   | EdgeEffect       | None   | 设置边缘显示效果(只有设置loop为false时，才能出现效果)。      |
| disableSwipe | boolean          | false  | 禁用组件滑动切换功能。                                       |

表**SwiperEffectMode**枚举说明：

| 名称   | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| Spring | 弹性物理动效，滑动到边缘后可以根据初始速度或通过触摸事件继续滑动一段距离，松手后回弹。 |
| Fade   | 渐隐物理动效，滑动到边缘后展示一个波浪形的渐隐，根据速度和滑动距离的变化渐隐也会发送一定的变化。 |
| None   | 滑动到边缘后无效果。                                         |

表**SwiperController**枚举说明：

Swiper容器组件的控制器，可以将此对象绑定至Swiper组件，然后通过它控制翻页。

| 接口名称             | 功能描述     |
| -------------------- | ------------ |
| showNext():void;     | 翻至下一页。 |
| showPrevious():void; | 翻至上一页。 |

#### 事件方法

| 名称     | 参数类型                | 描述                                 |
| -------- | ----------------------- | ------------------------------------ |
| onChange | (index: number) => void | 当前显示的组件索引变化时触发该事件。 |

## 3、模块结构

```mermaid
classDiagram
    SwiperComponent ..> SwiperController
    RenderSwiper ..> SwiperComponent
        class SwiperComponent{
            +CreateRenderNode() RenderNode
            +CreateElement() Element
            +AppendChild() void
            +GetIndex() uint32_t
            +SetIndex() void
            +SetDuration() void
            +GetDuration() double
            +GetAxis() Axis
            +SetAxis() void
            +IsLoop() bool
            +SetLoop() void
            +IsAutoPlay() bool
            +SetAutoPlay() void
            +IsShow() bool
            +SetShow() void
            +IsAnimationOpacity() bool
            +SetAnimationOpacity() void
            +GetItemSpace() Dimension
            +SetItemSpace() void
            +GetDisplayCount() int32_t
            +SetDisplayCount() void
            +SetDigitalIndicator() void
            +GetDigitalIndicator() bool
            +GetFadeColor() Color
            +SetFadeColor() void
            +GetCachedSize() int32_t
            +SetCachedSize() void
            +GetPreviousMargin() Dimension
            +SetPreviousMargin() void
            +GetNextMargin() Dimension
            +SetNextMargin() void
            +GetEdgeEffect() EdgeEffect
            +SetEdgeEffect() void
            +SetAnimationFinishEventId() void
            +GetAnimationFinishEventId() EventMarker
            +GetAutoPlayInterval() double
            +SetAutoPlayInterval() void
            +GetDisplayMode() SwiperDisplayMode
            +SetDisplayMode() void
            +GetAnimationCurve() AnimationCurve
            +SetAnimationCurve() void
            +GetChangeEventId() EventMarker
            +SetChangeEventId() void
            +GetRotationEventId() EventMarker
            +SetRotationEventId() void
            +GetClickEventId() EventMarker
            +SetClickEventId() void
            +GetSwiperController() SwiperController
            +GetRotationController() RotationController
            +SetShowIndicator() void
            +IsShowIndicator() bool
            +GetIndicator() SwiperIndicator
            +SetIndicator() void
            +GetChangeEndListener() SwiperChangeEndListener
            +SetChangeEndListener() void
            +SetMoveCallback() void
            +GetMoveCallback() MoveCallback
            +SetSlideContinue() void
            +GetSlideContinue() bool
            +DisableSwipe() void
            +GetDisableSwipe() bool
            +SetDeclaration() void
            +GetDisableRotation() bool
            +SetDisableRotation() void
            +SetMainSwiperSize() void
            +GetMainSwiperSize() MainSwiperSize
            +GetLazyForEachComponent() LazyForEachComponent
            -RefPtr<SwiperDeclaration> declaration_
            -bool show_
            -bool slideContinued_
            -bool disableRation_
            -bool disableSwipe_
            -SwiperChangeEndListener changeEndListener_
            -MoveCallback moveCallback_
            -MainSwiperSize mainSwiperSize_
            -WeakPtr<V2::LazyForEachComponent> lazyForEachComponent_
        }
        class RosenRenderSwiper {
            +Update() void
            +Paint() void
            -UpdateIndicator() void
            -PaintIndicator() void
            -PaintMask() void
            -LayoutDigitalIndicator() void
            -CanvasDrawIndicator() void
            -PaintFade() void
            -PaintShadow() void
            -PrepareIndicatorProperties() IndicatorProperties
            -DrawIndicator() void
            -DrawIndicatorBackground() void
            -DrawIndicatorItems() void
            -GetRRect() void
            -HideIndicatorPoint() bool
            -GetIndicatorPointMoveOffset() void
            -InitMoveRange() void
            -RefPtr<RenderText> renderDigitalIndicator_
            -int32_t moveStartIndex_
            -int32_t moveEndIndex_
        }
        class SwiperController {
            +SwipeTo() void
            +SetSwipeToImpl() void
            +ShowPrevious() void
            +SetShowPrevImpl() void
            +ShowNext() void
            +SetShowNextImpl() void
            -SwipeToImpl swipeToImpl_
            -ShowPrevImpl showPrevImpl_
            -ShowNextImpl showNextImpl_
        }
        RenderNode <|-- RenderSwiper
        RenderSwiper <|-- FlutterRenderSwiper
        RenderSwiper <|-- RosenRenderSwiper
        ComponentGroupElement <|-- SwiperElement
        ComponentGroup <|-- SwiperComponent
        class RenderSwiper {
            +Update() void
            +UpdateTouchRect() void
            +PerformLayout() void
            +IsChildrenTouchEnable() bool
            +GetCurrentIndex() int32_t
            +OnFocus() void
            +OnBlur() void
            +RegisterChangeEndListener() void
            +UnRegisterChangeEndListener() void
            +OnRotation() bool
            +OnStatusChanged() void
            +IndicatorShowFocus() void
            +UpdateIndicatorFocus() void
            +GetMoveStatus() bool
            +ExecuteMoveCallback() void
            +DisableSwipe() void
            +GetMainSize() double
            +ShowPrevious() void
            +ShowNext() void
            +InitIndicatorAnimation() void
            +CalMaxStretch() void
            +MoveIndicator() void
            +DragIndicator() void
            +DragIndicatorEnd() void
            +DragEdgeStretch() void
            +VibrateIndicator() void
            +StartIndicatorAnimation() void
            +StopIndicatorAnimation() void
            +StartIndicatorSpringAnimation() void
            +StopIndicatorSpringAnimation() void
            +StartZoomInAnimation() void
            +StartZoomOutAnimation() void
            +StartZoomInDotAnimation() void
            +StartZoomOutDotAnimation() void
            +StopZoomAnimation() void
            +StopZoomDotAnimation() void
            +StartDragRetractionAnimation() void
            +StopDragRetractionAnimation() void
            +FinishAllSwipeAnimation() void
            +IsZoomAnimationStopped() bool
            +IsZoomOutAnimationStopped() bool
            +IsZoomOutDotAnimationStopped() bool
            +UpdateIndicatorLayout() void
            +UpdateIndicatorOffset() void
            +UpdateIndicatorHeadPosistion()void
            +UpdateIndicatorTailPosistion()void
            +UpdateIndicatorPointPosistion()void
            +UpdateMaskOpacity() void
            +UpdateZoomValue() void
            +UpdateZoomDotValue() void
            +UpdateEdgeStretchRate() void
            +UpdatePressStatus() void
            +UpdateHoverStatus() void
            +UpdatePositionOnStretch() void
            +UpdateIndicatorSpringStatus() void
            +GetIndicatorSpringStatus() SpringStatus
            +ResetIndicatorSpringStatus() void
            +ResetIndicatorPosition() void
            +ResetHoverZoomDot() void
            +MarkIndicatorPosition() void
            +SetBuildChildByIndex() void
            +SetDeleteChildByIndex() void
            +AddChildByIndex() void
            +RemoveChildByIndex() void
            +OnDataSourceUpdated() void
            +GetAutoPlay() bool
            +GetAutoPlayInterval() uint64_t
            +IsShowIndicator() bool
            +GetLoop() bool
            +GetDuration() double
            +IsVertical() bool
            +RefuseUpdatePosition() bool
            +OnPaintFinish() void
        }
        class FlutterRenderSwiper {
            +GetRenderLayer() RenderLayer
            +Paint() void
            +IsRepaintBoundary() bool
            -UpdateIndicator() void
            -PaintIndicator() void
            -PaintMask() void
            -LayoutDigitalIndicator() void
            -CanvasDrawIndicator() void
            -PaintFade() void
            -PaintShadow() void
            -PrepareIndicatorProperties() IndicatorProperties
            -DrawIndicator() void
            -DrawIndicatorBackground() void
            -DrawIndicatorItems() void
            -GetRRect() void
            -HideIndicatorPoint() bool
            -GetIndicatorPointMoveOffset() void
            -InitMoveRange() void
            -RefPtr<Flutter::ClipLayer> layer_
            -RefPtr<RenderText> renderDigitalIndicator_
            -int32_t moveStartIndex_
            -int32_t moveEndIndex_
        }
        class SwiperElement {
            +IsFocusable() bool
            +PerformBuild() void
            -registerCallBack() void
            -RequestChildFocus() void
            -HandleIndicatorFocus() void
            -RequestIndicatorFocus() bool
            -RequestCurrentItemFocus() bool
            -RefPtr<FocusNode> indicatorFocusNode_
            -bool showIndicator_
            -Axis axis_
        }

```

**组件相关类介绍：**

**SwiperCreator**: 主要负责解析描述swiper组件布局`dsl`的`json`字符串，生成`SwiperComponent`。

**SwiperComponent**：`Swiper`组件类，提供`Swiper`组件的对外接口，以及创建`RenderSwiper`类型的RenderNode。

**RenderSwiper**: 根据`SwiperComponent`的组件属性信息，给`Swiper`组件以及其子组件做布局，并且处理`Swiper`组件的滑动事件，使其子组件可以在容器内滑动或者自动播放。

**FlutterRenderSwiper**: 根据`SwiperComponent`的组件的导航点属性信息，绘制导航点。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 禁用滑动实现

```
// 示例demo
@Entry
@Component
struct SliderTest {
    @State value: number = 40
    build() {
           Swiper() {
                Image('common/1.png').width(100)
                Image('common/2.png').width(100)
                Image('common/3.png').width(100)
                Image('common/4.png').width(100)
                Image('common/1.png').width(100)
            }
            .disableSwipe(true)
    }
}
```

##### 设计思路：

**1.  前端传入布尔值，默认值为`flase`，保持原先的设计。**

**2. 若前端设置布尔值为true值，将值传入到`SwiperDeclaration`并保存，通过`SwiperDeclaration`传入到`SwiperComponent`，再通过`SwiperComponent`传入，在`render_swiper.cpp`的`OnTouchTestHit`方法中，设置布尔值来禁用`dragDetector`方法。**

### 4.2 关键流程

以下流程描述的是当用户设置true或者false后，从SwiperComponent构造并设置布尔值开始的时序图，关键流程主要在当轨道的布局计算，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    SwiperComponent->>RenderNode:Create()
    SwiperComponent->>RenderNode:DisableSwipe()
    SwiperComponent->>RenderElement: CreateElement()
    RenderNode->>RenderSwiper:PerformLayout()
    RenderNode->>RenderSwiper:Update()
    RenderNode->>FlutterRenderSwiper:Paint()
```

## 5、文件结构

```bash
foundation/ace/core/
    |---components
        |---swiper_component.h
    |---dsl/json
        |---swiper_creator.h
        |---swiper_creator.cpp
    |---test
        |---unittest
           |--- swiper
               |---BUILD.gn
               |--- swiper_creator_test.cpp
    |---rendering/common
        |---render_swiper.h
        |---render_swiper.cpp
    |---rendering/flutter
	    |---flutter_render_swiper.h
	    |---flutter_render_swiper.cpp
    |---test/unittest/swiper
	    |---mock_render_depend.h
	    |---render_swiper_test.cpp
    |---properties
        |--- swiper_indicator.h

```

## 6、伪代码示例

```typescript
@Entry
@Component
struct SwiperTest {
    @State disable: boolean = true;
    build() {
        Column() {
            Swiper() {
                Image('common/1.png').height(100)
                Image('common/2.png').height(100)
                Image('common/3.png').height(100)
                Image('common/4.png').height(100)
                Image('common/1.png').height(100)
            }
            .autoPlay(true)
            .loop(false)
            .displayMode(SwiperDisplayMode.AutoLinear)
            .effectMode(EdgeEffect.Fade)
            .borderWidth(3)
            .width(280).height(160)
            .borderColor(Color.Red)
            .enabled(this.disable)
            Button('disable' + this.disable)
                    .onClick(() => {
                        this.disable = !this.disable
                  })
            }
        }
    }
}
```

