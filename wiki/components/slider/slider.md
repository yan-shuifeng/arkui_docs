# Slider模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中Slider，组件的基本功能属性，主要用于作为音乐视频等媒体播放进度的控制条。

### 1.1 模块架构图

![slider](figures/slider.png)

### 1.2 模块效果图
![image-20211108114513854](figures/image-20211108114513854.png)

![image-20211108114553329](figures/image-20211108114553329.png)

![image-20211108114558573](figures/image-20211108114558573.png)

![image-20211108114603649](figures/image-20211108114603649.png)

![image-20211108114608529](figures/image-20211108114608529.png)

## 2、模块的对外接口

#### 接口说明

Slider(value:{value?: number, min?: number, max?: number, step?: number, style?: SliderStyle, direction?: Axis})

| 接口名称  | 参数        | 默认值  | 参数描述                                                     |
| --------- | ----------- | ------- | ------------------------------------------------------------ |
| value     | number      | 0       | 当前进度值                                                   |
| min       | number      | 0       | 设置最小值                                                   |
| max       | number      | 100     | 设置最大值                                                   |
| step      | number      | 1       | 设置Slider滑动跳动值，当设置相应的step时，Slider为间歇滑动。 |
| style     | SliderStyle | OutSet  | 设置Slider的滑块样式。                                       |
| direction | Axis        | Horizon | 设置Slider的绘制方向                                         |


**表1** Axis枚举说明：

| 名称       | 描述       |
| ---------- | ---------- |
| Vertical   | 纵向排列。 |
| Horizontal | 横向排列。 |

**表2** SliderStyle枚举说明：

| 名称    | 描述           |
| ------- | -------------- |
| Outset  | 滑块在滑轨上。 |
| Inset   | 滑块在滑轨内。 |
| Capsule | 胶囊状滑动条   |

#### 子组件

不包含子组件

#### 属性方法

| 名称          | 参数类型 | 默认值 | 描述                               |
| ------------- | -------- | ------ | ---------------------------------- |
| blockColor    | Color    | -      | 设置滑块的颜色。                   |
| trackColor    | Color    | -      | 设置滑轨的背景颜色。               |
| selectedColor | Color    | -      | 设置滑轨的已滑动颜色。             |
| showSteps     | boolean  | false  | 设置当前是否显示步长刻度值。       |
| showTips      | boolean  | false  | 设置滑动时是否显示气泡提示百分比。 |

#### 事件方法

通用事件仅支持：OnAppear，OnDisAppear。

| 名称     | 参数类型                                         | 描述                                                         |
| -------- | ------------------------------------------------ | ------------------------------------------------------------ |
| onChange | (value: numer, mode?: [SliderChangeMode] => void | Slider滑动时触发事件回调。<br />value：当前进度值。<br />mode：拖动状态。 |

**表3** SliderChangeMode枚举说明：

| 名称   | 描述               |
| ------ | ------------------ |
| Begin  | 用户开始拖动滑块。 |
| Moving | 用户拖动滑块中。   |
| End    | 用户结束拖动滑块。 |

## 3、模块结构

```mermaid
classDiagram
      SliderComponent ..> CircleBlock
      SliderComponent ..> LinearTrack
      SliderComponent ..> ProgressData
      SliderComponent ..> RotationController
      class SliderComponent{
          -ProgressData data_
          -RefPtr<BlockComponent> block_
          -RefPtr<TrackComponent> track_
          -EventMarker onMoveEndEventId_
          -EventMarker onMovingEventId_
          -bool isDisable_
          -bool showTips_
          -bool showSteps_
          -SliderMode mode_
          -RefPtr<RotationController> rotationController_
          +InitStyle() void
          +GetMinValue() double
          +GetMaxValue() double
          +GetStep() double
          +GetValue() double
          +GetTrack() double
          +GetBlock() double
          +GetOnMoveEndEventId() double
          +SetOnMoveEndEventId() void
          +SetOnMovingEventId() void
          +SetBlock() void
          +SetTrack() void
          +SetCurrentValue() void
          +GetOnMovingEventId() EventMarker
          +SetMinValue() void
          +SetMaxValue() void
          +SetStepValue() void
          +MoveSteps() double
          +SetTextDirection() void
          +GetRotationController() RotationController
          +SetDisable() void
          +GetDisable() bool
          +GetSliderMode() SliderMode
          +SetSliderMode() void
          +SetShowTips() void
          +NeedShowTips() bool
          +SetShowSteps() void
          +NeedShowSteps() bool
          +CreateElement() Element
          +CreateRenderNode() RenderNode
          +SetThemeStyle() void
      }
      class RotationController {
          -requestRotationImpl_
          +SetRequestRotationImpl()
          +RequestRotation()
      }
      class ProgressData{
          -double minValue_
          -double maxValue_
          -double value_
          -double stepValue_
          -double cachedValue_
          +SetMinValue()
          +SetMaxValue()
          +SetValue()
          +SetStepValue()
          +SetCachedValue()
          +MoveSteps() double
      }
      class CircleBlock {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      BlockComponent <|-- CircleBlock
      TrackComponent <|-- LinearTrack
      RenderNode <|-- RenderSlider
      RenderSlider <|-- FlutterRenderSlider
      RenderSlider ..> SliderComponent
      class RenderSlider {
        Update() void
        PerformLayout() void
        OnPaintFinish() void
        HandleFocusEvent() bool
        GetValue() double
        GetMax() double
        GetMin() double
        GetStep() double
        GetMode() SliderMode
        GetShowSteps() bool
        GetShowTips() bool
        GetErrorBit() bool
        GetFocus() bool
      }
      class FlutterRenderSlider {
          -RefPtr<RenderNode> block_
          -RefPtr<RenderNode> track_
          -RefPtr<Animator> controllerEnter_
          -RefPtr<Animator> controllerExit_
          -RefPtr<KeyframeAnimation<Color>> colorAnimationEnter_
          -RefPtr<KeyframeAnimation<Color>> colorAnimationExit_
          -RefPtr<Flutter::TransformLayer> layer_
          +Update() void
      }
      class SliderElement {
          -RefPtr<RenderSlider> slider_
          +OnKeyEvent() bool
          +OnFocus() void
          +OnBlur() void
      }

```

**组件相关类介绍：**

**TrackComponent**:表示滑动条组件的滑动轨道，包含绘制轨道所需的颜色属性。

**LinearTrack**：继承`TrackComponent`类，表示直线性的滑动轨道，并重写`CreateRenderNode`逻辑使其生成对应的`LinearTrackRenderNode`，以此来渲染轨道样式。

**BlockComponent**：表示滑动条组件中的滑块，包含绘制滑块的颜色，以及重写`CreateRenderNode`逻辑使其生成对应的`RenderNode`。并支持更多滑块样式的扩展。

**CircleBlock**：继承`BlockComponent`类，表示圆形的带border滑块。重写`CreateRenderNode`方法生成对应的`CircleBlock`并以此来渲染对应的`block`。

**ProgressData**:作为保存数据的类。其中包含`max`，`min`，`value`，`step`,`cachedValue`。

**SliderComponent**：`Slider`组件的具体实现类，通过拼接`Track`,`Block`,`ProgressData`三部分实现`slider`滑动功能。这种拼接的设计保证了较好的扩展性。

**流程相关类：**

**SliderComponent**：保存`slider`相关数据以及生成`element`和`renderNode`。

**RenderSlider**：作为`element`的衔接，主要负责Slider的更新，布局，也负责处理slider的点击，滑动事件。

`Update`方法中会初始化手势识别器已经更新`min`，`max`，`step`，`value`的数据，并计算当前`value`所占比例。

**FlutterRenderSlider**：

`Update` 方法中创建对应的`RenderTrack`和`RenderBlock`节点并挂载在Render树上。**（注意在每一次`update`时，不要重复挂载节点）**。

以及调用`RenderTrack`和`RenderBlock`的`Update`方法去更新对应的`RenderNode`节点。

`PerformLayout`方法负责确定其挂载的子节点`Block`和`Track`位置。并改变`Track`以划过区域的数值，以便由`Track`节点进行渲染。

此对象中使用父类`paint`方法，真正的绘制由子节点进行绘制。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 竖直方向滑动条实现

```
// 示例demo
@Entry
@Component
struct SliderTest {
  @State value: number = 40
  build() {
    Column()
      // sliderDirection属性可以设置滑动条的方向
      Slider({value: this.value, min:0, max:100, step: 1,style: SliderStyle.InSet, sliderDirection:Axis.Horizon})
        .blockColor(Color.Blue)
        .trackColor(Color.Orange)
        .selectedColor(Color.Green)
        .showSteps(true)
        .showTips(true)
        .onChange((value: number) => {
        console.info('value:' + this.value)
      })
    }
  }
}
```

> 上述代码，在构造中传入`sliderDirection`属性值，`sliderDirection`为枚举类型，类型为`Axis`，可选值`Axis.Horizon`和`Axis.Vertical`，分别代表滑动条为水平或是竖直方向。其中在水平方向，如果区分从左到右或者从右到左方向，设置通用属性`direction`，通用属性`Direction.Ltr`和`Direction.Rtl`可以设置从左到右或是从右到左的布局。

##### 设计思路：

**1.  前端传入枚举值，默认值为`Axis.Horizon`，保持原先的设计。**

**2. 若前端设置竖直枚举值Axis.Vertical值，通过`SliderComponent`传入，在轨道组件`TrackComponent`新增方向值，在`flutter_render_linear_track.cpp`的`Paint`方法中，新增绘制垂直的逻辑，新增滑块竖直滑动的逻辑。**

(具体逻辑待开发完毕补充)

#### 4.1.2 手表竖向滑动条支持自定义设置

```
// 示例demo
@Entry
@Component
struct SliderTest {
  @State value: number = 40
  build() {
    Column()
      // SliderStyle枚举属性新增Capsule枚举值，该枚举值代表可以设置手表平台下的滑动条。
      Slider({value: this.value, min:0, max:100, step: 1,style: SliderStyle.Capsule})
        .blockColor(Color.Blue)
        .trackColor(Color.Orange)
        .selectedColor(Color.Green)
        .showSteps(true)
        .showTips(true)
        .onChange((value: number) => {
        console.info('value:' + this.value)
      })
    }
  }
}
```

>上述代码，在构造中的`style`属性中，新增`SliderStyle`枚举值`Capsule`的枚举值，当设置成该枚举值的时候，就可以得到手表平台的滑动条的样式。
>
>按照当前的设计要求，保留原先的`watch_slider`，重新按照`watch_slider`的能力编写出一个可以在任意平台上，可以自定义显示任意位置和自定义显示大小尺寸的以及保留滑动条本身的能力的滑动条。

##### 设计思路：

**1. 前端传入`style`的属性值，默认值为`SliderStyle.Outset`，`SliderStyle`原先的`Outset`与`Inset`枚举值实现保持不变。**

**2. 当前端构造传入`SliderStyle.Capsule`枚举时,`js_slider`在`create`时创建一个新的`Capsule_Slider_Component`，该`component`继承`Slider_Component`类，该组件支持设置宽高，支持设置填充色。获取宽高和填充色后，在`flutter_render_capsule_slider_component`中绘制。**

### 4.2 关键流程

以下流程描述的是当用户设置水平或者竖直的枚举后，从SliderComponent构造并设置枚举值开始的时序图，关键流程主要在当轨道的布局计算，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    SliderComponent->>CircleBlock: Constructor()
    SliderComponent->>LinearTrack: Constructor()
    LinearTrack->>RenderTrack:Create()
    SliderComponent->>RotationController: Constructor()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderTrack:PerformLayout()
    Note over PipelineContext:Calculate the width, height and layout information of horizontal or vertical tracks
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderTrack:Paint()
    RenderTrack->>FlutterRenderTrack:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── slider
   ├── block_component.cpp
   ├── block_component.h
   ├── flutter_render_circle_block.cpp
   ├── flutter_render_circle_block.h
   ├── flutter_render_slider.cpp
   ├── flutter_render_slider.h
   ├── render_block.cpp
   ├── render_block.h
   ├── render_slider.cpp
   ├── render_slider.h
   ├── slider_component.cpp
   ├── slider_component.h
   ├── slider_element.h
   ├── slider_theme.h
   ├── BUILD.gn
├── track
   ├── track_component.cpp
   ├── track_component.h
   ├── render_track.cpp
   ├── render_track.h
   ├── flutter_render_linear_track.cpp
   ├── flutter_render_linear_track.h
   ├── BUILD.gn
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct SliderTest {
    @State value: number = 40
    build() {
        Column() {
            Slider({value: this.value, min:0, max:100, step: 1,style: sliderStyle.InSet, sliderDirection:Axis.Horizon})
                .blockColor(Color.Blue)
                .trackColor(Color.Orange)
                .selectedColor(Color.Green)
                .showSteps(true)
                .showTips(true)
	        .onChange((value: number) => {
                console.info('value:' + this.value)
	})
	}
    }
}
```

