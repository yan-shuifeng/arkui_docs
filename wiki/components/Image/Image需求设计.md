# Image模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中Image，图片组件，组件的基本功能属性，主要用来渲染展示图片。

### 1.1 模块架构图

![image](figures/image.png)

### 1.2 模块效果图

![image](figures/image1.gif)

![image](figures/image2.gif)



## 2、模块的对外接口

#### 接口说明

Image(src:  string \| PixelMap)

| 接口名称 | 参数                    | 默认值 | 参数描述                                                     |
| -------- | ----------------------- | ------ | ------------------------------------------------------------ |
| Image    | src:  string \|PixelMap | -      | 1、支持原始图片路径的uri，同时支持本地和网络路径，支持的图片格式包括静态类型（png，jpg，svg, bmp, webp）和动态类型（gif）。<br/>2、也支持通过指定资源类型（type）和资源名称（name）来引用，比如$m('image')。<br/>3、支持输入PixelMap对象，进行图片展示。 |

#### 子组件

不包含子组件

#### 属性方法

| 名称          | 属性类型                                                     | 默认值   | 描述                                                         |
| ------------- | ------------------------------------------------------------ | -------- | ------------------------------------------------------------ |
| alt           | string                                                       | -        | 加载时显示的占位图。支持本地路径图片资源，也支持通过指定资源类型（type）和资源名称（name）来引用，比如$m('image')。 |
| objectFit     | ImageFit                                                     | Cover    | 设置图片的缩放类型。可选值类型说明请见ImageFit 类型说明。<br>备注：1. svg类型图源不支持该属性  2. 默认为Cover |
| objectEdge    | {top?: number,<br>right?: number,<br>bottom?: number,<br>left?: number} \| number | 0        | 设置图片可缩放区域。填入的数值以图片源尺寸进行计算，也就是说输入的数值以px为单位。<br/>四个值时分别为上、右、下、左边的预留距离（顺时针顺序）。<br/>参数仅为数值时，四个方向预留距离同时生效。 |
| objectRepeat  | ImageRepeat                                                  | NoRepeat | 设置图片的重复样式。可选值类型说明请见ImageRepeat类型说明。<br>备注：1. svg类型图源不支持该属性  2. 默认为NoRepeat |
| interpolation | ImageInterpolation                                           | None     | 设置图片的插值效果，仅针对图片放大插值。可选值类型说明请见ImageInterpolation类型说明。<br/>备注：1. svg类型图源不支持该属性  2. 默认为None |
| renderMode    | ImageRenderMode                                              | Original | 设置图片渲染的模式。可选值类型说明请见ImageRenderMode类型说明。<br/>备注：1. svg类型图源不支持该属性  2. 默认为Original |
| copyable      | boolean                                                      | true     | 设置图片是否支持复制操作                                     |
| sourceSize    | {width: number, height: number}                              | -        | 设置图片解码尺寸，将原始图片解码成设置尺寸的图片。           |
| syncLoad      | boolean                                                      | false    | 1.可以设置同步异步，当前默认是异步。<br/>备注：同步加载时阻塞UI线程，不需要显示占位图。 |

ImageFit类型说明

| 类型      | 描述                                                         |
| --------- | ------------------------------------------------------------ |
| Cover     | 默认值，保持宽高比进行缩小或者放大，使得图片两边都大于或等于显示边界。 |
| Contain   | 保持宽高比进行缩小或者放大，使得图片完全显示在显示边界内。   |
| Fill      | 不保持宽高比进行放大缩小，使得图片填充满显示边界。           |
| None      | 保持原有尺寸显示。通常配合objectRepeat属性一起使用。         |
| ScaleDown | 保持宽高比显示，图片缩小或者保持不变。                       |

ImageRepeat类型说明

| 枚举名称 | 描述                       |
| -------- | -------------------------- |
| X        | 只在水平轴上重复绘制图片。 |
| Y        | 只在竖直轴上重复绘制图片。 |
| XY       | 在两个轴上重复绘制图片。   |
| NoRepeat | 默认值，不重复绘制图片。   |

ImageInterpolation类型说明

| 枚举名称 | 描述                                             |
| -------- | ------------------------------------------------ |
| None     | 默认值，不使用插值图片数据。                     |
| High     | 高度使用插值图片数据，可能会影响图片渲染的速度。 |
| Medium   | 中度使用插值图片数据。                           |
| Low      | 低度使用插值图片数据。                           |

ImageRenderMode类型说明

| 枚举名称 | 描述                                       |
| -------- | ------------------------------------------ |
| Original | 默认值，按照原图进行渲染，包括颜色。       |
| Template | 将图像渲染为模板图像，忽略图片的颜色信息。 |

#### 事件方法

通用事件支持

| 名称     | 参数类型                                        | 描述                                                   |
| -------- | ----------------------------------------------- | ------------------------------------------------------ |
| onChange | (event: {width: number, height: number})=> void | 图片成功加载时触发该回调，返回成功加载的图源尺寸大小。 |
| onError  | ()=> void                                       | 图片加载出现异常时触发该回调。                         |



## 3、模块结构

```mermaid
classDiagram
      RenderImage ..> ImageComponent
      class ImageComponent{
          -src string_
          -alt string_
          -Alignment alignment_
          -ImageObjectPosition imageObjectPosition_
          -Color color_
          -Color fillColor_
          -bool isColorSet_
          -bool isFillSet_
          -EventMarker loadSuccessEvent_
          -EventMarker loadFailEvent_
          -EventMarker svgAnimatorFinishEvent_
          -InternalResource::ResourceId resourceId_
          -Border border_
          -bool fitMaxSize_
          -bool hasObjectPosition_
          -bool matchTextDirection_
          -bool useSkiaSvg_
          -bool autoResize_
          -ImageFit imageFit_
          -ImageInterpolation imageInterpolation_
          -ImageRenderMode imageRenderMode_
          -ImageRepeat imageRepeat_
          -std::pair<Dimension, Dimension> imageSourceSize_
          -RefPtr<PixelMap> pixmap_
          +CreateRenderNode() RenderNode
          +CreateElement() Element
          +SetSrc()void
          +SetAlt() void
          +SetAlt() viod
          +SetAlignment() viod
          +SetColor() viod
          +SetLoadSuccessEvent() viod
          +SetLoadFailEvent() viod
          +SetSvgAnimatorFinishEvent() viod
          +SetResourceId() viod
          +SetBorder() viod
          +SetFitMaxSize() viod
          +SetMatchTextDirection() viod
          +SetImageFill() viod
          +SetImageFit() viod
          +SetImageInterpolation() viod
          +SetImageRenderMode() viod
          +SetImageRepeat() viod
          +SetImageSourceSize() viod
          +SetUseSkiaSvg() viod
          +SetPixmap() viod
          +SetAutoResize() viod
          +GetAlt() std::string
          +GetAlignment() Alignment
          +GetSrc() std::string
          +GetColor() Color
          +GetBorder() Border
          +GetLoadSuccessEvent() EventMarker
          +GetLoadFailEvent() EventMarker
          +GetSvgAnimatorFinishEvent() EventMarker
          +GetResourceId() InternalResource::ResourceId
          +GetFitMaxSize() bool
          +IsColorSet() bool
          +IsMatchTextDirection() bool
          +IsSrcSvgImage() bool
          +GetImageFit() ImageFit
          +GetImageInterpolation() ImageInterpolation
          +GetImageRenderMode() ImageRenderMode
          +GetImageRepeat() ImageRepeat
          +GetImageSourceSize() Dimension
          +GetUseSkiaSvg() bool
          +GetAutoResize() bool
          +IsSvgSuffix() bool
          +GetPixmap() PixelMap
          +SetHasObjectPosition() void
          +GetHasObjectPosition() void
          +SetImageObjectPosition() void
          +GetImageObjectPosition() ImageObjectPosition
          +GetImageFill() Color
          +MakeFromOtherWithoutSourceAndEvent() ImageComponent
      }

      RenderNode <|-- RenderImage
      RenderImage <|-- FlutterRenderImage
      RenderImage ..> ImageComponent
      RenderImage <|-- RosenRenderImage

      class RenderImage {
        Update() void
        PerformLayout() void
        FetchImageObject() void
        Dump() void
        IsSourceWideGamut() void
        NeedResize() bool
        CalculateResizeTarget() void
        ApplyImageFit() void
        ApplyContain() void
        ApplyCover() void
        ApplyFitWidth() void
        ApplyFitHeight() void
        ApplyNone() void
        FireLoadEvent() void
        SetRadius() void
        IsSVG() bool
        PerformLayoutBgImage() void
        ApplyObjectPosition() void
        GenerateImageRects() void
        CalculateImageRenderPosition() void
        ClearRenderObject() void
        PrintImageLog() void
      }

      class RosenRenderImage {
           Update() void
        PerformLayout() void
        FetchImageObject() void
        Dump() void
        IsSourceWideGamut() void
        NeedResize() bool
        CalculateResizeTarget() void
        ApplyImageFit() void
        ApplyContain() void
        ApplyCover() void
        ApplyFitWidth() void
        ApplyFitHeight() void
        ApplyNone() void
        FireLoadEvent() void
        SetRadius() void
        IsSVG() bool
        PerformLayoutBgImage() void
        ApplyObjectPosition() void
        GenerateImageRects() void
        CalculateImageRenderPosition() void
        ClearRenderObject() void
        PrintImageLog() void
      }
      class FlutterRenderImage {
          -ImageSourceInfo curSourceInfo_
          -ImageObjSuccessCallback imageObjSuccessCallback_
          -UploadSuccessCallback uploadSuccessCallback_
          -FailedCallback failedCallback_
          -OnPostBackgroundTask onPostBackgroundTask_
          -RefPtr<ImageObject> imageObj_
          -RefPtr<FlutterRenderTaskHolder> renderTaskHolder_
          +Update() void
      }


```

**组件相关类介绍：**

**ImageComponent：**用以设置相关属性，`Image`组件的具体实现类

**RenderImage：**作为`element`的衔接，主要负责Image的更新，布局。

**FlutterRenderImage**：基于Skia引擎的渲染类，继承**LoadImageCallback**，须实现LoadSuccessCallback和LoadFaildCallback函数。在LoadSuccessCallback中调用MarkNeedLayout()方法绘制图片。此处作为ImageProvider的使用者列出，不是模块内实现内容。

`Update` 方法中创建对应的`RenderTrack`节点并挂载在Render树上。**（注意在每一次`update`时，不要重复挂载节点）**。

以及调用`RenderTrack`的`Update`方法去更新对应的`RenderNode`节点。

`PerformLayout`方法负责确定其挂载的子节点`Track`位置。并改变`Track`的数值，以便由`Track`节点进行渲染。

此对象中使用父类`paint`方法，真正的绘制由子节点进行绘制。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 Image设置同步异步渲染

```
@Entry
@Component
struct imageTest {
    build() {
        Column() {
            Image('common/2.png').height(100)
            .syncLoad(true)
            Image('').height(100)
            .syncLoad(true)
            Image('common/4.png').height(100)
            .syncLoad(true)
            Image('common/1.png').height(100)
            .syncLoad(true)
            Text('alt').fontSize(12).fontColor(0xcccccc).width('96%').height(30)
                .alt('app.media.Image_none')
                .width(100).height(100).border({ width: 1 }).borderStyle(BorderStyle.Dashed)
        }
    }
}
```

设计思路：Image设置同步异步模式主要负责为上层组件提供图片，根据组件设置的图片源对图片进行加载时，同步则是图片单线程加载，异步则可以使用多线程加载，未加载出来的图片不影响其他图片的加载，图片加载成功或失败时回调注册组件的回调函数。支持的图片格式（包含png，jpg，bmp，webp，wbmp，gif等）。

### 4.2 关键流程

以下流程描述的是

```mermaid
sequenceDiagram
    Imagecompoent->>RenderNode:OnLayout()
    RenderNode->>RendeImage:PerformLayout()
    Note over PipelineContext:Calculate the width, height and layout information of horizontal or vertical tracks
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderImage:Paint()
    RenderImage->>FlutterRenderImage:Paint()
```



## 5、文件结构

```bash
ace_engine/frameworks/bridge/declarative_frontend
├── jsview
   ├── js_image.cpp
   ├── js_image.h
ace_engine/frameworks/core/components
├── Image
   ├── image_component.cpp
   ├── image_component.h
   ├── flutter_render_image.cpp
   ├── flutter_render_image.h
   ├── image_animator_component.cpp
   ├── image_animator_component.h
   ├── render_image.cpp
   ├── render_image.h
   ├── render_image_creator.cpp
   ├── image_component.cpp
   ├── image_component.h
   ├── image_element.h
   ├── image_theme.h
   ├── rosen_render_image.cpp
   ├── rosen_render_image.h
   ├── BUILD.gn
```



## 6、伪代码示例

```typescript
@Entry
@Component
struct imageTest {
    build() {
        Column() {
            Image('common/2.png').height(100)
            .syncLoad(true)
            Image('').height(100)
            .syncLoad(true)
            Image('common/4.png').height(100)
            .syncLoad(true)
            Image('common/1.png').height(100)
            .syncLoad(true)
            Text('alt').fontSize(12).fontColor(0xcccccc).width('96%').height(30)
                .alt('app.media.Image_none')
                .width(100).height(100).border({ width: 1 }).borderStyle(BorderStyle.Dashed)
        }
    }
}
```



