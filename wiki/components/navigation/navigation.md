Navigation组件设计

# 1、模块功能描述

Navigation组件一般作为Page页面的跟节点，通过属性设置来展示页面的标题、工具栏、菜单等。

## 1.1 模块架构图

![swiper_model_pic](figures\navigation_model_pic.png)

##  1.2 模块效果图



# 2、模块的对外接口

##  声明式开发接口

### 接口

Navigation()

#### 属性方法

| 名称      | 参数类型                             | 默认值                   | 描述                 |
| --------- | ------------------------------------ | ------------------------ | -------------------- |
| title     | string \| Builder                    | 无                       | 页面标题。           |
| subtitle  | string                               | 无                       | 页面副标题。         |
| menus     | Array<NavigationMenuItem> \| Builder | 无                       | 页面右上角菜单。     |
| titleMode | NavigationTitleMode                  | NavigationTitleMode.Free | 页面标题栏显示模式。 |

NavigationTitleMode枚举说明

| 名称 | 描述                                                         |
| ---- | ------------------------------------------------------------ |
| Free | 当内容为可滚动组件时，标题栏支持随着可滚动组件滑动时变化，当列表滑动到顶后向下继续标题栏滑动展开为大标题模式，当列表从顶端开始向上滑动时标题栏收起成小标题模式。 |
| Mini | 固定为小标题模式。                                           |
| Full | 固定为大标题模式。                                           |



#### 事件方法

除了支持通用事件之外，还支持如下事件。

| 名称                                                         | 功能描述                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| onTitleModeChanged(callback: (titleMode: NavigationTitleMode) => void) | 当titleMode为NavigationTitleMode.Free时，随着可滚动组件的滑动标题栏模式发生变化时触发此回调。titleMode: 变化后的标题栏模式。 |

#  3、模块结构



```mermaid
classDiagram
    Element <|-- RenderElemnt
    RenderElemnt <|-- ComponentGroupElement
    ComponentGroupElement <|-- NavigationContainerElement
    Component <|-- RenderComponent
    Component <|-- SingleChild
    SingleChild <|-- ComposedComponent
    RenderComponent <|-- ComponentGroup
    Element .. Component
    Element .. RenderNode
    Component .. RenderNode
    ComponentGroup <|-- NavigationContainerComponent
    class Component{
        +CreateElement()
    }
    class RenderComponent{
    +CreateRenderNode()
    }
    class SingleChild{
    }
    class ComponentGroup {
    }
    class Element{
    }
    class RenderElemnt {
    }
    class ComponentGroupElement {
    }
    Element <|-- ComposedElement
    class ComposedElement {
    }
    class ComposedComponent {
    }
    class NavigationContainerComponent{
        +CreateRenderNode()
    }
    ComposedComponent <|-- NavigationBarComponent
    class NavigationBarComponent {
        +InitStyle(const RefPtr<NavigationBarTheme>& theme)
        +Build(const WeakPtr<PipelineContext>& context)
    }
    ComposedElement <|-- NavigationBarElement
    class NavigationBarElement {
        +PerformBuild()
        +RequestNextFocus(bool vertical, bool reverse, const Rect& rect)
    }
    RenderNode <|-- RenderNavigationContainer
    RelatedContainer <|-- RenderNavigationContainer
    RenderNavigationContainer
    RenderNavigationContainer ..> NavigationContainerComponent
    class RenderNavigationContainer {
        Update() void
        PerformLayout() void
    }

    class NavigationContainerElement {
        +PerformBuild()
        +RequestNextFocus(bool vertical, bool reverse, const Rect& rect)
        -ConnectNavigator(const RefPtr<StageElement>& targetContainer)
    }

    class RelatedContainer {
        +OnRelatedStart()
        +OnRelatedPreScroll()
        +OnRelatedScroll()
        +OnRelatedEnd()
    }

    class RelatedChild {
        +RelatedEventStart()
        +RelatedScrollEventPrepare(const Offset& delta)
        +RelatedScrollEventDoing(const Offset& delta)
        +RelatedEventEnd()
        -bool enableRelatedEvent_
        -WeakPtr<RelatedContainer> relatedParent_
    }
    ComponentGroup <|-- CollapsingNavigationBarComponent
    class CollapsingNavigationBarComponent {

    }
    RenderNode <|-- RenderCollapsingNavigationBar
    RelatedChild <|-- RenderCollapsingNavigationBar
    class RenderCollapsingNavigationBar {
        +Update() void
        +PerformLayout() void
    }
```

**组件相关类介绍：**

NavigationBarComponent：NavigationBar组件类，提供NavigationBar组件的对外接口，以及根据属性设置创建对应的组件树.

NavigationBarElement: 创建NavigationBarComponent的组件对应的Element，重写PerformBuild, 根据Build创建的组件树，创建对应的Element树，并且给菜单按钮绑定菜单的选择事件。

# 4、关键流程

## 4.1 关联滑动

```mermaid
sequenceDiagram
    Scrollable->>+RenderNavigationContainer: OnRelatedStart()
    RenderNavigationContainer->>-RenderCollapsingNavigationBar: OnRelatedStart()
    Scrollable->>+RenderNavigationContainer: OnRelatedPreScroll()
    RenderNavigationContainer->>-RenderCollapsingNavigationBar: OnRelatedPreScroll()
    Scrollable->>+RenderNavigationContainer: OnRelatedScroll()
    RenderNavigationContainer->>-RenderCollapsingNavigationBar: OnRelatedScroll()
    Scrollable->>+RenderNavigationContainer: OnRelatedEnd()
    RenderNavigationContainer->>-RenderCollapsingNavigationBar: OnRelatedEnd()
```

# 5、文件结构件

```bash
ace_engine/frameworks/core/components
├── navigation_bar
    ├── BUILD.gn
    ├── navigation_bar_component.cpp
    ├── navigation_bar_component.h
    ├── navigation_bar_element.cpp
    ├── navigation_bar_element.h
    ├── navigation_bar_theme.h
    ├── navigation_container_component.cpp
    ├── navigation_container_component.h
    ├── navigation_container_element.cpp
    ├── navigation_container_element.h
    ├── render_collapsing_navigation_bar.cpp
    ├── render_collapsing_navigation_bar.h
    ├── render_navigation_container.cpp
    └── render_navigation_container.h
ace_engine/frameworks/core/pipeline/base/
    ├── related_node.cpp
    ├── related_node.h
```
