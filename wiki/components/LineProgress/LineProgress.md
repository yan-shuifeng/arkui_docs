# LineProgress模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中DataPanel组件增加面板类型，并增加线性进度条样式。

#### 说明

​	颜色目前不支持修改，后续会开接口

## 1.1模块效果图

![LineProgress](figures/LineProgress.png)

## 2、模块的对外接口

#### 接口说明

DataPanel(value:{values: number[], max?: number, type?: DataPanelType})

| 参数名 | 参数          | 必填 | 默认值   | 参数描述                      |
| ------ | ------------- | ---- | -------- | ----------------------------- |
| value  | number[]      | 是   | -        | 数据值列表，最大支持9个数据。 |
| max    | number        | 否   | 100      | 数据最大值                    |
| type   | DataPanelType | 否   | Rainbows | 数据面板类型                  |

DataPanelType枚举说明：

| 名称     | 描述         |
| -------- | ------------ |
| Line     | 线型数据面板 |
| Rainbows | 彩虹数据面板 |

## 3、模块结构

```mermaid
classDiagram
      JSDataPanel ..> PercentageDataPanelComponent
      PercentageDataPanelComponent ..> FlutterRenderPercentageDataPanel
      PercentageDataPanelComponent ..> RosenRenderPercentageDataPanel
      class JSDataPanel{
      	+create() void
      }
      class FlutterRenderPercentageDataPanel{
      	+PaintLinearProgress() void
      	+PaintColorSegment() void
      	+PaintBackground() void
      	+PaintSpace() void
      }
      class RosenRenderPercentageDataPanel{
      	+PaintLinearProgress() void
      	+PaintColorSegment() void
      	+PaintBackground() void
      	+PaintSpace() void
      }

```

## 4、关键流程

```mermaid
sequenceDiagram
    JSDataPanel->>PercentageDataPanelComponent: SetPanelType(ChartType type)
    PercentageDataPanelComponent->>FlutterRenderPercentageDataPanel: GetPanelType()
    PercentageDataPanelComponent->>RosenRenderPercentageDataPanel:GetPanelType()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components/data_panel
|-- rosen_render_data_panel.h
|-- rosen_render_data_panel.cpp
|-- flutter_render_data_panel.h
|-- flutter_render_data_panel.cpp

frameworks/bridge/declarative_frontend/jsview
|-- js_data_panel.cpp

frameworks/bridge/declarative_frontend/engine
|-- jsEnumStyle.js

api/@internal/component/ets
|-- data_panel.d.ts
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct DataPanelExample {
  public values1: number[] = [10, 10, 10, 10, 10,10,10,10,20]
  build() {
    Column({ space: 5 }) {
      DataPanel({ values: this.values1, max: 100 ,type : DataPanelType.Line}).width(100).height(10)
    }.width('100%').margin({ top: 5 })
  }
}
```

