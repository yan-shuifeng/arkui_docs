# Menu模块设计



## 1、模块功能描述

介绍菜单组件Menu的设计实现。Menu组件支持文本菜单项以及通过Builder构造器自定义菜单项组件。

如果菜单项使用文本，则仅支持设置菜单项文本和点击回调事件。如果使用Builder来构造菜单项，则可以自由定义各种组件和样式，灵活性更高。

## 2、模块的对外接口

#### 接口说明

| 名称     | 参数类型                                     | 默认值 | 参数描述                         |
| -------- | -------------------------------------------- | ------ | -------------------------------- |
| bindMenu | Array<[MenuItem](#li430441812114)>\| builder | -      | 给组件绑定菜单，点击后弹出菜单。 |

#### MenuItem 

| 参数名 | 参数类型   | 参数描述               |
| ------ | ---------- | ---------------------- |
| value  | string     | 菜单项文本。           |
| action | () => void | 点击菜单项的事件回调。 |

## 3、关键流程

#### 使用文本菜单项

```
@Entry
@Component
struct menuExample {
  build() {
    Column() {
      Text('click for Menu')
    }
    .width('100%')
    .margin({ top: 5 })
    .bindMenu([
      {
        value: 'Menu1',
        action: () => {
          console.info('handle Menu1 select')
        }
      },
      {
        value: 'Menu2',
        action: () => {
          console.info('handle Menu2 select')
        }
      },
    ])
  }
}
```

当在前端设置了文本菜单项的参数，主要是文本内容和点击回调事件，后端处理流程：

- 在JSViewAbstract::JsBindMenu中解析这些参数：先根据文本内容构造TextComponent，然后和解析出来的回调一起生成新的OptionComponent，将该OptionComponent通过MenuComponent添加到SelectPopupComponent的options_队列中。
- 在初始化OptionComponent时，将根据文本内容构造的TextComponent设置成子节点，并设置TextComponent的样式。
- 在RenderOption::PerformLayout中设置菜单项的尺寸大小以及弹出位置，主要根据鼠标点击的位置设置弹出菜单项的位置。

#### 通过Builder构造器生成菜单项

**基本设计思路：**在原有的支持文本菜单项流程基础上，将通过Builder生成的自定义组件封装成一个OptionComponent，添加到MenuComponent中。

```mermaid
sequenceDiagram
	JSEngine->>JSViewAbstruct: JsBindMenu()
	JSViewAbstruct ->> JSViewAbstruct: get custom component <br> (eg. TextComponent) from builder
	JSViewAbstruct ->> OptionComponent: SetCustomComponent()
	OptionComponent ->> OptionElement: CreateElement()
	OptionElement ->> OptionElement: PerformBuild()
	OptionElement ->> OptionComponent: Initialize()
	OptionComponent ->> TextComponent: set child
	TextComponent ->> TextElement: CreateElement()
	TextElement ->> TextElement: PerformBuild()
	TextComponent ->> RenderText: CreateRenderNode()
	RenderText ->> FlutterRenderText: Create()
	FlutterRenderText ->> FlutterRenderText: Paint()
```

当前端通过Builder自定义了菜单项组件后，后端处理流程：

- 在JSViewAbstract::JsBindMenu中获取解析builder参数，然后通过JsFunction::Execute生成自定义的customComponent，将其添加到OptionComponent中。最后将该OptionComponent通过MenuComponent添加到SelectPopupComponent的options_队列中。

- 在初始化OptionComponent时，需要新增对应的处理逻辑：如果customComponent存在，则调用OptionComponent::AppendChild方法，将customComponent设置成子节点。
- 在RenderOption::PerformLayout中新增对应处理逻辑：如果是自定义菜单项，则调用GetLastChild获取customComponent对应的RenderNode，然后调用其LayOut方法。



## 4、文件结构

```
ace_engine/frameworks/core/components
├── menu
   ├── BUILD.gn
   ├── menu_component.cpp
   ├── menu_component.h
   ├── menu_element.cpp
   └── menu_element.h
├── option
   ├── BUILD.gn
   ├── flutter_render_option.cpp
   ├── flutter_render_option.h
   ├── option_component.cpp
   ├── option_component.h
   ├── option_element.cpp
   ├── option_element.h
   ├── render_option.cpp
   ├── render_option_creator.cpp
   ├── render_option.h
   ├── rosen_render_option.cpp
   └── rosen_render_option.h
├── select_popup
   ├── BUILD.gn
   ├── flutter_render_select_popup.cpp
   ├── flutter_render_select_popup.h
   ├── render_select_popup.cpp
   ├── render_select_popup_creator.cpp
   ├── render_select_popup.h
   ├── rosen_render_select_popup.cpp
   ├── rosen_render_select_popup.h
   ├── select_popup_component.cpp
   ├── select_popup_component.h
   ├── select_popup_element.cpp
   └── select_popup_element.h

```



## 5、代码示例

​	

```
@Entry
@Component
struct MenuExample {
  @Builder MenuBuilder() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Text('text').fontSize(20)
    }
  }
  
  build() {
    Column() {
      Text('click for Menu')
    }
    .width('100%')
    .margin({ top: 5 })
    .bindMenu(this.MenuBuilder)
//    .bindMenu([
//      {
//        value: 'Menu1',
//        action: () => {
//          console.info('handle Menu1 select')
//        }
//      },
//      {
//        value: 'Menu2',
//        action: () => {
//          console.info('handle Menu2 select')
//        }
//      },
//    ])
  }
}
```

