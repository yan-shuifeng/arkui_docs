# Tabs模块设计

## 1、模块功能描述

本模块设计文档描述`Ace`子系统中`Tabs`, `TabContent`, `TabBar`组件的基本功能属性，`Tabs`通常放在内容区上方，展示不同的分类，页签名称应简洁明了，清晰描述分类的内容。

### 1.1 模块架构图

![](figures/Tab.png)

### 1.2 模块效果图
![](figures/1.PNG)

![](figures/2.PNG)

![](figures/3.PNG)

![](figures/4.PNG)

![](figures/5.PNG)

![](figures/6.PNG)

## 2、模块的对外接口

#### 2.1 Tabs接口说明

`Tabs(value: {barPosition?: BarPosition, index?: number, controller?: TabsController})`

| 参数名      | 参数类型       | 必填 | 默认值 | 参数描述                         |
| ----------- | -------------- | ---- | ------ | -------------------------------- |
| barPosition | BarPosition    | 否   | Start  | 指定页签位置来创建Tabs容器组件。 |
| index       | number         | 否   | 0      | 指定初次初始页签索引。           |
| controller  | TabsController | 否   |        | 设置Tabs控制器。                 |

**表1** `barPosition`枚举说明：

| 枚举值 | 描述                                                         |
| ------ | ------------------------------------------------------------ |
| Start  | vertical属性方法设置为true时，页签位于容器左侧；vertical属性方法设置为false时，页签位于容器顶部。 |
| End    | vertical属性方法设置为true时，页签位于容器右侧；vertical属性方法设置为false时，页签位于容器底部。 |

**`TabsController`对象说明：**

Tabs组件的控制器，用于控制Tabs组件进行页签切换。

| 名称        | 参数类型 | 返回值 | 描述                                         |
| ----------- | -------- | ------ | -------------------------------------------- |
| changeIndex | number   | void   | 控制Tabs切换到指定页签，index索引值从0开始。 |

##### 属性方法

| 名称       | 参数类型 | 默认值                           | 描述                                             |
| ---------- | -------- | -------------------------------- | ------------------------------------------------ |
| vertical   | boolean  | false。                          | 是否为纵向Tab。                                  |
| scrollable | boolean  | true。                           | 是否可以通过左右滑动进行页面切换。               |
| barMode    | BarMode  | 默认为BarMode.Fixed              | TabBar布局模式。                                 |
| barWidth   | number   | 不设置时使用系统主题中的默认值。 | TabBar的宽度值，不设置时使用系统主题中的默认值。 |
| barHeight  | number   | 不设置时使用系统主题中的默认值。 | TabBar的高度值，不设置时使用系统主题中的默认值。 |

**表2** `BarMode`枚举说明:

| 名称       | 描述                                         |
| ---------- | -------------------------------------------- |
| Scrollable | TabBar使用实际布局宽度, 超过总长度后可滑动。 |
| Fixed      | 所有TabBar平均分配宽度。                     |

##### 事件方法

| 名称                                        | 功能描述                  |
| ------------------------------------------- | ------------------------- |
| onChange(callback: (index: number) => void) | Tab页签切换后触发的事件。 |

#### 2.2 TabContent接口说明：

##### 属性方法

| 名称   | 参数类型                                                     | 默认值 | 描述                                                         |
| ------ | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| tabBar | string \|<br />{icon?: string, text?: string}\|<br />builder: Builder | -      | 设置TabBar上显示内容。<br/>如果icon采用svg格式图源，则要求svg图源删除其自有宽高属性值。如采用带有自有宽高属性的svg图源，icon大小则是svg本身内置的宽高属性值大小。<br />builder: tabBar构造器 |

##### 说明

- `TabContent`组件不支持设置通用宽度属性，其宽度默认撑满`Tabs`父组件。
- `TabContent`组件不支持设置通用高度属性，其高度由`Tabs`父组件高度与`TabBar`组件高度决定。

## 3、模块结构

```mermaid
classDiagram
      TabsComponent ..> TabBarIndicatorComponent
      TabsComponent ..> TabBarComponent
      TabsComponent ..> TabContentComponent
      TabsComponent ..> TabController
      TabsComponent --> TabsElement
      TabBarElement -->RenderTabBar
      TabBarComponent --> TabBarElement
      TabContentComponent --> TabContentElement
      TabContentElement --> RenderTabContent
      class TabsComponent{
          -std::list<RefPtr<Component>> tabContentChildren_
          -std::list<RefPtr<Component>>
          -RefPtr<TabController> controller_
          -RefPtr<TabBarIndicatorComponent>
          -RefPtr<TabContentComponent> tabContent_
          -RefPtr<TabBarComponent> tabBar_
          -RefPtr<FlexItemComponent> flexItem_
          +CreateElement() Element
          +SetTabsController() void
          +GetTabsController() TabController
          +GetTabContentChild() TabContentComponent
          +GetTabBarChild() TabBarComponent
          +AppendChild() void
          +RemoveChild() void
      }
      class TabController {
        -int32_t id_
        -int32_t index_
        -int32_t initialIndex_
        -bool pageReady_
        -Element contentElement_
        -Element barElement_
        -TabBarChangeListener tabBarChangeListener_
        +ValidateIndex() void
        +SetPageReady() void
        +SetIndex() void
        +SetIndexByController() void
        +SetIndexByScrollContent() void
        +GetIndex() int32_t
        +SetContentElement() void
        +SetBarElement() void
        +GetId() int32_t
        +ChangeDispatch() void
        +GetInitialIndex() int32_t
        +SetInitialIndex() void
        +OnTabBarChanged() void
        +SetTabBarChangeListener() void
      }
      class TabBarComponent{
          -TabBarIndicatorType indicatorSize_
          -TabBarMode mode_
          -Edge labelPadding_
          -Edge padding_
          +SetBarPosition() void
          +SetPadding() void
          +SetLabelPadding() void
          +GetPadding() Edge
          +GetLabelPadding() Edge
          +GetBarPosition() BarPosition
      }
      class TabContentComponent{
          -bool scrollable_
          -bool vertical_
          -float scrollDuration_
          +GetController() TabController
          +SetScrollable() void
          +SetVertical() void
          +SetScrollDuration() void
      }
      class TabBarIndicatorComponent{
      	-TabBarIndicatorStyle indicatorStyle_
        -Edge indicatorPadding_
        -Color indicatorColor_
        -Dimension indicatorWidth_
        -Decoration indicatorDecoration_
        +GetIndicatorStyle() TabBarIndicatorStyle
        +GetIndicatorPadding() Edge
        +GetIndicatorColor() Color
        +GetIndicatorWidth() Dimension
        +GetIndicatorDecoration() Decoration
      }
      BoxComponent <|-- TabBarIndicatorComponent
      ComponentGroup <|-- TabBarComponent
      ComponentGroup <|-- TabContentComponent
      RenderTabContent <|-- FlutterRenderTabContent
      RenderTabBar <|-- FlutterRenderTabBar
      class RenderTabContent {
      }
      class FlutterRenderTabContent {
      }
      class RenderTabBar {
      }
      class FlutterRenderTabBar {
      }
      class TabsElement {
      }
```

**组件相关类介绍：**

**TabsComponent**:表示`Tabs`组件类，包含控制类，下划线类，`TabBar`类，`TabContent`类。

**TabBarIndicatorComponent**：表示`Tabbar`选中下划线的类，包含选中下划线的颜色以及粗细等属性。

**TabBarComponent**：表示`Tabs`中的`TabBar`页签，重写`CreateRenderNode`逻辑使其生成对应的`RenderNode`。

**TabContentComponent**：表示`Tabs`的内容类，重写`CreateRenderNode`方法生成对应的`RenderTabContent`并以此来渲染对应的`TabContent`。

**TabController**:作为`Tabs`的控制器，通过该控制器可以控制`TabContent`和`TabBar`，通过`index`双向绑定完成切页。

**RenderTabBar**：作为`element`的衔接，主要负责`TabBar`的更新，布局，也负责处理`TabBar`的事件处理。

`Update`方法中会初始化手势识别器并更新属性数据，并通过`PerformLayout`方法完成布局。

**FlutterRenderTabBar**：通过`Paint`方法完成对`TabBar`的绘制。

**RenderTabContent**：作为`element`的衔接，主要负责`TabContent`的更新，布局，也负责处理`TabContent`的事件。`Update`方法中会初始化手势识别器并更新属性数据，并通过`PerformLayout`方法完成布局。

**FlutterRenderTabContent**：

处理`TabContent`的绘制相关逻辑，通过`Paint`方法完成对`TabContent`的绘制。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 tabBar属性自定义Builder实现

```
// 示例代码
@Builder function GlobalBuilder() {
  Text("Bar 3")
}
@Entry
@Component
struct TabsExample {
  private controller: TabsController = new TabsController()

  @Builder inlineBuilder() {
    Text("Bar 4")
  }

  build() {
    Column() {
      Tabs({ barPosition: BarPosition.Start, index: 1, controller: this.controller }) {
        TabContent() {
          Column().width('100%').height('100%').backgroundColor(Color.Pink)
        }.tabBar(GlobalBuilder)

        TabContent() {
          Column().width('100%').height('100%').backgroundColor(Color.Yellow)
        }.tabBar(this.inlineBuilder)

        TabContent() {
          Column().width('100%').height('100%').backgroundColor(Color.Yellow)
        }.tabBar('yellow')
      }
      .vertical(true).scrollable(true).barMode(BarMode.Fixed)
      .barWidth(70).barHeight(150).animationDuration(400)
      .onChange((index: number) => {
        console.info(index.toString())
      })
      .width('90%').backgroundColor(0xF5F5F5)
    }.width('100%').height(150).margin({ top: 5 })
  }
}
```

> 上述代码，在全局定义一个`@Builder`的修饰器，在`TabContent`的`tabBar`设置的时候，将`GlobalBuilder`设置到该属性中去 ，实际效果可以在`tabBar`的位置显示文本。

##### 设计思路：

> 设计以上的代码需要工具链和前端解析的支持，首先在前端工具链要支持识别上述的语法形式，并转换成相应的`JS`文件，在前端解析的时候，通过`builder`标识解析出一个`function`类型，并将其创建出一个`JsFunction`并执行，执行完以后返回一个`Root`的`Component`节点，并将该节点直接挂载到`TabBarComponent`组件上即可。

## 5、文件结构

```
ace_engine/frameworks/core/components
├── tab_bar
   ├── tabs_component.cpp
   ├── tabs_component.h
   ├── flutter_render_tab_bar.cpp
   ├── flutter_render_tab_bar.h
   ├── flutter_render_tab_bar_item.cpp
   ├── flutter_render_tab_bar_item.h
   ├── flutter_render_tab_content.cpp
   ├── flutter_render_tab_content.h
   ├── render_tab_bar.cpp
   ├── render_tab_bar.h
   ├── render_tab_bar_item.cpp
   ├── render_tab_bar_item.h
   ├── render_tab_content.cpp
   ├── render_tab_content.h
   ├── tab_bar_component.cpp
   ├── tab_bar_component.h
   ├── tab_bar_element.cpp
   ├── tab_bar_element.h
   ├── tab_bar_indicator_component.cpp
   ├── tab_bar_indicator_component.h
   ├── tab_bar_item_component.cpp
   ├── tab_bar_item_component.h
   ├── tab_bar_item_element.h
   ├── tab_bar_size_animation.cpp
   ├── tab_bar_size_animation.h
   ├── tab_content_component.cpp
   ├── tab_content_component.h
   ├── tab_content_element.cpp
   ├── tab_content_element.h
   ├── tab_content_item_component.cpp
   ├── tab_content_item_component.h
   ├── tab_content_item_element.cpp
   ├── tab_content_item_element.h
   ├── tab_controller.cpp
   ├── tab_controller.h
   ├── tab_theme.h
   ├── tabs_component.cpp
   ├── tabs_component.h
   ├── tabs_element.h
   ├── BUILD.gn
```

## 6、伪代码示例

```typescript
// 示例demo
@Entry
@Component
struct TabsExample {
  private controller: TabsController = new TabsController()

  build() {
    Column() {
      Tabs({ barPosition: BarPosition.Start, index: 1, controller: this.controller }) {
        TabContent() {
          Column().width('100%').height('100%').backgroundColor(Color.Green)
        }.tabBar('green')
          TabContent() {
          Column().width('100%').height('100%').backgroundColor(Color.Yellow)
        }.tabBar('yellow')
      }
      .vertical(true).scrollable(true).barMode(BarMode.Fixed)
      .barWidth(70).barHeight(150).animationDuration(400)
      .onChange((index: number) => {
        console.info(index.toString())
      })
      .width('90%').backgroundColor(0xF5F5F5)
    }.width('100%').height(150).margin({ top: 5 })
  }
}
```

