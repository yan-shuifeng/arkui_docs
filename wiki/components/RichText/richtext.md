# RichText模块设计
## 1.模块功能描述
本模块设计文档描述Ace子系统中rich_text，组件的基本功能属性，主要用于支持HTML格式的富文本显示。

### 1.1模块架构图
![slider](figures/richtext.png)

### 1.2模块效果图
![slider](figures/richtext1.png)
![slider](figures/richtext2.png)

## 2.模块的对外接口
#### 接口说明
RichText\(content:string\)

-   参数

  | 参数名 | 参数类型 | 必填 | 默认值 | 参数描述 |
  | -------- | -------- | -------- | -------- | -------- |
  | content | string | 是 | - | 表示HTML格式的字符串。 |

### 事件方法
主要支持如下事件
| 名称 | 描述 |
| -------- | -------- |
| onStart() => void | 加载网页时触发。 |
| onComplete() => void | 网页加载结束时触发。 |

## 支持标签

| 名称 | 描述 | 示例 |
| -------- | -------- | -------- |
| \<h1>--\<h6> | 被用来定义HTML，\<h1>定义重要等级最高的标题，\<h6>定义重要等级最低的标题。 | \<h1>这是一个标题\</h1>\<h2>这是h2标题\</h2> |
| \<p>\</p> | 定义段落。 | \<p>这是一个段落\</p> |
| \<br/> | 插入一个简单的换行符。 | \<p>这是一个段落\<br/>这是换行段落\</p> |
| \<hr/> | 定义HTML页面中的主题变化（比如话题的转移），并显示为一条水平线。 | \<p>这个一个段落\</p>\<hr/>\<p>这是一个段落\</p> |
| \<div>\</div> | 常用于组合块级元素，以便通过CSS来对这些元素进行格式化。 | \<div style='color:#0000FF'>\<h3>这是一个在div元素中的标题。\</h3>\</div> |
| \<i>\</i> | 定义与文本中其余部分不同的部分，并把这部分文本呈现为斜体文本。 | \<p>\<i>这是一个斜体\</i>\</p> |
| \<u>\</u> | 定义与常规文本风格不同的文本，像拼写错误的单词或者汉语中的专有名词，应尽量避免使用\<u>为文本加下划线，用户会把它混淆为一个超链接。 | \<p>这是带有下划线的段落\</p> |
| \<style>\</style> | 定义HTML文档的样式信息。 | \<style>h1{color:red;}p{color:blue;}\</style> |
| style | 属性规定元素的行内样式，写在标签内部，在使用的时候需用引号来进行区分，并以; 间隔样式，style='width: 500px;height: 500px;border: 1px soild;margin: 0 auto;'。 | \<h1 style='color:blue;text-align:center'>这是一个标题\</h1>\<p style='color:green'>这是一个段落。\</p> |
| \<script>\</script> | 用于定义客户端文本，比如JavaScript。 | \<script>document.write("Hello World!")\</script> |


## 3.模块结构
```
classDiagram
    RichtextComponent ..> RenderNode
    RichtextComponent ..> Element
    RichtextComponent ..> WebController
    RichtextComponent ..> webComponent
    class RichTextComponent {
        -RefPtr<WebComponent> webComponent_
        -string data
        +CreateRenderNode() RenderNode
        +CreateElement() Element
        +SetData() void
        +GetData() string
        +GetController() WebController
        +SetPageStartEventId() void
        +SetPageFinishEventId() void
    }
    class WebController {
        -RefPtr<WebDeclaration> declaration_
        -LoadUrlImpl loadUrlImpl_
        -ExecuteTypeScriptImpl executeTypeScriptImpl_
        -LoadDataWithBaseUrlImpl loadDataWithBaseUrlImpl_
        +LoadUrl() void
        +SetLoadUrlImpl() void
        +ExecuteTypeScriptImpl() void
        +LoadDataWithBaseUrlImpl() void
        +SetLoadDataWithBaseUrlImpl() void
        +Reload() void
    }
    class WebComponent {
        -RefPtr<WebDeclaration> declaration_
        -CreateCallback createCallback_
        -ReleasedCallback releasedCallback_
        -ErrorCallback errorCallback_
        -RefPtr<WebDelegate> delegate_
        -RefPtr<WebController> webController_
        -string type_
        -bool isJsEnable_
        -bool isContenAccessEnabled_
        -bool isFileAccessEnabled_
        +createRenderNode() RenderNode
        +createElement() Element
        +SetType() void
        +GetType() void
        +SetSrc() void
        +GetSrc() void
        +SetPageStartedEventId() void
        +GetPageStartedEventId() EventMarker
        +SetPageFinishedEventId() void
        +GetPageFinishedEventId() EventMarker
        +SetRequestFocusEventId() void
        +GetRequestFocusEventId() EventMarker
        +SetPageErrorEventId() void
        +GetPageErrorEventId() EventMarker
        +SetMessageEventId() void
        +GetMessageEventId() EventMarker
        +SetDeclaration() void
        +GetController() WebCont
        +SetWebController() void
        +GetJsEnabled() bool
        +SetJsEnabled() bool
        +GetContenAccessEnabled() bool
        +SetContenAccessEnabled() bool
        +GetFileAccessEnable() bool
        +setFileAccessEnable() bool 
    }
    RenderRichText ..>RenderNode
    RenderRichText ..>RichTextComponent
    class RenderRichText {
        -RefPtr<RichTextComponent> richText_;
        +Create() RenderNode
        +update() void
        +PerformLayout() void
        +OnPaintFinish() void
    }
    class RichTextElement {
        +PerformBuild() void
    }
```
组件相关类介绍：
WebComponent：继承自RenderComponent类，表示打开对应的HTML格式中的文本信息，通过传入的参数去对文本进行渲染，以满足对应的要求。
RichTextComponent：继承自SoleChildComponent类RichText的具体实现类，通过调用WebComponent，用Web的能力支持富文本的显示。


## 4.关键流程

### 4.1HTML格式的富文本显示
```
//示例
@Entry
@Component
struct RichTextExample {
  @State data: string = '<h1 style='text-align: center;'>h1标题</h1>
                        <h1 style='text-align: center;'><i>h1斜体</i></h1>
                        <h1 style='text-align: center;'><u>h1下划线</u></h1>
                        <h2 style='text-align: center;'>h2标题</h2>
                        <h3 style='text-align: center;'>h3标题</h3>
                        <p style='text-align: center;'>p常规</p><hr/>
                        <div style='width: 500px;height: 500px;border: 1px solid;margin: 0auto;'>
                        <p style='font-size: 35px;text-align: center;font-weight: bold; color: rgb(24,78,228)'>字体大小35px,行高45px</p>
                        <p style='background-color: #e5e5e5;line-height: 45px;font-size: 35px;text-indent: 2em;'>
                        这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字
                        这是一段文字</p></div>'

  build() {
    Flex({direction: FlexDirection.Column,alignItems: ItemAlign.Center,
    justifyContent: FlexAlign.Center }){
      RichText(this.data)
      .onStart(()=>{
        console.info("RichText onStart")
      })
      .onComplete(()=>{
        console.info("RichText onComplete")
      })
    }
  }
}
  上述代码，在构造中传入data属性值，data为字符串类型，类型为string，可选值为HTML格式的文本类型，
用来显示HTML中对应的文本。
```

##### 设计思路：
##### 1.前端传入一个html格式的字符串。不支持默认值的展示。
##### 2.通过前端传入的html格式的字符串，创建RichTextComponent以及WebComponent，设置WebComponent的属性，利用Web能力去支持富文本的显示。
###### (具体逻辑待开发完毕补充)

### 4.2关键流程
```
以上流程描述的是当前用户设置HTML格式的字符串后，从RichTextComponent构造并显示对应的时序图，关键流程主要在调用web能力去支持富文本的显示。
```
```
sequenceDiagram
    RichTextComponent->>Webcomponent: Constructor()
    WebComponent->>RenderNode: CreateRenderNode()
    WebComponent->>Element: CreateElement()
    WebComponent->>WebController: LoadDataWithBaseUrlImpl()
```

## 5.文件结构
```
ace_engine/frameworks/bridge
|-- declarative_fronted
    |-- BUILD.gn
    |-- engine/jsi
        |-- jsi_view_register.cpp
    |-- engine/quickjs
        |-- qis_view_register.cpp
    |-- engine/v8
        |-- v8_view_register.cpp
    |-- jsview
        |-- js_richtext.cpp
        |-- js_richtext.h
ace_engine/frameworks/core
    |-- BUILD.gn
    |-- components_v2/richtext
        |-- BUILD.gn
        |-- render_rich_text.cpp
        |-- render_rich_text.h
        |-- rich_text_component.cpp
        |-- rich_text_component.h
        |-- rich_text_element.cpp
        |-- rich_text_element.h
```

## 6.伪代码示例
```
@Entry
@Component
struct RichTextExample {
  @State data: string = '<h1 style='text-align: center;'>h1标题</h1>
                        <h1 style='text-align: center;'><i>h1斜体</i></h1>
                        <h1 style='text-align: center;'><u>h1下划线</u></h1>
                        <h2 style='text-align: center;'>h2标题</h2>
                        <h3 style='text-align: center;'>h3标题</h3>
                        <p style='text-align: center;'>p常规</p><hr/>
                        <div style='width: 500px;height: 500px;border: 1px solid;margin: 0auto;'>
                        <p style='font-size: 35px;text-align: center;font-weight: bold; color: rgb(24,78,228)'>字体大小35px,行高45px</p>
                        <p style='background-color: #e5e5e5;line-height: 45px;font-size: 35px;text-indent: 2em;'>
                        这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字这是一段文字
                        这是一段文字</p></div>'

  build() {
    Flex({direction: FlexDirection.Column,alignItems: ItemAlign.Center,
    justifyContent: FlexAlign.Center }){
      RichText(this.data)
      .onStart(()=>{
        console.info("RichText onStart")
      })
      .onComplete(()=>{
        console.info("RichText onComplete")
      })
    }
  }
}
```

