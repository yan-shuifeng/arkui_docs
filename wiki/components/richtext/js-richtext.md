# RichText模块设计<a name="ZH-CN_TOPIC_0000001237355048"></a>

## 1、模块功能描述<a name="section06615514119"></a>

本模块设计文档描述Ace子系统中的RichText组件的基本功能属性，主要用于支持html格式富文本的信息显示。

### 1.1模块构架图<a name="section877361219110"></a>

>![](figure/richtext1.png)

### 1.2 模块效果图<a name="section877361219110"></a>

>![](figure/richtext.png)

## 2、模块的对外接口<a name="section877361219116"></a>

### 子组件<a name="section877361219110"></a>

不包含子组件。

### 属性<a name="section877361219110"></a>

仅支持通用属性中的id、style和class属性。

### 样式<a name="section877361219110"></a>

仅支持通用样式中的display和visibility属性。

### 事件方法<a name="section877361219110"></a>

除了支持通用事件，还支持如下事件：
| 名称 | 参数类型 | 描述 | 
| -------- | -------- | -------- | 
| start | - | 开始加载时触发。 | 
| complete | - | 加载完成时触发。 |

## 3、模块结构<a name="section11682122941119"></a>

```mermaid
classDiagram
    RichtextComponent ..> WebComponent
    class RichTextComponent {
        -RefPtr<WebComponent> webComponent_
        -string data
        +CreateRenderNode() RenderNode
        +CreateElement() Element
        +SetData() void
        +GetData() string
        +GetController() WebController
        +SetPageStartEventId() void
        +SetPageFinishEventId() void
    }
    class WebComponent {
        -RefPtr<WebDeclaration> declaration_
        -CreateCallback createCallback_
        -ReleasedCallback releasedCallback_
        -ErrorCallback errorCallback_
        -RefPtr<WebDelegate> delegate_
        -RefPtr<WebController> webController_
        -string type_
        -bool isJsEnable_
        -bool isContenAccessEnabled_
        -bool isFileAccessEnabled_
        +createRenderNode() RenderNode
        +createElement() Element
        +SetType() void
        +GetType() void
        +SetSrc() void
        +GetSrc() void
        +SetPageStartedEventId() void
        +GetPageStartedEventId() EventMarker
        +SetPageFinishedEventId() void
        +GetPageFinishedEventId() EventMarker
        +SetRequestFocusEventId() void
        +GetRequestFocusEventId() EventMarker
        +SetPageErrorEventId() void
        +GetPageErrorEventId() EventMarker
        +SetMessageEventId() void
        +GetMessageEventId() EventMarker
        +SetDeclaration() void
        +GetController() WebCont
        +SetWebController() void
        +GetJsEnabled() bool
        +SetJsEnabled() bool
        +GetContenAccessEnabled() bool
        +SetContenAccessEnabled() bool
        +GetFileAccessEnable() bool
        +setFileAccessEnable() bool 
    }
    RenderRichText ..>RenderNode
    RenderRichText ..>RichTextComponent
    class RenderRichText {
        Create() RenderNode
        update() void
        PerformLayout() void
        OnPaintFinish() void
    }
    class RichTextElement {
        +PerformBuild() void
    }
```

### 组件相关类的介绍：<a name="section877361219110"></a>

WebComponent: 表示富文本组件的Web页面，包含展示Web网页所需的相关属性。

RichTextComponent: RichText组件的具体实现类，通过拼接Web部分实现RichText展示网页的功能。这种拼接的设计保证了较好的扩展性。

### 流程相关类：<a name="section877361219110"></a>
RichTextComponent：保存richtext相关数据以及生成element和renderNode。

RenderRichText: 作为element的衔接，主要负责richtext的更新，布局，也负责处理richtext的开始加载触发事件和结束加载的触发事件。

Update的方法中会初始化富文本已经更新data的数据。

## 4、关键流程<a name="section87414715115"></a>

### 4.1关键流程实现<a name="section877361219110"></a>

#### 4.1.1显示Web网页的实现<a name="section877361119110"></a>

```
//示例demo
<div style="flex-direction: column;width: 100%;">
   <richtext @start="onLoadStart" @complete="onLoadEnd">{{content}}</richtext>
</div>
export default {
    data:{
       content: `
       <div class="flex-direction:column;background-color: #ffffff;padding: 30px;margin-bottom: 30px;"  style="background-color: #FFFFFF">
          <style>h1{color:yellow;}</style>
          <p class="item-title">h1</p>
          <h1>文本测试(h1测试)</h1>
          <p class="item-title">h2</p>
          <h2>文本测试(h2测试)</h2>
       </div>
        `,
    },
  onLoadStart(){
     console.error("start load rich text.")
  },
  onLoadEnd(){
     console.error("end load rich text.")
  }
}
```

上述代码，在构造中传入data属性值， data为html格式的string类型的数据，其中class和style,代表通用类型的相关设置。之后在标题元素h1中，style是设置的标题元素的颜色属性，为黄色值；标题内容是“文本测试(h1测试)”；在标题元素h2中，标题内容是“文本测试(h2测试)”.上述代码中，设置Start和End两种事件，分别在页面加载开始和结束时触发，触发时显示各自不同的文本信息。

#### 设计思路： <a name="section877361119110"></a>

#### 1.前端传入包含html格式信息的data数据。 <a name="section877361119110"></a>

#### 2.若前端设置data值，通过RichTextComponent传入，在Web组件WebComponent中获得WebController类型的变量，在web_component.h文件的WebController类的LoadDataWithBaseUrl方法中，传入参数data设置web显示信息。

### 4.2关键流程<a name="section877361219110"></a>
 
以下流程描述的是当用户设置html数据data后，从RichTextComponent构造开始的时序图，关键流程主要在html文本信息的解析，将解析后的数据传入render层进行布局计算，布局计算完毕后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    RichTextComponent->>Webcomponent: Constructor()
    WebComponent->>RenderWeb: Create()
    WebComponent->>WebController: Constructor()
    PipelineContext->>RenderNode: OnLayout()
    RenderNode->>RenderWeb:PerformLayout()

    Note over PipelineContext:Calculate the width,height and layout information of horizontal or vertical tracks 

    PipelineContext->>RosenRenderContext:Repaint()
    RosenRenderContext->>RenderNode:Paint()
    RenderNode->>RenderWeb:Paint()
    RenderWeb->>RosenRenderWeb:Paint()
```   

## 5、文件结构<a name="section87414715115"></a>

```
ace_engine/framworks/core/components_v2
├──rich_text
   ├──rich_text_component.cpp
   ├──rich_text_component.h
   ├──rendre_rich_text.cpp
   ├──rendre_rich_text.h
   ├──rich_text_element.cpp
   ├──rich_text_element.h
   ├──BUILD.gn

ace_engine/framworks/core/components
├──web 
   ├──resource 
      ├──web_client_impl.cpp
      ├──web_client_impl.h
      ├──web_delegate.cpp
      ├──web_delegate.h
      ├──web_resource.cpp
      ├──web_resource.h
   ├──flutter_render_web.cpp
   ├──flutter_render_web.h
   ├──render_web_creator.cpp
   ├──render_web.cpp
   ├──render_web.h
   ├──rosen_render_web.cpp
   ├──rosen_render_web.h
   ├──web_component.cpp
   ├──web_component.h
   ├──web_element.cpp
   ├──web_element.h
   ├──web_event.h
   ├──BUILD.gn
```
## 6、伪代码示例<a name="section87414715115"></a>
```
<div style="flex-direction: column;width: 100%;">
   <richtext @start="onLoadStart" @complete="onLoadEnd">{{content}}</richtext>
</div>
export default {
    data:{
       content: `
       <div class="flex-direction:column;background-color: #ffffff;padding: 30px;margin-bottom: 30px;"  style="background-color: #FFFFFF">
          <style>h1{color:yellow;}</style>
          <p class="item-title">h1</p>
          <h1>文本测试(h1测试)</h1>
          <p class="item-title">h2</p>
          <h2>文本测试(h2测试)</h2>
       </div>
        `,
    },
  onLoadStart(){
     console.error("start load rich text.")
  },
  onLoadEnd(){
     console.error("end load rich text.")
  }
}
```