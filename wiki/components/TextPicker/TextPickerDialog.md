

#                     TextPicker模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中TextPicker组件的增强属性，通过弹窗的方式创建文本滑动选择器。

### 1.1 模块架构图

![picker](figures/picker.png)


### 1.2 模块效果图

![textpicker_inline](figures/textpicker_inline.jpg)



![textpicker_popup](figures/textpicker_popup.jpg)



## 2、模块的对外接口

#### 接口说明

static show(options?: TextPickerDialogOption, callback?: (value: TextPickerDialogResult) => void)

**表1** TextPickerDialogOption结构体说明：

| 属性名称                | 参数类型        | 默认值 | 参数描述                                                     |
| ----------------------- | --------------- | ------ | ------------------------------------------------------------ |
| range                   | string[]        |        | 指定选择器的数据选择范围;                                    |
| value                   | string          |        | 指定TextPicker组件需要展示的文本内容；缺省使用选中项的文本值； |
| selected                | number          |        | 指定选中项在数组中的index值，缺省使用第一个元素；            |
| style                   | TextPickerStyle |        | 选择器的样式，缺省使用TextPickerStyle.INLINE.                |
| defaultPickerItemHeight | number          |        | 默认Picker内容项元素高度。                                   |

**表2** TextPickerStyle枚举说明：

| 名称   | 描述                             |
| ------ | -------------------------------- |
| Inline | 选择器采用内嵌方式展示           |
| Block  | 选择器采用内嵌横向块状方式展示   |
| Fade   | 选择器采用上下渐变消失的方式展示 |

**表3** TextPickerDialogResult结构体说明：

| 名称   | 参数类型     | 参数说明           |
| ------ | ------------ | ------------------ |
| value  | string       | 当前选择项的值；   |
| index  | number       | 当前选择项的下标； |
| status | DialogStatus | 操作状态。         |

**表4** DialogStatus枚举说明：

| 名称   | 描述                     |
| ------ | ------------------------ |
| Accept | 点击弹窗中确认按钮状态； |
| Cancel | 点击弹窗中取消按钮状态； |
| Update | 互动选择器状态；         |

#### 子组件

不包含子组件

## 3、模块结构

```mermaid
classDiagram
      class PickerBaseComponent{
          +GetIsCreateDialogComponent() bool
          +SetIsCreateDialogComponent() void
          +OpenDialog() void
          +CloseDialog() void
          -bool isCreateDialogComponent_
      }
    
      RenderNode <|-- RenderPickerBase
      RenderPickerBase ..> PickerBaseComponent
      
      class RenderPickerBase {
          -SetButtonHandler() void
          -HandleFinish() void
          -OnDialogStatusChange() void
      }
```

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 增加PickerStyle枚举

```
// 示例demo
@Entry
@Component
struct Example {
  @State  select: number = 0
  private fruits: string[] = ['apple1', 'orange2', 'peach3', 'grape4']
  @State pickerItemHeight: number = 250;
  textPickerDialogResult = function(value: TextPickerDialogResult) {
    console.info("TextPicker::dialogResult is" + JSON.stringify(value))
  }
  build() {
    Column() {
      Button("TextPickerDialog").onClick(() => {
        TextPickerDialog.show({range: this.fruits, value: `choose fruit: ${this.fruits[this.select]}`, selected: this.select, defaultPickerItemHeight: this.pickerItemHeight}, this.textPickerDialogResult)
      })
    }
  }
}
```

##### 设计思路：

1.通过自定义的button回调执行show方法；

2.通过原有的功能创建出选择器，再将选择器挂在dialogComponent创建的窗口上；

### 4.2 关键流程

```mermaid
sequenceDiagram
    JSTextPickerDialog->>PickerTextComponent:Constructor()

    PickerTextComponent->>PickerColumnComponent:Constructor()
    PickerColumnComponent->>RenderPickerColumn:Create()
    RenderNode->>RenderPickerColumn:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderPickerColumn:Paint()
    RenderPickerColumn->>FlutterRenderPickerColumn:Paint()

    PickerTextComponent->>RenderPickerBase:Create()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderPickerBase:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderPickerBase:Paint()
    RenderPickerBase->>FlutterRenderPickerBase:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── textpicker
   ├── flutter_render_picker_base.cpp
   ├── flutter_render_picker_base.h
   ├── flutter_render_picker_column.cpp
   ├── flutter_render_picker_column.h
   ├── picker_base_component.cpp
   ├── picker_base_component.h
   ├── picker_base_element.cpp
   ├── picker_base_element.h
   ├── picker_column_component.cpp
   ├── picker_column_component.h
   ├── picker_column_element.cpp
   ├── picker_column_element.h
   ├── picker_text_component.cpp
   ├── picker_text_component.h
   ├── render_picker_base.cpp
   ├── render_picker_base.h
   ├── render_picker_column.cpp
   ├── render_picker_column.h
   ├── render_picker_option.cpp
   ├── render_picker_option.h
   ├── picker_theme.h
   ├── BUILD.gn
```

