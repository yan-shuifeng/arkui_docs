# TextTimer模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中TextTimer组件的基本功能属性，主要用于计数或倒计时。

### 1.1 模块架构图

![texttimer](figures/texttimer.png)

### 1.2 模块效果图

![texttimer_miaobiao](figures/texttimer_miaobiao.jpg)

![texttimer_jishiqi](figures/texttimer_jishiqi.jpg)


## 2、模块的对外接口

#### 接口说明

```
TextTimer({isCountDown: this.isCountDown, count: this.inputTime, controller: this.myTimerController})
```

| 参数        | 参数类型            | 默认值 | 参数描述                                                     |
| ----------- | ------------------- | ------ | ------------------------------------------------------------ |
| isCountDown | boolean             | false  | 是否倒计时                                                   |
| count       | number              | 60000  | isCountDown为true时，count值<=0则使用默认值，值>0则为计时器初始值；isCountDown为false时，count值<=0表示不限制，值>0则为计时上限。单位毫秒。 |
| controller  | TextTimerController |        | api控制器                                                    |

TextTimerController属性说明：

| 控制器事件名称 | 描述 |
| -------------- | ---- |
| start          | 开始 |
| pause          | 暂停 |
| reset          | 重置 |

#### 子组件

不包含子组件

#### 属性方法

| 名称      | 参数类型      | 默认值          | 描述     |
| --------- | ------------- | --------------- | -------- |
| format    | string        | 'hh：mm：ss.ms' | 时间格式 |
| fontColor | ResourceColor |                 | 字体颜色 |
| fontSize  | Length        |                 | 字体大小 |

#### 事件方法

| 名称    | 参数类型                                   | 描述                                                         |
| ------- | ------------------------------------------ | ------------------------------------------------------------ |
| onTimer | (utc: number, elapsedTime: number) => void | 时间文本发生变化时触发；utc为当前时间，elapsedTime为计时器跑过的时间；单位都是毫秒。 |

## 3、模块结构

```mermaid
classDiagram
	  RenderNode <|-- RenderTextTimer
      RenderTextTimer <|-- FlutterRenderTextTimer
      RenderTextTimer <|-- RosenRenderTextTimer
      RenderTextTimer ..> TextTimerComponent
      TextTimerElement ..> TextTimerComponent
      class TextTimerComponent{
          +CreateRenderNode() RenderNode
          +CreateElement() Element
          +Initialize() void
          +GetInputCount() double
          +SetInputCount() void
          +GetIsCountDown() bool
          +SetIsCountDown() void
          +GetFormat() const std::string&
          +SetFormat() void
          +GetOnTimer() const std::unique_ptr<std::function<func>>&
          +SetOnTimer() void
          
          -RefPtr<TextComponent> textComponent_
          -RefPtr<TextTimerController> textTimerController_
          -RefPtr<TextTimerDeclaration> declaration_
      }
      class RenderTextTimer {
       	  +Update() void
          +PerformLayout() void
          +OnPaintFinish() void
          +UpdateRenders() void
          +Tick() void
          +UpdateValue() void
          
          -HandleStart() void
          -HandlePause() void
          -HandleReset() void
          -RefPtr<TextComponent> textComponent_
          -RefPtr<RenderText> renderText_
          -RefPtr<Scheduler> scheduler_
      }
      class FlutterRenderTextTimer {
          +Paint() void
      }
      class RosenRenderTextTimer {
          +Paint() void
      }
      class TextTimerElement {
          +PerformBuild() void
      }

```

**组件相关类介绍：**

TextTimerComponent：保存texttimer相关数据和事件以及生成`element`和`renderNode`；

TextTimerElement：构建绘制组件需要的render；

RenderTextTimer：绘制与更新布局，绑定事件；

FlutterRenderTextTimer：绘制；

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 秒表

```
// 示例demo
@Entry
@Component
struct Example {
  @State isCountDown: boolean = false;
  @State inputTime: number = -1;
  myTimerController: TextTimerController = new TextTimerController();
  @State format: string = 'hh：mm：ss.ms';
  @State textSize: number = 50
  build() {
    Column() {
      TextTimer({isCountDown: this.isCountDown, count: this.inputTime, controller: this.myTimerController})
        .format(this.format)
        .fontColor(Color.Black)
        .fontSize(this.textSize)
        .onTimer((utc: number, elapsedTime: number) => {
          console.info('textTimer notCountDown utc is：' + utc + ', elapsedTime: ' + elapsedTime)
        })
      Row() {
        Button("start").onClick(() => {
          this.myTimerController.start();
        });
        Button("pause").onClick(() => {
          this.myTimerController.pause();
        });
        Button("reset").onClick(() => {
          this.myTimerController.reset();
        });
      }
    }
  }
}
```

**设计思路**：

1. isCountDown：是否倒计时，默认false。为false时，构造秒表组件；count为秒表上限，默认60000，设置负值则不限制上限；controller提供控制秒表的事件。
2. format可以设置时间样式，支持毫秒。默认'hh：mm：ss.ms'。
3. onTimer返回两个参数；utc是当前时间的时间戳，elapsedTime是秒表跑过的时间。

#### 4.1.2 计时器

```
// 示例demo
@Entry
@Component
struct Example {
  @State isCountDown: boolean = true;
  @State inputTime: number = 60000;
  myTimerController: TextTimerController = new TextTimerController();
  @State format: string = 'hh：mm：ss.ms';
  @State textSize: number = 50
  build() {
    Column() {
      TextTimer({isCountDown: this.isCountDown, count: this.inputTime, controller: this.myTimerController})
        .format(this.format)
        .fontColor(Color.Black)
        .fontSize(this.textSize)
        .onTimer((utc: number, elapsedTime: number) => {
          console.info('textTimer notCountDown utc is：' + utc + ', elapsedTime: ' + elapsedTime)
        })
      Row() {
        Button("start").onClick(() => {
          this.myTimerController.start();
        });
        Button("pause").onClick(() => {
          this.myTimerController.pause();
        });
        Button("reset").onClick(() => {
          this.myTimerController.reset();
        });
      }
    }
  }
}

```

##### 设计思路：

1. isCountDown：是否倒计时，默认false。为true时，构造计时器组件；count为初始时间，默认60000；controller提供控制计时的事件。
2. format可以设置时间样式，支持毫秒。默认'hh：mm：ss.ms'。
3. onTimer返回两个参数；utc是当前时间的时间戳，elapsedTime是计时器跑过的时间。

### 4.2 关键流程

```mermaid
sequenceDiagram
    TextTimerComponent->>RenderTextTimer:Create()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderTextTimer:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderPickerBase:Paint()
    RenderTextTimer->>FlutterRenderTextTimer:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── texttimer
   ├── flutter_render_texttimer.cpp
   ├── flutter_render_texttimer.h
   ├── render_texttimer_creator.cpp
   ├── render_texttimer.cpp
   ├── render_texttimer.h
   ├── rosen_render_texttimer.cpp
   ├── rosen_render_texttimer.h
   ├── texttimer_component.cpp
   ├── texttimer_component.h
   ├── texttimer_element.h
   ├── texttimer_theme.h
   ├── BUILD.gn
```
