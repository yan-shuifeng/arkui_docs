# Lottie对接模块设计



## 1、模块功能描述

Lottie是三方开源库，基于标准canvas API实现动画的2D绘制。

Lottie将动画资源以文件路径或json字符串形式为输入，转换为canvas api的调用，再组合Animator能力，实现动画播放。

开发框架复用lottie接口能力，完善并适当扩展标准canvas API来对接lottie，实现在类声明范式UI与类Web范式上的2D绘制。

Lottie对接现有的RequestAnimationFrame接口或Animator接口，实现动画帧播放。

## 2、模块的对外接口

#### 接口说明

Lottie API详见《ArkUI-声明式范式参考》中章节【画布组件】的子章节【Lottie】，或 [index.d.ts](figures\index.d.ts)。在保留源生接口属性规格不变的前提下，在IDE开发层面通过index.d.ts的精简与约束，简化开发者在基本功能上的使用。在API声明描述上进行说明，并通过demo演示代码对所有接口进行功能覆盖。

#### 子组件

不涉及

#### 属性方法

不涉及

#### 事件方法

不涉及

## 3、模块结构

Lottie依赖CanvasRenderingContext2D，涉及内部接口：

| 接口名称    | 参数 | 默认值 | 参数描述                                                     |
| ----------- | ---- | ------ | :----------------------------------------------------------- |
| getJsonData | path | -      | 输入动画资源json文件在hap包中的相对路径，返回文件字符串内容，由Lottie进行json格式化处理。 |

新增属性：

| 名称   | 参数类型 | 默认值 | 描述                                                         |
| ------ | -------- | ------ | :----------------------------------------------------------- |
| width  | Length   | -      | Canvas绘制的宽度，只读属性。Lottie结合动画资源的宽高进行自动适配。 |
| height | Length   | -      | Canvas绘制的高度，只读属性。Lottie结合动画资源的宽高进行自动适配。 |



```c++
classDiagram
      class AssetImageLoader{
          +LoadJsonData()
      }

      // 类Web范式
      class V8CanvasBridge{
          +GetJsonData()
          +WidthGetter()
          +HeightGetter()
      }
      class CanvasBridge{
          +GetJsonData()
          +WidthGetter()
          +HeightGetter()
      }
      class JsiCanvasBridge{
          +GetJsonData()
          +WidthGetter()
          +HeightGetter()
      }

      // 类声明范式
      class JSRenderingContext{
          +JsGetWidth()
          +JsGetHeight()
      }
      class JSCanvasRenderer{
          +JsGetJsonData()
      }
```

Lottie的Canvas绘制模块（CanvasRenderer）依赖的API

| 绘制指令                 | 支持情况 | 绘制指令               | 支持情况 |
| ------------------------ | -------- | ---------------------- | -------- |
| getContext('2d')         | **√**    | lineTo()               | **√**    |
| transform()              | **√**    | font                   | **√**    |
| setTransform()           | **√**    | lineCap                | **√**    |
| globalAlpha              | **√**    | lineJoin               | **√**    |
| restore()                | **√**    | miterLimit             | **√**    |
| save()                   | **√**    | fillStyle              | **√**    |
| beginPath()              | **√**    | fillText()             | **√**    |
| rect()                   | **√**    | lineWidth              | **√**    |
| closePath()              | **√**    | strokeStyle            | **√**    |
| clip()                   | **√**    | strokeText()           | **√**    |
| clearRect()              | **√**    | strokeStyle            | **√**    |
| moveTo()                 | **√**    | setLineDash()          | **√**    |
| bezierCurveTo()          | **√**    | lineDashOffset         | **√**    |
| fill()                   | **√**    | createLinearGradient() | **√**    |
| stroke()                 | **√**    | createRadialGradient() | **√**    |
| drawImage()              | **√**    | fillRect()             | **√**    |
| globalCompositeOperation | **√**    |                        |          |

## 4、关键流程

开发者视角业务流程

```mermaid
sequenceDiagram
	user->>Lottie: loadAnimation()
	Lottie->>CanvasContext: getJsonData('xxx.json')
	Lottie->>Lottie: parse(json)
	Lottie->>CanvasContext: get width/height, set style and so on
	Lottie->>CanvasContext: Animator.onFrame(callBack)/RequestAnimationFrame(callBack)
	CanvasContext->>Lottie: callBack { draw one frame }
	Lottie-->>user: AnimationItem
	user->>AnimationItem: control:pause/toggle/play/stop/setSpeed/setDirection()
```

Lottie动画加载流程

```mermaid
sequenceDiagram
	Lottie->>AnimationManager: loadAnimation(params)
	AnimationManager->>AnimationManager: var animItem = new AnimationItem()
	AnimationManager->>AnimationManager: setupAnimation(animItem, null);
    AnimationManager->>AnimationManager: animItem.addEventListener('_active', addPlayingCount)
    AnimationManager->>AnimationItem: animItem.setParams(params)
    AnimationItem->>AnimationItem: this.renderer = new CanvasRenderer(this, params.rendererSettings)
    AnimationItem->>CanvasRenderer: this.renderer.configAnimation(animData)
    CanvasRenderer->>CanvasRenderer: this.animationItem.container =createTag('canvas')
    CanvasRenderer->>CanvasRenderer: this.updateContainerSize()
    CanvasRenderer->>AnimationItem: return
    AnimationItem->>AnimationItem: this.checkLoaded()
    AnimationItem->>AnimationItem: this.trigger('DOMLoaded')
    AnimationItem->>AnimationItem: this.gotoFrame();
    AnimationItem->>AnimationItem: this.trigger('enterFrame')
    AnimationItem->>CanvasRenderer: this.renderer.renderFrame(this.currentFrame + this.firstFrame)
    AnimationItem->>AnimationItem: this.play()
    AnimationItem->>AnimationManager: return
    AnimationManager->>Lottie: return animItem;
```



## 5、文件结构

```bash
// 类范式api
ace_engine/frameworks/bridge/declarative_frontend/
├── jsview
   ├── js_canvas_renderer.h
   ├── js_canvas_renderer.cpp

// 类web api
ace_engine/frameworks/bridge/js_frontend/engine/
├── jsi
   ├── jsi_canvas_bridge.h
   ├── jsi_canvas_bridge.cpp
├── quickjs
   ├── canvas_bridge.h
   ├── canvas_bridge.cpp
├── v8
   ├── v8_canvas_bridge.h
   ├── v8_canvas_bridge.cpp

// 基础能力api
ace_engine/frameworks/core/
├── components/custom_paint
   ├── custom_paint_component.h
   ├── custom_paint_component.cpp
   ├── flutter_render_custom_paint.h
   ├── flutter_render_custom_paint.cpp
   ├── render_custom_paint.h
├── image
   ├── image_loader.h
   ├── image_loader.cpp
```



## 6、伪代码示例

```typescript
import lottie from 'lottie-web'

@Entry
@Component
struct Index {
  private controller: RenderingContext = new RenderingContext();
  private animateName: string = "grunt";

  private onPageHide(): void {
    console.log('onPageHide');
    lottie.destroy();
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Canvas(this.controller)
      .width('100%')
      .height('100%')
      .backgroundColor('#ff0000')

      Animator('__lottie_ets') // declare Animator('__lottie_ets') when use lottie
      Button('lottie')
        .onClick(() => {
          let anim = lottie.loadAnimation({
          container: this.controller,
          renderer: 'canvas',
          loop: true,
          autoplay: true,
          name: animateName,
          path: "common/lottie/data.json"
        })
      })
      Button('togglePause')
        .onClick(() => {
          lottie.togglePause(this.animateName);
        })
    }
    .width('100%')
    .height('100%')
  }
}
```

