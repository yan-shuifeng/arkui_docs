# Select模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中Select，组件的基本功能属性，主要提供下拉选择按钮，可以让用户在多个选项之间选择。

### 1.1 模块架构图

![Select](figures/1.png)

### 1.2 模块效果图
![Select](figures/2.png)

## 2、模块的对外接口

#### 接口说明

Select（options:Array<SelectOption>)

| 接口名称            | 参数 | 默认值 | 参数描述             |
| ------------------- | ---- | ------ | -------------------- |
| Select | Array<SelectOption>  | -      | 用户可以选择的列表项 |

**`SelectOption`参数说明**

| 接口名称 | 参数        | 默认值 | 参数描述     |
| -------- | ----------- | ------ | ------------ |
| value    | ResourceStr | -      | 下拉选项内容 |
| icon     | ResourceStr | -      | 下拉选项图片 |

#### 子组件

子组件Option

#### 属性方法

| 名称                    | 参数类型      | 默认值 | 描述                              |
| ----------------------- | ------------- | ------ | --------------------------------- |
| selected                | number        | -      | 设置下拉菜单选择项序号，从0开始。 |
| value                   | string        | -      | 设置下拉按钮本身的文本显示。      |
| font                    | Font          | -      | 设置下拉按钮本身的文本属性。      |
| fontColor               | ResourceColor | -      | 设置下拉按钮本身的文本颜色。      |
| selectedOptionBgColor   | ResourceColor | -      | 设置下拉菜单选中项的背景色。      |
| selectedOptionFont      | Font          | -      | 设置下拉菜单选中项的文本样式。    |
| selectedOptionFontColor | ResourceColor | -      | 设置下拉菜单选中项的文本颜色。    |
| optionBgColor           | ResourceColor | -      | 设置下拉菜单项的背景色。          |
| optionFont              | Font          | -      | 设置下拉菜单项的文本样式。        |
| optionFontColor         | ResourceColor | -      | 设置下拉菜单项的文本颜色。        |

#### 事件方法

支持通用事件；

| 名称       | 参数类型                | 描述                       |
| ---------- | ----------------------- | -------------------------- |
| onSelected | (index: number) => void | 下拉菜单选中某一项的回调。 |

## 3、模块结构

```mermaid
classDiagram
      class SelectComponent{
          +AppendSelectOption() void
          +ClearAllOptions() void
          +GetSelectOption() OptionComponent
          +SetOptionClickedCallback() void
          +SetTipText() void
          +GetTipText() TextComponent
          +SetSelectStyle() void
          +GetSelectStyle() TextStyle
          +SetTheme() void
          +CreateElement() Element
          +CreateRenderNode() RenderNode
      }
      RenderNode <|-- RenderSelect
      RenderSelect ..> SelectComponent
      class RenderSelect {
        +Update() void
        +PerformLayout() void
      }
      class SelectElement {
          -function<void(std::size_t)> onSelected_
          +HandleOptionClickedEvent() void
          +HandleClickedEvent() void
          +PerformBuild() void
      }
      SelectComponent ..> SelectElement
      class OptionComponent {
          -Color backgroundColor_
          -Color selectedBackgroundColor_
          -TextStyle textStyle_
          -TextStyle selectedTextStyle_
          +UpdateTextColor() void
          +GetTextStyle() TextStyle
          +SetTextStyle() void
          +GetSelectedTextStyle() TextStyle
          +SetSelectedTextStyle() void
          +GetBackgroundColor() Color
          +SetBackgroundColor() void
          +GetSelectedBackgroundColor() Color
          +SetSelectedBackgroundColor() void
          +CreateElement() Element
          +CreateRenderNode() RenderNode
      }
      RenderNode <|-- RenderOption
      RenderOption ..> OptionComponent
      class RenderOption {
          +Update() void
          +PerformLayout() void
      }
      class SelectPopupComponent {
          +AppendSelectOption() void
          +HandleOptionClick() void
          +CreateElement() Element
          +CreateRenderNode() RenderNode
      }
      class RenderSelectPopup {
          +Update() void
          +PerformLayout() void
      }
      RenderNode <|-- RenderSelectPopup
      RenderSelectPopup ..> SelectPopupComponent

```

**组件相关类介绍：**

**SelectComponent**类：该类是下拉菜单组件类，保存所有下拉菜单属性，其提供的`Initialize`组装内部控件树，`InitializePopup`组装弹出的下拉菜单（其内含有`SelectPopupCompoent`列表），并由它生成相应`Element`和`Render`类。

**SelectElement**类：该类主要封装事件处理逻辑，包含点击颜色动态变化，点击弹出下拉菜单，点击隐藏下拉菜单，当前选项切换处理，和发送变化事件给前端。

**RenderSelect**类：该类主要处理内部控件树的布局逻辑和各种触摸事件的识别和外发路由给Element类处理。

**SelectPopupComponent**类：该类附属于`SelectComponent`类，为其提供下拉菜单的一个具体选项(含有选项的所有属性)，其提供的`Initialize`方法组装内部控件树。

**SelectPopupElement**类：该类主要封装选项相关的事件处理逻辑，包含选项选中态颜色动态切换，选项点击态颜色动态切换，当前选项改变时候反选所有其他选项。

**RenderSelectPopup**类：该类主要处理选项的内部控件树布局和各类事件的识别外发路由，另外内部保存`SelectPopupComponent`为`Element`处理事件提供数据。

**OptionComponent**类：该类为选项类，提供下拉菜单的一个具体选项(含有选项的所有属性)。

**RenderOption**类：该类用于更新下拉菜单的具体选项属性。

## 4、关键流程

以下流程描述的是当用户设置水平或者竖直的枚举后，从`SelectComponent`构造并设置`value`开始的时序图，关键流程主要在当轨道的布局计算，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    SelectComponent->>RenderSelect: Constructor()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderSelect:PerformLayout()

    OptionComponent->>RenderOption: Constructor()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderOption:PerformLayout()

    SelectPopupComponent->>RenderSelectPopup: Constructor()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderSelectPopup:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderSelectPopup:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── option
    ├── flutter_render_option.cpp
    ├── flutter_render_option.h
    ├── rosen_render_option.cpp
    ├── rosen_render_option.h
    ├── render_option_creator.cpp
    ├── render_option.cpp
    ├── render_option.h
    ├── option_component.cpp
    ├── option_component.h
    ├── option_element.cpp
    ├── option_element.h
  ├── BUILD.gn
├── select
    ├── render_select.cpp
    ├── render_select.h
    ├── select_component.cpp
    ├── select_component.h
    ├── select_element.cpp
    ├── select_element.h
    ├── select_theme.h
    ├── BUILD.gn
├── select_popup
    ├── flutter_render_select_popup.cpp
    ├── flutter_render_select_popup.h
    ├── rosen_render_select_popup.cpp
    ├── rosen_render_select_popup.h
    ├── render_select_popup_creator.cpp
    ├── render_select_popup.cpp
    ├── render_select_popup.h
    ├── select_popup_component.cpp
    ├── select_popup_component.h
    ├── select_popup_element.cpp
    ├── select_popup_element.h
  ├── BUILD.gn
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct SliderExample {
  @State index: number = 0
  build() {
    Column() {

      Select([{value:'aaa',icon: $r("app.media.1")},
              {value:'bbb',icon: $r("app.media.2")},
              {value:'ccc'},
              {value:'ddd',icon: $r("app.media.3")},
              {value:'eee'},
              {value:'fff',icon: $r("app.media.4")}
      ])
        .selected(2)
        .value('123')
        .font({size: 30, weight: 50, family: 'serif', style: FontStyle.Normal })
        .fontColor(Color.Red)
        .selectedOptionFont({size: 70, weight: 30, family: 'serif', style: FontStyle.Normal })
        .selectedOptionFontColor(Color.Brown)
        .optionBgColor(Color.Pink)
        .optionFont({size: 50, weight: 10, family: 'serif', style: FontStyle.Italic })
        .optionFontColor(Color.Orange)
        .selectedOptionBgColor(Color.Yellow)
        .onSelected((index:number)=>{
          console.info("Select:" + index)
        })
    }
    .height(600)
    .width(800)
    .backgroundColor(Color.Pink)
  }
}
```

