# TextInput、Search、TextArea模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中的TextInput、Search和TextArea组件，组件的基本功能属性是提供单行文本输入，二期开发主要是在原本的组件中增加设置输入光标的位置功能。

### 1.1模块架构图

![textinput构架图](figures/input1.png)

### 1.2模块效果图

![textinput和area效果图](figures/input.png)

![textinput和area效果图](figures/input2.gif)

## 2、模块的对外接口

### 接口说明

Search(value?: string,placeholder?: string,icon? string, controller?: SearchController);

| 参数名称    | 参数类型         | 默认值 | 参数描述                                                     |
| ----------- | ---------------- | ------ | ------------------------------------------------------------ |
| value       | string           | -      | Search组件搜索文本;                                          |
| placeholder | string           | -      | 无输入时显示文本;                                            |
| icon        | string           | -      | 搜索图标路径，默认使用系统搜索图标，图标格式为svg，jpg和png; |
| controller  | SearchController | -      | 设置Search控制器;                                            |

TextInput(placeholder?: string,text?: string, controller?: TextInputController);

| 参数名称    | 参数类型            | 默认值 | 参数描述             |
| ----------- | ------------------- | ------ | -------------------- |
| placeholder | string              | -      | 无输入时显示文本;    |
| text        | string              | -      | 输入内容;            |
| controller  | TextInputController | -      | 设置TextInput控制器; |

TextArea(placeholder?: string,text?: string, controller?: TextAreaController);

| 参数名称    | 参数类型           | 默认值 | 参数描述            |
| ----------- | ------------------ | ------ | ------------------- |
| placeholder | string             | -      | 无输入时显示文本;   |
| text        | string             | -      | 输入内容;           |
| controller  | TextAreaController | -      | 设置TextArea控制器; |

### 子组件

不包含子组件

### 属性方法

| 名称             | 参数类型                                                     | 默认值 | 描述                                                         |
| ---------------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| type             | InputType                                                    | Normal | 可选值：<br>InputType.Normal: 基本输入模式<br>InputType.Password: 密码输入模式<br>InputType.Email: e-mail地址输入模式<br>InputType.Number: 纯数字输入模式 |
| placeholderColor | Color                                                        | -      | 设置placeholder颜色                                          |
| placeholderFont  | {size: number, weight: FontWeight, family: string, style: FontStyle} | -      | 设置placeholder文本                                          |
| enterKeyType     | value: EnterKeyType                                          | Done   | 设置输入法回车键类型：<br>EnterKeyType.Go: 显示Go文本<br>EnterKeyType.Search: 显示为搜索样式<br>EnterKeyType.Send: 显示为发送样式<br>EnterKeyType.Next: 显示为下一个样式<br>EnterKeyType.Done: 标准样式 |
| caretColor       | Color                                                        | -      | 设置输入框光标颜色                                           |

#### SearchController,TextInputController,TextAreaController

SearchController,TextInputController,TextAreaController组件的控制器，可以将此对象绑定至对应的SearchController,TextInputController,TextAreaController组件，然后通过它控制光标位置。

| 名称                              | 描述               |
| --------------------------------- | ------------------ |
| caretPosition(value: number):void | 设置输入光标的位置 |

### 事件方法

| 名称          | 参数类型                                            | 描述                                                         |
| ------------- | --------------------------------------------------- | ------------------------------------------------------------ |
| onEditChanged | (isEditing: boolean) => void                        | 输入状态变化时触发回调                                       |
| onSubmit      | (enterKey: EnterKeyType,adaptation:boolean) => void | 回车键或者软键盘回车键触发该回调，参数为当前软键盘回车键类型,adaptation匹配当前的结果，boolean类型 |
| onChange      | (value: string) => void                             | 输入发生变化时，触发回调onChange                             |
| onCopy        | onCopy(callback:(value: string) => void)            | 组件触发系统剪切板复制操作。                                 |
| onCut         | onCut(callback:(value: string) => void)             | 组件触发系统剪切板剪切操作。                                 |
| onPaste       | onPaste(callback:(value: string) => void)           | 组件触发系统剪切板粘贴操作。                                 |

**ClipboardData**

### 剪贴板类描述

| 名称      | 参数类型                   | 返回类型 | 描述                 |
| --------- | -------------------------- | -------- | -------------------- |
| setData   | type: string, data: string | void     | 写入剪贴板数据。     |
| getData   | type: string               | string   | 读取剪贴板文本数据。 |
| clearData | type: string               | void     | 清除剪贴板数据。     |

## 3、模块结构

```mermaid
classDiagram
    TextFieldComponent ..> TextFieldController
    class TextFieldComponent{
        +GetValue() string
        +SetValue() void
        +GetPlaceholder() string
        +GetPlaceholderColor() Color
        +SetPlaceholderColor() void
        +GetTextStyle() TextStyle
        +SetTextStyle() void
        +GetTextInputType() TextInputType
        +SetTextInputType() void
        +SetCursorColor() void
        +GetCursorColor() Color
        +SetCursorRadius()  void
        +GetCursorRadius() Dimension
        +ShowPasswordIcon() bool
        +SetShowPasswordIcon() void
        +GetIconImage() string
        +SetIconImage() void
        +GetShowIconImage() string
        +SetShowIconImage() void
        +GetHideIconImage() string
        +SetHideIconImage() void
        +GetIconSize() Dimension
        +SetIconSize() void
        +GetIconHotZoneSize() Dimension
        +SetIconHotZoneSize() void
        +GetTextFieldController() TextFieldController
        +SetTextFieldController() void
        +SetFocusPlaceholderColor() void
        +GetFocusPlaceholderColor() Color
        +SetFocusTextColor() void
        +GetFocusTextColor() Color
        +SetTextColor() void
        +GetTextColor() Color
        +GetSelectedColor() Color
        +SetSelectedColor() void
    }
    class TextFieldController{
        +SetHandler() void
        +Focus() void
        +ShowError() void
        +Delete() void
        +CaretPosition() void
        +SetCaretPosition() void
        -WeakPtr<Element> element_
        -std::function<void(const int32_t)> setCaretPosition_
    }
    RenderNode <|-- RenderTextField
    RenderTextField <|-- FlutterRenderTextField
    RenderTextField ..> TextFieldComponent
    TextFieldElement-->TextFieldComponent
    RenderTextField..>TextFieldController
    class RenderTextField{
        +Update() void
        +PerformLayout() void
        +UpdateEditingValue() void
        +PerformAction() void
        +OnStatusChanged() void
        +OnValueChanged() void
        +OnPaintFinish() void
        +OnKeyEvent() bool
        +RequestKeyboard() bool
        +CloseKeyboard() bool
        +ShowError() void
        +ShowTextOverlay() void
        +SetOnKeyboardClose() void
        +SetOnClipRectChanged() void
        +SetUpdateHandlePosition() void
        +SetUpdateHandleDiameter() void
        +SetUpdateHandleDiameterInner() void
        +SetIsOverlayShowed() void
        +UpdateFocusAnimation() void
        +GetEditingValue() TextEditingValue
        +GetPreEditingValue() TextEditingValue
        +Delete() void
        +SetTextStyle() void
    }
    class FlutterRenderTextField{
        +Paint() void
        +GetRenderLayer() RenderLayer
        +GetCaretRect() Rect
        +GetStartRect() Rect
        -UpdateCaretProto() void
        -ComputeOffsetForCaretUpstream() bool
        -ComputeOffsetForCaretDownstream() bool
        -ComputeOffsetForCaretCloserToClick() bool
        -MakeEmptyOffset() Offset
        -Measure() Size
        -MeasureParagraph() double
        -ComputeLayoutSize() Size
        -GetInnerRect() Rect
        -GetCaretRect() bool
        -ComputeOffsetAfterLayout() void
        -ComputeVerticalOffsetForCenter() Offset
        -SetShaderIfNeeded() void
        -ResetParagraphIfNeeded() void
        -ComputeExtendHeight() void
        -GetBoundaryOfParagraph() double
        -AdjustCursorAndSelectionForLtr() bool
        -AdjustCursorAndSelectionForRtl() bool
        -NeedAdjustPosition() bool
        -PaintCaret() void
        -PaintDecoration() void
        -PaintSelectCaret() void
        -PaintIcon() void
        -PaintSelection() void
        -PaintTextAndPlaceholder() void
        -PaintErrorText() void
        -PaintCountText() void
        -PaintOverlayForHoverAndPress() void
        -PaintTextField() void
        -Rect caretRect_
        -Rect startCaretRect_
        -Size lastLayoutSize_
    }
    class TextFieldElement{
        +CreateRenderNode() RenderNode
        +Update() void
        +OnFocus() void
        +OnBlur() void
        +OnKeyEvent() bool
        +RequestKeyboard() bool
        +CloseKeyboard() void
        +ShowError() void
        +Delete() void
        -OnSurfaceChanged() void
        -bool enabled_
        -bool editingMode_
        -bool isNextAction_
        -bool isRequestFocus_
        -int32_t callbackId_
    }
```

**组件相关类介绍：**

**TextFieldController**：前端通过`TextFieldController`的`Focus`接口调用到`TextFieldElement`的对应方法`RequestKeyboar`  `LostSelfFocus`从而控制焦点切换与键盘弹出。

**Textfieldcomponent**：`Textfield`组件类，提供`Textfield`组件的对外接口，以及创建`Rendertextfield`类型的RenderNode,前端TextFieldComponent中生成一个TextFieldController。

**Render_text_field**: 根据`Textfieldcomponent`的组件属性信息，给``Textfield`组件以及其子组件做布局，并且处理`Textfield`组件的相关输入。

## 4、关键流程

以下流程描述的是当用户设置number值后，前端设置输入光标的位置的值，触发controller回调，从TextFieldController传入值开始的时序图，关键流程主要在当轨道的布局计算，布局计算完毕以后所有的组件正常绘制即可。

### 4.1 关键流程实现

```
// 示例demo
//TextInput
@Entry
@Component
struct TextInputTest {
    @State text: string = ''
    controller: TextInputController = new TextInputController()
    build() {
        Column() {
            TextInput({ placeholder: '请输入', text: '', controller:this.controller})
                .type(InputType.Normal)
                .placeholderColor(Color.Orange)
                .placeholderFont({ size:40,weight: FontWeight.Normal, style: FontStyle.Normal })
                .enterKeyType(EnterKeyType.Go)
                .caretColor(Color.Blue)
                .height(60)
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
//TextArea
@Entry
@Component
struct TextAreaTest {
    controller: TextAreaController = new TextAreaController()
    build() {
        Column() {
            TextArea({ placeholder: 'input your word', text: '',controller:this.controller })
                .placeholderColor(Color.Orange)
                .placeholderFont({ size: 50, fontFamily:'cursive',weight:FontWeight.Normal,style:FontStyle.Italic})
                .caretColor(Color.Blue)
                .height(80)
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
//Search
@Entry
@Component
struct MySideBarComponent {
    @State searchButton: string = "搜索"
    private controller: SearchController = new SearchController()
    build() {
        Column() {
            Search({value: '', icon: "common/1.png",controller: this.controller })
                .searchButton(this.searchButton)
                .placeholderColor(Color.Yellow)
                .placeholderFont({ size: 28, weight: 100, family: 'serif', style: FontStyle.Italic })
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
```

**设计思路**：

**前端设置输入光标的位置的值，触发controller对象，将值传入到`text_field_controller`再传入到在`render_TextField.cpp`的`UpdateStartSelection`方法中，通过`MarkNeedLayout`实现瞬时触发更新效果**

### 4.2 关键流程时序图

```mermaid
sequenceDiagram
    TextFieldController->>TextFieldComponent:SetTextFieldController()
    TextFieldComponent->>RenderTextField:SetTextFieldController()
    TextFieldController->>RenderTextField:CaretPosition()
    TextFieldController->>RenderTextField:UpdateSelection()
    TextFieldController->>RenderTextField:MarkNeedLayout()
    RenderTextField->>FlutterRenderTextField:Paint()
```

## 5、文件结构

```
foundation/ace/core
    |--- / common
        |--- ime
            |--- constant.h
            |--- text_edit_controller.h
            |--- text_editing_value.cpp
            |--- text_editing_value.h
            |--- text_input.h
            |--- text_input_action.cpp
            |--- text_input_action.h
            |--- text_input_client.h
            |--- text_input_configuration.cpp
            |--- text_input_configuration.h
            |--- text_input_formatter.cpp
            |--- text_input_formatter.cpp
            |--- text_input_proxy.cpp
            |--- text_input_ proxy.h
            |--- text_input_type.cpp
            |--- text_input_type.h
            |--- text_range.h
            |--- text_selection.h
    |--- /components
        |--- text_field_component.cpp
        |--- text_field_component.h
        |--- render_text_field.h
        |--- render_text_field.cpp
    |--- /elements
        |--- text_field_elements.h
    |--- /dsl/json
        |--- text_field_creator.cpp
    |--- /rendering
        |--- common/render_ text_field.cpp
        |--- common/render_ text_field.h
        |--- flutter
            |--- flutter_render_ text_field.cpp
            |--- flutter_render_ text_field.h
```

```
foundation/ace/platform
    |-- android
        |-- java/src/com/Huawei/ace/plugin/editing
            |-- InputConnectionAdaptor.java
            |-- TextEditState.java
            |-- TextInputAction.java
            |-- TextInputConfiguration.java
            |-- TextInputPlugin.java
            |-- TextInputType.java
        |-- jni/editing
            |-- text_input_jni.cpp
            |-- text_input_jni.h
    |-- common/jni/editing
        |-- text_input_client_handler.cpp
        |-- text_input_client_handler.h
        |-- text_input_connection_impl.cpp
        |-- text_input_connection_impl.h
        |-- text_input_plugin.cpp
        |-- text_input_plugin.h
```

## 6、伪代码示例

```typescript
//TextInput
@Entry
@Component
struct TextInputTest {
    @State text: string = ''
    controller: TextInputController = new TextInputController()
    build() {
        Column() {
            TextInput({ placeholder: '请输入', text: '', controller:this.controller})
                .type(InputType.Normal)
                .placeholderColor(Color.Orange)
                .placeholderFont({ size:40,weight: FontWeight.Normal, style: FontStyle.Normal })
                .enterKeyType(EnterKeyType.Go)
                .caretColor(Color.Blue)
                .height(60)
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
//TextArea
@Entry
@Component
struct TextAreaTest {
    controller: TextAreaController = new TextAreaController()
    build() {
        Column() {
            TextArea({ placeholder: 'input your word', text: '',controller:this.controller })
                .placeholderColor(Color.Orange)
                .placeholderFont({ size: 50, fontFamily:'cursive',weight:FontWeight.Normal,style:FontStyle.Italic})
                .caretColor(Color.Blue)
                .height(80)
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
//Search
@Entry
@Component
struct MySideBarComponent {
    @State searchButton: string = "搜索"
    private controller: SearchController = new SearchController()
    build() {
        Column() {
            Search({value: '', icon: "common/1.png",controller: this.controller })
                .searchButton(this.searchButton)
                .placeholderColor(Color.Yellow)
                .placeholderFont({ size: 28, weight: 100, family: 'serif', style: FontStyle.Italic })
            Button('caretPosition')
                .onClick(() => {
                this.controller.caretPosition(4)
            })
        }
    }
}
```





































