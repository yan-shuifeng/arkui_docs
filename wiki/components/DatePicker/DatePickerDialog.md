

#               **DatePickerDialog模块设计**

## 1、模块功能描述

本模块设计文档描述Ace子系统中DatePicker组件的增强属性，通过弹窗的方式创建日期或者时间滑动选择器。

### 1.1 模块架构图

![picker](figures/picker.png)


### 1.2 模块效果图

![datepicker_inline](figures/datepicker_inline.jpg)



![datepicker_popup](figures/datepicker_popup.jpg)



## 2、模块的对外接口

#### 接口说明

static show(options?: DatePickerDialogOption, callback?: (value: DatePickerDialogResult) => void)

**表1** DatePickerDialogOption结构体说明：

| 接口名称        | 参数           | 默认值 | 参数描述                                                     |
| --------------- | -------------- | ------ | ------------------------------------------------------------ |
| start           | Date           |        | 指定日期选择器的起始日期；                                   |
| end             | Date           |        | 指定日期选择器的结束日期；                                   |
| value           | string         |        | 指定日期选择器需要展示的文本内容；缺省使用选中日期时间的文本值； |
| selected        | number         |        | 指定选中项在数组中的index值，缺省使用第一个元素；            |
| type            | DatePickerType |        | 指定日期选择器的类型，包括日期选择器，日期时间选择器和时间选择器，缺省使用日期选择器； |
| lunar           | boolean        | false  | 是否显示农历；                                               |
| useMilitaryTime | boolean        | false  | 是否展示24小时制，默认值会依据系统当前所选地区和语言选择当地习惯的小时制。 |

**表2** DatePickerType枚举说明：

| 名称 | 描述                         |
| ---- | ---------------------------- |
| Date | 日期选择器，展示年月日信息； |
| Time | 时间选择器，展示时间信息；   |

**表3** DatePickerDialogResult结构体说明：

| 名称   | 描述                             |
| ------ | -------------------------------- |
| year   | 日期滑动选择器中当前选择项的年； |
| month  | 日期滑动选择器中当前选择项的月； |
| day    | 日期滑动选择器中当前选择项的日； |
| hour   | 时间滑动选择器中当前选择项的时； |
| minute | 时间滑动选择器中当前选择项的分； |
| second | 时间滑动选择器中当前选择项的秒； |
| status | 操作状态；                       |

**表4** DialogStatus枚举说明：

| 名称   | 描述                     |
| ------ | ------------------------ |
| Accept | 点击弹窗中确认按钮状态； |
| Cancel | 点击弹窗中取消按钮状态； |
| Update | 互动选择器状态；         |

#### 子组件

不包含子组件

## 3、模块结构

```mermaid
classDiagram
      class PickerBaseComponent{
          +GetIsCreateDialogComponent() bool
          +SetIsCreateDialogComponent() void
          +OpenDialog() void
          +CloseDialog() void
          -bool isCreateDialogComponent_
      }
    
      RenderNode <|-- RenderPickerBase
      RenderPickerBase ..> PickerBaseComponent
      
      class RenderPickerBase {
          -SetButtonHandler() void
          -HandleFinish() void
          -OnDialogStatusChange() void
      }
```

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 增加PickerStyle枚举

```
// 示例demo
@Entry
@Component
struct Example {
  @State selectedDate: Date = new Date('2000-1-1')
  datePickerDialogResult = function(value: DatePickerDialogResult) {
    console.info("DatePicker::dialogResult is" + JSON.stringify(value))
  }
  build() {
    Column() {
      Button("DatePickerDialog").onClick(() => {
        DatePickerDialog.show({start: new Date('2000-1-1'), end: new Date('2100-1-1'), selected: this.selectedDate, type: DatePickerType.Time}, this.datePickerDialogResult)
      })
    }
  }
}

```

##### 设计思路：

1.通过自定义的button回调执行show方法；

2.通过原有的功能创建出选择器，再将选择器挂在dialogComponent创建的窗口上；

### 4.2 关键流程

```mermaid
sequenceDiagram
    JSDatePickerDialog->>PickerDateComponent:Constructor()

    PickerDateComponent->>PickerColumnComponent:Constructor()
    PickerColumnComponent->>RenderPickerColumn:Create()
    RenderNode->>RenderPickerColumn:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderPickerColumn:Paint()
    RenderPickerColumn->>FlutterRenderPickerColumn:Paint()

    PickerDateComponent->>RenderPickerBase:Create()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderPickerBase:PerformLayout()
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderPickerBase:Paint()
    RenderPickerBase->>FlutterRenderPickerBase:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── textpicker
   ├── flutter_render_picker_base.cpp
   ├── flutter_render_picker_base.h
   ├── flutter_render_picker_column.cpp
   ├── flutter_render_picker_column.h
   ├── picker_base_component.cpp
   ├── picker_base_component.h
   ├── picker_base_element.cpp
   ├── picker_base_element.h
   ├── picker_column_component.cpp
   ├── picker_column_component.h
   ├── picker_column_element.cpp
   ├── picker_column_element.h
   ├── picker_date_component.cpp
   ├── picker_date_component.h
   ├── picker_time_component.cpp
   ├── picker_time_component.h
   ├── render_picker_base.cpp
   ├── render_picker_base.h
   ├── render_picker_column.cpp
   ├── render_picker_column.h
   ├── render_picker_option.cpp
   ├── render_picker_option.h
   ├── picker_theme.h
   ├── BUILD.gn
```

