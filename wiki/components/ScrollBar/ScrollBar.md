# ScrollBar模块设计



## 1、模块功能描述

介绍滚动条组件ScrollBar的设计实现思路，用于配合可滚动组件使用，如List、Grid、Scroll。

ScrollBar组件负责定义可滚动区域的行为样式，ScrollBar的子节点负责定义滚动条的行为样式。

滚动条组件可以与可滚动组件进行联动：滚动条滚动后，可以使绑定的可滚动组件发生滚动；可滚动组件滚动后，滚动条组件也会发生滚动。

滚动条组件与可滚动组件通过Scroller进行绑定，且只有当两者方向相同时，才能联动，ScrollBar与可滚动组件仅支持一对一绑定。

## 2、模块的对外接口

#### 接口说明

ScrollBar(value: ScrollBarOption)

#### ScrollBarOption

| 接口名称  | 参数               | 必填 | 默认值   | 参数描述                                     |
| --------- | ------------------ | ---- | -------- | -------------------------------------------- |
| scroller  | Scroller           | 是   | -        | 滚动条控制器。                               |
| direction | ScrollBarDirection | 否   | Vertical | 滚动条的方向，控制可滚动组件对应方向的滚动。 |
| state     | BarState           | 否   | Auto     | 滚动条状态。                                 |

**表1** ScrollBarDirection枚举说明：

| 名称       | 描述         |
| ---------- | ------------ |
| Vertical   | 纵向滚动条。 |
| Horizontal | 横向滚动条。 |

**表2** BarState枚举说明：

| 名称 | 描述                                   |
| ---- | -------------------------------------- |
| On   | 常驻显示。                             |
| Off  | 不显示。                               |
| Auto | 按需显示(触摸时显示，无操作2s后消失)。 |

#### 子组件

可以包含单个子组件。

#### 属性方法

支持通用属性方法。

#### 事件方法

支持通用事件方法。

## 3、模块结构

```mermaid
classDiagram
	class ScrollBarProxy{
		-std::vector<ScrollableNodeInfo> scrollableNodes_
		-std::vector<WeakPtr<RenderScrollBar>> scrollBars_
		+RegisterScrollableNode(const ScrollableNodeInfo& scrollableNode)
		+RegisterScrollBar(const WeakPtr<RenderScrollBar>& scrollBar)
		+UnRegisterScrollableNode(const WeakPtr<RenderNode>& scrollableNode)
		+UnRegisterScrollBar(const WeakPtr<RenderScrollBar>& scrollBar)
		+NotifyScrollableNode(double distance, const WeakPtr<RenderScrollBar>& weakScrollBar)
		+NotifyScrollBar(const WeakPtr<RenderNode>& scrollableNode)
	}
	class RenderScrollBar{
		+SetPosition()
	}
	class RenderScroll{
		+SetPosition()
	}
	ScrollBarProxy o-- RenderScrollBar
	ScrollBarProxy o-- RenderScroll

```

ScrollBarProxy中保存有滚动组件和ScrollBar的节点信息，当滚动组件或ScrollBar发生滚动时，会通过ScrollBarProxy通知另一方进行位置更新。

## 4、关键流程

### 4.1、可滚动组件（Scroll为例）滚动后，ScrollBar位置更新

```mermaid
sequenceDiagram
	EveneManager->>RenderScroll: TouchEvent()
	RenderScroll->>ScrollBarProxy: NotifyScrollBar()
	ScrollBarProxy->>RenderScrollBar: SetPosition()

```

1、RenderScroll监听touch事件，并产生滚动效果；

2、RenderScroller内保存有ScrollBarProxy的引用，RenderScroller调用ScrollBarProxy的NotifyScrollBar通知ScrollBar更新位置信息；

3、ScrollBar更新位置后重新渲染。

### 4.2、ScrollBar滚动后，更新可滚动组件（Scroll为例）位置

```mermaid
sequenceDiagram
	EveneManager->>RenderScrollBar: TouchEvent()
	RenderScrollBar->>ScrollBarProxy: NotifyScrollableNode(double distance)
	ScrollBarProxy->>RenderScroll: UpdateScrollPosition()
```



## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── scroll_bar
   ├── scroll_bar_component.h
   ├── scroll_bar_component.cpp
   ├── scroll_bar_element.h
   ├── scroll_bar_element.cpp
   ├── render_scroll_bar.h
   ├── render_scroll_bar.cpp
   ├── flutter_render_scroll_bar.h
   ├── flutter_render_scroll_bar.cpp
   ├── scroll_bar_proxy.h
   ├── scroll_bar_proxy.cpp
   ├── BUILD.gn

```

## 6、代码示例

​	

```
@Entry
@Component
struct ScrollBarTest {
	scroller: Scroller = new Scroller()
	private arr: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
	
    build() {
        Stack() {
        	Scroll(this.scroller) {
                Column() {
                    ForEach(this.arr, (item) => {
                        Text(item.toString())
                            .width('90%').height(150).backgroundColor(0xFFFFFF)
                            .borderRadius(15).fontSize(16).textAlign(TextAlign.Center)
                            .margin({ top: 10 })
                	}, item => item)
                }.width('100%')
            }.width('100%')
            ScrollBar({ scroller: this.scroller, direction: ScrollDirection.Vertical, state: BarState.Auto }) {
            	Text()
            		.width(20).height(40).backgroundColor(0x01000000)
            		.border({ width: 3, color: Color.White, style: BorderStyle.Solid }).borderRadius(25)
            }.width(50).height(600).backgroundColor(0x01000000)
		}.width(200).height(400)
    }
}
```

