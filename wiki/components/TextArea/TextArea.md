# TextArea模块设计

## 1、模块功能描述

本设计文档描述Ace子系统中的TextArea组件，组建的基本功能是提供单行文本输入，二期开发主要是在原本的组件中增加输入过滤的功能。

### 1.1 模块架构图

![1637561251648](figures/TextArea1.png)

### 1.2 模块效果图
![1637561607121](figures/TextArea2.png)

## 2、模块的对外接口

#### 接口说明

TextArea(placeholder?: string)

参数

| 参数名      | 参数类型 | 必填 | 默认值 | 参数描述           |
| ----------- | -------- | ---- | ------ | ------------------ |
| placeholder | string   | 否   | -      | 无输入时显示文本。 |

#### 子组件

不包含子组件

#### 属性方法

| 名称             | 参数类型             | 默认值 | 参数描述                                                     |
| ---------------- | -------------------- | ------ | ------------------------------------------------------------ |
| placeholderColor | Color                | -      | 设置placeholder文本颜色。                                    |
| placeholderFont  |                      | -      | 设置placeholder文本样式： size: 设置文本尺寸，Length为number类型时，使用fp单位。 weight: 设置文本的字体粗细，number类型取值[100, 900]，取值间隔为100，默认为400，取值越大，字体越粗。 family: 设置文本的字体列表。使用多个字体，使用','进行分割，优先级按顺序生效。例如：'Arial, sans-serif'。 style: 设置文本的字体样式。设置文本水平对齐方式。 |
| textAlign        | TextAlign            | Start  | 设置文本水平对齐方式。                                       |
| caretColor       | Color                | -      | 设置输入框光标颜色。                                         |
| height           | Length               | -      | 设置组件自身的高度，缺省时使用元素自身内容需要的高度。       |
| fontColor        | Color                | -      | 设置文本颜色。                                               |
| fontSize         | Length               | -      | 设置文本尺寸，Length为number类型时，使用fp单位。             |
| fontStyle        | FontStyle            | Normal | 设置文本的字体样式。                                         |
| fontWeight       | number \| FontWeight | Normal | 设置文本的字体粗细，number类型取值[100, 900]，取值间隔为100，默认为400，取值越大，字体越粗。提供常用枚举值，参考：FontWeight |
| fontFamily       | string               | -      | 设置文本的字体列表。使用多个字体，使用','进行分割，优先级按顺序生效。例如：'Arial, sans-serif'。 |
| inputFilter      | string               | -      | 正则表达式，满足表达式的输入允许显示，不满足正则表达式的输入被忽略。 |

TextAlign枚举说明

| 名称   | 描述           |
| ------ | -------------- |
| Start  | 水平对齐首部。 |
| Center | 水平居中对齐。 |
| End    | 水平对齐尾部。 |

#### 事件方法

| 名称     | 参数类型                                    | 描述                       |
| -------- | ------------------------------------------- | -------------------------- |
| onChange | onChange(callback: (value: string) => void) | 输入发生变化时，触发回调。 |

## 3、模块结构

~~~mermaid
classDiagram
      class TextFieldComponent{
          -EventMarker onChange
          -EventMarker GetOnTextChange
          -EventMarker GetOnSelectChange
          -EventMarker GetOnFinishInput
          -EventMarker GetOnTap
          -EventMarker GetOnLongPress
          -EventMarker GetOnOptionsClick
          -EventMarker GetOnTranslate
          -EventMarker GetOnShare
          -EventMarker GetOnSearch
          +GetValue() string
          +SetValue() string
          +GetPlaceholder() string
          +SetPlaceholder() string
          +GetPlaceholderColor() color
          +SetPlaceholderColor() color
          +SetTextMaxLines() uint32_t
          +GetTextMaxLines() uint32_t
          +GetTextAlign() TextAlign
          +SetTextAlign() TextAlign
          +GetTextStyle() TextStyle
          +SetTextStyle() TextStyle
          +GetErrorTextStyle() TextStyle
          +SetErrorTextStyle() TextStyle
          +GetErrorSpacing() Dimension
          +SetErrorSpacing() Dimension
          +NeedObscure() bool
          +SetObscure() bool
          +IsEnabled() bool
          +SetEnabled() bool
          +SetInputFilter() string
          +GetInputFilter() string
          +SetEditingStyle() TextStyle
          +GetEditingStyle() TextStyle
          +SetPlaceHoldStyle() TextStyle
          +GetPlaceHoldStyle() TextStyle
      }
      class RenderTextField{
          +Update() void
          +PerformLayout() void
          +UpdateEditingValue() void
          +PerformAction() void
          +OnStatusChanged() void
          +OnValueChanged() void
          +OnPaintFinish() void
          +OnKeyEvent() bool
          +RequestKeyboard() bool
          +CloseKeyboard() bool
          +ShowError() void
          +ShowTextOverlay() void
          +SetOnValueChange() void
          +SetOnKeyboardClose() void
          +SetOnClipRectChanged() void
          +TextStyle textStyle_;
      }
      class FlutterRenderTextField{
          +PaintCaret() void
          +PaintDecoration() void
          +PaintSelectCaret() void
          +PaintIcon() void
          +PaintSelection() void
          +PaintTextAndPlaceholder() void
          +PaintErrorText() void
          +PaintCountText() void
          +PaintOverlayForHoverAndPress() void
          +PaintTextField() void
      }
      class TextFieldElement{
          +Update() void
          +OnFocus() void
          +OnBlur() void
          +OnKeyEvent() bool
          +RequestKeyboard() bool
          +CloseKeyboard() void
          +ShowError() void
          +Delete() void
      }
      class TextFieldTheme{
          +ParsePattern() void
      }
      class RenderNode{
          +Update() void
          +PerformLayout() void
          +IsRepaintBoundary() bool
          +GetRenderLayer() RenderLayer
      }
      TextFieldComponent <|-- TextFieldTheme
      RenderNode <|-- RenderTextField
      RenderTextField <|-- FlutterRenderTextField
      RenderTextField *-- TextFieldElement
      RenderTextField *-- TextFieldComponent
~~~



**TextFieldComponent**:表示文本输入组件所包含的字体样式，占位符样式，文本样式等属性。

**RenderTextField**：作为`element`的衔接，主要负责TextField的属性更新，布局。

`Update`方法中会更新`format`，`textStyle_`的数据。同时在`update`注册回调函数`SetCallback`,该回调函数用于更新文本组件信息，在FlutterRenderTextField中调用。

`PerformLayout`方法负责确定子组件renderTextField的布局信息。

**FlutterRenderTextField**：

`paint`会取系统文本样式的绘制。

## 4、关键流程

### 4.1 关键流程实现

```
@Entry
@Component
struct TextAreaTest {
  @State text: string = ''
  build() {
    Column() {
      TextArea({ placeholder: 'input your word' })
        .inputFilter('[1-9]*')
    }
  }
}
```

上述代码中，传入inputFilter正则表达式，对输入文本进行实时过滤。

**设计思路**

前端JS层传入inputFilter(string)正则表达式，后端RenderTextField层在SetEditingValue的时候对GetEditingValue得到的文本内容进行过滤。符合正则匹配的文本内容，显示在文本输入框内，不符合正则匹配的输入文本会被过滤掉，不会显示在输入框。同时会告诉用户匹配失败并且返回原始字符串。例：inputFilter('[1-9]*'),用户在输入的时候可以无限次输入1到9之间的数字，如果输入中文或者英文，内容会被实时过滤掉，不会显示在文本框上面。

### 4.2 关键流程

以下流程描述的是TextArea构造的时序图，关键流程主要，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    TextFieldComponent->>RenderNode: Create()
    TextFieldComponent->>TextFieldElement: CreateElement()
    RenderNode->>RenderTextField:PerformLayout()
    RenderNode->>RenderTextField:Update()
    RenderNode->>FlutterRenderTextField:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── TextArea
   ├── text_field_component.cpp
   ├── text_field_component.h
   ├── flutter_render_text_field.cpp
   ├── flutter_render_text_field.h
   ├── render_text_field.cpp
   ├── render_text_field.h
   ├── BUILD.gn
├── frameworks/bridge/declarative_frontend/jsview
   |-- js_text_area.h
   |-- js_text_area.cpp
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct TextAreaTest {
  @State text: string = ''
  build() {
    Column() {
      TextArea({ placeholder: 'input your word' })
        .placeholderColor("rgb(0,0,255)")
        .placeholderFont({ size: 30, family:'cursive',weight:100,style:FontStyle.Italic})
        .textAlign(TextAlign.Center)
        .caretColor("rgb(0,0,255)")
        .fontWeight(FontWeight.Bold)
        .fontColor(Color.Red)
        .height(50)
        .fontSize(30)
        .inputFilter('[1-9]*')
        .fontFamily("sans-serif")
        .fontStyle(FontStyle.Normal)
        .onChange((value: string) => {
          this.text=value
      })
      Text(this.text)
    }
  }
}
```

