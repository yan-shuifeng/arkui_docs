# TextClock模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中TextClock，组件的基本功能属性，主要用于显示系统时间，支持不同时区时间显示。

### 1.1 模块架构图

![1637561251648](figures/1637561251648.png)

### 1.2 模块效果图
![1637561622614](figures/1637561622614.png)

## 2、模块的对外接口

#### 接口说明

TextClock(options?:{hourswest?: number})

| 接口名称 | 参数   | 默认值 | 参数描述                                                     |
| -------- | ------ | ------ | ------------------------------------------------------------ |
| hourwest | number | -      | 设置当前时区信息，有效值为整数，时区范围为[-12, 12]，其中负值表示东时区，比如东八区为-8。不设置默认采用系统时间所在的时区。 |

#### 子组件

不包含子组件

#### 属性方法

| 名称   | 参数类型 | 默认值 | 描述                                                         |
| ------ | -------- | ------ | ------------------------------------------------------------ |
| format | string   | -      | 设置展示时间格式，如“yyyy/mm/dd”、“yyyy-mm-dd”等。支持的时间格式化字符串：yyyy,mm,mmm(英文月份简写),mmmm(英文月份全称),dd,ddd(英文星期简写),dddd(英文星期全称),HH/hh(24小时制/12小时制),MM/mm(分钟),SS/ss(秒)。 |
| status | boolean  | -      | 设置文本时钟的启停，用户可以自定义文本时钟的启停。当值为true,时钟显示当前自定义样式的时间，每秒回调一次；当值为false,时钟停止更新时间，停止更新回调。 |

#### 事件方法

| 名称         | 参数类型                       | 描述                                                         |
| ------------ | ------------------------------ | ------------------------------------------------------------ |
| onDateChange | event: (value: number) => void | 提供时间变化回调，回调参数为Unix Time Stamp，即自1970年1月1日（UTC）起经过的毫秒数。该事件最小回调间隔为秒。 |

## 3、模块结构

~~~mermaid
classDiagram
      class TextClockComponent{
          -string format_
          -bool isStart_
          -double hourswest_
          -EventMarker onDateChangeId
          +GetFormat() string
          +GetHoursWest() double
          +GetStatus() bool;
          +GetOnDateChangeEventId() EventMarker
          +SetFormat() void
          +SetStatus() void
          +SetHoursWest() void
          +SetOnDateChangeEventId() void
          +CreateRenderNode() RenderNode
          +CreateElement() Element
      }
      class RenderTextClock{
          +Update() void
          +PerformLayout() void
          +static Create() RenderNode
          +GetFormatDateTime() string
          +GetTextClockComponent()
          +GetCurrentMillisecond() int64_t
          +GetDateTime() DateTime
          +UpdateTimeText() void
          +SetOnDateChange()
          +string format_
          +double hoursWest_ = DBL_MAX;
          +TextStyle textStyle_;
          +bool isStart_ = true;
          +TextClockComponent textClockComponent_
          +TextComponent timeTextComponent_;
          +RenderText renderTimeText_
          +RenderText renderText
      }
      class FlutterRenderTextClock{
          +Paint() void
          +RequestRenderForNextSecond() void
          +GetRenderLayer() RenderLayer
          +IsRepaintBoundary() bool
          +FlutterRenderTextClock()
          +~FlutterRenderTextClock()
          -Flutter::OffsetLayer layer_
      }
      class TextComponent{
          +CreateRenderNode() RenderNode
          +CreateElement() Element
          +SetTextStyle() void
          +GetTextStyle() TextStyle
      }
      class RenderText{
          +Update() void
          +static Create() RenderNode
      }
      class RenderNode{
          +Update() void
          +PerformLayout() void
          +IsRepaintBoundary() bool
          +GetRenderLayer() RenderLayer
      }
      TextComponent <|-- TextComponentV2
      TextComponentV2 <|-- TextClockComponent
      RenderNode <|-- RenderTextClock
      RenderTextClock <|-- FlutterRenderTextClock
      RenderTextClock *-- TextClockComponent
      RenderTextClock *-- RenderText
      RenderTextClock *-- TextComponent
~~~



**TextClockComponent**:表示文本时钟组件所包含的时间格式，时区信息，文本样式，是否启动的状态等属性。

**RenderTextClock**：作为`element`的衔接，主要负责TextClock的属性更新，布局。

`Update`方法中会更新`format`，`isStart_`，`hourswest_`，`textStyle_`的数据。同时在`update`注册回调函数`timeCallback_`,该回调函数用于更新文本组件信息，在`FlutterRenderTextClock`的`delayTask`中调用。

`GetCurrentMillisecond`该方法用于获得自1970年1月1日（UTC）起经过的毫秒数

`GetFormatDateTim`该方法用于将当前系统时间转化成指定format的字符串，

`PerformLayout`方法负责确定子组件renderText的布局信息。

**FlutterRenderTextClock**：

`paint`会以每秒刷新一次的频率取系统时间进行文本时钟的绘制。在绘制之前，会抛出一个`delayTask`延时任务,`delayTime`是1s，在该延时任务中，去`MarkNeedRender`，触发`timeCallback_`回调更新绘制文本信息，以及触发`onDateChange`回调(向前端提供自1970年1月1日（UTC）起经过的毫秒数)。在延时任务到期之后，`TextClock`会进行重绘，并且同样的在其`Paint`函数中抛出下一个延时任务去计划下一秒的绘制。

## 4、关键流程

以下流程描述的是TextClock构造的时序图，关键流程，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    TextClockComponent->>RenderNode: Create()
    TextClockComponent->>RenderElement: CreateElement()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderTextClock:PerformLayout()
    RenderNode->>RenderTextClock:Update()
    RenderNode->>FlutterRenderTextClock:Paint()
```

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── TextClock
   ├── text_clock_component.cpp
   ├── text_clock_component.h
   ├── flutter_render_text_clock.cpp
   ├── flutter_render_text_clock.h
   ├── render_text_clock.cpp
   ├── render_text_clock.h
   ├── BUILD.gn
├── foundation/ace/base
   |-- time_util.cpp
   |-- time_util.h
├── frameworks/bridge/declarative_frontend/jsview
   |-- js_text_clock.h
   |-- js_text_clock.cpp
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct TextClockExmaple {
  @State format: string = 'MMMMddhhmmss'
  @State accumulateTime: number = 0
  @State isStart: boolean = true
  build() {
    Column() {
      Text('current milliseconds is' + this.accumulateTime)
      TextClock({hourswest:this.hourwest})
        .format(this.format)
        .onDateChange((value: number) => {
          this.accumulateTime = value
        })
        .status(this.isStart)
        .fontSize(50)
        .borderWidth(3)
        .borderColor(Color.Red)
      Button("click me")
        .onClick(()=>{
           this.isStart = !this.isStart
        })
    }
    .borderColor(Color.Blue)
    .borderWidth(3)
    .align(Alignment.Center)
  }
}
```

