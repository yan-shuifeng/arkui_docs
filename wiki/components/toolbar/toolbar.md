# ToolBar模块设计

## 1、模块功能描述

本模块设计文档描述`Ace`子系统中`ToolBar`工具栏组件，工具栏通常放在界面底部，用于展示针对当前界面的操作选项，目前`PC`上的工具栏大多出现在窗口或内容区的顶部（如窗口控制、业务工具栏）。

### 1.1 模块架构图

![ToolBar](figures/toolbar.jpg)

### 1.2 模块效果图

![1](figures/1.jpg)

![2](figures/2.jpg)

![3](figures/3.jpg)

![4](figures/4.jpg)

![5](figures/5.jpg)

![6](figures/6.jpg)

![7](figures/7.jpg)

![8](figures/8.jpg)

![9](figures/9.jpg)



## 2、模块的对外接口

#### 接口说明

##### 1. 工具栏内容设置

toolBar({ items:[{ (value: string, icon: string), action: () => void }] | builder: Builder})

| 接口名称 | 参数类型                                                     | 默认值 | 参数描述                                                     |
| -------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| items    | { <br />items: [{ (value: string, icon: string), action: () => void }] \|builder: Builder<br />} <br /> |        | 设置工具栏内容。<br />items: 工具栏所有项。<br />value: 工具栏单个选项的显示文本。<br />icon: 工具栏单个选项的图标资源路径。<br />action: 当前选项被选中的事件回调。<br />builder: 工具栏构造器 |

##### 2. 工具栏控制

| 名称        | 参数类型 | 默认值 | 描述                                                         |
| ----------- | -------- | ------ | ------------------------------------------------------------ |
| hideToolBar | boolean  | false  | 设置隐藏/显示工具栏：<br />true: 隐藏工具栏。<br />false: 不隐藏工具栏。 |

## 3、模块结构

```mermaid
classDiagram
      RenderToolBar -->ToolBarComponent
      ToolBarElement -->ToolBarComponent
      class ToolBarComponent{
          +CreateRenderNode() RenderNode
          +CreateElement() Element
      }
      class RenderToolBar {
          -LayoutChildren() void
          -MakeInnerLayoutParam() LayoutParam
          #GetMaxWidthBasedOnGridType() double
          +Update() void
          +PerformLayout() void
          +Create() RenderNode
          -int32_t toolBarsSize_
          -double toolBarHeight_
          -double toolBarWidth_
          -double actualWidth_
      }
      class FlutterRenderToolBar {
          -ClipLayer layer_
          +GetRenderLayer() RenderLayer
          +Paint() void
      }
      class ToolBarElement {
          +RequestNextFocus() bool
      }
      ComponentGroup <|-- ToolBarComponent
      RenderNode <|-- RenderToolBar
      RenderToolBar <|-- FlutterRenderToolBar
      RenderToolBar ..> ToolBarComponent
```

**组件相关类介绍：**

**ToolBarComponent**:表示工具栏`Component`类，实现相关接口。

**ToolBarElement**：表示工具栏的`Element`类，作为`ToolBarComponent`和`RenderToolBar`的桥接类。

**RenderToolBar**：表示工具栏的`Render`类，主要负责`ToolBar`的更新，布局，也负责处理`ToolBar`的事件，`update`方法负责更新`component`传来的值，`PerformLayout`负责布局操作。

**FlutterRenderToolBar**：继承`RenderToolBar`类，此对象中使用父类`paint`方法，真正的绘制由子节点进行绘制。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 toolBar属性自定义Builder实现

```
// 示例demo
@Builder function GlobalBuilder() {
  Text("toolBar builder")
}
@Entry
@Component
struct ToolBarExample {
  @State hideBar: boolean = false
  @State tabBarLabel: string = "tabBarLabel"
  build() {
    Navigation() {
      Stack() {
        Button('hide bar')
          .onClick(() => {
          this.hideBar = !this.hideBar
        })
      }.width('100%').height('100%')
    }.toolBar(GlobalBuilder)
    .hideToolBar(this.hideBar)
  }
}
```

> 上述代码，在全局定义一个`@Builder`的修饰器，在`Navigation`的`toolBar`设置的时候，将`GlobalBuilder`设置到该属性中去 ，实际效果可以在`toolBar`的位置显示文本，文本内容是`toolBar builder`。

##### 设计思路：

> 设计以上的代码需要工具链和前端解析的支持，首先在前端工具链要支持识别上述的语法形式，并转换成相应的`JS`文件，在前端解析的时候，通过`builder`标识解析出一个`function`类型，并将其创建出一个`JsFunction`并执行，执行完以后返回一个`Root`的`Component`节点，并将该节点直接挂载到`ToolBar`孩子的`display`的子组件，而`hideToolBar`可以通过`display`去控制显隐动画。

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── tool_bar
   ├── flutter_render_tool_bar.cpp
   ├── flutter_render_tool_bar.h
   ├── flutter_render_tool_bar_item.cpp
   ├── flutter_render_tool_bar_item.h
   ├── render_tool_bar.cpp
   ├── render_tool_bar.h
   ├── render_tool_bar_item.cpp
   ├── render_tool_bar_item.h
   ├── tool_bar_component.cpp
   ├── tool_bar_component.h
   ├── tool_bar_element.h
   ├── tool_bar_item_component.cpp
   ├── tool_bar_item_component.h
   ├── tool_bar_item_element.cpp
   ├── tool_bar_item_element.h
   ├── tool_bar_theme.h
   ├── BUILD.gn
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct ToolBarExample {
  @State hideBar: boolean = false
  @State tabBarLabel: string = "tabBarLabel"
  build() {
    Navigation() {
      Stack() {
        Button('hide bar')
          .onClick(() => {
            this.hideBar = !this.hideBar
          })
      }
      .width('100%').height('100%')
    }
      .toolBar({ items: [
      { value: 'app', icon: 'image/app.svg', action: () => {
        console.log("app")
      } },
      { value: 'add', icon: 'image/add.svg', action: () => {
        console.log("add")
      } },
      { value: 'collect', icon: 'image/collect.svg', action: () => {
        console.log("collect")
      } }      ] })
      .hideToolBar(this.hideBar)
  }
}
```

