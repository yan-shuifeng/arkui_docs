#  Progress模块设计

## 1、模块功能描述

本模块设计文档描述Ace子系统中Progress，组件的基本功能属性，主要用于进度相关数据的图形化。

### 1.1 模块架构图

![Progress](figures/Progress.png)

### 1.2 模块效果图
![Progress](figures/image-202111221.png)

![Progress](figures/image-202111222.png)

![Progress](figures/image-202111223.png)

![Progress](figures/image-202111224.png)

![Progress](figures/image-202111225.png)

## 2、模块的对外接口

#### 接口说明

Progress{value: number,  total?: number, style?: ProgressStyle}

| 接口名称 | 参数          | 默认值 | 参数描述         |
| -------- | ------------- | ------ | ---------------- |
| value    | number        | -      | 设置当前进度值。 |
| total    | number        | 100    | 指定进度总长。   |
| style    | ProgressStyle | Linear | 指定进度条样式。 |

**表1** ProgressStyle枚举说明：

| 名称      | 描述                                             |
| --------- | ------------------------------------------------ |
| Linear    | 线性进度条样式。                                 |
| Ring      | 环形无刻度进度条样式。                           |
| Eclipse   | 圆形进度条样式，展现类似月圆月缺的进度展示效果。 |
| ScaleRing | 环形有刻度进度条样式。                           |
| Capsule   | 胶囊进度条样式。                                 |

#### 子组件

不包含子组件

#### 属性方法

| 名称          | 参数类型                                                     | 默认值 | 描述                                                         |
| ------------- | ------------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| value         | number                                                       | -      | 设置当前进度值。                                             |
| color         | Color                                                        | -      | 设置进度条前景色。                                           |
| circularStyle | {strokeWidth: Length,<br>scaleCount: number,<br>scaleWidth: Length} | -      | stokeWidth: 设置环形进度条宽度；<br>scaleCount: 设置环形进度条刻度数;<br>scaleWidth: 设置环形进度条刻度粗细。 |

## 3、模块结构

```mermaid
classDiagram
      TrackComponent ..> ProgressComponent
      LinearTrack ..> TrackComponent
      MoonTrack ..> TrackComponent
      ScaleRingTrack ..> TrackComponent
      CircularTrack ..> TrackComponent
      CapsuleTrack ..> TrackComponent
      ProgressData ..> ProgressComponent
      class ProgressComponent{
          -ProgressData data_
          -RefPtr<TrackComponent> track_
          -ProgressType type_
          -bool playAnimation_
          +InitStyle(const RefPtr<ProgressTheme>& theme) void
          +GetTrack() RefPtr<TrackComponent>
          +SetTrack(const RefPtr<TrackComponent>& track) void
          +SetValue(double value) void
          +GetValue() double
          +SetCachedValue(double cachedValue) void
          +GetCachedValue() double
          +SetMaxValue(double max) void
          +GetMaxValue() double
          +SetMinValue(double min) void
          +GetMinValue() double
          +GetType() ProgressType
          +SetAnimationPlay(bool playAnimation) void
          +GetAnimationPlay() bool
          +GetSelectColor() Color&
          +SetSelectColor(const Color& color) void
          +GetTrackThickness() Dimension&
          +SetTrackThickness(const Dimension& thickness) void
          +SetScaleNumber(int32_t number) void
          +GetScaleWidth() Dimension&
          +SetScaleWidth(const Dimension& width) void   
      }
      class ProgressData{
          -double minValue_
          -double maxValue_
          -double value_
          -double stepValue_
          -double cachedValue_
          +SetMinValue()
          +SetMaxValue()
          +SetValue()
          +SetStepValue()
          +SetCachedValue()
          +MoveSteps() double
      }
      class LinearTrack {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      class MoonTrack {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      class ScaleRingTrack {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      class CircularTrack {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      class CapsuleTrack {
          +RefPtr<RenderNode> CreateRenderNode()
      }
      RenderNode <|-- RenderProgress
      RenderProgress <|-- FlutterRenderProgress
      RenderProgress ..> ProgressComponent
      class RenderProgress {
        Update() void
        PerformLayout() void
        OnPaintFinish() void
        GetErrorBit() bool
      }
      class FlutterRenderProgress {
          -RefPtr<RenderNode> track_
          +Update() void
          +PerformLayout() void
          +Paint(RenderContext& context, const Offset& offset) void
      }
      class ProgressElement {
          -WeakPtr<Component> customComponent_
          +Update() void
          +CanUpdate(const RefPtr<Component>& newComponent)() bool
      }

```

**组件相关类介绍：**

**TrackComponent**:表示进度条组件的进度，包含绘制进度条所需的颜色属性。

**LinearTrack**：继承`TrackComponent`类，表示直线的进度条，并重写`CreateRenderNode`逻辑使其生成对应的`LinearTrackRenderNode`，以此来渲染进度样式。

**MoonTrack**：继承`TrackComponent`类，表示圆形的进度条，并重写`CreateRenderNode`逻辑使其生成对应的`MoonTrackRenderNode`，以此来渲染进度样式。

**ScaleRingTrack**：继承`TrackComponent`类，表示有刻度环形的进度条，并重写`CreateRenderNode`逻辑使其生成对应的`ScaleRingTrackRenderNode`，以此来渲染进度样式。

**RingTrack**：继承`TrackComponent`类，表示无刻度环形的进度条，并重写`CreateRenderNode`逻辑使其生成对应的`RingTrackRenderNode`，以此来渲染进度样式。

**CapsuleTrack**：继承`TrackComponent`类，表示胶囊形的进度条，并重写`CreateRenderNode`逻辑使其生成对应的`CapsuleTrackRenderNode`，以此来渲染进度样式。

**ProgressData**:作为保存数据的类。其中包含`max`，`min`，`value`，`step`,`cachedValue`。

**ProgressComponent**：`Progress`组件的具体实现类，通过`Track`部分实现`Progress`显示进度功能。

**流程相关类：**

**ProgressComponent**：保存`Progress`相关数据以及生成`element`和`renderNode`。

**RenderProgress**：作为`element`的衔接，主要负责Progress的更新，布局。

`Update`方法中会初始化手势识别器已经更新`value`的数据，并计算当前`value`所占比例。

**FlutterRenderProgress**：

`Update` 方法中创建对应的`RenderTrack`节点并挂载在Render树上。**（注意在每一次`update`时，不要重复挂载节点）**。

以及调用`RenderTrack`的`Update`方法去更新对应的`RenderNode`节点。

`PerformLayout`方法负责确定其挂载的子节点`Track`位置。并改变`Track`的数值，以便由`Track`节点进行渲染。

此对象中使用父类`paint`方法，真正的绘制由子节点进行绘制。

## 4、关键流程

### 4.1 关键流程实现

#### 4.1.1 环形进度条实现

```
// 示例demo
@Entry
@Component
struct ProgressExample {
  build() {
    Column({ space: 5 }) {  
      Text('Ring Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Ring}).width(200)
      
      Text('Ring Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Ring }).color(Color.Red).value(50).width(200) 
    
      Text('ScaleRing Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.ScaleRing }).width(200)
      .circularStyle({strokeWidth: 15,scaleCount: 50,scaleWidth: 2})

      Text('ScaleRing Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.ScaleRing }).color(Color.Red).value(50).width(200) 
      .circularStyle({strokeWidth: 15,scaleCount: 50,scaleWidth: 2})
      
    }.width('100%').margin({ top: 5 })
  }
}
```

> 上述代码，在构造中的`style`属性中，新增`ProgressStyle`枚举值`Ring`与`ScaleRing`的枚举值，当设置成该枚举值的时候，就可以得到环形的进度条的样式。

##### 设计思路：

**1.  前端传入`style`的属性值，默认值为`ProgressStyle.Linear`，`ProgressStyle`原先的`Linear`与`Eclipse`枚举值实现保持不变，增加`Ring`与`ScaleRing`枚举值。**

**2.增加circularStyle方法,参数类型stokeWidth: 设置环形进度条宽度，scaleCount: 设置环形进度条刻度数，scaleWidth: 设置环形进度条刻度粗细 。**

**3. 当前端构造传入`ProgressStyle.ScaleRing`枚举时,`js_Progress`在`create`时创建一个新的`ScaleRingTrack`，该类继承`Progress_Component`类，该组件支持设置环形进度条宽度，设置环形进度条刻度数，设置环形进度条刻度粗细，支持设置填充色。获取宽度，刻度数，刻度粗细和填充色后，在`flutter_render_Progress`中绘制。**

#### 4.1.2 胶囊进度条实现

```
// 示例demo
@Entry
@Component
struct ProgressExample {
  build() {
    Column({ space: 5 }) {  
      Text('Capsule Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Capsule}).width(200)
      
      Text('Capsule Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Capsule }).color(Color.Red).value(50).width(200) 
      
    }.width('100%').margin({ top: 5 })
  }
}
```

> 上述代码，在构造中的`style`属性中，新增`ProgressStyle`枚举值`Capsule`的枚举值，当设置成该枚举值的时候，就可以得到胶囊形的进度条的样式。

##### 设计思路：

**1.  前端传入`style`的属性值，默认值为`ProgressStyle.Linear`，`ProgressStyle`原先的`Linear`与`Eclipse`枚举值实现保持不变，增加`Capsule`枚举值。**

**2.  当前端构造传入`ProgressStyle.Capsule`枚举时,`js_Progress`在`create`时创建一个新的`CapsuleTrack`，该类继承`Progress_Component`类，在`flutter_render_Progress`中绘制，在`flutter_render_linear_track.cpp`的`Paint`方法中，新增绘制胶囊轮廓的逻辑，新增胶囊动画的逻辑** 。

### 4.2 关键流程

以下流程描述的是当用户设置水平或者竖直的枚举后，从ProgressComponent构造并设置枚举值开始的时序图，关键流程主要在当轨道的布局计算，布局计算完毕以后所有的组件正常绘制即可。

```mermaid
sequenceDiagram
    ProgressComponent->>TrackComponent: Constructor()
    TrackComponent->>LinearTrack: Constructor()
    LinearTrack->>RenderTrack:Create()
    PipelineContext->>RenderNode:OnLayout()
    RenderNode->>RenderTrack:PerformLayout()
    Note over PipelineContext:Calculate the width, height and layout information of horizontal or vertical tracks
    PipelineContext->>FlutterRenderContext:Repaint()
    FlutterRenderContext->>RenderNode:Paint()
    RenderNode->>RenderTrack:Paint()
    RenderTrack->>FlutterRenderTrack:Paint()
```

## 5、文件结构

```bash
foundation/ace/core/
|---components
	|---progress_component.h
	|---progress_component.cpp
	|---loading_progress_component.h
	|---loading_progress_component.cpp
	|---track_component.h
	|---track_component.cpp
|---dsl
	|---json
|---progress_creator.h
|---progress_creator.cpp
|---loading_progress_creator.h
|---loading_progress_creator.cpp
	|---test/unittest/progress
	    |---progress_creator_test.cpp
	    |---BUILD.gn
|---element
|---progress_element.h
|---loading_progress_element.h
|---rendering/common
	|---render_progress.h
	|---render_progress.cpp
	|---render_loading_progress.h
	|---render_loading_progress.cpp
	|---render_track.h
	|---render_track.cpp
|---rendering/flutter
	|---flutter_render_progress.h
	|---flutter_render_progress.cpp
	|---flutter_render_loading_progress.h
	|---flutter_render_loading_progress.cpp
	|---flutter_render_linear_track.h
	|---flutter_render_linear_track.cpp
	|---flutter_render_circular_track.h
	|---flutter_render_circular_track.cpp
	|---flutter_render_scale_track.h
	|---flutter_render_scale_track.cpp
```

## 6、伪代码示例

```typescript
@Entry
@Component
struct ProgressExample {
  build() {
    Column({ space: 5 }) {
      Text('Linear Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Linear }).width(200)

      Text('Linear Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Linear }).color(Color.Red).value(50).width(200)
      
      Text('Eclipse Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Eclipse }).width(200)

      Text('Eclipse Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Eclipse }).color(Color.Red).value(50).width(200)   

      Text('Ring Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Ring}).width(200)
      
      Text('Ring Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Ring }).color(Color.Red).value(50).width(200) 
        
      Text('ScaleRing Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.ScaleRing}).width(200)
      .circularStyle({strokeWidth: 15,scaleCount: 50,scaleWidth: 2})

      Text('ScaleRing Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.ScaleRing }).color(Color.Red).value(50).width(200) 
      .circularStyle({strokeWidth: 15,scaleCount: 50,scaleWidth: 2})
       
      Text('Capsule Progress').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 10, style: ProgressStyle.Capsule}).width(200)
      
      Text('Capsule Progress Color').fontSize(9).fontColor(0xCCCCCC).width('90%')
      Progress({ value: 20, total: 150, style: ProgressStyle.Capsule }).color(Color.Red).value(50).width(200) 
        
    }.width('100%').margin({ top: 5 })
  }
}
```

