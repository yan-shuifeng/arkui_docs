## 基本流程：从JS到C++组件

```mermaid
sequenceDiagram
	Build->>JSView: ConstructorCallback: new js object
	Build->>JSView: Create: ComposedComponent
	Vsync->>Pipeline: Flush Build
	ComposedComponent->>ComposedComponent: PerformBuild: CallRenderFunction
	ComposedComponent->>JSView: InternalRender: Call js build function
	JSView->>JSView: ExecuteRender
	Build->>JSSwiper: create
	JSSwiper->>ViewStackProcess: Add: SwiperComponent
	Build->>JSColumn: create
	JSColumn->>ViewStackProcess: Add: ColumnComponent
	Build->>JSText: create
	JSText->>ViewStackProcess: Add: TextComponent
	Build->>JSText: pop
	JSText->>ViewStackProcess: Pop: TextComponent
	ViewStackProcess->>TextComponent: WrapComponents
	ViewStackProcess->>ColumnComponent: AppendChild: TextComponent
	Build->>JSColumn: pop
	JSColumn->>ViewStackProcess: PopContainer: ColumnComponent
	ViewStackProcess->>ColumnComponent: WrapComponents
	ViewStackProcess->>SwiperComponent: AppendChild: ColumnComponent
	Build->>JSSwiper: pop
	JSColumn->>ViewStackProcess: PopContainer: componentsStack_.size() == 1 return
	JSView->>ViewStackProcess: Finish: Get SwiperComponent
	ComposedElement->>ComposedElement: PerformBuild: After call CallRenderFunction, Get Component
	ComposedElement->>ComposedElement: UpdateChild: update new child tree
	Element->>Element: UpdateChildWithSlot: use InflateComponent to build swiper
	SwiperComponent->>SwiperComponent: CreateElement: create V2::SwiperElement
	V2SwiperElement->>V2SwiperElement: SetNewComponent: MarkNeedRebuild
	V2SwiperElement->>V2SwiperElement: Mount: Mount to parent and Rebuild
	V2SwiperElement->>V2SwiperElement: Prepare: create render node
	V2SwiperElement->>V2SwiperElement: Update: first update render node with components and markNeedLayout, add slef to pipeline.
	V2SwiperElement->>V2SwiperElement: PerformBuild: use ElementProxyHost to contains the children of swiper, don't mount child at this time.
	V2SwiperElement->>V2ElementProxyHost: UpdateChildren: create MultiComposedComponent to contains the swiper children
	V2ElementProxyHost->>ElementProxy: Create: create proxy contains to contains the swiper children
	ElementProxy->>MultiComposedElementProxy: create MultiComposedElementProxy for the MultiComposedComponent.
	MultiComposedElementProxy->>MultiComposedElementProxy: Update: in update functions: try to create ElementProxy for each child.
	MultiComposedElementProxy->>ElementProxy: Create: create RenderElementProxy contains to contains the ColumnComponent child
	RenderElementProxy->>RenderElementProxy: Update: call host(V2SwiperElement) to OnUpdateElement
	V2SwiperElement->>V2SwiperElement: UpdateChild: Traversing the Create update like swiper, it will build FlexElement to contains the TextElement
	vsync->>Pipeline: FlushLayout: flush layout for marked renderNode.
	RenderSwiper->>RenderSwiper: PerformLayout: PerformLayout for RenderFlex Traversing, and mark self need paint.
	Pipeline->>Pipeline: FlushRender: flush render for painted.
	RosenRenderContext->>RosenRenderContext: Repaint: use context to paint
	RosenRenderContext->>RenderNode: RenderWithContext: call RenderSwiper to render.
	RenderSwiper->>RenderSwiper: Paint: paint process.
```
