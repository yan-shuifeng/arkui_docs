# SwipeGesture模块功能设计

## 1 模块功能描述

本模块设计文档描述Ace子系统中SwipeGesture，组件的基本功能属性，主要用于作为获取模块的滑动事件。

### 1.1 模块架构图

![swipegesture](figures\swipegesture.png)

### 1.2 模块效果图

![swipegesture](figures\swipegesture.gif)

## 2 模块的对外接口

### 2.1 接口说明

SwipeGesture(value?:{fingers?: number, direction?: Direction, speed?: number})

|  参数名   | 参数类型  |  是否可选（默认值）   |                           参数描述                           |
| :-------: | :-------: | :-------------------: | :----------------------------------------------------------: |
|  fingers  |  number   |        否（1）        |              触发滑动的手指数最小为1指最大为5指              |
| direction | Direction | 否（ Direction.All ） | 滑动方向，可选值为All：所有方向，Horizontal：水平方向，Vertical:竖直方向 |
|   speed   |  number   |       否（100）       |                 识别滑动的最小速度（VP/秒）                  |

### 2.2 事件方法

|          |                                    |                      |
| :------: | :--------------------------------: | :------------------: |
| onAction | (event: SwipeGestureEvent) => void | 滑动手势识别成功回调 |

### 2.3 示例

```typescript
@Entry
@Component
struct SwipeGestureExample {
  @State decoType : TextDecorationType = TextDecorationType.None;
  @State decoColor : string = "#000000";
  @State case : TextCase = TextCase.Normal;
  @State idx1 : number = 0;
  @State idx2 : number = 0;
  @State idx3 : number = 0;
  @State colOpacity : number = 1;
  @State colBlur : number = 0;
  @State translateX : number = 0;
  @State translateY : number = 0;
  @State rotateAngle : number = 0;
  @State scale : number = 1;
  @State speed : number = 1;
  private decoTypeList : TextDecorationType[] = [TextDecorationType.None, TextDecorationType.Underline, TextDecorationType.LineThrough, TextDecorationType.Overline];
  private decoColorList : string[] = ["#000000", "#FF0000", "#0000FF"];
  private caseList : TextCase[] = [TextCase.Normal, TextCase.LowerCase, TextCase.UpperCase];
  build() {
    Column() {
      Text("JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST   == SPEED ： " + this.speed)
        .fontSize(16)
        .fontColor("#CD5C5C")
        .decoration({type: this.decoType, color: this.decoColor})
        .textCase(this.case)
    }
    .width(260).height(260)
    .opacity(this.colOpacity)
    .blur(this.colBlur)
    .translate({x: this.translateX, y: this.translateY})
    .rotate({x: 0, y: 0, z: 1, angle: this.rotateAngle})
    .scale({x: this.scale, y: this.scale})
    .border({width:3, color: 0xffff4500})
    .backgroundColor("#FFE4B5")
    .gesture(
    GestureGroup(GestureMode.Parallel,
    LongPressGesture({repeat: true, duration: 500})
      .onAction(() => {
        this.colOpacity -= 0.1
      })
      .onActionEnd(() => {
        this.colOpacity = 1
      })
        SwipeGesture({fingers: 3})
          .onAction((event: SwipeGestureEvent) => {
            this.speed = event.speed
            this.rotateAngle = event.angle
          })
    )
      .onCancel(() => {
        console.info("luoying---GestureGroup Cancel!!!")
      })
    )
    .onAppear(() => {
      console.info("luoying --- Show Column")
    })
    .onDisAppear(() => {
      console.info("luoying --- Hide Column")
    })
  }
}
```

## 3 模块结构

```mermaid
classDiagram
      GestureComponent ..> SlideGesture
      SlideGesture ..> SlideRecognizer
      SlideGesture --> Gesture
      class SlideGesture{
          -SlideDirection direction_
          -double speed_
          -RefPtr<TrackComponent> track_
          +CreateRecognizer((WeakPtr<PipelineContext>) RefPtr<GestureRecognizer>
      }
      SlideRecognizer --> MultiFingersRecognizer
      class SlideRecognizer {
          -SlideDirection direction_
          -double speed_
          -WeakPtr<PipelineContext> context_
          -std::map<int32_t, TouchPoint> touchPoints_
          -std::map<int32_t, Offset> fingersDistance_
          -TimeStamp time_
          -TimeStamp touchDownTime_
          -bool slidingEnd_
          -bool slidingCancel_
          -Point globalPoint_
          -OnSlideFingersFunc onChangeFingers_
          -OnSlideDirectionFunc onChangeDirection_
          -OnSlideSpeedFunc onChangeSpeed_
          -int32_t newFingers_
          -double angle_
          -double initialAngle_
          -double currentAngle_
          -double resultAngle_
          -double resultSpeed_
          -double newSpeed_
          -SlideDirection newDirection_
          +OnAccepted() void
          +OnRejected() void
          -HandleTouchDownEvent(const TouchPoint&) void
          -HandleTouchUpEvent(const TouchPoint&) void
          -HandleTouchMoveEvent(const TouchPoint&) void
          -HandleTouchCancelEvent(const TouchPoint&) void
          -ReconcileFrom(const RefPtr<GestureRecognizer>&) bool
          -IsSlideGestureAccept() GestureAcceptResult
          -Reset() void
          -SendCallbackMsg(const std::unique_ptr<GestureEventFunc>&) void
          -ChangeFingers(int32_t) void
          -ChangeDirection(const SlideDirection&) void
          -ChangeSpeed(double) void
          -ComputeAngle() double
          -GetTouchRestrict() const TouchRestrict&
      }

```

### 3.1 组件相关类介绍

**SwipeGesture** ：表示当前的手势，包含滑动手势的消息函数和属性。

**SwipeRecognizer** ：表示滑动手势识别器，通过识别手势动作，返回滑动手势发生的消息。

## 4 关键流程

```typescript
@Entry
@Component
struct GestureExample {
  @State rotateAngle : number = 0;
  @State speed : number = 1;
  build() {
    Column() {
      Text("JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST   == SPEED ： " + this.speed)
        .fontSize(16)
        .fontColor("#CD5C5C")
    }
    .width(260).height(260)
    .rotate({x: 0, y: 0, z: 1, angle: this.rotateAngle})
    .border({width:3, color: 0xffff4500})
    .backgroundColor("#FFE4B5")
    .gesture(
        SwipeGesture({fingers: 3})
          .onAction((event: SwipeGestureEvent) => {
            this.speed = event.speed
            this.rotateAngle = event.angle
          })
    )
  }
}
```


**设计思路**：

1.前端传入SlideGesture，支持三指操作。

2.JSSlideGesture在create函数中根据前端传入的参数和默认参数来创建SlideGesture，在其中保存slidegesture的各种属性，并将其push到GestureComponent中。

**流程图**

```mermaid
sequenceDiagram
    JSSlideGesture->>SlideGesture: Create()
    SlideGesture->>SlideRecognizer: CreateRecognizer()
    RenderBox->>Gesture:CreateRecognizer()
```



## 5 文件结构

```bash
frameworks/core/gestures
├── slide_recognizer.h
├── slide_recognizer.cpp
├── slide_gesture.h
├── slide_gesture.cpp
├── gesture_info.h
├── gesture_info.cpp
├── js_gesture.h
├── js_gesture.cpp
├── gesture_type.h
├── BUILD.gn
```
## 6 伪代码示例

```bash
@Entry
@Component
struct SwipeGestureExample {
  @State rotateAngle : number = 0;
  @State speed : number = 1;
  build() {
    Column() {
      Text("JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST JUST A TEST   == SPEED ： " + this.speed)
        .fontSize(16)
        .fontColor("#CD5C5C")
    }
    .width(260).height(260)
    .rotate({x: 0, y: 0, z: 1, angle: this.rotateAngle})
    .border({width:3, color: 0xffff4500})
    .backgroundColor("#FFE4B5")
    .gesture(
        SwipeGesture({fingers: 3})
          .onAction((event: SwipeGestureEvent) => {
            this.speed = event.speed
            this.rotateAngle = event.angle
          })
    )
  }
}
```
