# Popup模块设计



## 1、模块功能描述

介绍气泡组件popup的设计实现。

## 2、模块的对外接口

#### 接口说明

| 名称      | 参数类型                                               | 默认值 | 参数描述                                                     |
| --------- | ------------------------------------------------------ | ------ | ------------------------------------------------------------ |
| bindPopup | show: boolean, popup: PopupOption \| CustomPopupOption | -      | 定义并显示Popup组件。<br />show: 当前弹窗提示是否显示，默认值为false。<br />popup: 当前弹窗提示的参数。 |

#### PopupOption

| 参数名          | 参数类型                                                     | 必填 | 默认值 | 参数描述                                         |
| --------------- | ------------------------------------------------------------ | ---- | ------ | ------------------------------------------------ |
| message         | string                                                       | 是   | -      | 弹窗信息内容。                                   |
| placementOnTop  | boolean                                                      | 否   | false  | 是否在组件上方显示，默认值为false。              |
| primaryButton   | {<br />    value: string,<br />    action: () => void<br />} | 否   | -      | 第一个按钮。                                     |
| secondaryButton | {<br />    value: string,<br />    action: () => void<br />} | 否   | -      | 第二个按钮。                                     |
| onStateChange   | (isVisible: boolean) => void                                 | 否   | -      | 弹窗状态变化事件回调，参数为弹窗当前的显示状态。 |

#### CustomPopupOption

| 接口名称      | 参数                            | 必填 | 默认值 | 参数描述                                                     |
| ------------- | ------------------------------- | ---- | ------ | ------------------------------------------------------------ |
| builder       | () => any                       | 是   | -      | 气泡提示内容的构造器。                                       |
| placement     | Placement                       | 否   | Bottom | 气泡组件优先显示的位置，当前位置显示不下时，会自动调整位置。 |
| maskColor     | Color\|string\|Resource\|number | 否   | -      | 气泡提示遮障层的颜色。                                       |
| popupColor    | Color\|string\|Resource\|number | 否   | -      | 气泡提示的颜色。                                             |
| enableArrow   | boolean                         | 否   | true   | 是否显示箭头，默认上、下方向的气泡会显示箭头。               |
| autoCancel    | boolean                         | 否   | true   | 点击是否自动关闭气泡。                                       |
| onStateChange | (isVisible: boolean) => void    | 否   | -      | 弹窗状态变化事件回调，参数为弹窗当前的显示状态。             |

#### Placement枚举说明

| 名称        | 描述                         |
| ----------- | ---------------------------- |
| Left        | 气泡提示优先位于组件左侧。   |
| Right       | 气泡提示优先位于组件右侧。   |
| Top         | 气泡提示优先位于组件上侧。   |
| Bottom      | 气泡提示优先位于组件下侧。   |
| TopLeft     | 气泡提示优先位于组件左上角。 |
| TopRight    | 气泡提示优先位于组件右上角。 |
| BottomLeft  | 气泡提示优先位于组件左下角。 |
| BottomRight | 气泡提示优先位于组件右下角。 |



## 3、模块结构

```mermaid
classDiagram
	class JSViewAbstruct {
		+JsBindPopup
	}
	class PopupParam {
		-bool enableArrow_
		-Color maskColor_
		-Color backgroundColor_
		-Placement placement_
		-EventMarker onVisibilityChange_
	}
	class PopupComponent {
		-RefPtr<PopupParam> popupParam_
		-RefPtr<PopupController> popupController_
		-RefPtr<Component> customComponent_
	}
	class PopupElement {
		+PerformBuild()
		+ShowPopup()
		+OnStateChange()
	}
	PopupComponent *-- PopupParam
	PopupElement *-- PopupComponent
	PopupComponent <.. JSViewAbstruct
	
	class BubbleComponent {
		-RefPtr<PopupParam> popupParam_
		#RefPtr<Component> child_
	}
	class BubbleElement {
		+PerformBuild()
	}
	class RenderBubble {
		+Update(const RefPtr<Component>& component)
		+PerformLayout()
		+RefPtr<RenderNode> Create()
		-Placement placement_
		-Color maskColor_
		-Color backgroundColor_
		-std::function<void(const std::string&)> onVisibilityChange_
	}
	class FlutterRenderBubble {
		+Paint(RenderContext& context, const Offset& offset)
		+PaintMask(RenderContext& context)
		+PaintBubble(RenderContext& context)
		+PaintShadow(SkCanvas* skCanvas)
	}
	BubbleElement *-- BubbleComponent
	BubbleElement *-- RenderBubble
	FlutterRenderBubble <|-- RenderBubble
	
	PopupElement <.. BubbleComponent
```

## 4、关键流程

```mermaid
sequenceDiagram
	JSEngine->>JSViewAbstruct: JSBindPopup()
	JSViewAbstruct->>ViewStackProcessor: GetPopupComponent()
	ViewStackProcessor ->> PopupComponent: new instance of popup component
	PopupComponent ->> PopupElement: CreateElement()
	PopupElement ->> PopupElement: PerformBuild()
	PopupElement ->> PopupElement: ShowPopup()
	PopupElement ->> BubbleComponent: new instance and set child
	BubbleComponent ->> BubbleElement: CreateElement()
	BubbleElement ->> BubbleElement: PerformBuild()
	BubbleComponent ->> RenderBubble: CreateRenderNode()
	RenderBubble ->> FlutterRenderBubble: Create()
	FlutterRenderBubble ->> FlutterRenderBubble: Paint()
```

1、JS引擎调用JSViewAbstruct的JSBindPopup方法，调用时，会创建PopupComponent节点，并根据入参，创建出PopupComponent的子节点，然后将BubbleComponent挂载到组件树上；

2、PopupElement在PerformBuild时，如果此时show属性为true，则创建BubbleComponent，并将创建好的Component设置为BubbleComponent的子节点，再将BubbleComponent挂载到Stack上；

3、BubbleElement进行PerformBuild，RenderBubble进行布局计算，最终FlutterRenderBubble负责渲染绘制。

## 5、文件结构

```bash
ace_engine/frameworks/core/components
├── popup
   ├── popup_component.h
   ├── popup_component.cpp
   ├── popup_element.h
   ├── popup_element.cpp
   ├── BUILD.gn
├── bubble
   ├── bubble_component.h
   ├── bubble_component.cpp
   ├── bubble_element.h
   ├── bubble_element.cpp
   ├── render_bubble.h
   ├── render_bubble.cpp
   ├── flutter_render_bubble.h
   ├── flutter_render_bubble.cpp
   ├── BUILD.gn

```

## 6、代码示例

​	

```
@Entry
@Component
struct PopupExample {
  @State noHandlePopup: boolean = false
  @State handlePopup: boolean = false
  @State customPopup: boolean = false

  @Builder popupBuilder() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Text('Content of CustomPopup').fontSize(20)
    }.width(100).height(50).backgroundColor(Color.Gray)
  }

  build() {
    Column({ space: 160 }) {

      Button('no handle popup')
        .onClick(() => {
          this.noHandlePopup = !this.noHandlePopup
        })
        .bindPopup(this.noHandlePopup, {
          message: 'content1 content1',
          placementOnTop: false,
          onStateChange: (e) => {
            console.info(e.isVisible.toString())
            if (!e.isVisible) {
              this.noHandlePopup = false
            }
          }
        })

      Button('with handle popup')
        .onClick(() => {
          this.handlePopup = !this.handlePopup
        })
        .bindPopup(this.handlePopup, {
          message: 'content2 content2',
          placementOnTop: true,
          secondaryButton: {
            value: 'ok',
            action: () => {
              this.handlePopup = !this.handlePopup
              console.info('secondaryButton click')
            }
          },
          onStateChange: (e) => {
            console.info(e.isVisible.toString())
          }
        })

      Button('custom popup')
        .onClick(() => {
          this.customPopup = !this.customPopup
        })
        .bindPopup(this.customPopup, {
          builder: this.popupBuilder,
          placement: Placement.Bottom,
          maskColor: 0x33000000,
          popupColor: Color.Gray,
          enableArrow: false,
          onStateChange: (e) => {
            if (!e.isVisible) {
              this.customPopup = false
            }
          }
        })

    }.width('100%').padding({ top: 5 })
  }
}
```

