# 新框架切换

https://gitee.com/openharmony/arkui_ace_engine/pulls/5419

 以上代码合入后支持通过版本类型以及配置文件控制应用切换新框架。

## 1. 版本类型控制切换：

API 版本为9且为Beta4 版本或者API 版本大于9的应用直接切换到新框架；

## 2.配置文件控制切换:

未满足```1.```中切换新框架条件可通过```acenewpipe.config```配置文件进行配置。

配置文件中首行设置切换类型：

ENABLED：指定应用切换至新框架，增加需要切换的bundleName；

**acenewpipe.config 示例:**

```
ENABLED
// tag : DISABLED, ENABLED_FOR_ALL, ENABLED
// read below when use tag ENABLED
com.example.myapplication
com.example.showtoast
```

**使用方式：**

调整```acenewpipe.config后```执行```push_new_pipe_config.bat```即可指定切换新框架。



**push_new_pipe_config.bat 示例:**

```
hdc shell mount -o remount,rw /

hdc file send acenewpipe.config /etc/

pause
hdc shell reboot
```

