# UI组件树构建介绍

![](figures/UI组件树创建.png)

- 当应用启动时，ArkUI框架会初始化应用UI根节点，应用UI根节点使用StagePattern用于实现页面切换等特殊操作；
- 初始化完成后，ArkUI框架会开始加载页面文件，在加载页面文件时会创建页面根节点CustomNode（@Entry标识的自定义组件）；
- 页面根节点组件会在路由管理类中挂载到页面包装节点上（PagePattern）；页面包装节点会挂载到根节点下；
- 页面根节点在布局测试过程中，会调用内部的build方法，生成子树结构挂载到页面根节点下。

## 组件树根节点创建

``` c++
void PipelineContext::SetupRootElement()
{
    CHECK_RUN_ON(UI);
    rootNode_ = FrameNode::CreateFrameNodeWithTree(
        V2::ROOT_ETS_TAG, ElementRegister::GetInstance()->MakeUniqueId(), MakeRefPtr<StagePattern>(), Claim(this));
    rootNode_->SetHostRootId(GetInstanceId());
    rootNode_->SetHostPageId(-1);
    rootNode_->GetRenderContext()->UpdateBgColor(Color::WHITE);
    CalcSize idealSize { CalcLength(rootWidth_), CalcLength(rootHeight_) };
    MeasureProperty layoutConstraint;
    layoutConstraint.selfIdealSize = idealSize;
    layoutConstraint.maxSize = idealSize;
    rootNode_->UpdateLayoutConstraint(layoutConstraint);
    window_->SetRootFrameNode(rootNode_);
    stageManager_ = MakeRefPtr<StageManager>(rootNode_);
    rootNode_->AttachToMainTree();
    LOGI("SetupRootElement success!");
}
```

- 应用启动时会调用PipelineContext的SetupRootElement方法，在SetupRootElement方法中：
  - 创建FrameNode节点，该节点使用的模板为StagePattern，主要用于进行页面路由相关操作；
  - 根节点会设置给window，用于初始化后端渲染根节点；
  - 使用根节点创建路由管理类StageManager，用于给前端页面路由管理类调用，进行后端页面路由的实现。
  - 最后调用AttachToMainTree，将创建的节点标记为主树节点。

## 页面根节点创建

### 页面文件执行和节点挂载

加载应用页面时，ArkUI框架会调用FrontendDelegateDeclarative对象的LoadPage函数执行页面文件加载：

``` c++
void FrontendDelegateDeclarative::LoadPage(
    int32_t pageId, const PageTarget& target, bool isMainPage, const std::string& params, bool isRestore)
{
    ...
    auto page = AceType::MakeRefPtr<JsAcePage>(pageId, document, target.url, target.container);
    page->SetPageParams(params);
    page->SetFlushCallback([weak = AceType::WeakClaim(this), isMainPage, isRestore](const RefPtr<JsAcePage>& acePage) {
        auto delegate = weak.Upgrade();
        if (delegate && acePage) {
            delegate->FlushPageCommand(acePage, acePage->GetUrl(), isMainPage, isRestore);
        } else {
            LOGE("flush callback called unexpected");
            delegate->ProcessRouterTask();
        }
    });
    taskExecutor_->PostTask(
        [weak = AceType::WeakClaim(this), page, isMainPage] {
            auto delegate = weak.Upgrade();
            if (!delegate) {
                return;
            }
            delegate->loadJs_(page->GetUrl(), page, isMainPage);
            page->FlushCommands();
            ...
        },
        TaskExecutor::TaskType::JS);
}
```
- 在`delegate->loadJs_(page->GetUrl(), page, isMainPage)`代码中会调用虚拟机的接口执行生成的js代码（针对Ark虚拟机执行js文件编译生成的abc文件）；
- 执行完js代码后，会执行`page->FlushCommands()`代码调用Page的SetFlushCallback函数中设置的回调函数，执行`delegate->FlushPageCommand(acePage, acePage->GetUrl(), isMainPage, isRestore)`代码；

``` C++
void FrontendDelegateDeclarative::FlushPageCommand(
    const RefPtr<JsAcePage>& page, const std::string& url, bool isMainPage, bool isRestore)
{
    ...
    OnPageReady(page, url, isMainPage, isRestore);
    ...
}

void FrontendDelegateDeclarative::OnPageReady(
    const RefPtr<JsAcePage>& page, const std::string& url, bool isMainPage, bool isRestore)
{
    ...
    if (Container::IsCurrentUseNewPipeline()) {
        auto context = DynamicCast<NG::PipelineContext>(pipelineContext);
        auto stageManager = context ? context->GetStageManager() : nullptr;
        if (stageManager) {
            stageManager->PushPage(page->GetRootNode());
        } else {
            LOGE("fail to push page due to stage manager is nullptr");
        }
        isStagingPageExist_ = false;
        if (isMainPage) {
            OnPageShow();
        }
        return;
    }
    ...
}
```
- 在FrontendDelegateDeclarative对象的FlushPageCommand函数中会调用OnPageReady函数；
- 在OnPageReady函数中会调用页面路由管理类（在组件树根节点创建时创建）的方法`stageManager->PushPage(page->GetRootNode())`，将保存在Page中生成的页面根节点进行加载显示；

``` C++
void StageManager::PushPage(const RefPtr<UINode>& node)
{
    ...
    auto pageNode = FrameNode::CreateFrameNode(
        V2::PAGE_ETS_TAG, ElementRegister::GetInstance()->MakeUniqueId(), MakeRefPtr<PagePattern>());
    node->MountToParent(pageNode);
    pageNode->MountToParent(rootNode_);
    ...
    // Make Dirty and flush layout task.
    rootNode_->MarkDirtyNode();
    UITaskScheduler::GetInstance()->FlushLayoutTask(true, true);
}
```

- 在StageManager类的PushPage方法中，会将页面生成的页面根节点包装到PagePattern的页面包装节点下；
- PagePattern的页面包装节点最终会挂载到应用UI根节点下，由应用UI根节点驱动页面进行布局展示。

### 页面根节点生成

#### 应用原始页面文件

```typescript
@Entry
@Component
struct Index {
  @State message: string = 'Hello World'

  build() {
    Stack() {
      Text('Hello world')
    }.width('80%').height('80%')
    .backgroundColor('#fff111')
  }
}
```

入口页面自定义组件为Index，该Index自定义组件内部渲染一个Stack组件。

#### 使用IDE编译后生成页面执行文件（目录类似：entry/build/default/intermediates/assets/default/js/MainAbility/pages/index.js)

```js
...
class Index extends View {
    ...
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            Stack.height('100%');
            Stack.width('100%');
            Stack.backgroundColor(Color.Red);
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('hello world');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
...
loadDocument(new Index(undefined, {}));
...
```

当ArkUI框架加载上述js文件时，会执行如今关键步骤：

- 创建Index对象；
- 调用loadDocument全局方法，入参为Index对象。

#### Index对象分析

- Index对象继承View对象，View对象在前端框架中定义（foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/engine/stateMgmt.js）

```mermaid
classDiagram
	class View
	class NativeView{
		render() void
	}
	View--|>NativeView
```

- View对象本身继承NativeView对象，而NativeView对象在C++侧通过JS引擎提供的接口注入相关定义（foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/jsview/js_view.cpp）：

  ```C++
  void JSViewPartialUpdate::JSBind(BindingTarget object)
  {
      JSClass<JSViewPartialUpdate>::Declare("NativeView");
      ...
      JSClass<JSViewPartialUpdate>::StaticMethod("create", &JSViewPartialUpdate::Create, opt);
      ...
      JSClass<JSViewPartialUpdate>::Bind(object, ConstructorCallback, DestructorCallback);
  }
  ```

- NativeView对象通过`JSClass<JSViewPartialUpdate>::Bind(object, ConstructorCallback, DestructorCallback)`代码注册了对象创建和析构时的回调监听函数，从而当子类Index对象创建时，会走到C++侧注册的回调函数：`JSViewPartialUpdate::ConstructorCallback`中：

  ```C++
  void JSViewPartialUpdate::ConstructorCallback(const JSCallbackInfo& info)
  {
      LOGD("creating C++ and JS View Objects ...");
      JSRef<JSObject> thisObj = info.This();
      auto* instance = new JSViewPartialUpdate(thisObj);
  
      auto context = info.GetExecutionContext();
      instance->SetContext(context);
  
      //  The JS object owns the C++ object:
      // make sure the C++ is not destroyed when RefPtr thisObj goes out of scope
      // JSView::DestructorCallback has view->DecRefCount()
      instance->IncRefCount();
  
      info.SetReturnValue(instance);
  }
  
  JSViewPartialUpdate::JSViewPartialUpdate(JSRef<JSObject> jsViewObject)
  {
      jsViewFunction_ = AceType::MakeRefPtr<ViewFunctions>(jsViewObject);
      LOGD("JSView constructor");
      // keep the reference to the JS View object to prevent GC
      jsViewObject_ = jsViewObject;
  }
  ```

  上述回调函数中，Index对象（info.This()会返回View的JS对象在C++侧的表示）会作为入参传入，代码中基于Index对象创建JSViewPartialUpdate的C++对象，并在JSViewPartialUpdate的构造函数中引用Index对象，防止Index对象在执行loadDocument方法后因为引用计数为0而销毁。同时JSViewPartialUpdate对象会将自身的指针保存在Index对象的内部成员字段中，便于后续基于Index对象获取JSViewPartialUpdate对象。

#### loadDocument方法分析

loadDocument全局函数，同样在C++侧通过JS引擎接口注入相关定义（foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/engine/jsi/jsi_view_register.cpp）

``` c++
void JsRegisterViews(BindingTarget globalObj)
{
    auto runtime = std::static_pointer_cast<ArkJSRuntime>(JsiDeclarativeEngineInstance::GetCurrentRuntime());
    if (!runtime) {
        LOGE("JsRegisterViews can't find runtime");
    }
    auto vm = runtime->GetEcmaVm();
    globalObj->Set(vm, panda::StringRef::NewFromUtf8(vm, "loadDocument"),
        panda::FunctionRef::New(const_cast<panda::EcmaVM*>(vm), JsLoadDocument));
	...
}

panda::Local<panda::JSValueRef> JsLoadDocument(panda::JsiRuntimeCallInfo* runtimeCallInfo)
{
    ...
    panda::Local<panda::ObjectRef> obj = firstArg->ToObject(vm);
    UpdateRootComponent(obj);
    return panda::JSValueRef::Undefined(vm);
}

void UpdateRootComponent(const panda::Local<panda::ObjectRef>& obj)
{
    JSView* view = static_cast<JSView*>(obj->GetNativePointerField(0));
    ...
    if (Container::IsCurrentUseNewPipeline()) {
        auto pageRootNode = view->CreateUINode();
        page->SetRootNode(pageRootNode);
    } else {
       ...
    }
    ...
}
```

在执行JsLoadDocument方法时，入参为Index对象，通过`obj->GetNativePointerField(0)`代码可以获取到上一步Index对象创建时保存的JSViewPartialUpdate对象，调用JSViewPartialUpdate对象的CreateUINode就能创建相应的页面根节点。

```C++
RefPtr<NG::UINode> JSViewPartialUpdate::CreateUINode()
{
    ACE_SCOPED_TRACE("JSView::CreateSpecializedComponent");
    ...
    auto customNode = NG::CustomNode::CreateCustomNode(viewId, key);
    node_ = customNode;
    ...

    auto renderFunction = [weak = AceType::WeakClaim(this)]() -> RefPtr<NG::UINode> {
        auto jsView = weak.Upgrade();
        CHECK_NULL_RETURN(jsView, nullptr);
        if (!jsView->isFirstRender_) {
            LOGW("the js view has already called initial render");
            return nullptr;
        }
        jsView->isFirstRender_ = false;
        return jsView->InitialUIRender();
    };
    customNode->SetRenderFunction(renderFunction);
    ...
    return customNode;
}

RefPtr<NG::UINode> JSViewPartialUpdate::InitialUIRender()
{
    needsUpdate_ = false;
    // call build function
    RenderJSExecution();
    // Get the subtree node
    return NG::ViewStackProcessor::GetInstance()->Finish();
}
```

## 自定义组件build函数执行和子树创建挂载

![](figures/子树build和挂载.png)

- 在页面通过StageManager挂载到UI树根节点时，UI主树根节点会标记Dirty，并触发新的布局任务创建；
- 在创建布局任务时，各个UI节点会创建LayoutWrapper布局任务包装器并设置相应的布局算法；
- 在Index自定义组件创建自定义组件布局算法时，会将子树创建方法保存到布局算法中；
- LayoutWrapper布局任务包装器同样也是树形结构，执行布局任务的入口为该树形结构根节点，节点在执行布局任务时，会同时执行子节点的布局任务；
- 当执行到Index自定义组件的布局任务时，会先调用子树创建方法创建子树结构（build函数中的UI描述），基于子树结构创建相应的布局包装器；
- 在执行完构建任务后，会对创建的布局包装器同样进行布局任务执行，实现子树布局包装器大小测算和位置布局；
- 整体完成对LayoutWrapper树形结构的布局任务后，LayoutWrapper会将布局结果返回给对应的UI节点，更新相应的大小，触发后续的渲染任务；
- 在自定义组件布局包装器返回布局结果的同时，自定义组件CustomNode还会将新构建生成的子树节点挂载到自身下面，完成子树结构挂载到UI主树操作。

### build函数转换

``` javascript
@Entry
@Component
struct Index {
  @State message: string = 'Hello World'

  build() {
    Stack() {
    }.width('80%').height('80%')
    .backgroundColor('#fff111')
  }
}

// 页面编译后生成的js文件：
...
class Index extends View {
    ...
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            Stack.height('100%');
            Stack.width('100%');
            Stack.backgroundColor(Color.Red);
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('hello world');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
...
loadDocument(new Index(undefined, {}));
...
```

如**Index对象分析**章节描述，Index对象继承前端框架的view对象（foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/state_mgmt_pu/src/lib/view.ts）：

```typescript
abstract class View extends NativeView implements
  ...
  protected abstract initialRender(): void;
  protected abstract rerender(): void;

  protected initialRenderView(): void {
    this.initialRender();

    // set propertyHasChanged_ for each @State, @Link (but not @Prop, @StorageProp)
    // state variable, from now on any change to the var will be notified 
    this.setStateSourcePropertiesUnchanged();
    this.setTwoWaySyncPropertiesUnchanged();
    this.setOneWaySyncPropertiesUnchanged();
  }
  ...
 
  // the current executed update function
  public observeComponentCreation(compilerAssignedUpdateFunc: UpdateFunc): void {
    ...
    compilerAssignedUpdateFunc(elmtId, /* is first rneder */ true);
    ...
  }
  ...
}
```

- 基类对象会使用initialRenderView函数包装initialRender函数。

另外，Index对象创建时会创建C++侧的JSViewPartialUpdate对象：

```C++
JSViewPartialUpdate::JSViewPartialUpdate(JSRef<JSObject> jsViewObject)
{
    jsViewFunction_ = AceType::MakeRefPtr<ViewFunctions>(jsViewObject);
    LOGD("JSView constructor");
    // keep the reference to the JS View object to prevent GC
    jsViewObject_ = jsViewObject;
}
```

- 在JSViewPartialUpdate对象构造函数中会创建ViewFunctions对象，入参为Index对象：

``` c++
ViewFunctions::ViewFunctions(const JSRef<JSObject>& jsObject)
{
    InitViewFunctions(jsObject, JSRef<JSFunc>(), true);
}

void ViewFunctions::InitViewFunctions(
    const JSRef<JSObject>& jsObject, const JSRef<JSFunc>& jsRenderFunction, bool partialUpdate)
{
    jsObject_ = jsObject;

    if (partialUpdate) {
        if (jsObject->GetProperty("initialRender")->IsFunction()) {
            JSRef<JSVal> jsRenderFunc = jsObject->GetProperty("initialRenderView");
            if (jsRenderFunc->IsFunction()) {
                jsRenderFunc_ = JSRef<JSFunc>::Cast(jsRenderFunc);
            } else {
                LOGE("View lacks mandatory 'initialRenderView()' function, fatal internal error.");
            }
        } else {
            LOGE("View lacks mandatory 'initialRender()' function, fatal internal error.");
        }

        JSRef<JSVal> jsRerenderFunc = jsObject->GetProperty("rerender");
        if (jsRerenderFunc->IsFunction()) {
            jsRerenderFunc_ = JSRef<JSFunc>::Cast(jsRerenderFunc);
        } else {
            LOGE("View lacks mandatory 'rerender()' function, fatal internal error.");
        }
    }
    ...
}
```

- ViewFunctions对象会通过Index对象，找到initialRenderView函数，并保存在ViewFunctions的jsRenderFunc_成员内，该成员在CustomNode通过回调函数调用JSViewPartialUpdate对象的InitialUIRender函数时会用到：

``` C++
RefPtr<NG::UINode> JSViewPartialUpdate::InitialUIRender()
{
    needsUpdate_ = false;
    RenderJSExecution();
    return NG::ViewStackProcessor::GetInstance()->Finish();
}

void JSView::RenderJSExecution()
{
    JAVASCRIPT_EXECUTION_SCOPE_STATIC;
    ...
    {
        ACE_SCORING_EVENT("Component.Build");
        jsViewFunction_->ExecuteRender();
    }
    ...
}

void ViewFunctions::ExecuteRender()
{
    if (jsRenderFunc_.IsEmpty()) {
        LOGE("no render function in View!");
        return;
    }

    auto func = jsRenderFunc_.Lock();
    JSRef<JSVal> jsThis = jsObject_.Lock();
    jsRenderResult_ = func->Call(jsThis);
}
```

### InitialUIRender函数传递

- 如**loadDocument方法分析**章节中提到，页面Index对象会调用JSViewPartialUpdate对象的CreateUINode方法生成自定义组件节点CustomNode，在创建CustomNode过程中，会将InitialUIRender函数作为函数闭包设置给CustomNode对象。

```C++
RefPtr<NG::UINode> JSViewPartialUpdate::CreateUINode()
{
    ...
    auto customNode = NG::CustomNode::CreateCustomNode(viewId, key);
    node_ = customNode;
    ...

    auto renderFunction = [weak = AceType::WeakClaim(this)]() -> RefPtr<NG::UINode> {
        auto jsView = weak.Upgrade();
        CHECK_NULL_RETURN(jsView, nullptr);
        if (!jsView->isFirstRender_) {
            LOGW("the js view has already called initial render");
            return nullptr;
        }
        jsView->isFirstRender_ = false;
        return jsView->InitialUIRender();
    };
    customNode->SetRenderFunction(renderFunction);
    ...
    return customNode;
}
```

- 在CustomNode对象的SetRenderFunction函数会将InitialUIRender函数设置给CustomNodePattern对象；
- CustomNodePattern对象会在布局算法创建的函数中将该InitialUIRender函数设置给自定义组件布局算法：

``` c++
class ACE_EXPORT CustomNode : public FrameNode {
    DECLARE_ACE_TYPE(CustomNode, FrameNode);

public:
    static RefPtr<CustomNode> CreateCustomNode(int32_t nodeId, const std::string& viewKey);
    ...
    void SetRenderFunction(const RenderFunction& renderFunction)
    {
        auto pattern = DynamicCast<CustomNodePattern>(GetPattern());
        if (pattern) {
            pattern->SetRenderFunction(renderFunction);
        }
    }
    ...
}

class CustomNodePattern : public Pattern {
    DECLARE_ACE_TYPE(CustomNodePattern, Pattern);

public:
    ...
    RefPtr<LayoutAlgorithm> CreateLayoutAlgorithm() override
    {
        if (isBuildDone_) {
            return MakeRefPtr<CustomNodeLayoutAlgorithm>(nullptr);
        }
        isBuildDone_ = true;
        return MakeRefPtr<CustomNodeLayoutAlgorithm>(renderFunction_);
    }

    void SetRenderFunction(const RenderFunction& renderFunction)
    {
        renderFunction_ = renderFunction;
    }
    ...
private:
    RenderFunction renderFunction_;
    bool isBuildDone_ = false;
};
```

### InitialUIRender函数执行创建子树结构

如前所述，InitialUIRender函数会执行ViewFunctions对象中的ExecuteRender函数，然后调用ViewStackProcessor获取生成的子树根节点。

``` c++
RefPtr<NG::UINode> JSViewPartialUpdate::InitialUIRender()
{
    needsUpdate_ = false;
    RenderJSExecution();
    return NG::ViewStackProcessor::GetInstance()->Finish();
}
```

ViewFunctions对象中的ExecuteRender函数执行时最终会调用Index对象的initialRender的js函数：

``` javascript
class Index extends View {
    ...
    initialRender() {
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Stack.create();
            Stack.height('100%');
            Stack.width('100%');
            Stack.backgroundColor(Color.Red);
            if (!isInitialRender) {
                Stack.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        this.observeComponentCreation((elmtId, isInitialRender) => {
            ViewStackProcessor.StartGetAccessRecordingFor(elmtId);
            Text.create('hello world');
            if (!isInitialRender) {
                Text.pop();
            }
            ViewStackProcessor.StopGetAccessRecording();
        });
        Text.pop();
        Stack.pop();
    }
    rerender() {
        this.updateDirtyElements();
    }
}
```

observeComponentCreation函数在基类View对象中进行了定义，主要用于分配组件id，执行函数闭包，保存执行完的函数闭包用于下次更新时执行。

``` typescript
  // the current executed update function
  public observeComponentCreation(compilerAssignedUpdateFunc: UpdateFunc): void {
    const elmtId = ViewStackProcessor.AllocateNewElmetIdForNextComponent();
    console.debug(`${this.constructor.name}[${this.id__()}]: First render for elmtId ${elmtId} start ....`);
    compilerAssignedUpdateFunc(elmtId, /* is first rneder */ true);

    this.updateFuncByElmtId.set(elmtId, compilerAssignedUpdateFunc);
    console.debug(`${this.constructor.name}[${this.id__()}]: First render for elmtId ${elmtId} - DONE.`);
  }
```

针对首次加载场景，可以简化为如下执行：

``` javascript
class Index extends View {
    ...
    initialRender() {
        ...
        Stack.create();
        ...
        Text.create('hello world');
        ...
        Text.pop();
        Stack.pop();
    }
    ...
}
```

Stack和Text的JS静态方法，在C++侧同样也是通过JS引擎接口注入相关定义，create方法最终会调用到StackView对象的Create方法和TextView对象的Create方法，pop方法最终会调用`JSContainerBase::Pop()`代码。（`JSClass<JSStack>::Inherit<JSContainerBase>()`C++侧声明js对象的继承结构）

``` c++
void JSStack::JSBind(BindingTarget globalObj)
{
    JSClass<JSStack>::Declare("Stack");

    MethodOptions opt = MethodOptions::NONE;
    JSClass<JSStack>::StaticMethod("create", &JSStack::Create, opt);
    ...
    JSClass<JSStack>::Inherit<JSContainerBase>();
    ...
}

void JSStack::Create(const JSCallbackInfo& info)
{
    ...
    if (Container::IsCurrentUseNewPipeline()) {
        NG::StackView::Create(alignment);
        return;
    }
    ...
}

void StackView::Create()
{
    ...
    auto frameNode =
        FrameNode::CreateFrameNode(V2::STACK_ETS_TAG, 0, AceType::MakeRefPtr<StackPattern>());
    ViewStackProcessor::GetInstance()->Push(frameNode);
    ...
}

void JSText::JSBind(BindingTarget globalObj)
{
    JSClass<JSText>::Declare("Text");
    MethodOptions opt = MethodOptions::NONE;
    JSClass<JSText>::StaticMethod("create", &JSText::Create, opt);
    ...
    JSClass<JSStack>::Inherit<JSContainerBase>();
    ...
}

void JSText::Create(const JSCallbackInfo& info)
{
	...
    if (Container::IsCurrentUseNewPipeline()) {
        NG::TextView::Create(data);
        return;
    }
	...
}

void TextView::Create(const std::string& content)
{
    ...
    auto* stack = ViewStackProcessor::GetInstance();
    auto frameNode = FrameNode::CreateFrameNode(V2::TEXT_ETS_TAG, 0, AceType::MakeRefPtr<TextPattern>());
    stack->Push(frameNode);
    ...
}

void JSContainerBase::Pop()
{
    if (Container::IsCurrentUseNewPipeline()) {
        NG::ViewStackProcessor::GetInstance()->PopContainer();
        return;
    }
    ViewStackProcessor::GetInstance()->PopContainer();
}

void JSContainerBase::JSBind()
{
    JSClass<JSContainerBase>::Declare("JSContainerBase");
    // staticmethods
    MethodOptions opt = MethodOptions::NONE;
    JSClass<JSContainerBase>::StaticMethod("pop", &JSContainerBase::Pop, opt);
    JSClass<JSContainerBase>::Inherit<JSViewAbstract>();
}
```

StackView和TextView在创建完UI组件后都会调用ViewStackProcessor对象的Push方法，JSContainerBase对象的Pop方法最终会调用ViewStackProcessor对象的Pop方法：

``` c++
void ViewStackProcessor::Push(const RefPtr<UINode>& element, bool isCustomView)
{
    ...
    elementsStack_.push(element);
}

void ViewStackProcessor::PopContainer()
{
    auto top = GetMainElementNode();
    // for container node.
    if (!top->IsAtomicNode()) {
        Pop();
        return;
    }
    ...
}

void ViewStackProcessor::Pop()
{
    if (elementsStack_.size() == 1) {
        return;
    }

    auto element = elementsStack_.top();
    elementsStack_.pop();
	...
    element->MountToParent(GetMainElementNode());
    LOGD("ViewStackProcessor Pop size %{public}zu", elementsStack_.size());
}

class ViewStackProcessor {
...
std::stack<RefPtr<UINode>> elementsStack_;  
...
}
```

从上面的代码可以看到，ViewStackProcessor通过压栈出栈的方式实现了UI子树结构的创建：

- StackView的Create方法中，将Stack组件压栈到`elementsStack_`中，`elementsStack_`当前数量为1；

- TextView的Create方法中，将Text组件压栈到`elementsStack_`中，`elementsStack_`当前数量为2；

- JSContainerBase对象的Pop方法（Text.pop()）中，将Text组件从`elementsStack_`中出栈，并把Text组件作为子节点挂载到`elementsStack_`的栈顶节点（Stack）

- JSContainerBase对象的Pop方法（Stack.pop()）中，由于当前Size为1，直接return不做处理；

- Stack最终会在执行完js侧的函数调用后，执行ViewStackProcessor的finish方法返回给InitialUIRender的调用方（Index自定义组件）：

  ```c++
  RefPtr<NG::UINode> JSViewPartialUpdate::InitialUIRender()
  {
      needsUpdate_ = false;
      RenderJSExecution();
      return NG::ViewStackProcessor::GetInstance()->Finish();
  }
  
  RefPtr<UINode> ViewStackProcessor::Finish()
  {
      if (elementsStack_.empty()) {
          LOGE("ViewStackProcessor Finish failed, input empty render or invalid root component");
          return nullptr;
      }
      auto element = elementsStack_.top();
      elementsStack_.pop();
      ...
      return element;
  }
  ```

  


