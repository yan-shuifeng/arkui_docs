## 如何编译NG新结构镜像并刷机验证应用

### 如何编译NG结构

#### 下载代码

按照OpenHarmony官网说明，下载源码：

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md

https://gitee.com/openharmony/docs/blob/master/en/device-dev/get-code/sourcecode-acquire.md

#### 编译代码

```bash
repo sync -c -j32 --no-tags
repo forall -c 'git lfs pull'
bash build/prebuilts_download.sh
./build.sh --product-name rk3568 --ccache
```

#### 扩展说明

- 编译生成用于vscode的clangd插件联想的compile_commands.json文件

  - 修改build/lite/hb_internal/build/build_process.py文件，增加--export-compile-commands命令：

    ```python
            gn_cmd = [
                gn_path,
                'gen',
                '--args={}'.format(" ".join(self._args_list)),
                '--export-compile-commands',
                self.config.out_path,
            ] + gn_args
    ```

    

#### 刷RK镜像

- 将编译生成的镜像包文件拷贝到window下：路径下out目录下，out/rk3568/images.tar.gz

- 执行cmd命令：

  ```bash
  tar -zxvf images.tar.gz
  ```

- 下载RK刷机工具，并按照指导进入Loader模式刷机：

  - 指导目录：https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97#https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/%E7%83%A7%E5%BD%95%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3.md
  - 刷机工具：https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97/windows
  - 刷机步骤：https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/%E7%83%A7%E5%BD%95%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3.md
    - 注意：新版本需要先导入config配置表，config配置表在生成的镜像文件中。

- 首次刷机后，可以通过push相关so加快调试，push命令：

  ```bash
  hdc wait-for-device shell mount -o remount,rw /
  hdc shell mount -o remount,rw /vendor
  hdc shell setenforce 0
  hdc file send xxx/libace.z.so /system/lib/libace.z.so
  hdc file send xxx/libace_engine_ark.z.so /system/lib/libace_engine_ark.z.so
  hdc file send xxx/libace_engine_declarative.z.so /system/lib/libace_engine_declarative.z.so
  hdc file send xxx/libace_engine_declarative_ark.z.so /system/lib/libace_engine_declarative_ark.z.so
  hdc file send xxx/libace_engine_pa_ark.z.so /system/lib/libace_engine_pa_ark.z.so
  hdc file send xxx/libace_engine_pa_qjs.z.so /system/lib/libace_engine_pa_qjs.z.so
  hdc file send xxx/libace_engine_qjs.z.so /system/lib/libace_engine_qjs.z.so
  hdc file send xxx/libace_engine_qjs_debug.z.so /system/lib/libace_engine_qjs_debug.z.so
  hdc file send xxx/libace_ndk.z.so /system/lib/libace_ndk.z.so
  hdc file send xxx/libace_uicontent.z.so /system/lib/libace_uicontent.z.so
  hdc file send xxx/libark_debugger.z.so /system/lib/libark_debugger.z.so
  hdc file send xxx/libconfiguration.z.so /system/lib/module/libconfiguration.z.so
  hdc file send xxx/libdevice.z.so /system/lib/module/libdevice.z.so
  hdc file send xxx/libgrid.z.so /system/lib/module/libgrid.z.so
  hdc file send xxx/libhdc_register.z.so /system/lib/libhdc_register.z.so
  hdc file send xxx/libintl_qjs.z.so /system/lib/libintl_qjs.z.so
  hdc file send xxx/libmediaquery.z.so /system/lib/module/libmediaquery.z.so
  hdc file send xxx/libplugincomponent.z.so /system/lib/module/libplugincomponent.z.so
  hdc file send xxx/libprompt.z.so /system/lib/module/libprompt.z.so
  hdc file send xxx/librouter.z.so /system/lib/module/librouter.z.so
  hdc file send xxx/libui_service_mgr.z.so /system/lib/libui_service_mgr.z.so
  hdc file send xxx/libuiservice.z.so /system/lib/libuiservice.z.so
  hdc shell reboot
  ```

  

#### 使用最小化更新的SDK替换IDE内置的API9的SDK

- 从赖波的提交上获取门禁版本：[Update for Partial-Update · Pull Request !886 · OpenHarmony/developtools_ace-ets2bundle - Gitee.com](https://gitee.com/openharmony/developtools_ace-ets2bundle/pulls/886)

  ![](figures/sdk_download.png)

- 解压后替换IDE中的SDK：

  ![](figures/sdk_ets.png)

  - 解压文件，在解压得到的文件中获取版本信息，并用该版本信息重命名文件夹：

    ![](figures/版本号.png)

    ​	

  - 将重命名的文件夹放到IDE的SDK目录中，完成后可以从IDE的SDK管理中看到当前生效的版本：

    ![](figures/sdk2.png)

    ![](figures/sdk1.png)

  - 使用cmd命令：npm install重新编译SDK。（如果npm命令找不到，从官网下载npm进行安装：[Node.js (nodejs.org)](https://nodejs.org/en/)）(具体可以咨询赖波)

    ![](figures/sdk4.png)

  - 使用SDK9创建并编译安装SDK9的应用：

    ![](figures/sdk3.png)

#### 安装应用，使能NG流程

- 当前NG流程代码通过包名进行动态选择，针对指定包名启动NG流程：

  ```bash
  hdc shell
  param set persist.ace.newpipe.pkgname com.example.myapplication
  param set persist.ace.partial.pkgname com.example.myapplication
  ```
  
  com.example.myapplication为指定需要采用NG流程的应用包名
  
  上述命令执行后，重新打开应用就可以使用NG流程。
  
  可以通过日志看到当前应用的启动方式：（new pipeline标识NG新框架流程）
  
  ![](figures/sdk5.png)

##### 补充说明

针对日志丢失问题，可以输入一下命令仅打开ACE的日志

```bash
hdc shell
hilog -b X
hilog -b D -D 0xD003900
hilog -b D -D 0xD003B00
hilog -p off
hilog -Q domainoff
hilog -Q pidoff
hilog -G 500M
hilog -r
```

Trace工具命令：

```
hdc shell setenforce 0
hdc shell "bytrace -t 4 -b 16384 --overwrite sched freq ace app disk ohos graphic sync workq ability > /data/mynewtrace.ftrace"
hdc shell "sed -i '1,2d' /data/mynewtrace.ftrace"
hdc file recv /data/mynewtrace.ftrace .
```

