# 单树结构diff设计

## 背景

在当前UI结构上，ArkUI通过三棵树模型（Component、element和render）实现数据驱动的UI更新功能，如下图所示：

![](figures/三棵树工作原理.png)

在UI首次创建时，会先生成Component树，基于Component树会生成最初的element树和render树，render树在生成时会忽略非渲染节点对象（Composed），并且RenderNode对象本身会保存在element中方便下次更新。另外，Composed对象会生成页面唯一的id标识，用于后续的更新。

在更新场景下，ArkUI当前设计上以自定义组件（@Component）为最小维度，当自定义组件内部状态变量改变时会重新触发build构建生成新的component子树，在生成子树时原先分配的id会保留，新生成的component子树会通过该id找到对应的element节点，element节点会将老的子树接口和最新生成的子树结构进行diff对比：

- 如果在新的Component子树节点上树形结构发生了变换（新增或者删除子节点），则将自身element的子节点也进行相应的操作（新增或者删除），并同步更新Render子树。
- 如果在新的Component子树节点上树形结构未发生变换，则将新的Component中生成的属性更新到对应的render节点上，实现属性刷新。

总体来看，三颗树分别承载如下功能：

- Component：每次创建或者更新时都会重新生成相应的子树结构，成员方法提供了创建element和render节点，成员变量保存相应的属性值；
- Element：维持UI组件树形结构，承载diff任务，在新的Component子树生成并请求更新时，会基于老的树形结构进行diff来实现树形结构更新和渲染节点属性更新；
- Render：承载布局渲染任务，保存Component结构中的属性值，基于保存的属性值驱动内容布局和渲染。

三个树模型解决了声明式范式下组件diff的难题，不过也带来了一些缺陷：

- 额外的树形结构会带来额外的内存开销；

- 在列表滑动等实时性要求严苛的场景下，列表动态创建相应的列表项时需要创建更多的对象（component、element和render），同时属性值也需要进行多次拷贝赋值（属性值在创建component组件时会先复制到component内的成员中，在element和render节点创建完成后，上述的属性值会再次拷贝到render节点中以便进行内容布局和绘制），复杂场景下可能带来帧率的下降。

## 新设计带来的新思考

当前ArkUI的更新范围以自定义组件为界，当自定义组件内部存在复杂的UI描述结构时，每次更新重新执行build会带来更多的开销。为此ArkUI进一步优化了状态管理的架构设计，引入了最小化更新方案，通过建立UI组件和状态变量的依赖关系来实现更快的更新性能，该方案主要引入了如下几个机制（[最小化更新方案](https://gitee.com/guido_grassel/ace_reports/blob/master/partialUpdateSolutionDesign.md)）：

- 组件标识能力除了自定义组件外也覆盖到其他UI组件上，每个UI组件在生成时都会赋予页面唯一的id；
- 在执行构建过程中，通过是否读取状态变量为条件建立UI组件和状态变量的依赖；
- 当状态变量改变时，基于上面建立的依赖关系找到具体需要更新的UI组件id，通过该id驱动后端进行更新。

由于最小化更新方案下，组件的diff在前端进行了处理，后端的element树不再承载diff任务，仅仅作为联系component和render的功能，故可以进一步优化三棵树结构，提升UI树构建的性能。

## 单树结构模型

### 概述

![](figures/单树diff说明.png)

- FrameNode节点承载原来的component和render功能，避免了属性值在多个节点对象上的拷贝操作，element节点在最小化更新方案下承载component和render的纽带也由于component和render归一后不再需要，故element节点可以删除。
- 在状态改变时，由于最小化更新方案实现了id和变更的关联，故UI框架可以根据id快速找到后端对应的FrameNode节点进行组件树更新和属性更新。