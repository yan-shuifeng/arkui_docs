# ArkUI组件从老框架迁移到新框架介绍

针对存量ArkUI组件，本文介绍如何从老框架迁移到新框架上，主要以基础组件的Text组件和容器组件的Swiper组件进行介绍。

## Text组件迁移

### Text组件老结构分析

Text组件在老结构上包括TextComponent、TextElement和RenderText（RosenRenderText、FlutterRenderText）：

- TextComponent：主要用来设置Text组件的属性、事件，对应新框架上的LayoutProperty、PaintProperty、EventHub的子类；
- TextElement：主要用于Text树形结构的创建、维护和diff能力，在新框架上由框架实现，不需要额外新建类；
- RosenRenderText：主要包括从TextComponent中获取属性值和事件，进行布局计算和文本绘制，同时处理Text组件相关的事件，在新框架上分别对于LayoutAlgorithm的子类（布局），NodePaintMethod的子类（绘制），EventHub的子类（事件）。

### TextComponent对象迁移

对象分析：

``` mermaid
classDiagram
class TextComponent {
	std::optional~TextAlign~ alignment_
	RefPtr~TextDeclaration~ declaration_
}
class TextDeclaration {
	std::shared_ptr~TextSpecializedStyle~ TextSpecializedStyle_
	std::shared_ptr~TextSpecializedAttribute~ TextSpecializedAttribute_
}
class TextSpecializedStyle {
	TextStyle textStyle
    Color focusColor
    CopyOption copyOption
    bool isMaxWidthLayout
    bool autoMaxLines
}
class TextSpecializedAttribute {
	std::string data
}
class TextStyle {
	std::vector<std::string> fontFamilies_
    Dimension fontSize_
    Dimension lineHeight_
    FontWeight fontWeight_
    FontStyle fontStyle_
    TextBaseline textBaseline_
    Dimension baselineOffset_
    TextOverflow textOverflow_
    TextAlign textAlign_
    Color textColor_
    Color textDecorationColor_
    TextDecoration textDecoration_
    uint32_t maxLines_
    TextCase textCase_
}
TextComponentV2 --|> TextComponent
TextComponent *-- TextDeclaration
TextDeclaration *-- TextSpecializedAttribute
TextDeclaration *-- TextSpecializedStyle
TextSpecializedStyle *-- TextStyle
```

#### 属性组对象创建

从类图上可以看到TextComponent属性主要分布在TextSpecializedStyle和TextSpecializedAttribute中，这边基于上述两个对象并以SDK接口为标准，创建Text组件的属性组对象TextParagraph：

```C++
class TextParagraph {
public:
    TextParagraph() = default;
    TextParagraph(std::string content, const FontStyle& fontStyle, const TextLineStyle& textLineStyle)
        : content_(std::move(content)), propFontStyle_(std::make_unique<FontStyle>(fontStyle)),
          propTextLineStyle_(std::make_unique<TextLineStyle>(textLineStyle))
    {}
    explicit TextParagraph(std::string content) : content_(std::move(content)) {}
    ~TextParagraph() = default;
   ...
    void Reset()
    {
        content_.clear();
        propFontStyle_.reset();
        propTextLineStyle_.reset();
    }
    ...
    ACE_DEFINE_PARAGRAPH_PROPERTY(FontStyle, FontSize, Dimension);
    ACE_DEFINE_PARAGRAPH_PROPERTY(FontStyle, TextColor, Color);
    ACE_DEFINE_PARAGRAPH_PROPERTY(FontStyle, ItalicFontStyle, ItalicFontStyle);
    ACE_DEFINE_PARAGRAPH_PROPERTY(FontStyle, FontWeight, FontWeight);
    ACE_DEFINE_PARAGRAPH_PROPERTY(FontStyle, FontFamily, std::vector<std::string>);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, LineHeight, Dimension);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextBaseline, TextBaseline);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, BaselineOffset, Dimension);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextAlign, TextAlign);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextCase, TextCase);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextDecorationColor, Color);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextDecoration, TextDecoration);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, TextOverflow, TextOverflow);
    ACE_DEFINE_PARAGRAPH_PROPERTY(TextLineStyle, MaxLines, uint32_t);
    ...
};
```

（Text属性组比较复杂，一般情况下建议使用struct结构体定义属性对象，使用ACE_DEFINE_PROPERTY_GROUP_ITEM宏进行定义，例如foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_styles.h）

#### TextLayoutProperty对象创建

建立完属性组之后，可以创建LayoutProperty布局属性集合TextLayoutProperty，并通过ACE_DEFINE_PROPERTY_GROUP宏将属性组作为自身的成员，通过ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP向前端暴露属性设置方法和脏区标记flag：

```c++
class ACE_EXPORT TextLayoutProperty : public LayoutProperty {
    DECLARE_ACE_TYPE(TextLayoutProperty, LayoutProperty);

public:
    TextLayoutProperty() = default;
    ~TextLayoutProperty() override = default;
    RefPtr<LayoutProperty> Clone() const override
    {
        auto value = MakeRefPtr<TextLayoutProperty>();
        value->LayoutProperty::UpdateLayoutProperty(DynamicCast<LayoutProperty>(this));
        value->propTextParagraph_ = CloneTextParagraph();
        return value;
    }
    void Reset() override
    {
        LayoutProperty::Reset();
        ResetTextParagraph();
    }

    ACE_DEFINE_PROPERTY_GROUP(TextParagraph, TextParagraph);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, Content, std::string, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, FontSize, Dimension, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextColor, Color, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, ItalicFontStyle, ItalicFontStyle, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, FontWeight, FontWeight, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, FontFamily, std::vector<std::string>, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, LineHeight, Dimension, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextBaseline, TextBaseline, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, BaselineOffset, Dimension, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextAlign, TextAlign, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextOverflow, TextOverflow, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, MaxLines, uint32_t, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextDecoration, TextDecoration, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextDecorationColor, Color, PROPERTY_UPDATE_MEASURE);
    ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP(TextParagraph, TextCase, TextCase, PROPERTY_UPDATE_MEASURE);
	...
};
```

#### TextPaintProperty对象创建

由于文本布局和绘制均需要重新创建Paragraph对象，故文本的属性值均放在TextLayoutProperty对象中，不需要额外创建TextPaintProperty对象。

#### 事件对接待补充

#### Pattern对象创建

在创建完相应的LayoutProperty布局属性集合、PaintProperty绘制属性集合后，可以创建相应的TextPattern对象：

```c++
class TextPattern : public Pattern {
    DECLARE_ACE_TYPE(TextPattern, Pattern);

public:
    TextPattern() = default;
    ~TextPattern() override = default;

    RefPtr<LayoutProperty> CreateLayoutProperty() override
    {
        return MakeRefPtr<TextLayoutProperty>();
    }

    // 由于Text组件可以包含Span子节点，故需要重写IsAtomicNode，返回false。
    bool IsAtomicNode() const override
    {
        return false;
    }
};
```

#### 前端对接

创建完相应的属性集合和Pattern对象后，可以开始对接XXXView前端：

##### C++层接口对象-TextView

创建TextView头文件，按照SDK接口维度提供相应的组件创建和属性设置方法：

```c++
class ACE_EXPORT TextView {
public:
    static void Create(const std::string& content);
    static void FontSize(const Dimension& value);
    static void TextColor(const Color& value);
    static void ItalicFontStyle(const ItalicFontStyle& value);
    static void FontWeight(const FontWeight& value);
    static void FontFamily(const std::vector<std::string>& value);
    static void TextAlign(const TextAlign& value);
    static void TextOverflow(const TextOverflow& value);
    static void MaxLines(const uint32_t& value);
    static void LineHeight(const Dimension& value);
    static void TextDecoration(const TextDecoration& value);
    static void TextDecorationColor(const Color& value);
    static void BaselineOffset(const Dimension& value);
    static void TextCase(const TextCase& value);
};
```

创建TextView源文件，使用`FrameNode::GetOrCreateFrameNode`方法创建UI组件并推送到ViewStackProcessor中：

```c++
void TextView::Create(const std::string& content)
{
    auto* stack = ViewStackProcessor::GetInstance();
    // 需要从stack获取当前分配给该组件的唯一标识ID，用于获取的Diff更新。
    auto nodeId = stack->ClaimNodeId();
    // 使用GetOrCreateFrameNode方法并传入TextPattern的创建方法，当对应的id不存在组件时会先调用TextPattern的创建方法创建Pattern对象，然后再创建FrameNode对象，并将该对象和ID进行绑定。
    auto frameNode =
        FrameNode::GetOrCreateFrameNode(V2::TEXT_ETS_TAG, nodeId, []() { return AceType::MakeRefPtr<TextPattern>(); });
    stack->Push(frameNode);

    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, Content, content);
}
```

使用ACE_UPDATE_LAYOUT_PROPERTY宏快速实现针对TextLayoutProperty的属性设置：（使用ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP定义的属性，可以在TextView的文件中使用ACE_UPDATE_LAYOUT_PROPERTY宏快速进行设置对接）

```c++
void TextView::FontSize(const Dimension& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, FontSize, value);
}

void TextView::TextColor(const Color& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextColor, value);
}

void TextView::ItalicFontStyle(const NG::ItalicFontStyle& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, ItalicFontStyle, value);
}

void TextView::FontWeight(const Ace::FontWeight& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, FontWeight, value);
}

void TextView::FontFamily(const std::vector<std::string>& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, FontFamily, value);
}

void TextView::TextAlign(const Ace::TextAlign& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextAlign, value);
}

void TextView::TextOverflow(const Ace::TextOverflow& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextOverflow, value);
}

void TextView::MaxLines(const uint32_t& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, MaxLines, value);
}

void TextView::LineHeight(const Dimension& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, LineHeight, value);
}

void TextView::TextDecoration(const Ace::TextDecoration& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextDecoration, value);
}

void TextView::TextDecorationColor(const Color& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextDecorationColor, value);
}

void TextView::BaselineOffset(const Dimension& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, BaselineOffset, value);
}

void TextView::TextCase(const Ace::TextCase& value)
{
    ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, TextCase, value);
}
```

##### JS层接口对接

创建完TextView的C++层接口后，JS层接口（js_text.cpp）可以使用`Container::IsCurrentUseNewPipeline()`方法快速迁移到TextView对应的接口中：

```c++
void JSText::Create(const JSCallbackInfo& info)
{
    std::string data;
    if (info.Length() > 0) {
        ParseJsString(info[0], data);
    }

    if (Container::IsCurrentUseNewPipeline()) {
        NG::TextView::Create(data);
        return;
    }
	...
}

void JSText::SetFontSize(const JSCallbackInfo& info)
{
    if (info.Length() < 1) {
        LOGE("The argv is wrong, it is supposed to have at least 1 argument");
        return;
    }
    Dimension fontSize;
    if (!ParseJsDimensionFp(info[0], fontSize)) {
        return;
    }
    if (Container::IsCurrentUseNewPipeline()) {
        NG::TextView::FontSize(fontSize);
        return;
    }
	...
}
```

### RenderText对象迁移

#### 创建TextLayoutAlgorithm对象迁移RenderText中的布局测试方法

RenderText的布局方法在PerformLayout函数内：

```c++
void RenderText::PerformLayout()
{
    ...
    Size size = Measure();
    SetLayoutSize(GetLayoutParam().Constrain(size));
	...
}

Size RosenRenderText::Measure()
{
    if (!text_ || CheckMeasureFlag()) {
        return GetSize();
    }
    textStyle_.SetMaxLines(maxLines_);
    lastLayoutMaxWidth_ = GetLayoutParam().GetMaxSize().Width();
    lastLayoutMinWidth_ = GetLayoutParam().GetMinSize().Width();
    lastLayoutMaxHeight_ = GetLayoutParam().GetMaxSize().Height();
    lastLayoutMinHeight_ = GetLayoutParam().GetMinSize().Height();
    if (!textStyle_.GetAdaptTextSize()) {
        if (!UpdateParagraph()) {
            LOGE("fail to initialize text paragraph");
            return Size();
        }
        paragraph_->Layout(lastLayoutMaxWidth_);
    } else {
        if (!AdaptTextSize(lastLayoutMaxWidth_)) {
            LOGE("fail to initialize text paragraph in adapt text size step");
            return Size();
        }
    }
    needMeasure_ = false;
    // If you need to lay out the text according to the maximum layout width given by the parent, use it.
    if (text_->GetMaxWidthLayout()) {
        paragraphNewWidth_ = GetLayoutParam().GetMaxSize().Width();
        return GetSize();
    }
    // The reason for the second layout is because the TextAlign property needs the width of the layout,
    // and the width of the second layout is used as the width of the TextAlign layout.
    if (!NearEqual(lastLayoutMinWidth_, lastLayoutMaxWidth_)) {
        paragraphNewWidth_ = std::clamp(GetTextWidth(), lastLayoutMinWidth_, lastLayoutMaxWidth_);
        if (!NearEqual(paragraphNewWidth_, paragraph_->GetMaxWidth())) {
            ApplyIndents(paragraphNewWidth_);
            paragraph_->Layout(std::ceil(paragraphNewWidth_));
        }
    }
    EffectAutoMaxLines();
    return GetSize();
}
```

将PerformLayout函数方法迁移到TextLayoutAlgorithm的MeasureContent方法中：（对于新框架来说，文本大小测算属于内容区测算，需要重写LayoutAlgorithm的MeasureContent方法），对于Padding和Marrgin等边距和对齐方式计算，原先承载在RenderBox中，新框架需要继承BoxLayoutAlgorithm：

```c++
// 继承BoxLayoutAlgorithm实现原先RenderBox的布局功能。
class ACE_EXPORT TextLayoutAlgorithm : public BoxLayoutAlgorithm {
    DECLARE_ACE_TYPE(TextLayoutAlgorithm, BoxLayoutAlgorithm);

public:
    TextLayoutAlgorithm();
    ~TextLayoutAlgorithm() override = default;
    void OnReset() override;
	
    // 重写MeasureContent实现文本内容大小测试。
    std::optional<SizeF> MeasureContent(
        const LayoutConstraintF& contentConstraint, LayoutWrapper* layoutWrapper) override;

    const RefPtr<Paragraph>& GetTxtParagraph();

private:
    bool CreateParagraph(const TextStyle& textStyle, const RefPtr<PipelineContext>& context, std::string content);
    TextDirection GetTextDirection(const std::string& content);
    double GetTextWidth() const;

    RefPtr<Paragraph> paragraph_;

    ACE_DISALLOW_COPY_AND_MOVE(TextLayoutAlgorithm);
};
```

编写TextLayoutAlgorithm的源文件，迁移老框架PerformLayout的功能：

```c++
std::optional<SizeF> TextLayoutAlgorithm::MeasureContent(
    const LayoutConstraintF& contentConstraint, LayoutWrapper* layoutWrapper)
{
    auto frameNode = layoutWrapper->GetHostNode();
    CHECK_NULL_RETURN(frameNode, std::nullopt);
    auto pipeline = frameNode->GetContext();
    CHECK_NULL_RETURN(pipeline, std::nullopt);
    // 从LayoutWrapper中获得布局属性集合对象，DynamicCast到TextLayoutProperty获取文本相关属性值。
    auto textLayoutProperty = DynamicCast<TextLayoutProperty>(layoutWrapper->GetLayoutProperty());
    CHECK_NULL_RETURN(textLayoutProperty, std::nullopt);
    const auto& textParagraph = textLayoutProperty->GetTextParagraph();
    auto themeManager = pipeline->GetThemeManager();
    TextStyle textStyle =
        textParagraph
            ? CreateTextStyleUsingTheme(textParagraph->GetFontStyle(), textParagraph->propTextLineStyle_,
                  themeManager ? themeManager->GetTheme<TextTheme>() : nullptr)
            : CreateTextStyleUsingTheme(nullptr, nullptr, themeManager ? themeManager->GetTheme<TextTheme>() : nullptr);
    if (!CreateParagraph(textStyle, pipeline, textParagraph ? textParagraph->content_ : "")) {
        return std::nullopt;
    }
    if (contentConstraint.selfIdealSize.has_value() && NonNegative(contentConstraint.selfIdealSize->Width())) {
        paragraph_->Layout(contentConstraint.selfIdealSize->Width());
    } else {
        paragraph_->Layout(contentConstraint.maxSize.Width());
    }
    auto height = static_cast<float>(paragraph_->GetHeight());
    double baselineOffset = 0.0F;
    textStyle.GetBaselineOffset().NormalizeToPx(
        pipeline->GetDipScale(), pipeline->GetFontScale(), pipeline->GetLogicScale(), height, baselineOffset);
    float heightFinal =
        std::min(static_cast<float>(height + std::fabs(baselineOffset)), contentConstraint.maxSize.Height());
    return SizeF(static_cast<float>(GetTextWidth()), heightFinal);
}
```

#### TextPattern集成TextLayoutAlgorithm对象

完成TextLayoutAlgorithm对象功能开发后，可以重写Pattern的CreateLayoutAlgorithm方法，返回TextLayoutAlgorithm对象用于文本布局任务：

```c++
class TextPattern : public Pattern {
    DECLARE_ACE_TYPE(TextPattern, Pattern);

public:
    TextPattern() = default;
    ~TextPattern() override = default;

    RefPtr<LayoutProperty> CreateLayoutProperty() override
    {
        return MakeRefPtr<TextLayoutProperty>();
    }

    RefPtr<LayoutAlgorithm> CreateLayoutAlgorithm() override
    {
        return MakeRefPtr<TextLayoutAlgorithm>();
    }

    bool IsAtomicNode() const override
    {
        return false;
    }
};
```

重写Pattern的OnDirtyLayoutWrapperSwap回调函数，在布局任务完成后，获取Paragraph对象用于触发后续绘制任务：

```c++
class TextPattern : public Pattern {
    DECLARE_ACE_TYPE(TextPattern, Pattern);

public:
    TextPattern() = default;
    ~TextPattern() override = default;

    RefPtr<LayoutProperty> CreateLayoutProperty() override
    {
        return MakeRefPtr<TextLayoutProperty>();
    }

    RefPtr<LayoutAlgorithm> CreateLayoutAlgorithm() override
    {
        return MakeRefPtr<TextLayoutAlgorithm>();
    }

    bool IsAtomicNode() const override
    {
        return false;
    }

private:
    bool OnDirtyLayoutWrapperSwap(const RefPtr<LayoutWrapper>& dirty, bool skipMeasure, bool skipLayout) override;

    RefPtr<Paragraph> paragraph_;
};

bool TextPattern::OnDirtyLayoutWrapperSwap(const RefPtr<LayoutWrapper>& dirty, bool skipMeasure, bool skipLayout)
{
    // 当该次布局任务属于忽略测试或者忽略内容区测算大小时，可以认为文本内容没有发生改变，不需要重写绘制，返回false。
    if (skipMeasure || dirty->SkipMeasureContent()) {
        return false;
    }
    auto layoutAlgorithmWrapper = DynamicCast<LayoutAlgorithmWrapper>(dirty->GetLayoutAlgorithm());
    CHECK_NULL_RETURN(layoutAlgorithmWrapper, false);
    auto textLayoutAlgorithm = DynamicCast<TextLayoutAlgorithm>(layoutAlgorithmWrapper->GetLayoutAlgorithm());
    CHECK_NULL_RETURN(textLayoutAlgorithm, false);
    // 从布局完成的LayoutWrapper中获取Paragraph对象保存起来，用于后续绘制任务使用，这边需要返回true让框架触发绘制任务创建和执行。
    paragraph_ = textLayoutAlgorithm->GetTxtParagraph();
    return true;
}
```

#### 创建TextPaintMethod对象迁移RosenRenderText中的绘制方法

RosenRenderText的绘制方法在Paint函数中：

```c++
void RosenRenderText::Paint(RenderContext& context, const Offset& offset)
{
	...
    auto canvas = static_cast<RosenRenderContext*>(&context)->GetCanvas();
    auto rsNode = static_cast<RosenRenderContext*>(&context)->GetRSNode();
    if (!canvas || !rsNode || !paragraph_) {
        LOGE("Paint canvas or paragraph is null");
        return;
    }
    rsNode->SetPaintOrder(true);
    RenderNode::Paint(context, offset);
	...
    paragraph_->Paint(canvas, newX, newY);
}
```

将Paint内容迁移到TextPaintMethod对象中的GetContentPaintImpl函数中：

```c++
class ACE_EXPORT TextPaintMethod : public NodePaintMethod {
    DECLARE_ACE_TYPE(TextPaintMethod, NodePaintMethod)
public:
    explicit TextPaintMethod(const RefPtr<Paragraph>& paragraph) : paragraph_(paragraph) {}
    ~TextPaintMethod() override = default;

    CanvasDrawFunction GetContentPaintImpl(PaintWrapper* paintWrapper) override
    {
        CHECK_NULL_RETURN(paragraph_, nullptr);
        // 内容区大小和位置信息从PaintWrapper对象中获取。
        auto offset = paintWrapper->GetContentOffset();
        // 这边需要返回的是绘制任务闭包，将绘制任务包装到闭包内，Canvas对象有闭包参数传入。
        return [paragraph = paragraph_, offset](
                   const RefPtr<Canvas>& canvas) { paragraph->Paint(canvas, offset.GetX(), offset.GetY()); };
    }

private:
    RefPtr<Paragraph> paragraph_;
};
```

#### TextPattern集成TextPaintMethod对象

完成TextPaintMethod对象功能开发后，可以重写Pattern的CreateNodePaintMethod方法，返回TextPaintMethod对象用于文本绘制任务：

```c++
class TextPattern : public Pattern {
    DECLARE_ACE_TYPE(TextPattern, Pattern);

public:
    TextPattern() = default;
    ~TextPattern() override = default;

    RefPtr<NodePaintMethod> CreateNodePaintMethod() override
    {
        return MakeRefPtr<TextPaintMethod>(paragraph_);
    }

    RefPtr<LayoutProperty> CreateLayoutProperty() override
    {
        return MakeRefPtr<TextLayoutProperty>();
    }

    RefPtr<LayoutAlgorithm> CreateLayoutAlgorithm() override
    {
        return MakeRefPtr<TextLayoutAlgorithm>();
    }

    bool IsAtomicNode() const override
    {
        return false;
    }

private:
    bool OnDirtyLayoutWrapperSwap(const RefPtr<LayoutWrapper>& dirty, bool skipMeasure, bool skipLayout) override;

    RefPtr<Paragraph> paragraph_;
};
```

#### 事件迁移待补充

### TDD用例集成

（待补充）