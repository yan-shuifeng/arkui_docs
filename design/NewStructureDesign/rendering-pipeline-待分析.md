## 渲染管线 / Rendering pipeline

### 渲染管线结构 / Rendering pipeline structure

![](./figures/渲染管线结构.png)

### 任务说明 / Task description

- Animation：动画阶段。在动画过程中会修改相应的FrameNode节点触发脏区标记，在特定场景下会执行用户侧ets代码实现自定义动画；
- Event：事件处理阶段，比如手势事件处理。在手势处理过程中也会修改FrameNode节点触发脏区标记，在特定场景下会执行用户侧ets代码实现自定义事件；

- Rebuild：自定义组件（@Component）在首次创建挂载或者状态变量变更时会标记为需要rebuild状态，在下一次Vsync过来时会执行rebuild流程，rebuild流程会执行程序UI代码，通过调用View的方法生成相应的组件树结构和属性样式修改任务。
- CreateUITask：当FrameNode节点标记为脏区后会根据修改类型创建相应的Wrapper工具类用于执行相应的布局和渲染任务。
- Measure：布局包装器执行相关的大小测算任务。
  - 测算完成后会继续执行布局任务。
  - 特定场景下，比如自定义组件的测算任务，懒加载组件的测算任务会执行用户侧ets代码，动态生成和测算相应的节点子树。
- Layout：布局包装器执行相关的布局任务。
  - 布局完成后会生成geometryNode，用于绘制任务时的大小位置信息获取。
- Render：绘制任务包装器执行相关的绘制任务，执行完成后会标记请求刷新RSNode绘制
- FlushMessage：通过RSUIDirector请求刷新界面绘制。

**English**

- Animation: Animation stage. During the animation process, the corresponding FrameNode node will be modified to trigger the dirty area mark, and the user-side ets code will be executed to achieve custom animation in specific scenes.
- Event: The event processing stage, such as gesture event processing. During the gesture processing process, the FrameNode node will also be modified to trigger the dirty area mark, and the user-side ets code will be executed to implement custom events in specific scenarios.

- Rebuild: The custom component (@Component) will be marked as needing to be rebuilt when the mount is created for the first time or the state variable is changed. The rebuild process will be executed the next time Vsync comes over. The rebuild process will execute the program UI code and generate the corresponding component tree structure and attribute style modification tasks by calling the View method.
- CreateUITask: When the FrameNode node is marked as a dirty area, the corresponding Wrapper tool class will be created according to the modification type to perform the corresponding layout and rendering tasks.
- Measure: The layout wrapper performs related size measurement tasks.
  - After the calculation is completed, the layout task will continue.
  - In specific scenarios, such as the calculation task of custom components, the calculation task of lazy loading components will execute the user-side ets code, dynamically generate and calculate the corresponding node subtree.
- Layout: The layout wrapper performs related layout tasks.
  - After the layout is completed, a geometryNode will be generated, which is used to obtain the size and position information when drawing the task.
- Render: The drawing task wrapper performs related drawing tasks, and after the execution is complete, it will mark the request to refresh the RSNode drawing.
- FlushMessage: Request to refresh the interface drawing through RSUIDirector.

## 线程模型 / Thread model

![](./figures/线程模型结构.png)

- Main Thread：主线程，包括脚本运行、事件循环、生命周期、命中测试、UI组件创建挂载和属性刷新等。
  - 当UI组件标记为脏区后（刷新刷新、动画、事件等Modifier修改任务）会生成相应的UI任务（LayoutTask、RenderTask）。
- UI TaskPool：UI任务池，用于执行UI任务。
  - 针对可以运行在不同线程的UI任务，进行多线程平行处理。
  - 针对同线程不同优先级的UI任务，可以设计并发模型进行任务并发。

**English**

- Main Thread: The main thread, including script running, event loop, life cycle, hit testing, UI component creation and mounting, and property refresh.
  - When the UI component is marked as a dirty area (refresh, animation, event, modifier modification tasks), the corresponding UI tasks (LayoutTask, RenderTask) will be generated.
- UI TaskPool: UI task pool, used to perform UI tasks.
  - Multithreaded parallel processing for UI tasks that can run on different threads.
  - For UI tasks with different priorities in the same thread, a concurrency model can be designed for task concurrency.

### UI任务 / UI task

#### 任务包装器 / Task wrapper

包装器主要用于封装任务，便于后续优先级队列分发和多线程处理。

##### LayoutWrapper

提供布局相关的任务封装，实现任务执行时上下文环境隔离。

The wrapper is mainly used to encapsulate tasks to facilitate subsequent priority queue distribution and multithreading processing.

![](./figures/6.png)

输入：

- LayoutProperty：布局相关属性集合，比如长宽、对齐方式等；
- LayoutAlgorithm：布局算法，比如文本布局、线性布局等；

输出：

- GeometryNode：组件大小位置信息集合。

**English**

**Input：**
- LayoutProperty: A collection of layout-related attributes, such as length, width, alignment, etc.
- LayoutAlgorithm: Layout algorithms, such as text layout, linear layout, etc.

**Output:**
- GeometryNode: A collection of component size and location information.


##### RenderWrapper

提供绘制相关的任务封装，实现任务执行时上下文环境隔离。

Provide drawing-related task encapsulation to achieve context isolation during task execution.

![](./figures/7.png)

输入：

- RenderProperty：绘制相关属性集合，比如进度条轨道颜色等；
- ContentPaintImpl：内容区绘制方法，比如文本绘制等；
- RenderContext：绘制上下文，比如提供skia的获取等；

输出：

- GeometryNode：组件大小位置信息集合。

**English**

**Input：**
- RenderProperty: Draw a collection of related attributes, such as the color of the progress bar track, etc.
- ContentPaintImpl: Content area drawing method, such as text drawing, etc.
- RenderContext: Drawing context, such as providing skia acquisition, etc.

**Output：**
- GeometryNode: A collection of component size and location information.


#### 任务创建-组件创建挂载场景 / Task creation-Component creation mount scene

![](./figures/组件创建挂载任务.png)

- @Component标识的自定义组件内部的build函数中的UI描述在自定义组件挂载时不会立即执行；
- 自定义组件挂载到FrameNode的主树上后会标记脏区；
- 针对脏区标记，渲染管线会启动脏区任务创建，针对标记为脏区的节点会创建对应的LayoutWrapper布局包装对象；
- 针对LayoutWrapper会创建对应的wrapper子树用于封装布局任务（布局任务大部分场景下父子节点有关联关系，故需要针对树形结构进行布局）；
- 在布局任务执行过程中，自定义组件的布局任务会触发内部的RenderFunction函数用于执行build函数内部的UI描述生成对应的子树结构；
- 对应的子树会挂载在自定义组件的节点下，并创建对应的LayoutWrapper用于执行子树布局任务；
- 当布局任务完成后，LayoutWrapper会将得到的大小位置信息更新到FrameNode节点上；
- FrameNode节点更新大小位置信息后会标记脏区，用于创建渲染任务；
- 渲染任务在新图形后端场景下不依赖父子关系，可以单独创建相应的渲染任务执行。

**English**

- The UI description in the build function inside the custom component identified by @ Component will not be executed immediately when the custom component is mounted.
- After the custom component is mounted on the main tree of FrameNode, the dirty area will be marked.
- For dirty area marking, the rendering pipeline will start the dirty area task creation, and the corresponding LayoutWrapper layout wrapper object will be created for nodes marked as dirty areas.
- For LayoutWrapper, a corresponding wrapper subtree will be created to encapsulate the layout task (the layout task has a relationship between parent and child nodes in most scenarios, so it needs to be laid out for the tree structure).
- During the execution of the layout task, the layout task of the custom component will trigger the internal RenderFunction function to execute the UI description inside the build function to generate the corresponding subtree structure.
- The corresponding subtree will be mounted under the node of the custom component, and the corresponding LayoutWrapper will be created to perform the subtree layout task.
- When the layout task is completed, LayoutWrapper will update the obtained size and location information to the FrameNode node.
- The FrameNode node will mark dirty areas after updating the size and location information, which is used to create rendering tasks.
- The rendering task does not depend on the parent-child relationship in the new graphics backend scene, and the corresponding rendering task can be created separately for execution.

#### 任务创建-组件更新场景（待基于最小化更新方案进行更新）/ Task creation-component update scenario (to be updated based on the partial update scheme)

## 组件结构模型 / Component structure model

### FrameNode结构 / FrameNode structure

``` mermaid
classDiagram
	class FrameNode{
	}
	class Pattern{
	}
	class GeometryNode{
	}
	class LayoutProperty{
	}
	FrameNode o-- GeometryNode
	FrameNode o-- LayoutProperty
	FrameNode o-- RenderProperty
	FrameNode o-- Pattern
	Pattern..>LayoutAlgorithm
	Pattern..>LayoutProperty
	Pattern..>RenderProperty
	Pattern..>ContentPaintImpl
	Pattern <|-- TextPattern
	TextPattern ..> TextRenderProperty
	TextPattern ..> TextLayoutProperty
	TextPattern ..> TextContentPaintImpl
	TextPattern ..> TextLayoutAlgorithm
	LayoutAlgorithm <|-- TextLayoutAlgorithm
	TextContentPaintImpl --|> ContentPaintImpl
	RenderProperty <|-- TextRenderProperty
	LayoutProperty <|-- TextLayoutProperty
```

- FrameNode：页面主节点，主要包含LayoutProperty布局属性集合，RenderProperty渲染属性集合，Pattern模板类，GeometryNode大小位置信息类；
- LayoutProperty：布局属性集合，不同的pattern可以创建具体的子类用于保存布局类属性集合，在FrameNode创建布局任务时作为布局任务的输入。
- RenderProperty：渲染属性集合，不同的pattern可以创建具体的子类用于保存渲染类属性集合，在FrameNode创建渲染任务时作为渲染任务的输入。
- LayoutAlgorithm：布局算法类，不同的pattern创建不同的布局算法，作为布局任务的输入。
- ContentPaintImpl：内容绘制方法，不同的pattern创建不同的内容绘制方法，作为渲染任务的输入。
- GeometryNode：大小位置信息类。

**English**

- FrameNode: The main node of the page, which mainly contains the LayoutProperty layout attribute set, RenderProperty rendering attribute set, Pattern template class, GeometryNode size and location information class.
- LayoutProperty: A collection of layout properties. Different patterns can create specific subclasses to save the collection of layout class properties, which are used as input for layout tasks when FrameNode creates layout tasks.
- RenderProperty: A collection of rendering properties. Different patterns can create specific subclasses to hold the collection of rendering class properties, which are used as input for rendering tasks when FrameNode creates rendering tasks.
- LayoutAlgorithm: Layout algorithm class, different patterns create different layout algorithms as input for layout tasks.
- ContentPaintImpl: Content drawing method, different patterns create different content drawing methods as input for rendering tasks.
- GeometryNode: Size and location information class.

### 包装类结构 / Packaging structure

#### Modifier

``` mermaid
classDiagram
	class Modifier~Target~{
		FlushModify(Target* target) void
		ModifierTask modifierImpl_
	}
	class PropertyModifier~Property, Target~{
		std::optional~Property~ value_
	}
	Modifier~Target~ <|-- PropertyModifier~Property, Target~
	PropertyModifier~Property, Target~ <|-- BgColorModifier~Color, RenderContext~
	PropertyModifier~Property, Target~ <|-- WidthModifier~CalcLength, LayoutProperty~
	PropertyModifier~Property, Target~ <|-- FontColorModifier~CalcLength, RenderProperty~
	RenderContext <.. BgColorModifier~Color, RenderContext~
	LayoutProperty <.. WidthModifier~CalcLength, LayoutProperty~
	RenderProperty <.. FontColorModifier~CalcLength, RenderProperty~
	ModifyTask "1" --> "1..*" Modifier~Target~
```



#### LayoutWrapper

``` mermaid
classDiagram
	class LayoutWrapper{
		Measure(const std::optional<LayoutConstraintF>& parentConstraint) void
		Layout(const std::optional<OffsetF>& parentGlobalOffset) void
		std::list~LayoutWrapper~ children_
	}
	class LayoutAlgorithmWrapper{
		RefPtr~LayoutAlgorithm~ layoutAlgorithm_
	}
	LayoutAlgorithmWrapper o-- LayoutAlgorithm
	LayoutWrapper o-- LayoutAlgorithmWrapper
	LayoutWrapper o-- LayoutProperty
	LayoutWrapper o-- FrameNode
	LayoutWrapper o-- GeometryNode
	LayoutAlgorithm <|-- TextLayoutAlgorithm
	LayoutAlgorithm <|-- XXXLayoutAlgorithm
	LayoutProperty <|-- TextLayoutProperty
	LayoutProperty <|-- XXXLayoutProperty
```



#### RenderWrapper

``` mermaid
classDiagram
	class RenderWrapper{
		FlushRender() void
	}
	RenderWrapper o-- GeometryNode
	RenderWrapper o-- RenderContext
	RenderWrapper o-- ContentPaintImpl
	RenderWrapper o-- RenderProperty
	ContentPaintImpl -- Pattern
	RenderProperty -- Pattern
```


## 深入了解 / In-depth understanding

### 背景介绍 / Background introduction

- [单节点组合模型设计](单节点组合模型设计.md)
- [单树结构diff设计](单树结构diff设计.md)
  - [属性更新diff设计](属性更新diff设计.md)
  - [ForEach场景设计](ForEach场景设计.md)
  - [LazyForEach场景设计](LazyForEach场景设计.md)
  - [If场景设计](If场景设计.md)
- [UI任务模型](UI任务模型.md)
- [多前端适配](多前端适配.md)
- [事件总线](事件总线.md)
- [渲染层抽象](渲染层抽象.md)
- [无障碍预览支持](无障碍预览支持.md)
- [动画设计](动画设计.md)

### 流程介绍

- [页面首次创建流程介绍](页面首次创建流程介绍.md)
- [文本组件创建流程介绍](