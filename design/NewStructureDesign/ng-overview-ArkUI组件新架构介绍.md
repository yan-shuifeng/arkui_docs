# ArkUI-组件新架构概述 / ArkUI- Overview of the new component architecture (NG)

## UI组件的树形结构说明 / Description of the tree structure of UI components

UI组件的树形结构主要包括前端的UI描述结构、FrameNode节点树和后端渲染树组成，如下图所示:

The tree structure of the UI component mainly includes the front-end UI description structure, the FrameNode node tree, and the back-end rendering tree, as shown in the figure below:

![](figures/UI组件树形结构介绍.png)

- 后端基于前端UI描述创建相应的UINode节点树，UINode节点包括自定义组件节点（CustomNode），语法节点（SyntaxNode类似LazyForEach）和控件节点（FrameNode）；
- FrameNode节点树主要用于处理UI组件属性更新、布局测算、事件处理等业务逻辑；
- CustomNode作为FrameNode的子类，用于处理自定义组件相关业务逻辑，比如执行build函数；
- SyntaxNode作为语法节点，实现IfElse、ForEach、LazyForEach功能，并且不生成后端渲染RenderNode渲染节点；
- RenderNode渲染节点树用于渲染展示。

- The backend creates a corresponding UINode node tree based on the front-end UI description. The UINode node includes a custom component node (CustomNode), a syntax node (SyntaxNode is similar to LazyForEach), and a control node (FrameNode).
- The FrameNode node tree is mainly used to handle business logic such as UI component attribute update, layout calculation, and event processing.
- CustomNode, as a subclass of FrameNode, is used to handle business logic related to custom components, such as executing the build function.
- Syntax node, as a syntax node, implements the functions of IfElse, ForEach, and LazyForEach, and does not generate a backend rendering RenderNode rendering node-
- RenderNode rendering node tree is used for rendering display.

## 上下文结构 / Context structure

![](figures/FrameNode上下文结构.png)

- JSView：引擎对接层，提供js引擎数据解析能力，将前端的UI描述对接到后端C++函数调用；（在代码中主要命名为JSXXX（如JSStack），路径在foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/jsview目录下）
- View：C++组件对外提供的接口类，包括组件的创建、属性设置、事件设置等；（在代码中主要命名为XXXView（如StackView），路径在foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/组件名/的目录下）
- FrameNode：C++组件类（路径在foundation/arkui/ace_engine/frameworks/core/components_ng/base目录下），内部包括：
  - GeometryNode：存储大小位置信息；
  - PaintProperty：内容区绘制类属性集合，不同组件可以创建子类，保存相关渲染属性，比如进度条的滑轨颜色；
  - LayoutProperty：布局类属性集合，不同组件可以创建子类，保存相关布局属性，比如Flex容器的Flex属性；
  - Pattern：组件模板类，不同组件会对应不同的Pattern模板类，用于提供不同组件差异化RenderProperty、LayoutProperty、布局算法、绘制方法的能力；
  - RenderContext：绘制上下文，对接不同后端，提供统一的canvas接口、渲染属性（转换矩阵、背景色等）接口。
  - AnimationOption：动画配置相关属性集合，设置给后端RenderContext进行动画配置。
  - Inspector：调测相关能力，如获取组件大小位置信息，获取组件属性信息等；
- LayoutWrapper：工具类，承载布局任务，在组件需要重新布局时，会生成相应的LayoutWrapper对象，LayoutWrapper对象输入为LayoutProperty、LayoutAlgorithm，输出为GeometryNode。（路径在foundation/arkui/ace_engine/frameworks/core/components_ng/layout目录下）
  - LayoutWrapper布局包装器设计目的主要有以下几点考虑：
    - 布局过程中不可避免的会出现变量需要在不同阶段不同函数内调用，从而需要将该变量按照成员变量保存，而布局任务作为瞬时任务按需触发，不应该将布局过程中产生的变量保存在FrameNode，而LayoutWrapper作为临时对象，也是在布局时创建，过程中的变量保存在该临时变量中，不影响FrameNode；
    - LayoutWrapper在创建时会拷贝FrameNode中布局任务相关的布局属性以及布局算法，在布局过程中不再依赖FrameNode，可以实现布局任务的独立运行，为后续布局任务的并行并发提供基础。
    - FrameNode不再具体处理布局任务，实现属性更新和布局过程的解耦，方便代码维护以及TDD用例设计执行。

- RenderWrapper：工具类，承载绘制任务，在组件需要重新布局时，会生成相应的RenderWrapper对象，RenderWrapper对象输入为PaintProperty、绘制任务实现，RenderContext绘制上下文，输出为Canvas的绘制指令。（路径在foundation/arkui/ace_engine/frameworks/core/components_ng/render目录下）；
  - RenderWrapper绘制包装器的设计目的同LayoutWrapper，主要用于和FrameNode解耦，提供并行并发任务基础。

- EventHub：事件管理类，提供事件回调设置统一入口，针对设置的事件回调类型，创建不同的事件管理器进行事件处理，比如手势事件创建GestureEventHub进行统一处理。（foundation/arkui/ace_engine/frameworks/core/components_ng/event目录下）
- Adapter：后端渲染抽象层，主要用于获取抽象的Canvas绘制接口，创建后端渲染节点树，执行显式隐式动画等；

**English**

- JSView: Engine docking layer, which provides js engine data parsing capabilities, and connects the front-end UI description to the back-end C++ function call. (Mainly named JSXXX (such as JSStack) in the code, the path is in the foundation/arkui/ace_engine/frameworks/bridge/declarative_frontend/jsview directory).
- View: The interface classes provided by C++ components to the outside world, including component creation, property settings, event settings, etc. (Mainly named XXXView (such as StackView) in the code, the path is in the directory of foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/component name/)
- FrameNode: C++ component class (the path is in the foundation/arkui/ace_engine/frameworks/core/components_ng/base directory), which includes：
  - GeometryNode: Store size and location information.
  - PaintProperty: A collection of drawing class attributes in the content area. Different components can create subclasses and save related rendering attributes, such as the slide color of the progress bar.
  - LayoutProperty: A collection of layout class properties. Different components can create subclasses to save related layout properties, such as the Flex properties of the Flex container.
  - Pattern: Component template class, different components will correspond to different pattern template classes, which are used to provide different components with the ability to differentiate RenderProperty, LayoutProperty, layout algorithms, and drawing methods.
  - RenderContext: Drawing context, docking with different backends, providing a unified canvas interface, rendering properties (conversion matrix, background color, etc.) interface.
  - AnimationOption: A collection of animation configuration related properties, set to the backend RenderContext for animation configuration.
  - Inspector: Adjust and test related capabilities, such as obtaining component size and location information, obtaining component attribute information, etc.
- LayoutWrapper: A tool class that hosts layout tasks. When the component needs to be re-laid out, the corresponding LayoutWrapper object will be generated. The input of the LayoutWrapper object is LayoutProperty, LayoutAlgorithm, and the output is GeometryNode. (The path is in the foundation/arkui/ace_engine/frameworks/core/components_ng/layout directory). LayoutWrapper The design purpose of the layout wrapper mainly has the following considerations：
  - It is inevitable that variables need to be called in different functions at different stages during the layout process, so the variable needs to be saved as a member variable, and the layout task is triggered as an instantaneous task on demand. The variables generated during the layout process should not be saved in the FrameNode, and LayoutWrapper, as a temporary object, is also created during the layout, and the variables in the process are saved in the temporary variable, which does not affect the FrameNode.
  - LayoutWrapper will copy the layout properties and layout algorithms related to the layout tasks from the FrameNode when it is created. It no longer relies on FrameNode during the layout process, which can realize the independent operation of layout tasks and provide the basis for the parallelism and concurrency of subsequent layout tasks.
  - FrameNode no longer handles layout tasks specifically, and realizes the decoupling of attribute updates and layout processes to facilitate code maintenance and TDD use case design and execution.

- RenderWrapper: A tool class that carries the drawing task. When the component needs to be re-laid out, the corresponding RenderWrapper object will be generated. The input of the RenderWrapper object is PaintProperty, the drawing task implementation, and the RenderContext drawing context. The output is the drawing instruction of Canvas. (The path is in the foundation/arkui/ace_engine/frameworks/core/component s_ng/render directory)
  - RenderWrapper The design purpose of the drawing wrapper is the same as LayoutWrapper, which is mainly used to decouple from FrameNode and provide the basis for parallel and concurrent tasks.

- EventHub: Event management class, which provides a unified entry for event callback settings, and creates different event managers for event processing for the set event callback types, such as gesture events to create GestureEventHub for unified processing. (Under the foundation/arkui/ace_engine/frameworks/core/component s_ng/event directory)
- Adapter: Back-end rendering abstraction layer, mainly used to obtain abstract Canvas drawing interface, create back-end rendering node tree, perform explicit and implicit animation, etc.

## 组件树构建 / Component tree creation

![](figures/UI组件树创建.png)

- 当应用启动时，ArkUI框架会初始化应用UI根节点，应用UI根节点使用StagePattern用于实现页面切换等特殊操作；
- 初始化完成后，ArkUI框架会开始加载页面文件，在加载页面文件时会创建页面根节点CustomNode（@Entry标识的自定义组件）；
- 页面根节点组件会在路由管理类中挂载到页面包装节点上（PagePattern）；页面包装节点会挂载到根节点下；
- 页面根节点在布局测试过程中，会调用内部的build方法，生成子树结构挂载到页面根节点下。

**English**

- When the application starts, the ArkUI framework initializes the application UI root node, and the application UI root node uses StagePattern to implement special operations such as page switching.
- After the initialization is complete, the ArkUI framework will start loading the page file, and the page root node CustomNode (a custom component identified by @Entry) will be created when the page file is loaded.
- The page root node component will be mounted to the page wrapper node (PagePattern) in the routing management class; the page wrapper node will be mounted to the root node.
- During the layout test process of the page root node, the internal build method will be called to generate a subtree structure and mount it to the page root node.

### 代码走读 / Code day reading

[UI组件树构建介绍](UI组件树构建介绍.md)

## 组件Diff和脏区任务 / Component Diff and dirty area tasks

![](figures/单树diff说明.png)

- 页面在首次创建时，会给每个UINode节点生成唯一的标识id；
- 当页面状态变量发生变化时，前端框架会标记持有该状态变量的自定义组件为更新脏区，并保存在渲染管线对象中，并请求下一个vsync信号；
- 当下一个vsync信号到来时，渲染管线会调用该脏区节点进行更新任务执行；
- 自定义组件在执行更新任务会，会通过前端框架识别build函数内部子节点的更新条件，重新执行依赖状态变量（发生改变）的子节点的UI描述方法，更新新的UI属性和条件变量；
- 当UI更新设置新的UI属性和条件变量时，会与老的UI属性和条件变量进行对比，针对产生差异的UI组件标记为布局脏区；此外，针对if语法节点，会基于当前的条件值，动态删除其子节点，实现if语法的更新；
- 由于布局任务大部分场景依赖父子关系，标记为布局脏区的子节点通常情况下会标记父节点为布局脏区；
- 上述布局脏区节点会保存到渲染管线中，由渲染管线在刷新布局阶段执行这些脏区节点的重新布局；
- 布局脏区节点完成布局后，会更新组件相应的大小位置等信息并生成渲染脏区节点；
- 渲染脏区节点同样会保存在渲染管线中，由渲染管线在属性绘制阶段执行这些脏区节点的重新绘制。

**English**

- When the page is created for the first time, a unique identification id will be generated for each UINode node.
- When the page state variable changes, the front-end framework will mark the custom component holding the state variable as the update dirty area, and save it in the rendering pipeline object, and request the next vsync signal.
- When the next vsync signal arrives, the rendering pipeline will call the dirty area node to perform the update task.
- When the custom component performs the update task, it will identify the update conditions of the child nodes inside the build function through the front-end framework, re-execute the UI description method of the child nodes that depend on the state variable (changed), and update the new UI properties and condition variables.
- When the UI update sets new UI properties and condition variables, it will be compared with the old UI properties and condition variables, and the UI components that make the difference will be marked as layout dirty areas; in addition, for if syntax nodes, their child nodes will be dynamically deleted based on the current condition value to achieve if syntax updates.
- Since most scenes of layout tasks rely on parent-child relationships, child nodes marked as dirty layout areas will usually be marked as dirty layout areas by the parent node.
- The above layout dirty area nodes will be saved to the rendering pipeline, and the rendering pipeline will perform the re-layout of these dirty area nodes during the layout refresh phase.
- Layout the dirty area node After the layout is completed, the corresponding size and location information of the component will be updated and the rendering dirty area node will be generated.
- Rendering dirty area nodes will also be saved in the rendering pipeline, and the rendering pipeline will perform the redrawing of these dirty area nodes during the attribute drawing phase.

### 代码走读 / Code day reading

[UI组件Diff介绍（待补充）](UI组件Diff介绍.md)

## 渲染管线交互 / Rendering pipeline interaction

![](figures/渲染管线结构.png)

- 当渲染管线内加入脏区节点时，会请求下一帧vsync；
- FlushAnimation：当下一帧vsync过来后，会首先执行动画，动画处理过程中会生成额外的DirtyNode保存在渲染管线中；
- FlushDirtyNodeUpdate：处理更新脏区节点，此时会刷新待更新的UI节点属性和参数，并生成额外的DirtyNode保存在渲染管线中；
- FlushTouchEvents：集中处理手势事件，在该阶段可能会修改UI节点的属性并标记布局或者渲染脏区（比如列表滑动）；
- FlushLayoutTask：集中处理布局脏区节点，生成并执行布局任务；
- FlushRenderTask：集中处理渲染脏区节点，生成并执行渲染任务；
- FlushMessage：驱动后端进行渲染刷新。

- When a dirty area node is added to the rendering pipeline, the next frame vsync will be requested.
- FlushAnimation: When the next frame of vsync comes over, the animation will be executed first, and additional dirtynodes will be generated during the animation process and saved in the rendering pipeline.
- FlushDirtyNodeUpdate: Process the update of the dirty area node. At this time, the UI node properties and parameters to be updated will be refreshed, and additional dirtynodes will be generated and saved in the rendering pipeline.
- FlushTouchEvents: Centralized processing of gesture events. At this stage, the properties of UI nodes may be modified and the layout may be marked or dirty areas may be rendered (such as list sliding).
- FlushLayoutTask: Centralized processing of layout dirty area nodes, generate and execute layout tasks.
- FlushRenderTask: Centralized processing of rendering dirty area nodes, generate and execute rendering tasks.
- FlushMessage: Drive the backend to refresh the rendering.

## 关键类和数据结构说明 / Key classes and data structures

### FrameNode结构说明 / FrameNode class

```mermaid
classDiagram
UINode <|-- FrameNode
class FrameNode {
	GetOrCreateFrameNode(tag, nodeId, const std::function<RefPtr<Pattern>(void)>& patternCreator) RefPtr~FrameNode~
    CreateFrameNode(tag, nodeId, pattern, isRoot) RefPtr~FrameNode~
    GetFrameNode(tag, nodeId) RefPtr~FrameNode~
    CreateLayoutWrapperOnCreate() RefPtr~LayoutWrapper~
    CreateLayoutWrapper(bool forceMeasure, bool forceLayout) RefPtr~LayoutWrapper~
	CreateLayoutTask(bool onCreate, bool forceUseMainThread) optional~UITask~
	CreateRenderTask(bool forceUseMainThread) optional~UITask~
	SwapDirtyLayoutWrapperOnMainThread(const RefPtr~LayoutWrapper~& dirty) void

	RefPtr~GeometryNode~ geometryNode_
    RefPtr~LayoutProperty~ layoutProperty_
    RefPtr~PaintProperty~ paintProperty_
    RefPtr~RenderContext~ renderContext_
    RefPtr~EventHub~ eventHub_;
    RefPtr~Pattern~ pattern_;
}
class UINode {
	MarkDirtyNode(PropertyChangeFlag extraFlag = PROPERTY_UPDATE_NORMAL) void
	AdjustLayoutWrapperTree(const RefPtr<LayoutWrapper>& parent, bool forceMeasure, bool forceLayout) void
	
	std::list~UINode~ children_
    WeakPtr~PipelineContext~ context_
    WeakPtr~UINode~ parent_
}
FrameNode *-- GeometryNode
FrameNode *-- LayoutProperty
FrameNode *-- PaintProperty
FrameNode *-- RenderContext
FrameNode *-- EventHub
FrameNode *-- Pattern
FrameNode ..> LayoutWrapper
FrameNode ..> RenderWrapper
```

- 成员变量说明：

  - children_：子节点信息，可能为语法节点（SyntaxNode）（如if，foreach）和组件节点（FrameNode）；
  - parent_：父节点信息，可能为语法节点（SyntaxNode）（如if，foreach）和组件节点（FrameNode）；
  - context_：渲染管线对象；
  - geometryNode_：组件大小位置信息；
  - layoutProperty_：布局属性集合信息，比如长宽信息；
  - paintProperty_：内容区绘制属性集合信息，比如进度条轨道颜色；
  - renderContext_：后端绘制上下文，比如设置背景色，获取Canvas等；
  - eventHub_：事件回调集合，保存事件信息，比如点击，进度条变更通知等；
  - pattern_：组件模板类，提供不同类型组件对应的LayoutProperty子类，PaintProperty子类，EventHub子类，布局方法和绘制方法等；

- Description of member variables：
  - children_: Child node information, which may be syntax nodes (such as if, foreach) and component nodes (FrameNode).
  - parent_: Parent node information, which may be a syntax node (such as if, foreach) and a component node (FrameNode).
  - context_: render pipeline object.
  - geometryNode_: Component size and location information.
  - layoutProperty_: Layout attribute collection information, such as length and width information.
  - paintProperty_: Information about the collection of drawing attributes in the content area, such as the color of the progress bar track.
  - renderContext_: Back-end drawing context, such as setting background color, obtaining Canvas, etc.
  - eventHub_: Event callback collection, save event information, such as clicks, progress bar change notifications, etc.
  - pattern_: Component template class, providing LayoutProperty subclasses, PaintProperty subclasses, EventHub subclasses, layout methods and drawing methods corresponding to different types of components, etc.

- 成员方法说明 / Member methods：

  - `MarkDirtyNode(PropertyChangeFlag extraFlag = PROPERTY_UPDATE_NORMAL)`标记当前节点为脏区节点 / Marks the current node as a dirty area node：

    ```C++
    void FrameNode::MarkDirtyNode(PropertyChangeFlag extraFlag)
    {
        MarkDirtyNode(IsMeasureBoundary(), IsRenderBoundary(), extraFlag);
    }
    
    void FrameNode::MarkDirtyNode(bool isMeasureBoundary, bool isRenderBoundary, PropertyChangeFlag extraFlag)
    {
        // extraFlag主要用于传递额外脏区标记类型设置给对应的属性集合，比如手势滑动的时候，会额外传递PROPERTY_REQUEST_NEW_CHILD_NODE标记，用于再创建布局脏区时仅限制自身重新布局，而不需要父节点重新布局。
        // extraFlag is mainly used to pass the type setting of the extra dirty area tag to the
        // corresponding property set.
        // For example, when the gesture is swiped, the PROPERTY_REQUEST_NEW_CHILD_NODE tag
        // will be passed additionally, which is used to re-create the layout of the dirty area.
        // Only restrict yourself to re-layout, without the need for the parent node to re-layout.

        layoutProperty_->UpdatePropertyChangeFlag(extraFlag);
        paintProperty_->UpdatePropertyChangeFlag(extraFlag);
        
        // 在MarkDirtyNode函数中会识别脏区类型，脏区类型通过标志位进行判断，标志位的值是布局属性集合和渲染属性集合的标志位进行或操作得到；
        // 而布局属性集合和渲染属性集合的标志位，会在设置新的属性值时进行diff更新，比如设置不同的长宽值时，布局属性集合会标记当前的flag为PROPERTY_UPDATE_MEASURE。
        // The dirty area type will be identified in the MarkDirtyNode function.
        // The dirty area type is judged by the flag bit. The value of the flag bit is the flag bit of the
        // layout attribute set and the rendering attribute set obtained by the operation.
        // And the flags of the layout attribute set and the rendering attribute set will be updated
        // diff when the new attribute value is set. For example, when different length and width
        // values are set, the layout attribute set will mark the current flag as
        // PROPERTY_UPDATE_MEASURE.
        auto flag = layoutProperty_->GetPropertyChangeFlag() | paintProperty_->GetPropertyChangeFlag();
        if (CheckNoChanged(flag)) {
            return;
        }
        if (CheckNeedRequestMeasureAndLayout(flag)) {
            if (isLayoutDirtyMarked_) {
                return;
            }
            isLayoutDirtyMarked_ = true;
            
            // 节点属于非布局边界且需要父亲参与布局时，会标记父节点为布局脏区节点。
            // When the node belongs to a non-layout boundary and requires the parent to participate
            // in the layout, the parent node will be marked as the layout dirty area node.
            if (!isMeasureBoundary && CheckNeedRequestParent(flag)) {
                auto parent = GetAncestorNodeOfFrame();
                if (parent) {
                    parent->MarkDirtyNode(PROPERTY_UPDATE_BY_CHILD_REQUEST);
                }
                return;
            }
            
            // 节点属于布局边界或者不需要父亲参与布局时，会标记自身。
            // When the node belongs to the layout boundary or does not require the parent
            // to participate in the layout, it will mark itself.
            UITaskScheduler::GetInstance()->AddDirtyLayoutNode(Claim(this));
            return;
        }
        layoutProperty_->CleanDirty();
        // If has dirtyLayoutBox, need to mark dirty after layout done.
        if (isRenderDirtyMarked_ || isLayoutDirtyMarked_) {
            return;
        }
        isRenderDirtyMarked_ = true;
        if (isRenderBoundary) {
            // 节点属于渲染边界时，仅标记自身，通常情况下，节点都是独立渲染，不需要标记父节点。
            // When the node belongs to the rendering boundary, it only marks itself.
            // Under normal circumstances, the nodes are rendered independently and
            // there is no need to mark the parent node.
            UITaskScheduler::GetInstance()->AddDirtyRenderNode(Claim(this));
            return;
        }
        auto parent = GetAncestorNodeOfFrame();
        if (parent) {
            parent->MarkDirtyNode(PROPERTY_UPDATE_RENDER_BY_CHILD_REQUEST);
        }
    }
    ```

  - `static RefPtr<FrameNode> GetOrCreateFrameNode(const std::string& tag, int32_t nodeId, const std::function<RefPtr<Pattern>(void)>& patternCreator)`获取或者创建组件节点，通过指定nodeId获取相应的UI组件，如果指定的nodeid没有相应的UI组件，则创建新的FrameNode节点，创建FrameNode节点依赖的Pattern对象则通过patternCreator进行创建，在XXXView对接代码中，使用该方法获取或者创建新的UI组件。
  - `static RefPtr<FrameNode> GetOrCreateFrameNode(const std::string& tag, int32_t nodeId, const std::function<RefPtr<Pattern>(void)>& patternCreator)` obtains or creates a component node, obtains the corresponding UI component by specifying the NodeID. If the specified NodeID does not have a corresponding UI component FrameNode, a new FrameNode node is created, and the pattern object that the FrameNode node depends on is created through patternCreator. In the XXXView docking code, use this method to obtain or create a new UI component.

    ```c++
    RefPtr<FrameNode> FrameNode::GetOrCreateFrameNode(
        const std::string& tag, int32_t nodeId, const std::function<RefPtr<Pattern>(void)>& patternCreator)
    {
        auto frameNode = GetFrameNode(tag, nodeId);
        if (frameNode) {
            return frameNode;
        }
        auto pattern = patternCreator ? patternCreator() : MakeRefPtr<Pattern>();
        return CreateFrameNode(tag, nodeId, pattern);
    }
    ```

  - `static RefPtr<FrameNode> CreateFrameNode(tag, nodeId, pattern, isRoot)`创建组件节点，FrameNode创建需要使用该静态方法而不是构造方法进行创建节点，该静态方法中会调用构造函数，并初始化当前UI节点：
  - `static RefPtr<FrameNode> CreateFrameNode(tag, nodeId, pattern, isRoot)` creates a component node. FrameNode creation requires the static method instead of the constructor to create the node. The constructor will be called by the static method and the current UI node will be initialized:

    ```c++
    RefPtr<FrameNode> FrameNode::CreateFrameNode(
        const std::string& tag, int32_t nodeId, const RefPtr<Pattern>& pattern, bool isRoot)
    {
        auto frameNode = MakeRefPtr<FrameNode>(tag, nodeId, pattern, isRoot);
        frameNode->SetContext(PipelineContext::GetCurrentContext());
        frameNode->InitializePatternAndContext();
        // 组件创建完成后，会保存在ElementRegister对象中，便于后续基于id获取已经创建的节点进行更新。
        // After the component is created, it will be saved in the ElementRegister object to
        // facilitate subsequent updates based on the id of the node that has been created.
        ElementRegister::GetInstance()->AddUINode(frameNode);
        return frameNode;
    }
    ```

  - `static RefPtr<FrameNode> GetFrameNode(const std::string& tag, int32_t nodeId)`基于id获取对应的组件节点，在更新场景下，会通过id获取组件节点而不是重新创建节点。
  - `static RefPtr<FrameNode> GetFrameNode(const std::string& tag, int32_t nodeId)` obtains the corresponding component node based on the id. In the update scenario, the exiting component node will be obtained by id instead of recreating the node.

    ```c++
    RefPtr<FrameNode> FrameNode::GetFrameNode(const std::string& tag, int32_t nodeId)
    {
        auto frameNode = ElementRegister::GetInstance()->GetSpecificItemById<FrameNode>(nodeId);
        if (!frameNode) {
            return nullptr;
        }
        if (frameNode->tag_ != tag) {
            LOGE("the tag is changed");
            ElementRegister::GetInstance()->RemoveItemSilently(nodeId);
            return nullptr;
        }
        return frameNode;
    }
    
    void TextView::Create(const std::string& content)
    {
        auto* stack = ViewStackProcessor::GetInstance();
        auto nodeId = stack->ClaimNodeId();
        // 当id对应的组件存在时，走更新场景，不重新创建组件。
        // When the component corresponding to the id exists,
        // the scene will be updated and the component will not be recreated.
        auto frameNode = FrameNode::GetFrameNode(V2::TEXT_ETS_TAG, nodeId);
        if (!frameNode) {
            frameNode = FrameNode::CreateFrameNode(V2::TEXT_ETS_TAG, nodeId, AceType::MakeRefPtr<TextPattern>());
        }
        stack->Push(frameNode);
    
        ACE_UPDATE_LAYOUT_PROPERTY(TextLayoutProperty, Content, content);
    }
    ```

  - `RefPtr<LayoutWrapper> CreateLayoutWrapperOnCreate()`当UI组件创建完成后，执行第一次布局任务时，调用该函数创建布局包装器
  - `RefPtr<LayoutWrapper> CreateLayoutWrapperOnCreate()` When the UI component is created and the first layout task is performed, this function is called to create a layout wrapper

    ```c++
    RefPtr<LayoutWrapper> FrameNode::CreateLayoutWrapperOnCreate()
    {
        isLayoutDirtyMarked_ = false;
        // 使用布局属性创建布局包装器
        // Create a layout wrapper using layout properties
        auto layoutWrapper = MakeRefPtr<LayoutWrapper>(WeakClaim(this), geometryNode_->Clone(), layoutProperty_->Clone());
        layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm()));
    
        // 布局任务通常需要父子节点参与，布局包装器会调用子节点的AdjustLayoutWrapperTree方法创建布局包装器的树形结构
        // Layout tasks usually require the participation of parent and child nodes.
        // The layout wrapper will call the AdjustLayoutWrapperTree method
        // of the child node to create the tree structure of the layout wrapper.
        const auto& children = GetChildren();
        for (const auto& child : children) {
            child->AdjustLayoutWrapperTree(layoutWrapper, true, true);
        }
        layoutProperty_->CleanDirty();
        return layoutWrapper;
    }
    
    void UINode::AdjustLayoutWrapperTree(const RefPtr<LayoutWrapper>& parent, bool forceMeasure, bool forceLayout)
    {
        for (const auto& child : children_) {
            child->AdjustLayoutWrapperTree(parent, forceMeasure, forceLayout);
        }
    }
    
    // 重写父类UINode的实现
    // Rewrite the implementation of the parent class UINode
    void FrameNode::AdjustLayoutWrapperTree(const RefPtr<LayoutWrapper>& parent, bool forceMeasure, bool forceLayout)
    {
        ACE_DCHECK(parent);
        auto layoutWrapper = CreateLayoutWrapper(forceMeasure, forceLayout);
        parent->AppendChild(layoutWrapper);
    }
    ```

  - `RefPtr<LayoutWrapper> CreateLayoutWrapper(bool forceMeasure = false, bool forceLayout = false)`UI组件属性更新后，调用该函数创建相应的布局包装器。
  - `RefPtr<LayoutWrapper> CreateLayoutWrapper(bool forceMeasure = false, bool forceLayout = false)` After the UI component properties are updated, call this function to create the corresponding layout wrapper.

    ```c++
    RefPtr<LayoutWrapper> FrameNode::CreateLayoutWrapper(bool forceMeasure, bool forceLayout)
    {
        // forceMeasure和forceLayout参数由调用方指定是否需要强制进行重新测算大小或者重新布局，比如当父节点大小发生变化时，子节点虽然layoutProperty_没有发生改变，但也需要重新进行大小测试，此时父节点会传递forceMeasure标志位强制子节点重新测试大小。
        // The forceMeasure and forceLayout parameters are specified by the caller whether it is
        // necessary to force a re-measurement of the size or re-layout.
        // For example, when the size of the parent node changes, although the layoutProperty_
        // of the child node has not changed, it also needs to be re-tested.
        // At this time, the parent node will pass the forceMeasure flag
        // to force the child node to re-test the size.
        isLayoutDirtyMarked_ = false;
        auto flag = layoutProperty_->GetPropertyChangeFlag();
        auto layoutWrapper = MakeRefPtr<LayoutWrapper>(WeakClaim(this), geometryNode_->Clone(), layoutProperty_->Clone());
        do {
            if (CheckMeasureFlag(flag) || CheckRequestNewChildNodeFlag(flag) || forceMeasure) {
                layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm()));
                bool forceChildMeasure = CheckMeasureFlag(flag) || forceMeasure;
                // 父节点大小发生变化时，通常子节点大小需要重新测算。
                // When the size of the parent node changes, the size
                // of the child node usually needs to be re-calculated.
                UpdateChildrenLayoutWrapper(layoutWrapper, forceChildMeasure, false);
                break;
            }
            if (CheckLayoutFlag(flag) || forceLayout) {
                // 当不需要重新大小测算，仅需要布局时，在创建布局算法的时候，会让LayoutAlgorithmWrapper算法包装器忽略测算阶段。
                // When there is no need to re-size the calculation, only the layout is required.
                // When creating the layout algorithm, the LayoutAlgorithmWrapper algorithm wrapper
                // will ignore the calculation stage.
                layoutWrapper->SetLayoutAlgorithm(
                    MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm(), true, false));
                UpdateChildrenLayoutWrapper(layoutWrapper, false, false);
                break;
            }
            // 当不需要重新大小测算和布局时，在创建布局算法的时候，会让LayoutAlgorithmWrapper算法包装器忽略测算和布局阶段，返回原来geometryNode_的大小位置。
            // When there is no need to re-size the calculation and layout,
            // when creating the layout algorithm, the LayoutAlgorithmWrapper algorithm
            // wrapper will ignore the calculation and layout stages
            // and return to the original size position of geometryNode_.
            layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(nullptr, true, true));
        } while (false);
        layoutProperty_->CleanDirty();
        return layoutWrapper;
    }
    
    LayoutAlgorithmWrapper(
            const RefPtr<LayoutAlgorithm>& layoutAlgorithmT, bool skipMeasure = false, bool skipLayout = false)
            : layoutAlgorithm_(layoutAlgorithmT), skipMeasure_(skipMeasure), skipLayout_(skipLayout)
        {}
    
    void FrameNode::UpdateChildrenLayoutWrapper(const RefPtr<LayoutWrapper>& self, bool forceMeasure, bool forceLayout)
    {
        const auto& children = GetChildren();
        for (const auto& child : children) {
            child->AdjustLayoutWrapperTree(self, forceMeasure, forceLayout);
        }
    }
    ```

  - `std::optional<UITask> CreateLayoutTask(bool onCreate = false, bool forceUseMainThread = false)`创建布局任务，该方法会被渲染管线调用，当节点标记为脏区节点时，渲染管线会调用该方法创建布局任务并执行。
  - `std::optional<UITask> CreateLayoutTask(bool onCreate = false, bool forceUseMainThread = false)` creates a layout task. This method will be called by the rendering pipeline. When the node is marked as a dirty area node, the rendering pipeline will call this method to create a layout task and execute it.

    ```c++
    std::optional<UITask> FrameNode::CreateLayoutTask(bool onCreate, bool forceUseMainThread)
    {
        if (!isLayoutDirtyMarked_) {
            return std::nullopt;
        }
        ACE_SCOPED_TRACE("prepare layout task");
       
        // 创建布局包装器的树形结构，包括了自身和子孙节点
        // Create a tree structure of the layout wrapper, including its own and descendant nodes
        RefPtr<LayoutWrapper> layoutWrapper;
        if (!onCreate) {
            UpdateLayoutPropertyFlag();
            layoutWrapper = CreateLayoutWrapper();
        } else {
            layoutWrapper = CreateLayoutWrapperOnCreate();
        }
       
        // 创建布局任务闭包。
        // Create a layout task closure.
        auto task = [layoutWrapper, layoutConstraint = GetLayoutConstraint(), offset = GetParentGlobalOffset(),
                        forceUseMainThread]() {
            {
                // 测算LayourWrapper树上各个节点的大小。
                // Calculate the size of each node in the LayourWrapper tree.
                ACE_SCOPED_TRACE("LayoutWrapper::Measure");
                layoutWrapper->Measure(layoutConstraint);
            }
            {
                // 对LayourWrapper树上各个节点的进行布局。
                // Layout each node on the LayourWrapper tree.
                ACE_SCOPED_TRACE("LayoutWrapper::Layout");
                layoutWrapper->Layout(offset);
            }
            {
                // 将布局完成后的结果通过包装器的MountToHostOnMainThread方法返回给FrameNode。
                // Return the result after the layout is completed to FrameNode
                // through the MountToHostOnMainThread method of the wrapper.
                ACE_SCOPED_TRACE("LayoutWrapper::MountToHostOnMainThread");
                if (forceUseMainThread || layoutWrapper->CheckShouldRunOnMain()) {
                    layoutWrapper->MountToHostOnMainThread();
                    return;
                }
                auto host = layoutWrapper->GetHostNode();
                CHECK_NULL_VOID(host);
                host->PostTask([layoutWrapper]() { layoutWrapper->MountToHostOnMainThread(); });
            }
        };
        if (forceUseMainThread || layoutWrapper->CheckShouldRunOnMain()) {
            return UITask(std::move(task), MAIN_TASK);
        }
        return UITask(std::move(task), layoutWrapper->CanRunOnWhichThread());
    }
    
    ```

  - `std::optional<UITask> CreateRenderTask(bool forceUseMainThread = false)`创建渲染任务，该方法会被渲染管线调用，当节点标记为脏区节点时，渲染管线会调用该方法创建渲染任务并执行。
  - `std::optional<UITask> CreateRenderTask(bool forceUseMainThread = false)` creates a rendering task. This method will be called by the rendering pipeline. When the node is marked as a dirty area node, the rendering pipeline will call this method to create a rendering task and execute it.

    ```c++
    std::optional<UITask> FrameNode::CreateRenderTask(bool forceUseMainThread)
    {
        if (!isRenderDirtyMarked_) {
            return std::nullopt;
        }
        ACE_SCOPED_TRACE("prepare render task");
        LOGD("create ui render task");
        auto wrapper = CreateRenderWrapper();
        auto task = [wrapper]() {
            ACE_SCOPED_TRACE("FrameNode::RenderTask");
            wrapper->FlushRender();
        };
        if (forceUseMainThread || wrapper->CheckShouldRunOnMain()) {
            return UITask(std::move(task), MAIN_TASK);
        }
        return UITask(std::move(task), wrapper->CanRunOnWhichThread());
    }
    
    RefPtr<RenderWrapper> FrameNode::CreateRenderWrapper()
    {
        isRenderDirtyMarked_ = false;
        // 创建渲染任务包装器，并通过SetContentPaintImpl设置相应的内容区绘制方法。
        // Create a rendering task wrapper and set the corresponding
        // content area drawing method through SetContentPaintImpl.
        auto renderWrapper = MakeRefPtr<RenderWrapper>(renderContext_, geometryNode_->Clone(), paintProperty_->Clone());
        renderWrapper->SetContentPaintImpl(pattern_->CreateContentPaintImpl());
        paintProperty_->CleanDirty();
        return renderWrapper;
    }
    ```

  - `void SwapDirtyLayoutWrapperOnMainThread(const RefPtr<LayoutWrapper>& dirty)`该方法由LayoutWrapper布局包装器在MountToHostOnMainThread的方法中调用，在该方法中，会刷新相应的组件大小和位置，触发绘制脏区的标记。
  - `void SwapDirtyLayoutWrapperOnMainThread(const RefPtr<LayoutWrapper>& dirty)` this method is called by the LayoutWrapper layout wrapper in the method of MountToHostOnMainThread. In this method, the corresponding component size and position will be refreshed, triggering the drawing of dirty area markers.

    ```c++
    void FrameNode::SwapDirtyLayoutWrapperOnMainThread(const RefPtr<LayoutWrapper>& dirty)
    {
        LOGD("SwapDirtyLayoutWrapperOnMainThread, %{public}s", GetTag().c_str());
        CHECK_NULL_VOID(dirty);
        // 在布局完成后，可能节点会被标记为非激活状态，比如列表滑动时，列表项划出可视区外。
        // After the layout is completed, the node may be marked as inactive.
        // For example, when the list slides, the list items are drawn out of the visible area.
        if (dirty->IsActive()) {
            pattern_->OnActive();
        } else {
            pattern_->OnInActive();
        }
        auto layoutAlgorithmWrapper = DynamicCast<LayoutAlgorithmWrapper>(dirty->GetLayoutAlgorithm());
        CHECK_NULL_VOID(layoutAlgorithmWrapper);
        // 各个组件在布局完成后由不同的绘制条件判断，需要检查各个组件模板类是否实现创建内容区绘制任务。比如容器组件一般不进行内容区绘制，此时会在OnDirtyLayoutWrapperSwap函数中返回false，避免创建多余的内容区绘制任务。
        // Each component is judged by different drawing conditions after the layout is completed.
        // It is necessary to check whether the template class of each component implements
        // the task of creating a content area. For example, container components generally
        // do not draw content areas. At this time, false will be returned in the OnDirtyLayoutWrapperSwap
        // function to avoid creating redundant content area drawing tasks.
        auto needRerender = pattern_->OnDirtyLayoutWrapperSwap(
            dirty, layoutAlgorithmWrapper->SkipMeasure(), layoutAlgorithmWrapper->SkipLayout());
        if (needRerender || CheckNeedRender(paintProperty_->GetPropertyChangeFlag())) {
            MarkDirtyNode(true, true, PROPERTY_UPDATE_RENDER);
        }
        // 容器组件或者自定义组件在布局完成后，可能发生了节点树的变化，此时需要同步后端渲染节点。比如自定义组件会执行build方法生成和挂载子树，列表组件在滑动场景下，可视区内的列表项会发生变化。
        // After the layout of the container component or custom component is completed,
        // the node tree may have changed. At this time, the backend rendering nodes need to be synchronized.
        // For example, a custom component will execute the build method to generate and mount a subtree.
        // When the list component is sliding in the scene, the list items in the visible area will change.
        if (needSyncRenderTree_) {
            RebuildRenderContextTree(dirty->GetChildrenInRenderArea());
            needSyncRenderTree_ = false;
        }
        // 更新后端渲染节点的大小位置。
        // Update the size and position of the backend rendering node.
        if (geometryNode_->GetFrame().GetRect() != dirty->GetGeometryNode()->GetFrame().GetRect()) {
            renderContext_->SyncGeometryProperties(RawPtr(dirty->GetGeometryNode()));
        }
        SetGeometryNode(dirty->MoveGeometryNode());
    }
    ```

	### Pattern及其相关类结构相关说明 / Pattern and related classes

```mermaid
classDiagram
class Pattern {
	IsAtomicNode() bool
    CreatePaintProperty() RefPtr~PaintProperty~
    CreateLayoutProperty() RefPtr~LayoutProperty~
    CreateLayoutAlgorithm() RefPtr~LayoutAlgorithm~
    CreateNodePaintMethod() RefPtr~NodePaintMethod~
    CreateEventHub() RefPtr~EventHub~
    OnDirtyLayoutWrapperSwap(dirtyLayoutWrapper, skipMeasure, skipLayout) bool
    GetHost() RefPtr~FrameNode~
    OnInActive() void
    OnActive() void
    OnModifyDone() void
}
class LayoutAlgorithm {
	MeasureContent(contentConstraint, layoutWrapper) std::optional~SizeF~
	Measure(LayoutWrapper* layoutWrapper) void
	Layout(LayoutWrapper* layoutWrapper) void
}
class LayoutAlgorithmWrapper {
	SkipMeasure() bool
	SkipLayout() bool
	RefPtr~LayoutAlgorithm~ layoutAlgorithm_
	bool skipMeasure_
	bool skipLayout_
}
class NodePaintMethod {
	GetContentPaintImpl(PaintWrapper* paintWrapper) CanvasDrawFunction
	GetForegroundPaintImpl(PaintWrapper* paintWrapper) CanvasDrawFunction
	GetOverlayPaintImpl(PaintWrapper* paintWrapper) CanvasDrawFunction
}
LayoutAlgorithmWrapper *-- LayoutAlgorithm
LayoutWrapper *-- LayoutAlgorithmWrapper
Pattern <|-- TextPattern
Pattern <|-- ImagePattern
Pattern <|-- StackPattern
Pattern --> PaintProperty
Pattern --> LayoutAlgorithm
NodePaintMethod <-- Pattern
LayoutProperty <-- Pattern
FrameNode *-- Pattern
```

#### Pattern类 / Pattern class

- `virtual bool IsAtomicNode() const`

当前组件是否为原子节点，原子节点的组件不包含子节点，比如Image，基类默认值为true。如果是容器组件，Pattern子类必须重写上述方法返回false。

Whether the current component is an atomic node, the component of the atomic node does not contain child nodes, such as Image, the default value of the base class is true. If it is a container component, the Pattern subclass must rewrite the above method to return false.

- `virtual RefPtr<LayoutProperty> CreateLayoutProperty()`

  创建组件布局的布局属性集合（子类LayoutProperty），比如Row和Column的CrossAxisAlign属性。布局属性设计开发时必须遵循下列要求：

  - 同类别的属性需要统一到struct结构体中，使用`ACE_DEFINE_PROPERTY_GROUP_ITEM`宏（第一个参数为属性名称，第二个参数为属性类型）定义相应的数据，可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_styles.h。
  - LayoutProperty子类使用`ACE_DEFINE_PROPERTY_GROUP`宏（第一个参数为属性组名称，第二个参数为属性组类型）将上述的struct结构体声明为自己的成员变量，可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_property.h。
  - 属性值的设置接口需要通过LayoutProperty子类暴露给前端（XXXView层），属性设置接口在完成上述两个步骤后，使用`ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP`宏（第一个参数为属性组名称，第二个参数为当前需要设置的属性值名称，第三个参数为当前属性值的类型，第四个参数为当属性值发生变化时需要标记的脏区类型（PROPERTY_UPDATE_MEASURE：改变自身大小尺寸，PROPERTY_UPDATE_LAYOUT：改变子节点在自身内部区域的放置方式）），可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_property.h。
  - 如果该属性值非常独立，不需要相应的属性组，则在LayoutProperty子类中使用`ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP`宏（第一次参数为属性名称，第二个参数为属性类型，第三个参数为属性变化时需要标记的脏区类型），可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/list/list_layout_property.h。
  - 完成上述步骤后，属性值在前端（XXXView层）层可以通过`ACE_UPDATE_LAYOUT_PROPERTY`宏（第一个参数为子类LayoutProperty类型，第二个参数为属性名称，第三个参数为设置的属性值），可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/list/list_view.cpp或者foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/column_view.cpp。

  Creates a collection of layout properties (subclasses LayoutProperty) for component layout, such as the CrossAxisAlign properties of Row and Column.The following requirements must be followed when designing and developing layout attributes：

    - Attributes of the same category need to be unified into the struct structure, use the `ACE_DEFINE_PROPERTY_GROUP_ITEM` macro (the first parameter is the attribute name, the second parameter is the attribute type) to define the corresponding data, you can refer to foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_styes.h.
    - LayoutProperty subclasses use the `ACE_DEFINE_PROPERTY_GROUP` macro (the first parameter is the attribute group name, the second parameter is the attribute group type) to declare the above struct structure as its own member variable, you can refer to foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_property.h.
    - The property value setting interface needs to be exposed to the front end (XXXView layer) through the LayoutProperty subclass. After completing the above two steps, the property setting interface uses the `ACE_DEFINE_PROPERTY_ITEM_WITH_GROUP` macro (the first parameter is the name of the property group, the second parameter is the name of the property value that currently needs to be set, the third parameter is the type of the current property value, and the fourth parameter is the type of dirty area that needs to be marked when the property value changes (`PROPERTY_UPDATE_MEASURE`: change its own size, `PROPERTY_UPDATE_LAYOUT`: Change the placement of child nodes in their own internal area)), you can refer to foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/linear_layout_property.h.
    - If the attribute value is independent and does not require a corresponding attribute group, then use the `ACE_DEFINE_PROPERTY_ITEM_WITHOUT_GROUP` macro in the LayoutProperty subclass (the first parameter is the attribute name, the second parameter is the attribute type, and the third parameter is the type of dirty area that needs to be marked when the attribute changes), you can refer to foundation/arkui/ace_engine/frameworks/core/component s_ng/pattern/list/list_layout_property.h.
    - After completing the above steps, the attribute value can be passed through the `ACE_UPDATE_LAYOUT_PROPERTY` macro in the front-end (XXXView layer) layer (the first parameter is the sub-class LayoutProperty type, the second parameter is the attribute name, and the third parameter is the set attribute value), you can refer to foundation/arkui/ace_engine/frameworks/core/component s_ng/pattern/list/list_view.cpp or foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/linear_layout/column_view.cpp.


- `virtual RefPtr<PaintProperty> CreatePaintProperty()`

  创建组件绘制的绘制属性集合（子类PaintProperty），比如进度条的滑块颜色属性。绘制属性设计开发时同样遵循布局属性的要求，使用相同的宏（前端（XXXView层）层可以通过`ACE_UPDATE_PAINT_PROPERTY`宏）简化开发。

  Creates a collection of drawing properties (subclasses PaintProperty) for component drawing, such as the slider color properties of the progress bar.The design and development of drawing attributes also follow the requirements of layout attributes, and use the same macro (the front-end (XXXView layer) layer can use the `ACE_UPDATE_PAINT_PROPERTY` macro) to simplify development.

- `virtual RefPtr<LayoutAlgorithm> CreateLayoutAlgorithm()`

  创建组件大小测算和布局的算法。该方法会在框架创建对应的布局包装器进行布局前调用，组件需要在该函数中返回自身大小测算和布局的算法。返回的布局算法后，框架会额外使用`LayoutAlgorithmWrapper`对象封装一层，`LayoutAlgorithmWrapper`对象主要基于组件更新场景的脏区类型设置`skipMeasure`和`skipLayout`来略过特定布局，比如脏区类型只是重新布局子节点位置而不涉及大小变化，则`skipMeasure`会被设置为true，在LayoutWrapper在布局过程中会忽略测算阶段，具体布局基类参考LayoutAlgorithm类说明。

  Creates algorithms for component size calculation and layout. This method will be called before the framework creates the corresponding layout wrapper for layout. The component needs to return its own size calculation and layout algorithm in this function. After the returned layout algorithm, the framework will use an additional layer of the `LayoutAlgorithmWrapper` object to encapsulate. The `LayoutAlgorithmWrapper` object is mainly based on the dirty area type of the component update scene to set `skipMeasure` and `skiplayout` to skip a specific layout. For example, the dirty area type is only to re-layout the location of the child nodes without involving size changes, then `skipMeasure` will be set to true, and the LayoutWrapper will ignore the calculation stage during the layout process. Refer to the specific layout base class. LayoutAlgorithm class description.

  ```c++
  LayoutAlgorithmWrapper(const RefPtr<LayoutAlgorithm>& layoutAlgorithmT, bool skipMeasure = false, bool skipLayout = false): layoutAlgorithm_(layoutAlgorithmT), skipMeasure_(skipMeasure), skipLayout_(skipLayout) {}
  
  RefPtr<LayoutWrapper> FrameNode::CreateLayoutWrapperOnCreate()
  {
      ...
      auto layoutWrapper = MakeRefPtr<LayoutWrapper>(WeakClaim(this), geometryNode_->Clone(), layoutProperty_->Clone());
      layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm()));
      ...
  }
  
  RefPtr<LayoutWrapper> FrameNode::CreateLayoutWrapper(bool forceMeasure, bool forceLayout)
  {
      isLayoutDirtyMarked_ = false;
      auto flag = layoutProperty_->GetPropertyChangeFlag();
      auto layoutWrapper = MakeRefPtr<LayoutWrapper>(WeakClaim(this), geometryNode_->Clone(), layoutProperty_->Clone());
      ...
          if (CheckMeasureFlag(flag) || CheckRequestNewChildNodeFlag(flag) || forceMeasure) {
              layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm()));
              ...
              break;
          }
          if (CheckLayoutFlag(flag) || forceLayout) {
          	// 不需要测算，仅做布局时，skipMeasure为true
            // No calculation is required, skipMeasure is true when only layout is done
              layoutWrapper->SetLayoutAlgorithm(
                  MakeRefPtr<LayoutAlgorithmWrapper>(pattern_->CreateLayoutAlgorithm(), true, false));
              ...
          }
          layoutWrapper->SetLayoutAlgorithm(MakeRefPtr<LayoutAlgorithmWrapper>(nullptr, true, true));
      ...
  }
  ```

- `virtual RefPtr<NodePaintMethod> CreateNodePaintMethod()`

  创建组件内容的绘制方法。该方法会在框架创建对应的绘制包装器进行内容绘制前调用，组件需要在该函数中返回自身内容绘制的方法实现，具体可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/text/text_paint_method.h，foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/image/image_paint_method.h。具体查看NodePaintMethod基类说明。

  Creates a drawing method for component content. This method will be called before the framework creates the corresponding drawing wrapper for content drawing. The component needs to return the implementation of its own content drawing method in this function. For details, please refer to foundation/arkui/ace_engine/frameworks/core/component s_ng/pattern/text/text_paint_method.h，foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/image/image_paint_method.h. See the NodePaintMethod base class description for details.

- `virtual RefPtr<EventHub> CreateEventHub()`

  创建组件事件集合。该方法会在前端设置事件时调用，UI组件的事件归类到EventHub的子类中，当后端触发事件场景时，调用EventHub子类触发前端事件。

  Creates a collection of component events. This method will be called when the event is set on the front end, and the events of the UI component are classified into the subclasses of EventHub. When the event scene is triggered on the back end, the EventHub subclasses are called to trigger the front-end events.

- `virtual bool OnDirtyLayoutWrapperSwap(const RefPtr<LayoutWrapper>& dirty, bool skipMeasure, bool skipLayout)`

  当布局完成后，框架会调用FrameNode的SwapDirtyLayoutWrapperOnMainThread方法，在该方法中会调用上述pattern方法来告诉框架是否需要启动内容区的绘制流程。第一个参数为布局完成后的包装器，可以通过该参数获取布局完成后的组件大小，内容区大小等信息，同时LayoutWrapper类中还提供了`SkipMeasureContent`方法会来获取当次布局内容区的大小是否不变。第二个参数则是说明当次布局计算是否略过了大小测算，第三个参数说明当次布局计算是否略过了内容区和子节点的布局。一般情况下如果布局内容区不变或者布局计算忽略了大小测算，可以认为内容不变，不需要重新绘制，返回false即可。由于父子节点绘制独立，从而容器组件一般也返回false（不涉及容器组件本身内容，例外情况比如列表的分割线场景需要返回true）。

  When the layout is completed, the framework will call FrameNode's SwapDirtyLayoutWrapperOnMainThread method, in which the above pattern method will be called to tell the framework whether it needs to start the drawing process of the content area. The first parameter is the wrapper after the layout is completed. This parameter can be used to obtain information such as the size of the component and the size of the content area after the layout is completed. At the same time, the LayoutWrapper class also provides the `SkipMeasureContent` method to get whether the size of the content area of the current layout remains the same. The second parameter is to indicate whether the current layout calculation has skipped the size calculation, and the third parameter is to indicate whether the current layout calculation has skipped the layout of the content area and child nodes. Under normal circumstances, if the layout content area remains unchanged or the layout calculation ignores the size calculation, it can be considered that the content remains unchanged and does not need to be redrawn. Just return false. Since the parent-child nodes are drawn independently, the container component generally returns false (it does not involve the content of the container component itself, with exceptions such as the dividing line scene of the list, it needs to return true).

- `RefPtr<FrameNode> GetHost() const`

  获取宿主FrameNode对象。

  Get the host FrameNode object.

- `virtual void ()`和`virtual void OnActive()`

  在布局完成后，有可能组件的可见性发生了变化，如果组件从可见变为不可见区域时，会回调OnInActive事件，反之会回调OnActive事件。

  After the layout is completed, it is possible that the visibility of the component has changed. If the component changes from visible to invisible area, the OnInActive event will be called back, and vice versa. The onActive event will be called back.

- `virtual void OnModifyDone()`

  通过前端(XXXView)执行完完当前UI组件的属性方法时会回调该函数，在该函数中，可以获取组件最新的属性设置值。

  When the property method of the current UI component is executed through the front end (XXXView), the function will be called back. In this function, the latest property setting value of the component can be obtained.

#### LayoutAlgorithm类

- `virtual std::optional<SizeF> MeasureContent(const LayoutConstraintF& contentConstraint, LayoutWrapper* layoutWrapper)`

  组件涉及内容区大小测算时，子类LayoutAlgorithm需要重写上述方法，返回内容区大小，比如文本大小。contentConstraint限制为内容区大小限制（去掉了Padding，Margin）。具体可以参考foundation/arkui/ace_engine/frameworks/core/components_ng/pattern/text/text_layout_algorithm.h。

  When the component involves the calculation of the size of the content area, the subclass LayoutAlgorithm needs to rewrite the above method to return the size of the content area, such as the size of the text. The contentConstraint limit is the content area size limit (padding and Margin are removed). For details, please refer to foundation/arkui/ace_engine/frameworks/core/component s_ng/pattern/text/text_layout_algorithm.h.

  （组件内容区概念可参考[ArkUI组件布局介绍](ArkUI组件布局介绍.md)）

- `virtual void Measure(LayoutWrapper* layoutWrapper) `

  测算组件自身大小和子组件大小（容器组件）。组件自身大小逻辑：

  - 如果显式设置了width和height属性，则组件自身大小为width和height属性设置的大小；（flex容器能在上述大小基础上进行额外的拉伸或者压缩）
  - 如果未显示设置width和height属性，则组件自身大小为内容区大小（非容器组件）加上Padding内边距值，针对容器组件，则按照容器组件具体的布局规范来，比如Row和Column组件，自身大小为子组件大小总和加上Padding内边距值。

  大部分非容器组件，可以直接继承**BoxLayoutAlgorithm**类，自身大小测算逻辑已经在BoxLayoutAlgorithm类中实现。

  Calculates the size of the component itself and the size of the subcomponents (container components). The size logic of the component itself：

  - If the width and height properties are explicitly set, the size of the component itself is the size set by the width and height properties (the flex container can be additionally stretched or compressed on the basis of the above size)
  - If the width and height properties are not displayed, the size of the component itself is the size of the content area (non-container components) plus the Padding inner margin value. For container components, the specific layout specifications of the container components are followed, such as Row and Column components. The size of the component itself is the sum of the size of the subcomponents plus the Padding inner margin value.

  Most non-container components can directly inherit the **BoxLayoutAlgorithm** class, and their own size calculation logic has been implemented in the BoxLayoutAlgorithm class.

- `virtual void Layout(LayoutWrapper* layoutWrapper)`

  设置组件内容（比如文本）或者子组件的布局位置。组件内容的位置定位逻辑：

  - 当组件内容大小小于组件本身大小时，会生效align属性，通过align属性设置组件内容在组件区域的对齐方式（比如左上，居中对齐等）。

  大部分非容器组件，可以直接继承**BoxLayoutAlgorithm**类，内容区对齐方式已经在BoxLayoutAlgorithm类中实现，容器类组件按照各容器自身的布局规范对子节点进行布局。

  Sets the content of the component (such as text) or the layout position of the subcomponents. Location positioning logic of component content：

  - When the size of the component content is smaller than the size of the component itself, the align attribute will take effect. The align attribute is used to set the alignment of the component content in the component area (such as upper left, center alignment, etc.).

  Most non-container components can directly inherit the **BoxLayoutAlgorithm** class. The alignment of the content area has been implemented in the BoxLayoutAlgorithm class. The container class components layout the child nodes in accordance with the layout specifications of each container itself.

#### NodePaintMethod类

- UI组件的绘制层级从下到上为：

  - 背景层：绘制背景色，背景图等，该层由后端渲染节点完成，UI组件在NodePaintMethod类中不需要额外处理；

  - 内容层：UI组件内容区绘制，比如进度条等；

  - 前景层：前景颜色，边框属性；

  - 遮罩层：比如调测的边框绘制，同时父容器在该层的绘制内容会遮盖子组件的绘制内容。

- The drawing level of UI components is from bottom to top：

  - Background layer: draw background color, background image, etc. This layer is completed by the back-end rendering node, and the UI components do not require additional processing in the NodePaintMethod class.

  - Content layer: Drawing the content area of UI components, such as progress bars, etc.

  - Foreground layer: foreground color, border attributes.

  - Masking layer: For example, the border of the test is drawn, and the drawing content of the parent container in this layer will cover the drawing content of the child component.

- `virtual CanvasDrawFunction GetContentPaintImpl(PaintWrapper* paintWrapper)`

  创建内容区绘制方法。
  Create a content area drawing method.

- `virtual CanvasDrawFunction GetForegroundPaintImpl(PaintWrapper* paintWrapper)`

  创建前景层绘制方法。
  Create a foreground layer drawing method.

- `virtual CanvasDrawFunction GetOverlayPaintImpl(PaintWrapper* paintWrapper)`

  创建overLay层绘制方法。
  Create an overLay layer drawing method.
