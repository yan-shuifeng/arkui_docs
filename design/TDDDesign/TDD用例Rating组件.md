# TDD用例Rating组件

|大类	| 小类 |	功能点	| 涉及文件|	测试输入条件|	测试预期结果 |
| --- | --- | :-- | --- | --- |--- |
|Rating组件	| Rating({rating: number})	|判断Rating是否设置了正确的ratingScore	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	| rating输入0	|rating_layout_property.cpp获取到rating数值并正确布局，即显示0个foreground星数|
|Rating组件	| Rating({rating: number})|	判断Rating是否设置了正确的ratingScore	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	|rating输入大于0，小于5	|rating_layout_property.cpp获取到rating数值并正确布局，即显示rating个foreground星数|
|Rating组件	| Rating({rating: number})|	判断Rating是否设置了正确的ratingScore	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	| rating输入大于5	| rating_layout_property.cpp获取到rating数值并正确布局，即显示5个foreground星数|
| Rating组件|	Rating({indicator: bool})|	判断Rating是否设置了正确的indicator	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	| indicator输入true	|rating_layout_property.cpp获取到indicator数值并正确布局，即Rating组件的ratingScore可以被改动|
| Rating组件|	Rating({indicator: bool})	|判断Rating是否设置了正确的indicator	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	| indicator输入false	|rating_layout_property.cpp获取到indicator数值并正确布局，即Rating组件的ratingScore不可以被改动|
|Rating组件	|Rating().Star(number)	|判断Rating是否设置了正确的Star数量	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	| indicator输入10	|rating_layout_property.cpp获取到starNum数值并正确布局，即Rating组件显示10个star number|
|Rating组件|	Rating().stepSize(number)	|判断Rating是否设置了正确的StepSize数量	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp|	indicator输入0.2	|rating_layout_property.cpp获取到StepSize数值并正确布局，即在ratingScore变化的时候，以stepSize为单位|
|Rating组件	| Rating().stepSize(number)	|判断Rating是否设置了正确的StepSize数量	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp	|indicator输入0.2	|rating_layout_property.cpp获取到StepSize数值并正确布局，即在ratingScore变化的时候，以stepSize为单位|
|Rating组件	| Rating().starStyle({ backgroundUri: string, foregroundUri: string, secondaryUri?: string })|	判断Rating是否设置了正确的ratingStyle，即三个imgae的url	|js_rating.cpp,rating_view.cpp,rating_layout_property.cpp,rating_property_group.cpp,rating_layout_algorithm.cpp |	{ backgroundUri: string, foregroundUri: string, secondaryUri?: string }设置开发者自定义的图片路径	|rating_layout_property.cpp获取到 backgroundUri、foregroundUri、secondaryUri数值并正确布局，即显示开发者设置的图片||

