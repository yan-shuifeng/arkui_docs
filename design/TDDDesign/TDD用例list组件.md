# TDD用例List组件

|大类	    |  小类                        |	功能点	                                    | 涉及文件                                                                      |	测试输入条件              |	测试预期结果                     |
| ---       | ---                          | :--                                            | ---                                                                           |  ---                        |---                                 |
|List组件	| List(space:number)       	   |判断列表项间距是否正常设置给List组件并布局   	|js_list.cpp,list_view.cpp,list_layout_property.cpp,list_layout_algorithm.cpp	|space输入0	                  |ListLayoutProperty获取到space数值并正确布局     |
|List组件	| List(space:number)       	   |判断列表项间距是否正常设置给List组件并布局   	|js_list.cpp,list_view.cpp,list_layout_property.cpp,list_layout_algorithm.cpp	|space输入大于0的Dimension    |ListLayoutProperty获取到space数值并正确布局     |
|List组件	| List(space:number)           |判断列表项间距是否正常设置给List组件并布局   	|js_list.cpp,list_view.cpp,list_layout_property.cpp,list_layout_algorithm.cpp	|space输入小于0的Dimension 	  |ListLayoutProperty获取到space数值并以0进行布局  |
|List组件   | List(initialIndex:number)    |判断列表初始序号是否正常设置给List组件并布局   	|js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|initialIndex输入0   	|ListLayoutProperty获取到initialIndex数值并正确布局  |
|List组件   | List(initialIndex:number)    |判断列表初始序号是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|initialIndex输入大于0小于等于最后一个item序列号的值	|ListLayoutProperty获取到initialIndex数值并正确布局|
|List组件	| List(initialIndex:number)    |判断列表初始序号是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|initialIndex输入小于0的值	    |ListLayoutProperty获取到initialIndex数值并以0进行正常布局|
|List组件	| List(initialIndex:number)    |判断列表初始序号是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|initialIndex输入大于最后一个item序列号的值	    |ListLayoutProperty获取到initialIndex数值并以0进行正常布局|
|List组件	| List(listDirection:Axis)     |判断列表方向是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|listDirection输入Axis::Vertical	    |ListLayoutProperty获取到listDirection的值并正确布局|
|List组件	| List(listDirection:Axis)     |判断列表方向是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|listDirection输入Axis::Horizontal      |ListLayoutProperty获取到listDirection的值并正确布局|
|List组件	| List(Divider:ItemDivider)    |判断分割线相关属性是否正常设置给List组件并布局  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|divider输入strokerWidth的值	    |ListLayoutProperty获取到divider的值并正确布局|
|List组件	| List(Divider:ItemDivider)    |判断分割线相关属性是否正常设置给List组件并布局  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|divider输入color的值	    |ListLayoutProperty获取到divider的值并正确布局|
|List组件	| List(Divider:ItemDivider)    |判断分割线相关属性是否正常设置给List组件并布局  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|divider输入startMargin的值	    |ListLayoutProperty获取到divider的值并正确布局|
|List组件	| List(Divider:ItemDivider)    |判断分割线相关属性是否正常设置给List组件并布局  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|divider输入endMargin的值	    |ListLayoutProperty获取到divider的值并正确布局|
|List组件	| List(editMode:boolean)       |判断编辑模式是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|editMode输入false	    |ListLayoutProperty获取到editMode的值|
|List组件	| List(editMode:boolean)       |判断编辑模式是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|editMode输入true	    |ListLayoutProperty获取到editMode的值|
|List组件	| List(edgeEffect:EdgeEffect)  |判断链式联动动效是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|EdgeEffect输入EdgeEffect::Spring	    |ListLayoutProperty获取到EdgeEffect的值并正确布局|
|List组件	| List(edgeEffect:EdgeEffect)  |判断链式联动动效是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|EdgeEffect输入EdgeEffect::None	    |ListLayoutProperty获取到EdgeEffect的值并正确布局|
|List组件	| List(chainAnimation:boolean) |判断边缘滑动效果是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|chainAnimation输入false	    |ListLayoutProperty获取到chainAnimation的值并正确布局|
|List组件	| List(chainAnimation:boolean) |判断边缘滑动效果是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|chainAnimation输入true |ListLayoutProperty获取到chainAnimation的值并正确布局|
|List组件	| List(multiSelectable:boolean)|判断鼠标框选是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|multiSelectable输入true	    |ListLayoutProperty获取到multiSelectable的值|
|List组件	| List(multiSelectable:boolean)|判断鼠标框选是否正常设置给List组件并布局        |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|multiSelectable输入false	    |ListLayoutProperty获取到multiSelectable的值|
|List组件	| List(restoredId:int32_t)     |判断组件迁移标识符是否正常设置给List组件并布局  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|restoredId输入数值	    |ListLayoutProperty获取到restoredId的值|
|List组件	| List(lanes:int32_t {Dimension,Dimension})  |判断lanes是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|lanes输入1	    |ListLayoutProperty获取到lanes的值并正确布局|
|List组件	| List(lanes:int32_t {Dimension,Dimension})  |判断lanes是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|lanes输入大于1的整数值	    |ListLayoutProperty获取到lanes的值并正确布局|
|List组件	| List(lanes:int32_t {Dimension,Dimension})  |判断lanes是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|lanes输入小于等于0的值	    |ListLayoutProperty获取到lanes的值并以1进行布局|
|List组件	| List(lanes:int32_t {Dimension,Dimension})  |判断lanes是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|不设置lanes，设置minLength和maxLength的值	    |ListLayoutProperty获取到minLength和maxLength的值并计算得到lanes的值进行布局|
|List组件	| List(lanes:int32_t {Dimension,Dimension})  |判断lanes是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|不设置lanes，minLength和maxLength的值	    |ListLayoutProperty以lanes=1的值进行布局|
|List组件	| List(alignListItem:ListItemAlign)  |判断alignListItem是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|alignListItem输入ListItemAlign::Center	    |ListLayoutProperty获取到alignListItem的值并正确进行布局|
|List组件	| List(alignListItem:ListItemAlign)  |判断alignListItem是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|alignListItem输入ListItemAlign::End	    |ListLayoutProperty获取到alignListItem的值并正确进行布局|
|List组件	| List(alignListItem:ListItemAlign)  |判断alignListItem是否正常设置给List组件并布局    |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|alignListItem输入ListItemAlign::Start	    |ListLayoutProperty获取到alignListItem的值并正确进行布局|
