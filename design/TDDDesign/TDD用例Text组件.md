|大类    |小类                  |功能点|涉及文件|测试输入条件|测试预期结果|
|:------:|:----:|:---:|:---:|:---:|:---:|
|Text组件|Text(content: string)|判断文本内容是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|content输入Hello World|TextLayoutProperty文本获取到字符串，flag为MEASURE|
|Text组件|Text(fontColor: Color)|判断文本颜色是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|fontColor输入Color::FromRGB(255, 100, 100)|TextLayoutProperty文本获取到Color::FromRGB(255, 100, 100)，flag为MEASURE|
|Text组件|Text(fontSize: Dimension)|判断文本尺寸是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|fontSize输入Dimension(20.1, DimensionUnit::PX)|TextLayoutProperty文本获取到Dimension(20.1, DimensionUnit::PX)，flag为MEASURE|
|Text组件|Text(fontStyle: FontStyle)|判断文本字体样式是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|fontStyle输入FontStyle::ITALIC|TextLayoutProperty文本获取到FontStyle::ITALIC，flag为MEASURE|
|Text组件|Text(fontWeight: FontWeight)|判断文本字体粗细是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|fontWeight输入FontWeight::W100|TextLayoutProperty文本获取到FontWeight::W100，flag为MEASURE|
|Text组件|Text(fontFamily: FontStyle)|判断文本字体列表是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|fontFamily输入FontStyle::ITALIC|TextLayoutProperty文本获取到FontStyle::ITALIC，flag为MEASURE|
|Text组件|Text(textAlign: TextAlign)|判断文本的多行文本的文本对齐方式是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|textAlign输入TextAlign::CENTER|TextLayoutProperty文本获取到TextAlign::CENTER，flag为MEASURE|
|Text组件|Text(textOverflow: TextOverflow)|判断文本超长时的显示方式是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|textOverflow输入TextOverflow::CLIP|TextLayoutProperty文本获取到TextOverflow::CLIP，flag为MEASURE|
|Text组件|Text(maxLines: uint32_t)|判断文本的最大行数是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|maxLines输入10|TextLayoutProperty文本的最大行数获取到10，flag为MEASURE|
|Text组件|Text(lineHeight: Dimension)|判断文本的文本行高是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|lineHeight输入Dimension(20.1, DimensionUnit::PX)|TextLayoutProperty文本获取到Dimension(20.1, DimensionUnit::PX)，flag为MEASURE|
|Text组件|Text(textDecoration: TextDecoration)|判断文本装饰线样式是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|textDecoration输入TextDecoration::INHERIT|TextLayoutProperty文本获取到TextDecoration::INHERIT，flag为MEASURE|
|Text组件|Text(textDecorationColor: Color)|判断文本装饰线颜色是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|textDecorationColor输入Color::FromRGB(255, 100, 100)|TextLayoutProperty文本获取到Color::FromRGB(255, 100, 100)，flag为MEASURE|
|Text组件|Text(baselineOffset: Dimension)|判断文本基线的偏移量是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|baselineOffset输入 Dimension(20.1, DimensionUnit::PX)|TextLayoutProperty文本获取到 Dimension(20.1, DimensionUnit::PX)，flag为MEASURE|
|Text组件|Text(textCase: TextCase)|判断文本大小写是否正常设置给布局集合对象|js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp|textCase输入TextCase::LOWERCASE|TextLayoutProperty文本获取到TextCase::LOWERCASE，flag为MEASURE|
