# TDD用例Text组件

|大类	    |  小类                            |	功能点	                            | 涉及文件                                                              |	测试输入条件           |	测试预期结果                                      |
| ---       | ---                              | :--                                    | ---                                                                   |  ---                     |---                                                   |
|Text组件	| Text(content?: string)       	   |判断文本内容是否正常设置给布局集合对象  |js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp	|content输入空字符串	   |TextLayoutProperty文本获取到空字符，flag为MEASURE     |
|Text组件	| Text(content?: string)       	   |判断文本内容是否正常设置给布局集合对象  |js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp	|content输入特定字符串     |TextLayoutProperty文本获取到特定字符串，flag为MEASURE |
|Text组件	| Text(content?: string)           |判断文本内容是否正常设置给布局集合对象  |js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp	|content输入带特殊字符内容 |TextLayoutProperty文本获取到指定字符串，flag为MEASURE |
|Text组件   | Text(content?: string)           |判断文本内容是否正常设置给布局集合对象  |js_text.cpp，text_view.cpp,text_pattern.cpp,text_layout_property.cpp	|content输入带表情字符     |TextLayoutProperty文本获取到指定字符串，flag为MEASURE |
