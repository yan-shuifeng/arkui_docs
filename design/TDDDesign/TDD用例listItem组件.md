# TDD用例ListItem组件

|大类	        |  小类                                |	功能点	                            | 涉及文件                                                      |	测试输入条件        |	测试预期结果                     |
| ---           | ---                                  | :--                                    | ---                                                           |  ---                  |---                                 |
|ListItem组件	| ListItem(sticky:Sticky)       	   |判断ListItem是否设置了正确的sticky   	|js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|sticky输入Sticky.None	|listItem获取到stick值并正确布局     |
|ListItem组件	| ListItem(sticky:Sticky)       	   |判断ListItem是否设置了正确的sticky   	|js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|sticky输入Sticky.Normal|listItem获取到stick值并正确布局     |
|ListItem组件	| ListItem(editable:boolean)           |判断ListItem是否设置了正确的editable   	|js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|editable输入false   	|listItem获取到editable值并正确布局  |
|ListItem组件   | ListItem(editable:boolean)           |判断ListItem是否设置了正确的editable   	|js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|editable输入true   	|listItem获取到editable值并正确布局  |
|ListItem组件   | ListItem(selectable:boolean)         |判断ListItem是否设置了正确的selectable  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|selectable输入false	|listItem获取到selectable值并正确布局|
|ListItem组件	| ListItem(selectable:boolean)         |判断ListItem是否设置了正确的selectable  |js_listItem.cpp,listItem_view.cpp,listItem_layout_property.cpp	|selectable输入true	    |listItem获取到selectable值并正确布局|
