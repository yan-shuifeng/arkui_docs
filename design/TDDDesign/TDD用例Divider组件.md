|大类    |小类                  |功能点|涉及文件|测试输入条件|测试预期结果|
|:------:|:----:|:---:|:---:|:---:|:---:|
|Divider组件|Divider(content: null)|判断分割线组件是否正常创建出来|js_divider.cpp，divider_view.cpp,divider_pattern.cpp,divider_layout_property.cpp|无|无|
|Divider组件|Divider(vertical: bool)|判断分割线垂直属性是否正常设置给布局集合对象|js_divider.cpp，divider_view.cpp,divider_pattern.cpp,divider_layout_property.cpp|verticalr输入true|DividerLayoutProperty分割线获取到true，flag为LAYOUT|
|Divider组件|Divider(color: Color)|判断分割线颜色是否正常设置给布局集合对象|js_divider.cpp，divider_view.cpp,divider_pattern.cpp,divider_layout_property.cpp|color输入Color::FromRGB(255, 100, 100)|DividerLayoutProperty分割线获取到Color::FromRGB(255, 100, 100)，flag为MEASURE|
|Divider组件|Divider(strokeWidth: Dimension)|判断分割线宽度是否正常设置给布局集合对象|js_divider.cpp，divider_view.cpp,divider_pattern.cpp,divider_layout_property.cpp|strokeWidth输入Dimension(20, DimensionUnit::PX)|DividerLayoutProperty分割线获取到Dimension(20, DimensionUnit::PX)为LAYOUT|
|Divider组件|Divider(lineCap: LineCap)|判断分割线端点样式是否正常设置给布局集合对象|js_divider.cpp，divider_view.cpp,divider_pattern.cpp,divider_layout_property.cpp|lineCap输入LineCap::ROUND|DividerLayoutProperty分割线获取到LineCap::ROUND，flag为MEASURE|