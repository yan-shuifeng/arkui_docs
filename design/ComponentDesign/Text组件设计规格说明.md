# Text组件设计规格说明

## 组件基础功能说明

### 组件场景说明

显示一段不可编辑的文本，

### 组件布局规格

组件初次构建所有属性均需要布局
组件属性更改时需对比更改前后属性值是否变化，如果变化则重新布局
在属性方法未设置时根据默认布局属性进行布局
在属性方法设置时，根据设置属性进行布局

### 组件绘制规格

组件初次构建所有属性均需要绘制
组件属性更改时需对比更改前后属性值是否变化，如果变化则重新绘制
所有属性均需要绘制，则共用布局属性集合
在属性方法未设置时根据默认属性进行绘制
在属性方法设置时根据设置属性进行绘制

## 组件自定义能力说明

### fontColor属性方法

- 使用场景：设置文本颜色
- 输入输出规格：输入Color类型，无具体输出
- 默认值：无
- 对于布局和绘制的影响：有影响

### fontSize属性方法

- 使用场景：设置文本尺寸
- 输入输出规格：输入Dimension类型，无具体输出
- 默认值：无
- 对于布局和绘制的影响：有影响

### fontStyle属性方法

- 使用场景：设置文本是否为斜体
- 输入输出规格：输入枚举类型FontStyle，无具体输出
- 默认值：FontStyle.Normal
- 取值范围：FontStyle::NORMAL, FontStyle::ITALIC
- 对于布局和绘制的影响：有影响

### fontWeight属性方法

- 使用场景：设置文本字体的粗细
- 输入输出规格：输入FontWeight类型，无具体输出
- 默认值：FontWeight.Normal
- 取值范围：FontWeight::W100, FontWeight::W200, FontWeight::W300, FontWeight::W400, FontWeight::W500, FontWeight::W600, FontWeight::W700, FontWeight::W800, FontWeight::W900, FontWeight::BOLD, FontWeight::NORMAL, FontWeight::BOLDER, FontWeight::LIGHTER, FontWeight::MEDIUM, FontWeight::REGULAR
- 对于布局和绘制的影响：有影响

### fontFamily属性方法

- 使用场景：设置文本字体
- 输入输出规格：输入字符串类型，使用‘,’分割按顺序生效，无具体输出
- 默认值：无
- 对于布局和绘制的影响：有影响

### textAlign属性方法

- 使用场景：设置多行文本的对齐方式
- 输入输出规格：输入枚举类型TextAlign，无具体输出
- 默认值：TextAlign.Start
- 取值范围：TextAlign::START, TextAlign::CENTER, TextAlign::END, TextAlign::LEFT, TextAlign::RIGHT, TextAlign::JUSTIFY
- 对于布局和绘制的影响：有影响

### textOverflow属性方法

- 使用场景：设置文本超长时的显示方式
- 输入输出规格：输入枚举类型TextOverflow，无具体输出
- 默认值：{overflow: TextOverflow.Clip}
- 取值范围：TextOverflow::CLIP, TextOverflow::ELLIPSIS, TextOverflow::NONE
- 取值含义：根据maxLines去限制文本长度，TextOverflow::CLIP（裁剪字符，保证字符的完整（放不下一个字就不放了）), TextOverflow::ELLIPSIS（显示不下的文本以省略号代替）, TextOverflow::NONE（以控件大小去裁切（可能出现半个字））
- 对于布局和绘制的影响：有影响

### maxLines属性方法

- 使用场景：设置文本的最大行数
- 输入输出规格：输入int32_t类型，无具体输出
- 默认值：Infinity
- 取值范围：[1,Infinity]
- 对于布局和绘制的影响：有影响

### lineHeight属性方法

- 使用场景：设置文本行高
- 输入输出规格：输入Dimension类型，无具体输出
- 默认值：无
- 取值范围：非正数显示为行高不限，非数值类型为非法值，行高不限时由字体配置文件决定行高
- 对于布局和绘制的影响：有影响

### decoration属性方法

- 使用场景：设置文本装饰线样式以及颜色
- 输入输出规格：输入json字符串
- 默认值："{type: TextDecorationType.None, color: Color.Black}"
#### TextDecoration方法
- 使用场景：设置文本装饰线样式
- 输入输出规格：取json的type字段，输入TextDecoration类型，无具体输出
- 取值范围：TextDecoration::NONE, TextDecoration::UNDERLINE, TextDecoration::OVERLINE, TextDecoration::LINE_THROUGH, TextDecoration::INHERIT
- 对于布局和绘制的影响：有影响
#### TextDecorationColor方法
- 使用场景：设置文本装饰线的颜色
- 输入输出规格：取js的color字段，输入Color类型，无具体输出
- 对于布局和绘制的影响：有影响

### baselineOffset属性方法

- 使用场景：设置文本基线的偏移量
- 输入输出规格：输入Dimension类型，无具体输出
- 默认值：无
- 对于布局和绘制的影响：有影响

### textCase属性方法

- 使用场景：设置文本大小写
- 输入输出规格：输入枚举类型TextCase，无具体输出
- 默认值：TextCase.Normal
- 取值范围：TextCase::NORMAL, TextCase::LOWERCASE, TextCase::UPPERCASE
- 对于布局和绘制的影响：有影响

### minFontSize属性方法（待补充）

### maxFontSize属性方法（待补充）

### letterSpacing属性方法（待补充）
