# Toggle组件设计规格说明

## 组件基础功能说明

### 组件场景说明

组件提供勾选框样式、状态按钮样式及开关样式。

### 组件布局规格

在属性方法未设置时，默认布局是怎么做的（需要考虑构造时不同的参数场景）

Toggle有一种接口：

Toggle(options: { type: ToggleType, isOn?: boolean })

- 参数
  | 参数名 | 参数类型 | 必填 | 默认值 | 参数描述 |
  | -------- | -------- | -------- | -------- | -------- |
  | type | ToggleType | 是 | - | 开关类型。 |
  | isOn | boolean | 否 | false | 开关是否打开，true：打开，false：关闭。 |

  当前Toggle中对接了Switch类型，另外两种类型还未对接。

### 组件绘制规格

在属性方法未设置时，Toggle的Switch类型绘制规格如下：

isOn为ture是默认为开启状态，为false则为关闭状态。

默认不设置宽高使用switchTheme中的宽高进行布局绘制。可以使用通用属性Width、Height、Size进行大小设置。

默认selectedColor为蓝色、switchPointColor为白色，可以通过设置这两个属性自定义绘制效果。


## 组件自定义能力说明

### Toggle属性方法

| 名称 | 参数 | 默认值 | 参数描述 |
| -------- | -------- | -------- | -------- |
| selectedColor | [ResourceColor](../../ui/ts-types.md) | - | 设置组件打开状态的背景颜色。 |
| switchPointColor | [ResourceColor](../../ui/ts-types.md) | - | 设置Switch类型的圆形滑块颜色。<br/>>&nbsp;**说明：**<br/>>&nbsp;仅对type为ToggleType.Switch生效。 |

### Toggle事件方法

| 名称 | 功能描述 |
| -------- | -------- |
| onChange(callback:&nbsp;(isOn:&nbsp;boolean)&nbsp;=&gt;&nbsp;void) | 开关状态切换时触发该事件。 |

