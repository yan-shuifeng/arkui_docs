# List组件设计规格说明

## 组件基础功能说明

### 组件场景说明

ListItem从API7开始支持，是List组件的子组件，用来展示List组件的具体Item。宽度默认充满整个List组件，必须配合List组件使用。可以用来单独呈现数据，例如图片和文字。

### 组件布局规格

ListItem组件中共有3个属性和1个事件。
3个属性中，sticky的默认值为Sticky::None，editable的默认值为false，selectable的默认值为true。
当所有属性采用默认值时，ListItem组件的布局规格如下：
ListItem组件将被包含在List组件中，用于呈现数据。在长度方向，ListItem组件将依据序列号，在List组件中依次排列。在宽度方向，ListItem组件将充满整个List组件。ListItem组件无吸顶效果。

### 组件绘制规格

当所有属性采用默认值时，ListItem组件的绘制规格如下：
ListItem无吸顶效果，在List组件中依序列号排列并绘制。ListItem组件无单独绘制任务，如果组件被用来呈现数据，例如图片和文字，则将绘制数据。

## 组件自定义能力说明

### sticky属性方法

- 使用场景：所有场景均可使用
- 输入输出规格：sticky的输入为一个Sticky枚举类，无标准输出，会影响布局和绘制
- 对于布局和绘制的影响：
sticky属性会影响ListItem的绘制效果，用于设置ListItem吸顶效果。如果sticky为Sticky::None，则无吸顶效果。如果sticky为Sticky::Normal，则当前item吸顶。

### editable属性方法

- 使用场景：使用触碰或者鼠标选中场景
- 输入输出规格：editable属性的输入为一个boolean类，无标准输出，不会影响布局和绘制
- 对于布局和绘制的影响：
editable属性决定了当前ListItem元素是否可编辑。若当前ListItem元素可编辑，则进入编辑模式后可以通过触碰或者鼠标选中删除。

### selectable属性方法

- 使用场景：使用触碰或者鼠标选中场景
- 输入输出规格：selectable属性的输入为一个boolean类，无标准输出，不会影响布局和绘制
- 对于布局和绘制的影响：
selectable属性决定了当前ListItem元素是否可被鼠标框选。只有当外层List组件的鼠标框选开启时，ListItem的框选才会生效。

### onSelect事件方法

- 使用场景：使用触碰或者鼠标选中场景
- 输入输出规格：输入为触碰或者鼠标框选，输出为触发回调函数。
- 对于布局和绘制的影响：
对于布局和绘制无影响。当ListItem元素被鼠标框选的状态改变时触发回调，进入鼠标框选范围即被选中返回true，移除鼠标框选范围即未被选中返回false。

