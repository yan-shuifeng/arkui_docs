# Button组件设计规格说明

## 组件基础功能说明

### 组件场景说明

按钮组件，可快速创建不同样式的按钮。

该组件从API Version 7开始支持。后续版本如有新增内容，则采用上角标单独标记该内容的起始版本。

### 组件布局规格

在属性方法未设置时，默认布局是怎么做的（需要考虑构造时不同的参数场景）

Button 有两种接口

接口1：Button(options?: {type?: ButtonType, stateEffect?: boolean}) 

​		无文本内容，可以包含单个子组件；options参数有type和stateEffect两种类型

接口2：Button(label?: ResourceStr, options?: { type?: ButtonType, stateEffect?: boolean })

​		使用文本内容创建相应的按钮组件，此时Button无法包含子组件；label 参数有ResourceStr类型，options参数有type和stateEffect两种类型

接口2中的文本可以不填，即变成接口1；

属性中stateEffect 类型为boolean，默认值为true，按钮按下时是否开启切换效果，对布局没有影响

属性中type 参数类型为ButtonType， 默认值为Capsule样式，会判断圆角半径是不是等于button高度的一半，不是就重新设置边界的直径。



### 组件绘制规格

当所有参数和属性采用默认值时，button组件的绘制规格如下：

1、stateEffect 值为true时，开启切换效果，对于绘制没有影响

2、ButtonType 默认值为Capsule样式时，borderRadius设置不生效，按钮圆角始终为高度的一半；

​		当需要适应child大小时

​				其中button是否设置宽和高会影响其绘制，设置，就用设置的参数；如果没有设置，就使用child 的宽和高

​		当不需要时

​				判断button的宽是否为0

​				是： 重新计算布局的大小

​				不是：layoutSize就是布局的大小

​		这里还有一定的约束，约束大小不能小于最小的宽高，不能大于最大宽高



## 组件自定义能力说明

### type属性方法

使用场景：设置Button样式

输入输出规格：输入Capsule、Circle、Normal，输出是button不同的样式

对于布局和绘制的影响：

Capsule：胶囊型按钮（圆角默认为高度的一半），其中borderRadius设置不生效，按钮圆角始终为高度的一半。 在布局中会判断当前的rrectRadius_ 是不是button高度的一半，不相等，设置rrectRadius_ 等于button高度的一半，设置边界半径rrectRadius_ +border.Top().GetWidth()

Circle：圆形按钮，borderRadius即为按钮半径，若未设置borderRadius按钮半径则为宽、高中较小值的一半。

Normal：普通按钮（默认不带圆角）

### stateEffect属性方法

使用场景：开启切换效果

状态切换时是否开启切换效果，当状态置为false时，点击效果关闭。

对于布局和绘制的影响：无

### Button事件方法

- 无事件