# Swiper组件设计规格说明

## 组件基础功能说明

### 组件场景说明

滑动容器，提供切换子组件显示的能力，并可以提供导航器indicator，提示用户，当前显示的元素是第几个。

```mermaid
graph LR
A[默认渲染]-->B(滑动手势)
B-->C(更新节点位置)
C-->D(滑动结束)
D-->E(启动位移动画)
E-->F(动画结束)

```

滑动手势通过PanRecognizer监听单指滑动事件实现，动画通过给Animator设置translate动画参数实现，在切换动画完成后，更新当前展示的元素index。



### 组件布局规格

默认展示第一个元素，其他元素按照顺序向后排列，滑动可进行当前展示元素的切换。



### 组件绘制规格

Swiper本身不会绘制，绘制能力由子节点和导航器组件承载。



## 组件自定义能力说明

### index属性方法

- 使用场景：设置当前在容器中显示的子组件的的索引值。
- 是否影响布局：影响布局，index值不同，当前展示的元素就不同。
- 实现：通过在布局阶段，设置子节点相对于Swiper的偏移实现。



### autoPlay、interval、duration、curve属性方法

- 使用场景：

  autoPlay设置是否启用自动轮播功能，默认为false；

  interval设置自动轮播的时间间隔，默认3000ms；

  duration设置切换动画的时长，默认400ms；

  curve设置动画曲线，默认为Curve.Ease；

  当设置autoPlay为true时，会每3秒钟自动切换下一个元素显示，切换动画时长为400ms。

- 是否影响布局：不影响布局。

- 实现：启动一个Scheduler，当累计时长到达interval时，启动展示元素切换动画，并设置duration时长，同时清零计时器，进行重新计时。



### indicator、indicatorStyle属性方法

- 使用场景：

  indicator：设置是否启用导航点，默认启用。

  在启用后，点击导航点区域，可以完成前后元素的切换。拖拽滑动子节点时，会触发导航器的联动效果。

  indicatorStyle：设置导航点的样式。

- 是否影响布局：indicator影响布局，会影响导航点是否需要测算、布局、绘制、渲染；indicatorStyle影响渲染。

- 实现：导航器效果通过组合一个IndicatorPattern实现，在IndicatorPattern完成导航点的测算、布局、绘制，Swiper仅负责与IndicatorPattern进行元素切换的消息通知交互。



### loop属性方法

- 使用场景：在第一个元素时，切换上一个，或者在最后一个元素时，切换下一个时，如果loop为false，无法切换，如果设置loop为true，可进行切换。
- 是否影响布局：不影响布局。



### vertical属性方法

- 使用场景：设置是否为纵向滑动，默认为false，子节点横向排列，左右滑动完成切换；设置为true时，子节点竖向排列，上下滑动完成切换。
- 是否影响布局：影响布局，vertical为false时，子元素横向排列；vertical为true时，子元素竖向排列。
- 实现：在布局时，将子节点根据vertical取值，横向或竖向排列，PanRecognizer也跟随vertical设置。



### itemSpace属性方法

- 使用场景：设置子组件与子组件之间的间隙，默认为0.
- 是否影响布局：影响子节点的布局，子元素之间的间距不同，子节点的布局位置会发生改变。
- 实现：在布局子节点时，将itemSpace计算在内，每个子节点之间保持间距，给子节点的布局约束，需减掉这部分值。



### cachedCount属性方法

- 使用场景：设置预加载子组件的个数。

- 是否影响布局：影响子节点布局，在懒加载时，cachedCount影响布局的节点个数，参与布局的节点数为displayCount+cachedCount * 2。

- 实现：在LazyForEach场景下，通过displayCount+cachedCount * 2计算出需要加载的元素个数进行加载；

  在非LazyForEach场景下，设置不在范围内的子节点为hidden。



### disableSwipe属性方法

- 使用场景：设置是否禁用子组件滑动切换功能，默认为false。
- 是否影响布局：不影响布局，仅影响手势处理。
- 实现：在设置disableSwipe后，不初始化PanRecognizer，若已初始化，则不处理滑动手势。



### displayMode属性方法（文档补齐）

- 使用场景：设置Swiper子组件的显示模式，可选值有Stretch和AutoLinear，默认为Stretch。
- 是否影响布局：影响布局。



### displayCount属性方法（文档补齐）

- 使用场景：设置展示的元素个数，默认为1。
- 是否影响布局：影响布局，displayCount不同时，展示的子元素个数不同。
- 实现：将Swiper的大小等分成几份，设置布局约束到子节点，使Swiper中能显示多个子节点。



### effectMode属性方法（文档补齐）

- 使用场景：设置当非loop的swiper在边界出拖拽时的效果，例如显示第一个元素时，拖拽显示上一个元素，或者显示最后一个元素时，拖拽显示下一个元素，默认为None。可选值有Spring（回弹）、Fade（阴影）、None。
- 是否影响布局：不影响布局，仅在拖动到边界时触发。



### SwiperController方法

- showNext：展示下一个元素
- showPrevious：展示上一个元素
- finishAnimation：停止动画（文档补齐）



### onChange事件方法：在当前展示的元素索引发生变化时触发该回调。

- 触发场景：

  1、通过滑动屏幕，元素切换动画完成后，触发回调。

  2、通过调用controller的showNext、showPrevious接口完成展示元素切换后，触发回调。

  3、通过更新index属性值，切换展示元素后，触发回调。

  4、设置自动轮播autoPlay为true后，每次自动轮播完成，触发回调。

  

