# Rating组件设计规格说明

## 组件基础功能说明

### 组件场景说明
提供在给定范围内选择评分的组件，主要用在评分/星级场景。开发者可以自行设置StarNum和RatingScore，当RatingScore变化时，可以触发onchange回调。

### 组件布局规格
Rating构造方法中rating影响绘制，即forgroundWidth。indicator会影响布局绘制，因为在非indicator的情况下，Rating的默认样式相对较小。StarNum默认为5。
```js
Rating(options?: { rating: number, indicator?: boolean })
```

#### 1. 计算Rating的width和height
1. Rating需包含3个ImageLoadingContext，分别代表：forground，secondary和background。这三个图片的ResouceId从RatingTheme中拿到。
2. 创建ImageLoadingContext，在ready回调里调用GetImageSize，拿到image的rawsize。
	- 当开发者未设置width和height时： rating width = 图片宽度*star总数，height = 图片height；
	- 当开发者只设置width或height时，用ratio来计算未知的height or width。
	- 当开发者设置width和height时，不需要计算。
#### 2. resize ImageSize
依据Rating的宽高，计算Single Sart的size，resize image size。
1. 计算singleStarWidth： singleStarWidth_ = ratingWidth /  starNum；
2. forgroundWidth、secondaryWidth和backgroundWidth，即singleStarWidth * ratingScore。
3. 调用CanvasImage setResizeTarget(size(singleStarWidth, ratingHeight)).

至此，已经计算好Rating的Size，Single Star的Size，和forground，secondary、background的总宽度。
### 组件绘制规格
在MeasureContent中已经计算好需要的Size，在GetContentDrawFunction中，要根据这些size，绘制forground，secondary和background Image。
根据forground，secondary和backgroundWidth，计算repeatNum；根据SingleStarWidth，计算每次绘制的offset，调用canvasclip接口切割canvas，创建ImagePainter的DrawImage，根据repeatNumber决定绘制次数。

## 组件自定义能力说明

### stars属性方法
- 使用场景：设置Rating的总星数。
- 输入输出规格：输入类型为number，输出为Rating组件的总星数，影响布局和绘制。
- 影响：影响backgroundImageWidth和RatingWidth，以及background Image 绘制的repeatNum。

### stepSize属性方法
- 使用场景：操作评级的步长。
- 输入输出规格：输入类型为number。影响绘制。
- 影响：计算drawScore = Round(ratingScore / stepSize_) * stepSize_。

### starStyle属性方法
- 使用场景：用户自定义3张图片，{ backgroundUri: string, foregroundUri: string, secondaryUri?: string }。
- 输入输出规格：输入类型为Object，影响布局和绘制。
- 影响：默认CanvasImage为RatingTheme中指定的ResouceId，如果用户指定，即使用用户指定图片。


### onChange事件方法

- 使用场景：操作评分条的评星发生改变时触发该回调。。
- 输入输出规格：不影响布局和绘制。

### 改变RatingScore事件方法
- 使用场景：当rating设置为indicator的时候，用户可以使用点击or滑动改变ratingScore的数据。
- 使用场景：当rating设置为indicator的时候，用户可以使用点击or滑动改变ratingScore的数据。
- 输入输出规格：输入touchPoint，计算touchPoint在第几个Star上，然后重新计算RatingScore。
- 影响：影响绘制。
