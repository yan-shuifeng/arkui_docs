# XComponent组件设计规格说明

## 组件基础功能说明

### 组件场景说明

XComponent组件从API Version 8 开始支持，主要用于EGL/OpenGLES和媒体数据写入，并显示在XComponent组件上。

XComponent拥有单独的surface，可以为开发者在c++层提供Native Window用来创建EGL/GLES环境，进而开发者可以使用标准的OpenGL ES开发渲染逻辑

### 组件布局规格

仅依赖通用属性和父组件约束进行布局

### 组件绘制规格

对于Rosen来说，Arkui组件所在的surface在上，XComponent独有的surface在下，图形那边会对上层的surface做挖孔处理，来露出下面的surface上开发者做的处理

~~对于Flutter来说，会把XComponent的surface直接用安卓的渲染接口合成~~

## 组件自定义能力说明

XComponent组件共有4个参数、2个事件方法

### 参数

- id：与XComponent组件的关系为一一对应关系，不可重复；开发者可以在C++层通过id来绑定对应的XComponent
- type：暂时只有"texture"
- libraryname：加载指定的动态库名称，C++层会在动态库中进行注册开发者定义的回调接口
- controller（可选）：给组件绑定一个控制器，通过控制器调用组件方法。

以上参数均不会影响组件的布局和绘制

### onLoad事件方法

使用场景：所有场景均可使用

参数：context，挂载了开发者定义的c++方法

何时触发：XComponent准备好surface后会触发该事件

对布局和绘制无影响

### onDestroy事件方法

使用场景：所有场景均可使用

何时触发：在surface销毁时触发该事件

对布局和绘制无影响

## 其他说明

### NativeXComponent

XComponent组件在应用的C++层实例，作为JS层和C++层XComponent绑定的桥。其提供了：

1. 获取XComponent属性的接口：
   - OH_NativeXComponent_GetXComponentId
   
     通过在new nativeXcomponentImpl后调用nativeXComponentImpl_->SetXComponentId(xcomponent->GetId())设置
   
   - OH_NativeXComponent_GetXComponentSize
   
     在measure后长宽发生变化时调用nativeXComponentImpl_->SetXComponentHeight(Width)设置
   
   - OH_NativeXComponent_GetXComponentOffset
   
     在layout后位置发生变化后调用nativeXComponentImpl_->SetXComponentOffsetX(Y)设置
   
   - OH_NativeXComponent_GetTouchEvent
   
   - OH_NativeXComponent_GetMouseEvent
2. surface的生命周期回调接口
   - OnSurfaceCreated
   
     在首次measure时触发该回调
   
   - OnSurfaceChanged
   
     在measure后长宽发生变化时触发该回调（非首次）
   
   - OnSurfaceDestroyed
   
     在页面back或replace后触发该回调
3. 事件触发时的回调接口：
   - DispatchTouchEvent
   - DispatchMouseEvent

### XComponentController

在JS层提供了以下方法：

1. getXComponentSurfaceId(): string

   获取XComponent对应Surface的ID，供@ohos接口使用，比如camera

2. setXComponentSurfaceSize(value: {surfaceWidth: number, surfaceHeight: number}): void

   设置XComponent持有Surface的宽度和高度

3. getXComponentContext(): Object

   获取XComponent实例对象的context，context包含的具体接口方法由开发者自定义

