# 蓝区环境配置


## 1. 安装XShell，登入服务器
到官网下载安装XShell，获取服务器账号并登入。


## 2. 服务器环境初始化
### 前提条件
> 1. 注册码云gitee帐号。
> 2.  注册码云SSH公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)。
> 3.  配置git用户信息。
	`git config --global user.name "yourname"`
	`git config --global user.email "your-email-address"`
	`git config --global credential.helper store`
> 4. 将repo添加到环境变量。
	`vim ~/.bashrc               # 编辑环境变量`
	`export PATH=~/bin:$PATH     # 在环境变量的最后添加一行repo路径信息`
	`source ~/.bashrc            # 应用环境变量`

## 3. 下载编译代码
- OpenHarmony主干代码获取
	```
	repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
	repo sync -c -j32 --no-tags
	repo forall -c 'git lfs pull'
	```
- 预编译
` bash build/prebuilts_download.sh`
- 编译代码
` ./build.sh --product-name rk3568 --ccache`
## 4. 刷机RK启动
> 1. **烧录前准备**
>> 1. **按照图片提示连接电源线， Debug 串口调试线， USB 烧写线**
![烧录前准备](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516164155093.png) 
>> 2.  **下载烧录工具**
烧录工具下载链接：[https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97](https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97)
> 2. **准备固件**
将刚才编译产生的镜像文件通过Xftp传输到本地，文件目录为out/rk3568/images.tar.gz
> 3. **烧录步骤**
> > 1. **安装 USB 驱动**
>双击 windows\DriverAssitant\ DriverInstall.exe 打开安装程序， 点击下图所示的“驱动安装” 按钮：
![安装驱动](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516164941133.png)
>> 2. **打开烧写工具**
>双击 windows\RKDevTool.exe 打开烧写工具， 如图所示， 默认是 Maskrom 模式：
![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516165202136.png)
>> 3. **导入配置**

![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516165506671.png)
![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516165624810.png)
![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516165646516.png)
双击后面的白色按钮，勾选需要烧写的固件
>> 4. 进行烧录
>![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516170351601.png)
>>>1. 按住VOL-/RECOVERY 按键（图中标注的①号键） 和 RESET 按钮（图中标注的②号键）不松开， 烧录工具此时显示“没有发现设备”
>![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516170510672.png)
>>>2. 松开 RESER 键， 烧录工具显示“发现一个 LOADER 设备” ， 说明此时已经进入烧写模式
>![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516170632612.png)
>>>3. 松开按键， 稍等几秒后点击执行进行烧录
>![picture](https://gitee.com/hihope_iot/docs/raw/master/HiHope_DAYU200/docs/image/image-20220516170730584.png)
>说明：

如果烧写成功， 在工具界面右侧会显示下载完成

如果烧写失败， 在工具界面右侧会用红色的字体显示烧写错误信息， 更多出错信息查看：Log 目录下的文件


## 5. vscode环境搭建

> 1. 在官网下载安装vscode，网址：https://code.visualstudio.com/
> 2. 应用商店搜索Chinese，安装简体中文插件。
> 3. 下载安装插件Remote—SSH，输入服务器账号密码连接服务器。(若是本地Ubuntu环境，则安装Remote-WSL插件)
> - 按Ctrl+Shift+P，输入“Remote-SSH”，选择并打开如下选项，新增SSH Host
> 
> ![](pic5.png)
> ![](pic6.png)
> 
> - 输入"ssh 账号@服务器IP:20389"，按Enter键确认。
> 
> ![](pic7.png)
> 
> - 选择添加configuration file
> 
> ![](pic8.png)
> 
> - 选择Connect
> 
> ![](pic9.png)
> 
> - 按Ctrl+Shift+P，输入“Remote-SSH”，选择并打开如下选项，连接远端。
> 
> ![](pic10.png)
> 
> - 选择远端服务器IP
> 
> ![](pic11.png)
> 
> - 输入密码
> 
> ![](pic12.png)
> 
> - 在左侧选择要打开的文件夹
> 
> ![](pic13.png)
> 
> 4. 安装Code Spell Checker插件
> 5. 在应用商店安装clangd插件，选择安装在SSH:服务器中。
> 6. 设置clangd插件
> 在代码根目录下新建一个名为“.clangd”的文件，例如/mnt/data/yanshuifeng/OpenHarmony/.clangd
> 打开编辑，将如下demo中所有/mnt/data/yanshuifeng/OpenHarmony改为自己代码的路径：
```
CompileFlags:
  Add:
    [
      -DUSE_ARK_ENGINE,
      -DENABLE_ROSEN_BACKEND,
      -DOHOS_STANDARD_SYSTEM,
      -DPIXEL_MAP_SUPPORTED,
      -DROSEN_CROSS_PLATFORM,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/arkui/ace_engine/frameworks,
      -I/mnt/data/yanshuifeng/OpenHarmony/third_party/flutter/engine,
      -I/mnt/data/yanshuifeng/OpenHarmony/third_party/flutter/skia,
      -I/mnt/data/yanshuifeng/OpenHarmony/ark/js_runtime,
      -I/mnt/data/yanshuifeng/OpenHarmony/ark/runtime_core,
      -I/mnt/data/yanshuifeng/OpenHarmony/ark/runtime_core/libpandabase,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/ace/napi,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/rosen/modules,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/rosen/modules/render_service_client/core,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/rosen/modules/render_service_base/include,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/interfaces/inner_api/surface,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/interfaces/inner_api/common,
      -I/mnt/data/yanshuifeng/OpenHarmony/drivers/peripheral/display/interfaces/include,
      -I/mnt/data/yanshuifeng/OpenHarmony/drivers/peripheral/base,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/communication/ipc/interfaces/innerkits/ipc_core/include,
      -I/mnt/data/yanshuifeng/OpenHarmony/foundation/graphic/graphic_2d/utils/buffer_handle/export,
      -I/mnt/data/yanshuifeng/OpenHarmony/prebuilts/clang/ohos/linux-x86_64/llvm/include/c++/v1,
      -I/mnt/data/yanshuifeng/OpenHarmony/prebuilts/clang/ohos/linux-x86_64/llvm/include/arm-linux-ohos/c++/v1,
      -isystem/mnt/data/yanshuifeng/OpenHarmony/out/rk3568/obj/third_party/musl/usr/include/arm-linux-ohos,
    ]
  Remove: 
  [
  ]
  Complier: /mnt/data/yanshuifeng/OpenHarmony/prebuilts/clang/ohos/linux-x86_64/llvm/bin/clang++
  CompilationDatabase: /mnt/data/yanshuifeng/OpenHarmony/out/rk3568
Diagnostics:
  ClangTidy:
    Add:
      [
        performance-*,
        bugprone-*,
        portability-*,
        modernize-*,
        readability-*,
        cert-*,
        clang-analyzer-*,
        concurrency-*,
        llvm-*,
        cppcoreguidelines-*,
        google-*,
        misc-*,
        concurrency-*,
        abseil-*,
        boost-*,
      ]
    Remove:
      [
        modernize-use-trailing-return-type,
        readability-implicit-bool-conversion,
        bugprone-easily-swappable-parameters,
        modernize-use-nodiscard,
        readability-uppercase-literal-suffix,
        cppcoreguidelines-pro-type-reinterpret-cast,
        cppcoreguidelines-pro-type-vararg,
        cppcoreguidelines-pro-bounds-array-to-pointer-decay,
      ]
    CheckOptions:
      readability-function-cognitive-complexity.Threshold: 80
      readability-identifier-length.MinimumVariableNameLength: 2
      readability-identifier-length.MinimumParameterNameLength: 2
      readability-identifier-length.MinimumLoopCounterNameLength: 1
  UnusedIncludes: Strict
InlayHints:
  Enabled: Yes
  ParameterNames: Yes
  DeducedTypes: No

  ```
>Ctrl+P搜索"build/hb/services/gn.py", 打开编辑，Ctrl+F搜索代码行: def gn_gen_cmd，找到位置，并加入
>`'gn_gen_cmd.append('--export-compile-commands')',`

> 并添加clangd arguments，如图所示：
> `--clang-tidy`
`--completion-style=detailed`
`--enable-config`
`--header-insertion=iwyu`
`--log=info`
`--pch-storage=memory`
![picture](pic4.png)
>全量编译
> `./build.sh --product-name rk3568 --ccache`
> 生成符号表，生成的符号表位于：out/rk3568/compile_commands.json
> Ctrl+Shift+P，输入clangd，选择“Restart language server”，等待IDE左下角的符号表导入完成。
## 6. 下载DevEco IDE
网址：https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta
开发一个hello world，教程：https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/quick-start/start-with-ets-stage.md/
使用自动签名，点击运行即可在rk3568中查看效果。
![picture](pic3.png)
## 7. 修改代码，打印日志
> 1. 在foundation/arkui/ace_engine/adapter/ohos/entrance/ace_ability.cpp中的onStart函数中添加一行日志代码.
> 2. 配置hdc
>> 1. 在DevEco中找到OpenHarmony Skd的安装地址，进入目录下的toolchains/版本号，将hdc_std复制到自定义文件夹，并配置环境变量。
>> 2. 打开控制台依次输入以下命令
```
        hdc shell
        hilog -b X
        hilog -b D -D 0xD003900
        hilog -b D -D 0xD003B00
        hilog -p off
        hilog -Q domainoff
        hilog -Q pidoff
        hilog -G 1G
        hilog -r
        hilog
 ```
>> 3. 运行自己开发的hello world程序即可在控制台看到自己添加的日志信息。
## 8. 提交pr到OpenHarmony的Ace仓
> 1. 点击右上角的Fork按钮，按照指引，建立一个属于个人的云上Fork分支。https://gitee.com/openharmony/arkui_ace_engine
> 2. 进入foundation/arkui/ace_engine目录并使用`git status`查看代码修改状况
> 3. 复制远程仓到本地 
> `git remote add 别名 远程仓SSH链接`
> 4. 创建分支 
> `repo start branchname --all`
> 4. 提交变更 
> `git add .`
> `git commit -sm '此次变更的总结'`
> 5. 查看log 
> `git log`
> 6. 将代码提交到个人的fork仓库 
> `git push 远程主机名 本地分支名 -f`
> 7. 创建Pull Request：访问您在码云上的fork仓页面，点击创建Pull Request按钮选择刚才创建的分支，添加此次pr的说明并提交pr。
## 9. 编译NG新结构镜像并刷机验证应用
### 1. 使用最小化更新的SDK替换IDE内置的API9的SDK(IDE需要是最新版本，900版本会有问题)
> 1. **获取门禁版本(代码也需要是最新的代码)：** http://ci.openharmony.cn/dailys/dailybuilds
> ![](pic16.png)
> 2. **解压后替换IDE中的SDK：**
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk_ets.png)
> 3. **解压文件，在解压得到的文件中获取版本信息，并用该版本信息重命名文件夹：**
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/%E7%89%88%E6%9C%AC%E5%8F%B7.png)
> 4. **将重命名的文件夹放到IDE的SDK目录中，完成后可以从IDE的SDK管理中看到当前生效的版本：**
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk2.png)
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk1.png)
> 5. **使用cmd命令：npm install重新编译SDK。（如果npm命令找不到，需要配置环境变量)**
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk4.png)
> 6. **使用SDK9创建并编译安装SDK9的应用：（只有FA模型生效）**
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk3.png)
### 2. 安装应用，使能NG流程
> 切换新框架的流程见 https://gitee.com/zhou-chaobo/arkui_docs/blob/master/%E6%96%B0%E6%A1%86%E6%9E%B6%E5%88%87%E6%8D%A2/acenewpipe.md
> 可以通过日志看到当前应用的启动方式：（new pipeline标识NG新框架流程。
> ![](https://gitee.com/yan-shuifeng/arkui_docs/raw/master/design/NewStructureDesign/figures/sdk5.png)

### 3. 用最小化更新的SDK编译普通应用

若需要使用最小化更新的SDK编译普通应用则需要修改下图中所示文件：

![picture](pic14.png)

partialUpdateMode为false则为正常的编译流程，改成true就是最小化更新的编译。

![picture](pic15.png)